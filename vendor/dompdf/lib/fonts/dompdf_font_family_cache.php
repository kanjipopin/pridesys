<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\6ed5261475a72ba92983868bca0014c4',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/7511877ae6d708470e8ac0c9fbdfddf8',
  ),
  'latoblackitalic' => array(
    'normal' => $fontDir . '/423af833d227d602ea08de155b7e6c76',
  ),
  'latoblack' => array(
    'normal' => $fontDir . '/4a6e8dcc8e7bcb40b9437097d5f16461',
  ),
  'latobolditalic' => array(
    'normal' => $fontDir . '/0b0901f3b99331147436a087e6a50241',
  ),
  'latobold' => array(
    'normal' => $fontDir . '/1b8a84246d7bfcb21ec0832a295db602',
  ),
  'latoitalic' => array(
    'normal' => $fontDir . '/39868ebed391cb3c9e0b368c867dd58a',
  ),
  'latoregular' => array(
    'normal' => $fontDir . '/856511fcf6482151e95584519dff2dbe',
  ),
  'latolightitalic' => array(
    'normal' => $fontDir . '/4970cae3c8f0664bc615320ba434a30b',
  ),
  'latolight' => array(
    'normal' => $fontDir . '/c478afd2b692be055335a095789251b3',
  ),
  'latohairlineitalic' => array(
    'normal' => $fontDir . '/40d29f15ba705d3ab6937b41b69a1d5d',
  ),
  'latohairline' => array(
    'normal' => $fontDir . '/0944a48c49e63836b75482e7fb51d0d6',
  ),
) ?>