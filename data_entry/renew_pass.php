<?php
  require_once('../config/db.php');
  session_start();
  
  $shmsg ="";
  $rid = isset($_GET['id']) ? $_GET['id'] : "";

  if(isset($_REQUEST['sBtn'])){
    $newpass = $_REQUEST['pass1'];

    $pass = $conn->prepare("UPDATE logis_company_subadmin set comp_pwd = '{$newpass}' where random_num = '{$rid}'");
    $pass->execute();
      $shmsg = "Password successfully updated.";
  }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Dereva Portal</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>
  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-3 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
            </div>

            <div class="col-sm-6 col-md-6">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="visible-xs"><a class="btn btn-default header-btn" href="login.php" role="button">Login</a></li>
                  <li class="visible-xs"><a class="btn btn-default header-btn" href="sign-up.php" role="button">Sign Up</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div>

            <div class="col-sm-3 col-md-3 headerBTN">
              <ul class="header-btns hidden-xs">
                <li><a class="btn btn-default header-btn" href="login.php" role="button">Login</a></li>
                <li><a class="btn btn-default header-btn" href="sign-up.php" role="button">Sign Up</a></li>
              </ul>
            </div>
          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <div class="enterprise-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <form enctype="multipart/form-data" method="POST" action="#" id="login_form" name="login_form">
              <h4>Update Password</h4>

              <p style="color: red;font-size: 15px;font-weight: 500;"><?php if(!empty($shmsg)) { echo $shmsg; } ?></p>
              
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="pass1" id="pass1" placeholder="New Password" required onchange="getpassword();">
              </div>
              <small style="float:left;font-size: 11px;display: block;" id="six_letter">Minimum 6 character required.</small>

              <div class="input-group" style="margin-top: 50px;">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Confirm Password" required onchange="getpassword();">
              </div>
              <small style="float: left;color: #ff0202;" id="chk_pass"></small>

              <div class="clearfix"></div>
              <button type="submit" id="sBtn" name="sBtn" class="btn btn-default" >Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <footer class="new-footer">
      <div class="container-fluid hidden-xs">
        <div class="col-sm-4">
          <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
            <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
            <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
          </p>
        </div>

        <div class="col-sm-4">
          <p>&copy; Dereva.com | All rights reserved.</p>
          <a href="terms-of-service.php">Terms of Service</a>
        </div>

        <div class="col-sm-4">
          <ul style="float: right;">
            <li>Follow Us</li><br>
            <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
            <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
            <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
            <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
          </ul>
        </div>
      </div>

    <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; Dereva.com | All rights reserved.</p>
      </div>
    </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>

    <script type="text/javascript">
      function getpassword(){
        var pwd = $("#pass1").val();
        var Cpwd = $("#pass2").val();

        if((pwd).length < 6 && (Cpwd).length < 6){
          $("#six_letter").css("color","red");
        } else {
          $("#six_letter").css("color","#333");
        }

        if(Cpwd != ""){
          if(pwd != Cpwd){
            $("#sBtn").attr("disabled",true);
            $("#pass1").css("border-color","red");
            $("#pass2").css("border-color","red");
            $("#chk_pass").show();
            $("#chk_pass").text("Confirm password and password are not matched.");
          }
          else {
            $("#sBtn").attr("disabled",false);
            $("#pass1").css("border-color","#e8e8e8");
            $("#pass2").css("border-color","#e8e8e8");
            $("#chk_pass").hide();
          }
        }
      }
    </script>
  </body>
</html>