<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['Email'];

  if($orderDetails == "false"){
    header("location: pricing_details.php");
  }

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<div class="page-rightWidth">
  <div class="col-sm-12">
    <div class="invoice-page">
      <div class="heading">
        <h4>Order History</h4>
        <div class="filters">
          <div class="form-inline">
            <!-- <button type="button" class="btn btn-default">Add</button> -->
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;
            border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 20px;width:140px;color: #897e74;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="functions/flag_driver.php" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th scope="col">Order Type</th>
                  <th scope="col">Amount(KES)</th>
                  <th scope="col">Drivers</th>
                  <th scope="col">Purchase Date</th>
                  <th scope="col">Expires Date</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $detail = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id DESC");
                $detail->execute();
                while($detail1 = $detail->fetch()){

                  if($detail1['package_id'] == "freetrail"){
                    $plan_name = "30 days trial";
                    $plan_price = "0";
                  }
                  else {
                    $plan = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$detail1['package_id']}' ");
                    $plan->execute();
                    $plan1= $plan->fetch();
                    $plan_name = $plan1['plan_name']."<br>(".$plan1['plan_days']." days)";
                    $plan_price = $plan1['plan_price'];
                 }   
              ?>
                <tr>
                  <td><?php echo "Driver <br>(30 days)"; ?></td>
                  <td>Ksh <?php echo $detail1['amount']; ?></td>
                  <td><?php echo $detail1['driver_limit']; ?></td>
                  <td><?php echo $detail1['package_start_date']; ?></td>
                  <td><?php echo $detail1['package_end_date']; ?></td>
                  <td><a href="invoice_details.php?id=<?php echo base64_encode($detail1['order_id']); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row visible-xs" style="margin: 0;">
  <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
          <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
        </li>
        <li>
          <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
        </li>
        <li>
          <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
        </li>
      </ul>
    </div>
  <!-- /.sidebar-collapse -->
  </div>
</div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  function checkNSN(){
    var NSN = $("#NSN").val();
    var compId = $("#compId").val();

    $.ajax({
      type : "post",
      url : "functions/ajax.php",
      data : { "type":"checkFlagDriverNSN","NSN":NSN,"compId":compId },

      success : function(msg){
        if(msg == "not working"){
          $(".nsnMsg").text("These driver is currently not working with your company.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "no driver"){
          $(".nsnMsg").text("Please check driver National ID.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "success"){
          $(".nsnMsg").text('');
          $("#submitFlag").attr("disabled",false);
        }
        
      }
    });
  }
</script>

<?php include'footer.php'; ?>