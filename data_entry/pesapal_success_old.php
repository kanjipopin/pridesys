<?php
	@session_start();
  	require_once('../config/db.php');
  	include 'header.php';

  	$todatedate = date("Y-m-d");
  	@$Email = $_SESSION['Email'];
  	@$plan = $_SESSION['plan'];

  	// GET COMPANY DETAILS
  	$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompDetails->execute();
	$getCompDetailsRow = $getCompDetails->fetch();
		$comp_name = $getCompDetailsRow['comp_name'];
		$fname = $getCompDetailsRow['fname'];
		$lname = $getCompDetailsRow['lname'];

	// GET PLAN DETAILS
	$getPlanDetails = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$plan}'");
	$getPlanDetails->execute();
	$getPlanDetailsRow = $getPlanDetails->fetch();
		$driver_limit = $getPlanDetailsRow['driver_limit'];


	// DATA FORM PESAPAL RESPONSE
	$reference = null;
	$pesapal_tracking_id = null;

	if(isset($_GET['pesapal_merchant_reference'])){
		$reference = $_GET['pesapal_merchant_reference'];
	}

	if(isset($_GET['pesapal_transaction_tracking_id'])){
		$pesapal_tracking_id = $_GET['pesapal_transaction_tracking_id'];
	}


	$checkPrevPak = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id desc");
	$checkPrevPak->execute();
	$checkPrevPakCount = $checkPrevPak->rowCount();
	$checkPrevPakRow = $checkPrevPak->fetch();

		$prves_Pack_EndDate = $checkPrevPakRow['package_end_date'];
		$prves_package_status = $checkPrevPakRow['package_status'];
		$prves_package_Drv_Limit = $checkPrevPakRow['driver_limit'];
		$package_activation_date = date("Y-m-d H:i:s");

	if($checkPrevPakCount > 0){

		if($prves_package_status == "completed" && $getCompDetailsRow['driver_limit'] > 0){

			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+".$getPlanDetailsRow['plan_days']." days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}


			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,package_start_date,package_end_date,package_status,package_activation_date) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$reference','$pesapal_tracking_id','$package_start_date','$package_end_date','current','$package_activation_date')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
				header("location: dashboard.php");
		}

		elseif($prves_package_status == "completed" && $getCompDetailsRow['driver_limit'] == 0){

			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+".$getPlanDetailsRow['plan_days']." days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}


			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,package_start_date,package_end_date,package_status,package_activation_date) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$reference','$pesapal_tracking_id','$package_start_date','$package_end_date','current','$package_activation_date')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
				header("location: dashboard.php");
		}


		elseif($prves_package_status == "current" && $getCompDetailsRow['driver_limit'] == 0){

			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+".$getPlanDetailsRow['plan_days']." days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}


			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,package_start_date,package_end_date,package_status,package_activation_date) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$reference','$pesapal_tracking_id','$package_start_date','$package_end_date','current','$package_activation_date')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
				header("location: dashboard.php");
		}
	}
	else {
		$package_start_date = date("Y-m-d");
		$package_end_date = date('Y-m-d', strtotime("+".$getPlanDetailsRow['plan_days']." days"));

		$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,package_start_date,package_end_date,package_status,package_activation_date) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$reference','$pesapal_tracking_id','$package_start_date','$package_end_date','current','$package_activation_date')");
		$insertTrailPack->execute();


		$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$driver_limit',working_driver='0' where Email='{$Email}'");
		$updateCompDriverLimit->execute();
			header("location: dashboard.php");
	}

?>