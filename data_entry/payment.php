<?php
	ob_start();
	session_start();
  	require_once('../config/db.php');
  	// include 'pricing_header.php';

  	$activeClass = "Pricing";
  	@$Email = $_SESSION['Email'];
	if($Email == ""){
		$connection->redirect('../index.php');
	}

	$type = $_REQUEST['type'];

	if($type == "first"){
		$driversLimit = $_REQUEST['drivers'];
		$time = $_REQUEST['time'];

		$amount_time = $time/30;

		// if($time == 30){ $amount_time = 1; }
		// if($time == 90){ $amount_time = 3; }
		// if($time == 180){ $amount_time = 6; }
		// if($time == 360){ $amount_time = 12; }

		$vat = 16;
		$amountWithOutVat = 49 * $driversLimit;
		$amountWithOutVat * $vat;
		$amountWithVat = ($amountWithOutVat * $vat) / 100;
		$amount1 = $amountWithVat + $amountWithOutVat;
		$amount = $amount1 * $amount_time;
	}
	// else {
	// 	$driversLimit = $_REQUEST['drivers'];

	// 	$vat = 16;
	// 	$amountWithOutVat = 49 * $driversLimit;
	// 	$amountWithOutVat * $vat;
	// 	$amountWithVat = ($amountWithOutVat * $vat) / 100;
	// 	$amount = $amountWithVat + $amountWithOutVat;
	// }


	$_SESSION['driversLimit'] = $driversLimit;
	$_SESSION['amount'] = $amount;
	$_SESSION['time'] = $time;

	// $getPlanDetails = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$plan}'");
	// $getPlanDetails->execute();
	// $getPlanDetailsRow = $getPlanDetails->fetch();

	$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompDetails->execute();
	$getCompDetailsRow = $getCompDetails->fetch();

		$comp_name = $getCompDetailsRow['comp_name'];
		$fname = $getCompDetailsRow['fname'];
		$lname = $getCompDetailsRow['lname'];
		$email = $getCompDetailsRow['Email'];
		$contact = $getCompDetailsRow['contact_num'];
		$city = $getCompDetailsRow['location'];
		$address = $getCompDetailsRow['address1']." ".$getCompDetailsRow['address2'];
		$pobox = explode("-", $getCompDetailsRow['po_box']);
		$postal = $pobox[1];

	$getOrderDetails = $conn->prepare("SELECT * from enterprise_order_details order by order_id desc");
	$getOrderDetails->execute();
	$getOrderDetailsRow = $getOrderDetails->fetch();

	$refrence_id = $getOrderDetailsRow['refrence_id'];
	if($refrence_id == ""){
		$new_refrence_id = "DE-10001";
	}
	else {
		$explode = explode("-", $refrence_id);
		@$create_refrence_id = $explode[1] + 1;
		$new_refrence_id = "DE-".$create_refrence_id;
	}

	//get form details
	$planName = "Enterprise plan";

	// $amount = 99 * $driversLimit;
	// $amount = number_format($amount, 2);
	// $currency = "USD";

	// LIVE MODE
	$token = "843E1DDF-36C2-4D2D-A83D-FDCC23E1A6DF";
	$ServiceType = "23254";

	// TEST MODE
	// $token = "9F416C11-127B-4DE2-AC7F-D5710E4C5E0A";
	// $ServiceType = "5525";
	$ServiceDate = date("Y-m-d H:i:s");

	$headers = array(
	    'Content-Type: application/xml',
	    'Accept-Charset: UTF-8'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://secure.3gdirectpay.com/API/v6/");

	$msgParameter = '<?xml version="1.0" encoding="utf-8"?>
		<API3G>
		<CompanyToken>'.$token.'</CompanyToken>
		<Request>createToken</Request>
		<Transaction>
		<PaymentAmount>'.$amount.'</PaymentAmount>
		<PaymentCurrency>KES</PaymentCurrency>
		<CompanyRef>DERE</CompanyRef>
		<RedirectURL>https://enterprise.dereva.com/pesapal_success.php</RedirectURL>
		<BackURL>https://enterprise.dereva.com/payment_cancel.php</BackURL>
		<CompanyRefUnique>0</CompanyRefUnique>
		<PTL>5</PTL>
		<customerFirstName>'.$fname.'</customerFirstName>
	    <customerLastName>'.$lname.'</customerLastName>
	    <customerEmail>'.$email.'</customerEmail>
	    <customerAddress>'.$address.'</customerAddress>
	    <customerZip>'.$postal.'</customerZip>
	    <customerPhone>'.$contact.'</customerPhone>
	    <customerCity>'.$city.'</customerCity>
		</Transaction>
		<Services>
		  <Service>
		    <ServiceType>'.$ServiceType.'</ServiceType>
		    <ServiceDescription>'.$planName.'</ServiceDescription>
		    <ServiceDate>'.$ServiceDate.'</ServiceDate>
		  </Service>
		</Services>
		</API3G>';

	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $msgParameter);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($ch);
	$err = curl_error($ch);
	curl_close($ch);

	if($err){
		echo "Error Response = ".$err;
	}
	else {
		$xml_string = $response;

		$xml = simplexml_load_string($xml_string);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);

		if($array['Result'] == "000" && $array['TransToken'] != ""){
			header('location: https://secure.3gdirectpay.com/pay.asp?ID='.$array['TransToken']);
		}
		else {
			header("location: https://enterprise.dereva.com/payment_cancel.php");
		}
	}
?>