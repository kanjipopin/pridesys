<?php
	session_start();
  	require_once('../config/db.php');
  	include("php-mailer/class.phpmailer.php");

  	$activeClass = "Pricing";
  	@$Email = $_SESSION['Email'];
	if($Email == ""){
		$connection->redirect('../index.php');
	}

	$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompDetails->execute();
	$getCompDetailsRow = $getCompDetails->fetch();
		$comp_name = $getCompDetailsRow['comp_name'];
		$fname = $getCompDetailsRow['fname'];
		$lname = $getCompDetailsRow['lname'];

		$package_start_date = date("Y-m-d");
		$package_end_date = date('Y-m-d', strtotime("+ 30 days"));
		$driver_limit = 1;
		$package_id = "dernetvpas_p0l30adn";
		$refrence_id = "DE_trail";
		$tracking_id = "";
		$package_status = "current";
		$package_activation_date = date("Y-m-d H:i:s");


	$checkFirstPack = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}'");
	$checkFirstPack->execute();
	$checkFirstPackCount = $checkFirstPack->rowCount();

	if($checkFirstPackCount == 0){
		$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,package_start_date,package_end_date,package_status,package_activation_date) 
			values ('$comp_name','$Email','$fname','$lname','$package_id','$driver_limit','$refrence_id','$tracking_id','$package_start_date','$package_end_date','$package_status','$package_activation_date')");
		$insertTrailPack->execute();

		$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit = '99',working_driver='0',payment_success_status='completed' where Email='{$Email}'");
		$updateCompDriverLimit->execute();
			// header("location: dashboard.php");


		$smtpQuery = $connection->OneRow("*","smtp_settings");

		$adminMsg = "<html><body>
		<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Enterprise' title='Dereva Enterprise'><br><br>";
		$adminMsg .= "<p>Dear Admin</p>";
		$adminMsg .= "<p>".$comp_name." has subscribed to 30 day Trial package.</p>";
		$adminMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Enterprise Team</p></td></tr></table></td></tr></table></body></html>";

		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug  = 0;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = $smtpQuery['smtp_secure'];
		$mail->Host = $smtpQuery['smtp_host']; 
		$mail->Port = $smtpQuery['smtp_port']; 
		$mail->Username = $smtpQuery['smtp_username']; 
		$mail->Password = $smtpQuery['smtp_password']; 
		$mail->From = $smtpQuery['smtp_username'];
		$mail->FromName = "Dereva Enterprise";
		$mail->Subject = "Free Trial";
		$mail->MsgHTML($adminMsg);
		$mail->AddAddress("enterprise@dereva.com", "Dereva Enterprise");
		$mail->send();


		$userMsg = "<html><body>
		<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
		$userMsg .= "<p>Dear ".$fname."</p>";
		$userMsg .= "<p>You have succesfully subscribed to the 30-Day Free trial plan and your subscription will expire on ".$package_end_date.".</p>";
		$userMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Enterprise Team</p></td></tr></table></td></tr></table></body></html>";

		$mail2 = new PHPMailer();
		$mail2->IsSMTP();
		$mail2->SMTPDebug  = 0;
		$mail2->SMTPAuth   = true;
		$mail2->SMTPSecure = $smtpQuery['smtp_secure'];
		$mail2->Host = $smtpQuery['smtp_host']; 
		$mail2->Port = $smtpQuery['smtp_port']; 
		$mail2->Username = $smtpQuery['smtp_username']; 
		$mail2->Password = $smtpQuery['smtp_password']; 
		$mail2->From = $smtpQuery['smtp_username'];
		$mail2->FromName = "Dereva Enterprise";
		$mail2->Subject = "Free Trial";
		$mail2->MsgHTML($userMsg);
		$mail2->AddAddress($Email, $fname);
		$mail2->send();
	}

	include 'header.php';
?>

<div class="page-rightWidth drivers-detail driver-page paymentStatus" style="margin-top: 0px;">
    <div class="heading">
        <h4>Order Success</h4>
    </div>

    <div class="PaymentSuccess">
    	<p style="margin-left: 20px;">
        	You have succesfully subscribed to the Free 30-Day Trail package. Please check your email for more information.
        </p>
    </div>
</div>

</div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<?php include'footer.php'; ?>