<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dereva</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <!-- WOW Effect -->
    <link rel="stylesheet" href="css/animate.css">
  </head>
  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-4 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="Dereva" title="Dereva"></a>
            </div>

            <div class="col-sm-8 col-md-9">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="how-it-works.php">How it works</a></li>
                    <!-- <li class="active"><a href="how-it-works.php">How it works</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Log in <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="login.php">Driver</a></li>
                        <li><a href="Employer/login.php">Employer</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sign Up <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="driver_sign_up.php">Driver</a></li>
                        <li><a href="Employer/employee_sign_up.php">Employer</a></li>
                      </ul>
                    </li>
                    <li class="visible-xs"><a class="btn btn-default find-job-btn" href="driver_sign_up.php" role="button">Find a Job</a></li> -->
                    <li><a class="btn btn-default signUP-btn" href="#" role="button">Sign Up</a></li>
                </ul>
                </div>
            </div>
          <!--   <div class="col-sm-4 col-md-3">
                <ul class="header-btns hidden-xs">
                  <li><a class="btn btn-default find-job-btn" href="driver_sign_up.php" role="button">Find a Job</a></li>
                  <li><a class="btn btn-default post-job-btn" href="Employer/employee_sign_up.php" role="button">Post a Job</a></li>
                </ul>
            </div> -->
          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <section class="enterprise-banner">
      <div class="container-fluid">
        <img src="images/enterprise-logo.png" class="img-responsive">
        <h3>Manage your drivers with ease</h3>
      </div>
    </section>

    <section class="enterprise-feature">
      <div class="container-fluid">
        <h2>Dereva Enterprise Features</h2>
        <div class="row">
          <div class="col-xs-6 col-sm-3">
            <img src="images/enterprise-feature-icon5.png" class="img-responsive feature-icon">
            <h4>Essential documents on demand</h4>
          </div>
          <div class="col-xs-6 col-sm-3">
            <img src="images/enterprise-feature-icon6.png" class="img-responsive feature-icon">
            <h4>Safe and Secure</h4>
          </div>
          <div class="col-xs-6 col-sm-3">
            <img src="images/enterprise-feature-icon7.png" class="img-responsive feature-icon">
            <h4>Edit records with ease</h4>
          </div>
          <div class="col-xs-6 col-sm-3">
            <img src="images/enterprise-feature-icon4.png" class="img-responsive feature-icon">
            <h4>Dedicated support</h4>
          </div>
        </div>
      </div>
    </section>

    <section class="why-enterprise">
      <div class="container">
        <h3>Why use Dereva Enterprise?</h3>
        
          <div class="row">
            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-7 txt align-ryt">
                    <p>Driving Licence <br>renewal <br>reminder</p>
                  </div>
                  <div class="col-xs-5">
                    <img src="images/driver-licence-renewal.png" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-5">
                    <img src="images/assign-vehicles.png" class="img-responsive">
                  </div>
                  <div class="col-xs-7 txt align-left">
                    <p>Assign vehicles <br/>to drivers</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-7 txt align-ryt">
                    <p>Vehicle Insurance <br/>renewal reminders</p>
                  </div>
                  <div class="col-xs-5">
                    <img src="images/vehicle-insurance.png" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-5">
                    <img src="images/manage-and-renew.png" class="img-responsive">
                  </div>
                  <div class="col-xs-7 txt align-left">
                    <p>Manage and renew <br/>driver contracts</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-7 txt align-ryt">
                    <p>Vehicle Inspection <br/>renewal reminders</p>
                  </div>
                  <div class="col-xs-5">
                    <img src="images/vehicle-inspection.png" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="block-content">
                <div class="row">
                  <div class="col-xs-5">
                    <img src="images/flag-bad-drivers.png" class="img-responsive">
                  </div>
                  <div class="col-xs-7 txt align-left">
                    <p>Flag bad drivers</p>
                  </div>
                </div>
              </div>
            </div>

          </div>

      </div>
    </section>

    <section class="redBG-block">
      <div class="container">
        <h3>So what are you waiting for?</h3>
        <a href="#" class="btn btn-default" role="button">Sign Up</a>
      </div>
    </section>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>
  
  <footer class="new-footer">
    <div class="container-fluid hidden-xs">
      <div class="col-sm-4">
        <p>&copy; dereva.com | All rights reserved.</p>
      </div>
      <div class="col-sm-4">
        <a href="terms-of-service.php">Terms of Service</a>
      </div>
      <div class="col-sm-4">
        <ul style="float: right;">
          <li>Follow Us</li>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
        </ul>
      </div>
    </div>

    <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <a href="terms-of-service.php">Terms of Service</a>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;">&copy; dereva.com | All rights reserved.</p>
      </div>
    </div>
  </footer>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script src="js/wow.js"></script>
  <script>
    wow = new WOW({
      animateClass: 'animated',
      offset:       100,
      callback:     function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
      }
    });
    wow.init();
</script>
      
  </body>
</html>