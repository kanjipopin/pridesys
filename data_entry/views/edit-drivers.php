<?php include'header.php'; ?>

    <div class="page-rightWidth">
        <div class="edit-driver-page">
            <div class="heading">
                <h4>Driver Details: <span style="color: #ed1f24;">Edit Mode</span></h4>
                <div class="filters">
                    <div class="form-inline">
                        <!-- <button type="button" class="btn btn-default">Add</button> -->
                        <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;
                    border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 20px;width:140px;color: #897e74;">< Back</a>
                    </div>
                </div>
            </div>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"> </div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/edit-drivers-forms.php" method="POST" id="form" name="form">
                    <h4>Personal Details</h4>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Working Status: </label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" name="workingStatus" id="workingStatus" value="working" <?php if($results['driver_work_status'] == "working"){ echo "checked"; } ?> > Working
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="workingStatus" id="workingStatus" value="notworking" <?php if($results['driver_work_status'] == "notworking"){ echo "checked"; } ?> > Not Working
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">National ID Number: </label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="NSN" id="NSN" placeholder="Enter a National Indetification Number *" value="<?php echo $results['drv_NSN']; ?>"  readonly>

                            <!-- <input type="hidden" class="form-control" name="oldNSN" id="oldNSN" value="<?php// echo $results['drv_NSN']; ?>"> -->
                            <!-- <input type="hidden" class="form-control" name="updateQuery" id="updateQuery" value="no"> -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>
                        <div class="col-sm-3">
                            <input type="hidden" name="driver_id" id="driver_id" value="<?php echo $results['drv_id']; ?>">
                            <input type="hidden" name="empdt_id" id="empdt_id" value="<?php echo $results['empdt_id']; ?>">
                            <input type="hidden" name="edocs_id" id="edocs_id" value="<?php echo $documentRow['edocs_id']; ?>">

                            <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter a First Name *" value="<?php echo $results['drv_fname']; ?>" >

                            <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id; ?>">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="mname" id="mname" placeholder="Enter a Middle Name *" value="<?php echo $results['drv_mname']; ?>" >
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter a Last Name *" value="<?php echo $results['drv_lname']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Gender :</label>
                        <div class="col-sm-3">
                            <input type="radio" name="gender" id="gender" value="male" <?php if($results['gender'] == "male") { echo "checked = 'checked'"; } ?> style="height:auto" > Male
                            <input type="radio" name="gender" id="gender" value = "female" <?php if($results['gender'] == "female") { echo "checked = 'checked'"; } ?> style="height:auto" > Female
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                            <input type="file" id="driverImg" name="driverImg">
                            <input type="hidden" id="driverImg1" name="driverImg1" value="<?php echo $results['drv_img']; ?>">

                            <img src="../<?php echo @$img = ltrim($results['drv_img'],"../"); ?>" class="img-responsive" id="img" name="img" style="width: 100px;"/>
                        </div>
                    </div>


                    <h4>Contact Information</h4>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Mobile Number :</label>
                        <div class="col-sm-5">
                            <div class="col-sm-6 padLEFT0">
                                <input type="text" class="form-control" id="contact1" name="contact1" placeholder="Enter a Mobile No.1" onkeypress="return isNumber(event)" maxlength="12" value="<?php echo $results['drv_contact_no']; ?>" >
                            </div>
                            <div class="col-sm-6 padRIGHT0">
                                <input type="text" class="form-control" id="contact2" name="contact2" placeholder="Enter a Mobile No.2" onkeypress="return isNumber(event)" maxlength="12" value="<?php echo $results['drv_alt_contact_no']; ?>" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Email :</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter a Email ID" value="<?php echo $results['drv_email']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Address :</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter a Address 1" value="<?php echo $results['drv_address']; ?>" >
                            <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter a Address 2" value="<?php echo $results['drv_address2']; ?>" >
                            <div class="col-sm-6 padLEFT0">
                                <select class="form-control" id="city" name="city" >
                                    <option value="" selected disabled>Select a City</option>
                                    <?php
                                    $city = $conn->prepare("SELECT * from kenya_city");
                                    $city->execute();
                                    $chkcity = "";
                                    while($cityRow = $city->fetch()){
                                        if($cityRow['city_id'] == $results['drv_city']){
                                            $chkcity = "selected";
                                        }
                                        else{
                                            $chkcity = "";
                                        }
                                        ?>
                                        <option value = "<?php echo $cityRow['city_id']; ?>"<?php echo $chkcity; ?>><?php echo $cityRow['city_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6 padRIGHT0">
                                <input type="text" class="form-control" id="pobox" name="pobox" placeholder="P.O. Box" value="<?php echo $results['drv_pobox']; ?>" >
                            </div>
                            <div class="col-sm-6 padLEFT0">
                                <input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="Postal Code" value="<?php echo $results['drv_postal']; ?>" >
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="nssf" name="nssf" placeholder="Enter a NSSF" maxlength="12" value="<?php// echo $results['empdt_nssfNo']; ?>" >
                        </div>
                      </div> -->

                    <!--  <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="nhifNo" name="nhifNo" placeholder="Enter a NHIF" maxlength="12" value="<?php //echo $results['empdt_nhif']; ?>" >
                        </div>
                      </div> -->

                    <h4>Driving Licence Details</h4>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Driving Licence Number:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Driving License Number" id="dlNum" name="dlNum" value="<?php echo $results['drv_driving_license_no']; ?>" >
                        </div>
                        <div class="col-sm-2">
                            <input  type="date" class="datepicker" placeholder="Issued Date" id="issueDate" name="issueDate" value="<?php echo $results['drv_dl_issue_date']; ?>" style="width: 100%;">
                        </div>
                        <div class="col-sm-2">
                            <input  type="date" class="datepicker" placeholder="Expiry Date" id="expiryDate" name="expiryDate" value="<?php echo $results['drv_dl_expiry_date']; ?>" style="width: 100%;">
                        </div>
                        <div class="col-sm-3">
                            <input type="file" class="fl" id="driverLicence" name="driverLicence">
                            <input type="hidden" id="driverLicence1" name="driverLicence1" value="<?php echo $documentRow['edocs_dl_copy']; ?>">
                            <?php if($documentRow['edocs_dl_copy'] != ""){ ?>
                                <small style="color:green">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_dl_copy'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_dl_copy'] == ""){ ?>
                                <small style="color:red">Not Uploaded</small>
                            <?php } ?>
                            <p id="drvng"></p>
                        </div>

                        <label for="inputEmail3" class="col-sm-3 control-label">Is License PSV? :</label>
                        <div class="col-sm-2">
                            <input type="radio" name="isLicensePSV" id="PSV_License_Yes" value="Yes" <?php if($results['isLicensePSV'] == "Yes") { echo "checked = 'checked'"; } ?> style="height:auto"> Yes
                            <input type="radio" name="isLicensePSV" id="PSV_License_No" value="No" <?php if($results['isLicensePSV'] == "No") { echo "checked = 'checked'"; } ?> style="height:auto"> No
                        </div>

                    </div>



                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Driver License Class: </label>
                        <div class="col-sm-5">
                            <select id="vehiclelist" name="vehiclelist[]" multiple="multiple" class="form-control chosen-select" >
                                <?php
                                $drvVehicle = $conn->prepare("SELECT * from logis_drivers_master where drv_NSN='{$nsn}'");
                                $drvVehicle->execute();
                                while($drvVehicleRow = $drvVehicle->fetch()){
                                    $vehicleclass = explode(",",$drvVehicleRow['logis_drivers_vehicle_class']);

                                    $vehicle = $conn->prepare("SELECT * from logis_vehicle where logis_vehicle_status = 'Active'");
                                    $vehicle->execute();
                                    while($vehicleRow = $vehicle->fetch()){
                                        $subcatId = $vehicleRow['logis_vehicle_id'];
                                        ?>

                                        <option value="<?php echo $vehicleRow['logis_vehicle_id']; ?>" <?php if(in_array($subcatId,$vehicleclass)) { echo "selected = 'selected'"; } ?> > <?php echo $vehicleRow['logis_vehicle_name']; ?> </option>

                                    <?php } } ?>
                            </select>
                        </div>
                    </div>

                    <!--<h4>Driver Port Pass</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Driver Port Pass Number :</label>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Drivers Port Pass Number" id="driver_port_number" name="driver_port_number" value="<?php /*echo $results['driver_port_number']; */?>">
                          </div>

                          <div class="col-sm-3">
                             <input  type="date" class="datepicker" placeholder="Driver Port Pass Expiry Date" id="driver_port_expire_date" name="driver_port_expire_date" value="<?php /*echo $results['driver_port_expire_date']; */?>" style="width: 100%;">
                          </div>
                      </div>

                      <h4>Vehicle Port Pass</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Vehicle Port Pass Number :</label>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Vehicle Port Pass Number" id="vehicle_port_number" name="vehicle_port_number" value="<?php /*echo $results['vehicle_port_number']; */?>">
                          </div>

                          <div class="col-sm-3">
                             <input  type="date" class="datepicker" placeholder="Vehicle Port Pass Expiry Date" id="vehicle_port_expire_date" name="vehicle_port_expire_date" value="<?php /*echo $results['vehicle_port_expire_date']; */?>" style="width: 100%;">
                          </div>
                      </div>-->

                    <h4>Vehicle Assignment</h4>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Vehicle type :</label>
                        <div class="col-sm-3">
                            <input type="radio" name="Vehicle_Type" id="PSV_Vehicles" value="PSV" <?php if($results['vehicleType'] == "PSV") { echo "checked = 'checked'"; } ?> style="height:auto"> PSV
                            <input type="radio" name="Vehicle_Type" id="Other_Vehicles" value="Other" <?php if($results['vehicleType'] == "Other" || $results['vehicleType'] == "") { echo "checked = 'checked'"; } ?> style="height:auto"> Other
                        </div>
                    </div>

                    <?php //if($results['vehicleType'] == "PSV") { ?>

                    <div class="form-group" id="toggle_Plat_Number">
                        <label for="inputEmail3" class="col-sm-3 control-label">Vehicle Plate Number: </label>
                        <div class="col-sm-6">
                            <input type="text" name="vehiPlatNumber" id="vehiPlatNumber" placeholder="Vehicle Plate Number" class="form-control" value="<?php echo $results['vehiPlatNumber']; ?>">
                        </div>
                    </div>

                    <?php //} ?>

                    <!--<h4>Vehicle Insurance Details</h4>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Insurance Type: </label>
                        <div class="col-sm-5">
                          <input type="radio" name="insuranceType" id="insuranceType" value="private" style="height:auto" <?php /*if($results['insurance_type'] == "private"){ echo "checked"; } */?>> Private
                          <input type="radio" name="insuranceType" id="insuranceType" value="commercial" style="height:auto" <?php /*if($results['insurance_type'] == "commercial"){ echo "checked"; } */?>> Commercial
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Insurance Reference Number: </label>
                        <div class="col-sm-5">
                          <input type="text" name="insuranceRefNum" id="insuranceRefNum" placeholder="Reference Number" class="form-control" value="<?php /*echo $results['insurance_ref_number']; */?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Valid till: </label>
                        <div class="col-sm-5">
                          <input type="date" name="insuranceExpDate" id="insuranceExpDate" placeholder="Expiry Date" class="form-control" value="<?php /*echo $results['insurance_exp_date']; */?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                          <input type="file" class="fl" id="insuranceImg" name="insuranceImg">
                          <input type="hidden" id="insuranceImg1" name="insuranceImg1" value="<?php /*echo $results['insurance_img']; */?>">
                          <?php /*if($results['insurance_img'] != ""){ */?>
                            <small style="color:green">Uploaded</small>
                            <a href="<?php /*echo @$cv = ltrim($results['insurance_img'],"../"); */?>" download>view</a>
                          <?php /*} elseif($results['insurance_img'] == ""){ */?>
                           <small style="color:red">Not Uploaded</small>
                          <?php /*} */?>
                        </div>
                      </div>-->


                    <!--<h4>Vehicle Inspection Details</h4>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Vehicle Inspection Details: </label>
                        <div class="col-sm-6">
                          <textarea name="vehicleInspection" id="vehicleInspection" placeholder="Vehicle Inspection Details" class="form-control"><?php /*echo $results['vehicleInspection']; */?></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Upload File :</label>
                        <div class="col-sm-5">
                          <input type="file" id="vehicleInspectionFile" name="vehicleInspectionFile">

                          <input type="hidden" id="vehicleInspectionFile1" name="vehicleInspectionFile1" value="<?php /*echo $results['vehicleInspectionFile']; */?>">
                            <?php /*if($results['vehicleInspectionFile'] != ""){ */?>
                              <small style="color:green">Uploaded</small>
                              <a href="<?php /*echo @$cv = ltrim($results['vehicleInspectionFile'],"../"); */?>" download>view</a>
                            <?php /*} elseif($results['vehicleInspectionFile'] == ""){ */?>
                             <small style="color:red">Not Uploaded</small>
                            <?php /*} */?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Valid till: </label>
                        <div class="col-sm-5">
                          <input type="date" name="inspectionExpDate" id="inspectionExpDate" placeholder="Expiry Date" class="form-control" value="<?php /*echo $results['vehicleInspection_date']; */?>">
                        </div>
                      </div>-->

                    <?php
                    $i = 0;
                    while($kinRow = $kin->fetch()){
                        $i++;
                        ?>
                        <div class="kin<?php echo $kinRow['drivers_kin_id']; ?>">
                            <h4>Next of kin <?php echo $i; ?> <button type="button" name="secondKinBtnDel" id="secondKinBtnDel" data-id="<?php echo $kinRow['drivers_kin_id']; ?>" class="btn-default btn" onclick="getdeleteKin(event);" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> - Remove</button></h4>

                            <div class="form-group">
                                <input type="hidden" class="form-control" name="kin_id" id="kin_id" value="<?php echo $kinRow['drivers_kin_id']; ?>">

                                <label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kinname[]" id="kinname" placeholder="Enter a Full Name" value="<?php echo $kinRow['drivers_kin_name']; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">National ID Number :</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kinnsn[]" id="kinnsn" placeholder="Enter a National ID" value="<?php echo $kinRow['drivers_kin_nsn']; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>
                                <div class="col-sm-5">
                                    <textarea class="form-control" name="kinadd[]" id="kinadd" placeholder="Enter a Address" ><?php echo $kinRow['drivers_kin_address']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kincontact[]" id="kincontact" placeholder="Enter a Contact Number" value="<?php echo $kinRow['drivers_kin_contact']; ?>" onkeypress="return isNumber(event)" maxlength="12" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kin_email[]" id="kin_email" placeholder="Enter a Email" value="<?php echo $kinRow['drivers_kin_email']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kinrelation[]" id="kinrelation" placeholder="Enter a Relationship" value="<?php echo $kinRow['drivers_kin_relation']; ?>" >
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if($kinCount < 3){ ?>
                        <input type="hidden" name="kinCount" id="kinCount" value="<?php echo $kinCount; ?>">

                        <div class="form-group col-offset-sm-3">
                            <button type="button" name="ThirdKinBtn" id="ThirdKinBtn" class="btn btn-default" onclick="AddKin();" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> + Add</button>
                        </div>
                    <?php } ?>

                    <div class="addMoreKin"></div>


                    <h4>Employement Contract</h4>
                    <!-- <div class="form-group"> -->
                    <!-- <label for="inputEmail3" class="col-sm-3 control-label">Payroll No./Employee Id :</label> -->
                    <!--  <div class="col-sm-5">
                            <input type="text" class="form-control" name="empId" id="empId" placeholder="Payroll No./Employee Id" value="<?php// echo $results['empdt_empID']; ?>" readonly > -->
                    <!-- <input type="hidden" class="form-control" name="updateEmpQuery" id="updateEmpQuery" value="no">   -->
                    <!-- </div> -->
                    <!-- </div> -->

                    <input type="hidden" class="datepicker" placeholder="Commenced Start Date" id="commenced_startDate" name="commenced_startDate" value="<?php echo $results['empdt_commenced_date']; ?>"  style="width: 100%;">

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Contract Date :</label>
                        <div class="col-sm-3">
                            <label for="inputPassword3" class="control-label">From</label>
                            <input  type="date" class="datepicker" placeholder="Select Date"  id="startDate" name="startDate" value="<?php echo $results['empdt_contract_start']; ?>"  style="width: 100%;">
                        </div>
                        <div class="col-sm-3">
                            <label for="inputPassword3" class=" control-label">To</label>
                            <input  type="date" class="datepicker" placeholder="Select Date"  id="endDate" name="endDate" value="<?php echo $results['empdt_contract_end']; ?>"  style="width: 100%;" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Branch Location:</label>
                        <div class="col-sm-5 load_branch">
                            <input type="text" name="deptId" id="deptId" class="form-control" placeholder="Branch" value="<?php echo $results['empdt_dept_id']; ?>" >

                            <!--  <select name="deptId" id="deptId" class="form-control chosen-select" >
                               <option default select ed disabled="disabled" value="0">Select Branch</option>-->
                            <?php
                            // while($dept = $department->fetch()){
                            //   $selectedDpt = "";
                            //   if($dept['dept_id'] == $results['empdt_dept_id']){
                            //     $selectedDpt = "selected";
                            //   }
                            //   else{
                            //     $selectedDpt = "";
                            //   }
                            //   echo "<option value='".$dept['dept_id']."'".$selectedDpt.">".$dept['dept_name']."</option>";
                            // }
                            ?>
                            <!-- </select> -->
                            <!--  <a class="btn btn-default add-branch" href="#" role="button">Add</a> -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Department:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="department" name="department" value="<?php echo $results['department']; ?>" placeholder="Department" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                            <input type="file" class="fl" id="ContractFile" name="ContractFile">
                            <input type="hidden" id="ContractFile1" name="ContractFile1" value="<?php echo $results['contract_file']; ?>">
                            <?php if($results['contract_file'] != ""){ ?>
                                <small style="color:green">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($results['contract_file'],"../"); ?>" download>view</a>
                            <?php } elseif($results['contract_file'] == ""){ ?>
                                <small style="color:red">Not Uploaded</small>
                            <?php } ?>
                        </div>
                    </div>

                    <h4>Employment Documents</h4>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Curriculum Vitae :</label>
                        <div class="col-sm-6">
                            <input type="file" id="cv" name="cv" style="display: inline-block;">
                            <input type="hidden" id="cv1" name="cv1" value="<?php echo $documentRow['edocs_cv']; ?>">
                            <?php if($documentRow['edocs_cv'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_cv'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_cv'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Application Letter :</label>
                        <div class="col-sm-6">
                            <input type="file" id="appLetter" name="appLetter" style="display: inline-block;">
                            <input type="hidden" id="appLetter1" name="appLetter1" value="<?php echo $documentRow['edocs_app_letter']; ?>">

                            <?php if($documentRow['edocs_app_letter'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_app_letter'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_app_letter'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="app"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Professional & Academic qualifications :</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="qualification" name="qualification" value="<?php echo $results['empdt_qualification']; ?>" placeholder="Enter Professional & Academic qualifications">
                        </div>
                        <div class="col-sm-6">
                            <input type="file" id="qualificationD" name="qualificationD" style="display: inline-block;">
                            <input type="hidden" id="qualificationD1" name="qualificationD1" value="<?php echo $documentRow['edocs_qualification']; ?>">

                            <?php if($documentRow['edocs_qualification'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_qualification'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_qualification'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="qulfy"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">ID Card copy :</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="idCard" name="idCard" value="<?php echo $results['empdt_Idcopy']; ?>" placeholder="Enter ID Card">
                        </div>
                        <div class="col-sm-6">
                            <input type="file" id="idCardD" name="idCardD" style="display: inline-block;">
                            <input type="hidden" id="idCardD1" name="idCardD1" value="<?php echo $documentRow['edocs_id_card']; ?>">

                            <?php if($documentRow['edocs_id_card'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_id_card'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_id_card'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="icard"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">KRA PIN copy :</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="pin" name="pin" value="<?php echo $results['empdt_pinCert']; ?>" placeholder="Enter KRA Pin">
                        </div>
                        <div class="col-sm-6">
                            <input type="file" id="pinD" name="pinD" style="display: inline-block;">
                            <input type="hidden" id="pinD1" name="pinD1" value="<?php echo $documentRow['edocs_pin_cert']; ?>">

                            <?php if($documentRow['edocs_pin_cert'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_pin_cert'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_pin_cert'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="pinn"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                        <!--   <div class="col-sm-3">
                          <input type="text" class="form-control" id="nssf1" name="nssf1" value="<?php //echo $results['empdt_nssfNo']; ?>">
                        </div> -->
                        <div class="col-sm-6">
                            <input type="file" id="nssfD" name="nssfD" style="display: inline-block;">
                            <input type="hidden" id="nssfD1" name="nssfD1" value="<?php echo $documentRow['edocs_nssf']; ?>">

                            <?php if($documentRow['edocs_nssf'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_nssf'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_nssf'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="nssfid"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                        <!--   <div class="col-sm-3">
                          <input type="text" class="form-control" id="nhifNo1" name="nhifNo1" value="<?php// echo $results['empdt_nhif']; ?>">
                        </div> -->
                        <div class="col-sm-6">
                            <input type="file" id="nhifD" name="nhifD" style="display: inline-block;">
                            <input type="hidden" id="nhifD1" name="nhifD1" value="<?php echo $documentRow['edocs_nhif']; ?>">

                            <?php if($documentRow['edocs_nhif'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_nhif'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_nhif'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="nhifid"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Certificate of Good Conduct :</label>
                        <div class="col-sm-6">
                            <input type="file" class="fl" id="bonafideCert" name="bonafideCert" style="display: inline-block;">
                            <input type="hidden" id="bonafideCert1" name="bonafideCert1" value="<?php echo $documentRow['edocs_conduct_cert']; ?>">

                            <?php if($documentRow['edocs_conduct_cert'] != ""){ ?>
                                <small style="color:green;display: inline-block;">Uploaded</small>
                                <a href="../<?php echo @$cv = ltrim($documentRow['edocs_conduct_cert'],"../"); ?>" download>view</a>
                            <?php } elseif($documentRow['edocs_conduct_cert'] == ""){ ?>
                                <small style="color:red;display: inline-block;">Not Uploaded</small>
                            <?php } ?>
                            <p id="bonaid"></p>
                        </div>
                    </div>




                    <!-- <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Annual Leave Days:</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="leave" name="leave" value="<?php// echo $results['empdt_annual_leaves']; ?>" onkeypress="return isNumber(event);" >
                        </div>
                      </div> -->

                    <h4>Salary</h4>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Gross Salary :</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control comma_value" id="grossSal" name="grossSal" value="<?php echo $results['gross_salary']; ?>" onkeypress="return isNumber(event)" >

                            <!-- <input type="hidden" class="form-control" name="updatesalary" id="updatesalary" value="no"> -->
                        </div>
                    </div>

                    <?php if($orderDetails != "false"){ ?>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="submit" name="submit" class="btn btn-default" style="background: #F00;border-color: #F00;">Submit</button>
                            </div>
                        </div>
                    <?php } ?>
                </form>
            </div>
            <div class="msg">
                <p>Sorry these data already exists. kindly update from drivers page.</p>
            </div>
        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>

    </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <script src="js/typeahead.min.js"></script>
    <script src="../js/datepicker.js"></script>
    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
            // $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            // $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            // $('#inspectionExpDate').datepicker({ format: "yyyy/mm/dd" });

            var vehiType = $('input[name=Vehicle_Type]:checked').val();

            if(vehiType === "Other") {

                document.getElementById("toggle_Plat_Number").style.display = "none";

            } else {

                document.getElementById("toggle_Plat_Number").style.display = "block";

            }


            $('input[type=radio][name=Vehicle_Type]').change(function() {

                if(this.value === "Other") {

                    document.getElementById("toggle_Plat_Number").style.display = "none";

                } else {

                    document.getElementById("toggle_Plat_Number").style.display = "block";

                }

            });

            $(".msg").hide();

            var format = function(num){
                var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
                if(str.indexOf(".") > 0) {
                    parts = str.split(".");
                    str = parts[0];
                }
                str = str.split("").reverse();
                for(var j = 0, len = str.length; j < len; j++) {
                    if(str[j] != ",") {
                        output.push(str[j]);
                        if(i%3 == 0 && j < (len - 1)) {
                            output.push(",");
                        }
                        i++;
                    }
                }
                formatted = output.reverse().join("");
                return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
            };


            $(function(){
                $(".comma_value").keyup(function(e){
                    $(this).val(format($(this).val()));
                });
            });


            $('#empId').change(function () {
                var value = $("#empId").val();

                if(value!=null)
                {
                    $.ajax({
                        method:"GET",
                        url: "functions/searchDriver.php?chkempId="+value
                    })
                        .done(function(empdata) {
                            empdata = JSON.parse(empdata);
                            var e = empdata.empdt_empID;

                            if(empdata!=false)
                            {
                                console.log("The Employee ID is Check"+empdata);
                                alert("Employee ID is aleardy exist Please enter a new Id.");
                                $("#empId").val('');
                            }
                            console.log("value of data="+empdata);
                        });
                }
                else{
                    alert("Please enter the Employee Id to proceed");
                }
            });

            var url = window.location.href;
            var msg = url.substring(url.indexOf('?')+5);
            console.log(msg);
            if(msg=="success"){
                $("#alertBox").removeClass("hidden");
                $("#alertBox").addClass("alert-success");
                $("#alertBox").html("Driver Details submitted succesfully");
                window.location.href ='drivers.php';
            }
            else if(msg=="error"){
                $("#alertBox").removeClass("hidden");
                $("#alertBox").addClass("alert-danger");
                $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
            }
            else{
                $("#alertBox").addClass("hidden");
                $("#alertBox").removeClass("alert-success");
                $("#alertBox").removeClass("alert-danger");
                $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
            }


            // $('#startDate').datepicker({
            //     format: "yyyy/mm/dd"
            // }).on('changeDate', function(ev){
            //  // alert("working");
            //    var date1 = $('#startDate').datepicker('getDate');
            //    var date2 = date1.setDate(date1.getDate()+364)
            //   $('#endDate').datepicker('setDate',date1);
            // });

            // $('#endDate').datepicker({
            //     format: "yyyy/mm/dd",
            // });

            // $('#issueDate').datepicker({
            //     format: "yyyy/mm/dd"
            // }).on('changeDate', function(ev){
            //   $("#expiryDate").val("");
            // });

            // $('#expiryDate').datepicker({
            //     format: "yyyy/mm/dd"
            // }).on('changeDate', function(ev){
            //  // alert("working");
            //    var date1 = $('#issueDate').datepicker('getDate');
            //    var date2 = $('#expiryDate').datepicker('getDate');
            //    if(date1 > date2)
            //    {
            //     alert("Date cannot be less than issue Date");
            //     $('#expiryDate').datepicker('setDate',date1);
            //    }

            // });
        });


        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $(document).ready(function() {
            $('#nssf').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nssf1').val(txtVal);
            });

            $('#nhifNo').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nhifNo1').val(txtVal);
            });
        });


        function getdeleteKin(event){
            var KinId = $(event.currentTarget).attr("data-id");

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : {"type":"deleteKin","KinId":KinId},

                success : function(msg){
                    // $(".kin"+KinId).hide();
                    location.reload();
                }
            });

        }
    </script>

    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script>
        $( "#form" ).validate({
            rules: {
                email: {
                    email: true
                },
            }
        });
    </script>

    <link href="css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#lstFruits').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>

    <script type="text/javascript">
        function AddKin(){

            var kinCount = $("#kinCount").val();
            var count = parseInt(kinCount) + parseInt(1);

            if(count <= 3){

                var KinDetails = '<div class="kin'+count+'"><h4>Next of kin '+count+' <button type="button" name="secondKinBtnDel" id="secondKinBtnDel" data-id="'+count+'" class="btn-default btn" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;" onclick="getRemoveKin(event);"> - Remove</button></h4><div class="form-group"><input type="hidden" class="form-control" name="kin_id" id="kin_id"><label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label><div class="col-sm-5"><input type="text" class="form-control" name="kinname[]" id="kinname" placeholder="Enter a Full Name" ></div></div><div class="form-group"><label for="inputEmail3" class="col-sm-3 control-label">National ID Number :</label><div class="col-sm-5"><input type="text" class="form-control" name="kinnsn[]" id="kinnsn" placeholder="Enter a National ID"  ></div></div><div class="form-group"><label for="inputEmail3" class="col-sm-3 control-label">Address :</label><div class="col-sm-5"><textarea class="form-control" name="kinadd[]" id="kinadd" placeholder="Enter a Address" ></textarea></div></div><div class="form-group"><label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label><div class="col-sm-5"><input type="text" class="form-control" name="kincontact[]" id="kincontact" placeholder="Enter a Contact Number" onkeypress="return isNumber(event)" maxlength="12" ></div></div><div class="form-group"><label for="inputEmail3" class="col-sm-3 control-label">Email :</label><div class="col-sm-5"><input type="text" class="form-control" name="kin_email[]" id="kin_email" placeholder="Enter a Email"></div></div><div class="form-group"><label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label><div class="col-sm-5"><input type="text" class="form-control" name="kinrelation[]" id="kinrelation" placeholder="Enter a Relationship" ></div></div></div>';

                $(".addMoreKin").append(KinDetails);
                $("#kinCount").val(count);

                if(count == 3){
                    $("#ThirdKinBtn").hide();
                }
            }
            else {
                $("#ThirdKinBtn").hide();
            }
        }


        function getRemoveKin(event){
            var ID = $(event.currentTarget).attr("data-id");
            $(".kin"+ID).remove();

            var KinCount = $("#kinCount").val();
            var count = KinCount - 1;

            if(count <= 3){
                $("#ThirdKinBtn").show();
                $("#kinCount").val(count);
            }
        }
    </script>

<?php include 'footer.php'; ?>