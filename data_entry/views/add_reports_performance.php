<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['Email'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $action = isset($_GET['action']) ? $_GET['action'] : "";

  if(isset($_REQUEST['submitReports'])){

    $driver = $_REQUEST['driver'];
    $report_type = $_REQUEST['report_type'];
    $from_date = date("Y-m-d", strtotime($_REQUEST['from_date']));
    $to_date = date("Y-m-d", strtotime($_REQUEST['to_date']));

    @$_SESSION['driver'] = $driver;
    @$_SESSION['from_date'] = $from_date;
    @$_SESSION['to_date'] = $to_date;

    if($report_type != "Performance"){

      if($report_type != "Give_report"){

        $report_id = "";
        $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_type = '{$report_type}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
        $checkDriver->execute();
        while($checkDriverRow = $checkDriver->fetch()){
          $report_id .= $checkDriverRow['report_id'].",";
        }
          $trimreport_id = rtrim($report_id,",");

          header("location: reports_performance.php?id=".$trimreport_id."&type=".$report_type);
      }
      else {
        $checkPerformanceDate = $conn->prepare("SELECT * from logis_report_performance where driver_id = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_start_date = '{$from_date}' and report_end_date = '{$to_date}'");
        $checkPerformanceDate->execute();
        $checkPerformanceDateCount = $checkPerformanceDate->rowCount();
        $checkPerformanceDateRow = $checkPerformanceDate->fetch();

        if($checkPerformanceDateCount > 0){

          $report_id = "";
          $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
          $checkDriver->execute();
          while($checkDriverRow = $checkDriver->fetch()){
            $report_id .= $checkDriverRow['report_id'].",";
          }
            $trimreport_id = rtrim($report_id,",");

            header("location: reports_performance.php?id=".$trimreport_id."&type=print&performancecount=".$checkPerformanceDateRow['rating']."&pid=".$checkPerformanceDateRow['report_id']);
        }
        else {
          $report_id = "";
          $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
          $checkDriver->execute();
          while($checkDriverRow = $checkDriver->fetch()){
            $report_id .= $checkDriverRow['report_id'].",";
          }
            $trimreport_id = rtrim($report_id,",");

            header("location: reports_performance.php?id=".$trimreport_id."&type=".$report_type);
        }
      }
    }
    else {

      $checkPerformanceDate = $conn->prepare("SELECT * from logis_report_performance where driver_id = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_start_date = '{$from_date}' and report_end_date = '{$to_date}'");
      $checkPerformanceDate->execute();
      $checkPerformanceDateCount = $checkPerformanceDate->rowCount();
      $checkPerformanceDateRow = $checkPerformanceDate->fetch();

      if($checkPerformanceDateCount > 0){

        $report_id = "";
        $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
        $checkDriver->execute();
        while($checkDriverRow = $checkDriver->fetch()){
          $report_id .= $checkDriverRow['report_id'].",";
        }
          $trimreport_id = rtrim($report_id,",");

          header("location: reports_performance.php?id=".$trimreport_id."&type=print&performancecount=".$checkPerformanceDateRow['rating']."&pid=".$checkPerformanceDateRow['report_id']);
      } else {

        $report_id = "";
        $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
        $checkDriver->execute();
        while($checkDriverRow = $checkDriver->fetch()){
          $report_id .= $checkDriverRow['report_id'].",";
        }
          $trimreport_id = rtrim($report_id,",");

        header("location: reports_performance.php?id=".$trimreport_id."&type=Give_report");
      }
    }
  }
?>

<link rel="stylesheet" href="../css/datepicker.css">

<div class="page-rightWidth">
  <div class="flag-driver-page">
      <div class="heading">
        <h4>Performance Report</h4>
        <div class="filters">
          <div class="form-inline">
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 15px;color: #897e74;font-size: 13px;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="#" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Driver : </label>
            <div class="col-sm-5">
              <select name="driver" id="driver" class="form-control" required>
                <option value="" disabled selected>Select Driver</option>
                <?php
                  $driverData = $conn->prepare("SELECT * from logis_drivers_master where driver_work_status = 'working' and drv_curr_comp_id = '{$comprow['comp_id']}' and delete_flag = 'No' and drv_isActive = 'Active'");
                  $driverData->execute();
                  while($driverDataRow = $driverData->fetch()){
                ?>
                  <option value="<?php echo $driverDataRow['drv_id']; ?>"><?php echo $driverDataRow['drv_fname']." ".$driverDataRow['drv_lname'] ?></option>                  
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">From Date : </label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="from_date" name="from_date" value="<?php echo @$_GET['from_date']; ?>" placeholder="From Date" required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">To Date : </label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="to_date" name="to_date" value="<?php echo @$_GET['to_date']; ?>" placeholder="To Date" required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Report Type : </label>
            <div class="col-sm-5">
            <select name="report_type" id="report_type" class="form-control">
              <option value="" disabled selected>Select Report Type</option>
              <option value="Give_report">All Reports</option>
              <option value="Performance">Rating</option>
              <option value="Accidents">Accidents</option>
              <option value="Inspections">Inspections</option>
              <option value="Incidents">Incidents</option>
              <option value="Achievements">Achievements</option>
              <option value="Other">Other</option>
            </select>
            </div>
          </div>
         
          <div class="modal-footer">
            <button type="submit" name="submitReports" id="submitReports" class="btn btn-default">Search</button>

            <!-- <a href="reports_performance.php" class="btn btn-default">Back</a> -->
          </div>
        </form>
      </div>
  </div>
</div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a class="active-class" href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script src="../js/datepicker.js"></script>

<script type="text/javascript">
  function getRportType(){
    var report_type = $("#report_type").val();

    $(".report_abstract_file").hide();
    $(".report_file").hide();

    if(report_type == "Accidents"){
      $(".report_abstract_file").show();
      $(".report_file").show();
    }
    else if(report_type == "Inspections"){
      $(".report_file").show();
    }
    if(report_type == "Incidents"){
      $(".report_abstract_file").show();
      $(".report_file").show();
    }
    if(report_type == "Achievements"){
      $(".report_file").show();
    }
    if(report_type == "Other"){
      $(".report_file").show();
    }
  }

  $(document).ready(function() {
    $("#from_date").datepicker();
    $("#to_date").datepicker();
  });

</script>

<?php include'footer.php'; ?>