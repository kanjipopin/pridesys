<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['Email'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $action = isset($_GET['action']) ? $_GET['action'] : "";

  if(isset($_REQUEST['submitReports'])){

    $driver = $_REQUEST['driver'];
    $company_id = $comprow['comp_id'];
    $vehicleNum = $_REQUEST['vehicleNum'];
    $report_type = $_REQUEST['report_type'];
    $report_detail = $_REQUEST['report_detail'];
    $report_status = $_REQUEST['report_status'];
    $date = date("Y-m-d H:i:s");

    $dataBaseReportName = "";
    $dataBaseAbstracttName = "";
    $uploaddir = 'uploads/company'.$company_id;
    if(!is_dir($uploaddir)){
      mkdir($uploaddir,true);
      chmod($uploaddir,0755);
    }
    $imgfolder = $uploaddir."/report";
    if(!file_exists($imgfolder)){
      mkdir($imgfolder,0755);
    }

    if(!empty($_FILES['report_file']['name'])){
      foreach($_FILES['report_file']['tmp_name'] as $key => $tmp_name ){

        $randomNumber = rand(1,1000);

        $type = substr($_FILES['report_file']['name'][$key],strrpos($_FILES['report_file']['name'][$key],'.')+1);
        $dataBaseReportName .= $imgfolder."/".date("d-m-Y")."-".time().$randomNumber."Report.".$type."|";
        $file_name = $imgfolder."/".date("d-m-Y")."-".time().$randomNumber."Report.".$type;
        $file_tmp = $_FILES['report_file']['tmp_name'][$key];
        move_uploaded_file($file_tmp,$file_name);
      }
    }
    $rtrimDataBaseReportName = rtrim($dataBaseReportName,"|");


    if(!empty($_FILES['report_abstract_file']['name'])){
      foreach($_FILES['report_abstract_file']['tmp_name'] as $key => $tmp_name ){

        $randomNumber = rand(1,1000);

        $type =substr($_FILES['report_abstract_file']['name'][$key],strrpos($_FILES['report_abstract_file']['name'][$key],'.')+1);
        $dataBaseAbstracttName .= $imgfolder."/".date("d-m-Y")."-".time().$randomNumber."Report.".$type."|";
        $file_names = $imgfolder."/".date("d-m-Y")."-".time().$randomNumber."Report.".$type;
        $file_tmp = $_FILES['report_abstract_file']['tmp_name'][$key];
        move_uploaded_file($file_tmp,$file_names);
      }
    }
    $rtrimDataBaseAbstractName = rtrim($dataBaseAbstracttName,"|");


    $insertReport = $conn->prepare("INSERT into logis_reports 
      (driver_name,company_id,vehicle,report_type,report_details,report_status,report_date,report_update,report_file,report_abstract_file) 
      values ('$driver','$company_id','$vehicleNum','$report_type','$report_detail','$report_status','$date','$date','$rtrimDataBaseReportName','$rtrimDataBaseAbstractName')");
    $insertReport->execute();

      header("location: reports.php");
  }
?>

<div class="page-rightWidth">
  <div class="flag-driver-page">
      <div class="heading">
        <h4>Add Reports</h4>
        <div class="filters">
          <div class="form-inline">
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 15px;color: #897e74;font-size: 13px;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="#" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Driver : </label>
            <div class="col-sm-5">
              <select name="driver" id="driver" class="form-control" required>
                <option value="" disabled selected>Select Driver</option>
                <?php
                  $driverData = $conn->prepare("SELECT * from logis_drivers_master where driver_work_status = 'working' and drv_curr_comp_id = '{$comprow['comp_id']}' and delete_flag = 'No' and drv_isActive = 'Active'");
                  $driverData->execute();
                  while($driverDataRow = $driverData->fetch()){
                ?>
                  <option value="<?php echo $driverDataRow['drv_id']; ?>"><?php echo $driverDataRow['drv_fname']." ".$driverDataRow['drv_lname'] ?></option>                  
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Vehicle : </label>
            <div class="col-sm-5">
              <input type="text" class="form-control" name="vehicleNum" id="vehicleNum" placeholder="Enter Vehicle Reg No. *" required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Report Type :</label>
            <div class="col-sm-5">
              <!-- <input type="text" class="form-control" name="report_type" id="report_type" placeholder="Enter Report Type *" required> -->

              <select name="report_type" id="report_type" class="form-control" onclick="getRportType();">
                <option value="" disabled selected>Select Report Type</option>
                <option value="Accidents">Accidents</option>
                <option value="Inspections">Inspections</option>
                <option value="Incidents">Incidents</option>
                <option value="Achievements">Achievements</option>
                <option value="Other">Other</option>
              </select>
            </div>
          </div>

          <div class="form-group report_file" style="display: none;">
            <label for="inputEmail3" class="col-sm-3 control-label">Upload Images* :</label>
            <div class="col-sm-5">
              <input type="file" class="form-control" name="report_file[]" id="report_file" multiple style="padding-bottom: 45px;">
              <small style="font-size: 11px;color: #777;">press ctrl to select multiple file</small>
            </div>
          </div>

          <div class="form-group report_abstract_file" style="display: none;">
            <label for="inputEmail3" class="col-sm-3 control-label">Upload Abstract* :</label>
            <div class="col-sm-5">
              <input type="file" class="form-control" name="report_abstract_file[]" id="report_abstract_file" multiple style="padding-bottom: 45px;">
              <small style="font-size: 11px;color: #777;">press ctrl to select multiple file</small>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Details :</label>
            <div class="col-sm-5">
              <textarea name="report_detail" id="report_detail" class="form-control" placeholder="Enter Details *" rows="5" required></textarea>
            </div>
          </div>

         

          <div class="modal-footer">
            <button type="submit" name="submitReports" id="submitReports" class="btn btn-default">Submit</button>

            <a href="reports.php" class="btn btn-default">Back</a>
          </div>
        </form>
      </div>
  </div>
</div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a class="active-class" href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  function getRportType(){
    var report_type = $("#report_type").val();

    $(".report_abstract_file").hide();
    $(".report_file").hide();

    if(report_type == "Accidents"){
      $(".report_abstract_file").show();
      $(".report_file").show();
    }
    else if(report_type == "Inspections"){
      $(".report_file").show();
    }
    if(report_type == "Incidents"){
      $(".report_abstract_file").show();
      $(".report_file").show();
    }
    if(report_type == "Achievements"){
      $(".report_file").show();
    }
    if(report_type == "Other"){
      $(".report_file").show();
    }
  }
</script>

<?php include'footer.php'; ?>