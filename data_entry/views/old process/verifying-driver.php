<?php include'header.php'; ?>

      <div class="col-sm-9 padRIGHT0 mrgnTOP15">
          <div class="wht-bg">
              <div class="heading">
                <h4>Verify Drivers</h4>
              </div>
              <div class="verifying-driver-page">
                <form class="form-inline">
                  <div class="form-group" style="padding-right: 0px;">
                    <label for="exampleInputName2" style="padding-right: 0;">Search Driver by :</label>
                  </div>
                  <div class="form-group">
                    <label for="NIN">National Identity Number</label>
                    <input type="text" class="form-control width120" id="searchNIN" value="">
                  </div>
                  <div class="form-group">
                    <label for="or" style="padding-right: 0px;">or</label>
                  </div>
                  <div class="form-group">
                    <label for="driverLicence">Driving License Number</label>
                    <input type="text" class="form-control width120" id="searchDLNum" value="">
                  </div>
                  <button type="button" class="btn btn-default getrate" id="btnSearch">Search</button>
                </form>
                <div class="brdr-btm"></div>

                <div class="showdiv" style="display: none;">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="" id="driverImg" class="img-responsive" alt="User Image" />
                    </div>
                    <div class="col-sm-9">
                        <h3 id="empName" style="display:inline-block;"> </h3>
                          <?php
                            $comp_id = $_SESSION['comp_id'];
                            $red = $conn->prepare("select * from logis_drivers_master");
                            $red->execute();
                            $redRow = $red->fetch();
                                if($redRow['drv_red_flag'] == "Yes"){
                          ?>
                              <img src = "images/flag.png" style="display: inline-block;">
                              <input type="hidden" name="redflagNo" id="redflagNo" value="No">
                              <i style="color:red;">driver has been red flagged.</i>
                         <?php } ?>
                        <ul class="driverDETAIL" id="driverDet">
                            <li id="city"></li>
                            <!-- <li id="mobile"></li> -->
                            <!-- <li id="phone"></li> -->
                        </ul>
                        <h4>National Identity Number : <span style="color: #979797;" id="NSN"></span></h4>
                        <h4>Driving License Number : <span style="color: #979797;" id="dlNum"></span></h4>
                    </div>
                </div>
                <div class="brdr-btm"></div>
                <h4>Previous Company Record</h4>
                <div class="table-responsive detail-table">
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th width="25%">Company Name</th>
                          <th width="15%">Periods</th>
                          <th width="20%" id="norate">Rating</th>
                          <th width="20%">Comment</th>
                          <th width="20%" class="text-center">Enquiry</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td id="comp_name"></td>
                          <td id="comp_periods"></td>
                          <td id="rated" style="display: none;"></td>
                          <td class="rateyo-readonly-widg"></td>
                          <td id="comment"></td>
                          <td class="text-center"><a class="fade_open btn btn-default request-btn" href="#fade" role="button">Request More Detail</a></td>
                      </tr>
                  </tbody>
              </table>
              </div>
              </div>
              </div>
          </div>
      </div>
      </div>
  </div>

   <!-- Popup Content -->

  <div id="fade" class="well rqst-more-detail-popup">
      <div class="popup-head">
        <h4>Request more Detail</h4>
      </div>
      <div class="popup-body">
          <form class="form-horizontal">
              <div class="form-group">
                <label for="exampleInputEmail1" class="col-sm-2 control-label">To</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="toEmail" placeholder="Email" value="Markweb Solutions" disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="col-sm-2 control-label">Subject</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="subject" placeholder="Subject" value="Driver Detail Request | James Vanes" disabled>
                  </div>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="col-sm-2 control-label">Comment</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
                </div>
              </div>
              <button type="button" class="btn btn-default send-btn">Send</button>
        </form>
      </div>
      <button class="fade_close btn btn-default popup-btn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
  </div>
  <!-- End Popup Content -->

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="js/rate/jquery.rateyo.min.css"/>
<script type="text/javascript" src="js/rate/jquery.min.js"></script>
<script type="text/javascript" src="js/rate/jquery.rateyo.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script src="js/jquery.popupoverlay.js"></script>
<script>
$(document).ready(function(){
    $("#btnSearch").click(function(){
        //$(".showdiv").show();
        var dlnum = $("#searchDLNum").val();
        var nsn = $("#searchNIN").val();
        var sType="";
        var value="";

        if(dlnum!=""){
          sType ='dlNum';
          value = dlnum;
          searchDriver(sType,value);
        }
        else if(nsn!=""){
          sType ='nsn';
          value = nsn;
          searchDriver(sType,value);
        }
        // console.log(sType+ "- Hi this is new box: "+value);         
    });

    $('#fade').popup({
      transition: 'all 0.3s',
      scrolllock: true
    });

    function searchDriver(sType,value){
      var sType = sType;
      var value = value;
        $.ajax({
         method:"GET",
         url: "functions/searchDriver.php?"+sType+"="+value
        })
        .done(function(data) {
              //$( this ).addClass( "done" );
              //converting JSON string to object
          data = JSON.parse(data);

          if(data.findDriver == false && data.raterow == false)
          {
             alert("Driver Details not available");
             $(".showdiv").hide();
          }
          else if(data.findDriver != false && data.raterow != false){
              $("#city").html('<img src="images/location.png" alt="Location">'+data.findDriver['drv_city']);
              $("#mobile").html('<img src="images/call-us.png" alt="Location">'+data.findDriver['drv_contact_no']);
              $("#phone").html('<img src="images/tel-no.png" alt="Location">'+data.findDriver['drv_alt_contact_no']);
              $("#NSN").text(data.findDriver['drv_NSN']);
              $("#dlNum").text(data.findDriver['drv_driving_license_no']);

              var i = data.findDriver['drv_img'];
              var img = i.replace("../","");
              $("#driverImg").attr("src",img);
              $("#empName").text(data.findDriver['drv_fname']+" "+data.findDriver['drv_mname']+" "+ data.findDriver['drv_lname']);
              $(".showdiv").show();
              $("#searchDLNum").val("");
              $("#searchNIN").val("");

              $("#comp_name").text(data.raterow['rate_comp_name']);
              $("#comp_periods").text('');
              $("#rated").text(data.raterow['rate_ratings']);
              $("#comment").text(data.raterow['rate_comments']);
              //alert("Input driver values " +data.drv_NSN);       
          }
          else if(data.findDriver != false && data.raterow == false){
              $("#city").html('<img src="images/location.png" alt="Location">'+data.findDriver['drv_city']);
              $("#mobile").html('<img src="images/call-us.png" alt="Location">'+data.findDriver['drv_contact_no']);
              $("#phone").html('<img src="images/tel-no.png" alt="Location">'+data.findDriver['drv_alt_contact_no']);
              $("#NSN").text(data.findDriver['drv_NSN']);
              $("#dlNum").text(data.findDriver['drv_driving_license_no']);

              var i = data.findDriver['drv_img'];
              var img = i.replace("../","");
              $("#driverImg").attr("src",img);
              $(".rateyo-readonly-widg").hide('data-readonly', 'true');
              $("#empName").text(data.findDriver['drv_fname']+" "+ data.findDriver['drv_lname']);
              $(".showdiv").show();
              $("#searchDLNum").val("");
              $("#searchNIN").val("");

              $("#comp_name").text("NaN");
              $("#comp_periods").text("NaN");
              $("#rated").text("NaN");
              $("#norate").css("display","none");
              $(".rateyo-readonly-widg").css("display","none");
              $("#comment").text("NaN");
                //alert("Input driver values " +data.drv_NSN);                    
          }
            // console.log("value of data "+data);

var rating = $("#rated").text();
if(rating == ""){
  rating = 0;        
}
console.log("On rating Rate="+rating);
$(".counter").text(rating);
$("#rateYo1").on("rateyo.init", function () { console.log("rateyo.init"); });
$("#rateYo1").rateYo({
  rating: rating,
  numStars: 5,
  precision: 2,
  starWidth: "64px",
  spacing: "5px",
  rtl: true,
  multiColor: {
    startColor: "#000000",
    endColor  : "#ffffff"
  },
  onInit: function () {
    console.log("On Init");
  },
  onSet: function () {
    console.log("On Set");
  }
}).on("rateyo.set", function () { console.log("rateyo.set"); })
  .on("rateyo.change", function () { console.log("rateyo.change"); });

$(".rateyo").rateYo();

$(".rateyo-readonly-widg").rateYo({
  rating: rating,
  numStars: 5,
  precision: 2,
  minValue: 1,
  maxValue: 5
}).on("rateyo.change", function (e, data) {
  console.log(data.rating);
});
    });
  }
});
</script>

<?php include'footer.php';?>