<?php include'header.php';

if(!$connection->is_logged_in()){
  $connection->redirect('../index.php');
}

?>

  <div class="bg-color">
    <div class="row">
      <div class="col-sm-3 padRIGHT0 padLEFT30">
          <div class="sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard.php"><img src="images/dashboard-icon1.png"/>Dashboard</a>
                        </li>
                        <li>
                            <a href="drivers.php"><img src="images/drivers-icon1.png"/>Drivers</a>
                        </li>
                        <li>
                            <a href="verifying-driver.php"><img src="images/verifyDriver-icon1.png"/>Verify Drivers</a>
                        </li>
                        <li>
                            <a href="payroll.php"><img src="images/payroll1.png"/>Payroll</a>
                        </li>
                        <li>
                            <a href="inbox.php"><img src="images/inbox-icon1.png"/>Inbox</a>
                        </li>
                        <li>
                            <a href="master.php"><img src="images/master.png"/>Master</a>
                        </li>
                       <!--  <li>
                            <a href="#"><img src="images/notification1.png"/>Notifications</a>
                        </li> -->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
      </div>

       <div class="col-sm-9 mrgnTOP15">
          <div class="wht-bg">
              <div class="heading">
                <h4>Driver</h4>
              </div>
              <div class="addDriver-form">
              	<div id="alertBox" class="alert hidden alert-message"> </div>
                  <form class="form-horizontal" enctype="multipart/form-data" method="POST" id="edit_form" name="edit_form">
                      <h4>Personal Detail</h4>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>
                        <div class="col-sm-3">
                          <input type="hidden" class="form-control" name="driver_id" id="driver_id" value="<?php echo $results['drv_id']; ?>">
                          
                          <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter a First Name" value="<?php echo $results['drv_fname']; ?>" required>

                          <!--<input type="hidden" class="form-control" name="compId" id="compId" value="<?php //echo $_SESSION['comp_id'];?>">-->
                          <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $c = $_SESSION['comp_id']; ?>">
                        </div>
                         <div class="col-sm-3">
                          <input type="text" class="form-control" name="mname" id="mname" placeholder="Enter a Middle Name" value="<?php echo $results['drv_mname']; ?>" required>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter a Last Name" value="<?php echo $results['drv_lname']; ?>" required>
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">National ID Number: </label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control" name="NSN" id="NSN" placeholder="Enter a National ID Number" value="<?php echo $results['drv_NSN']; ?>" required>
                          <input type="hidden" class="form-control" name="oldNSN" id="oldNSN" value="<?php echo $results['drv_NSN']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Driving Licence :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" placeholder="Enter licence no." id="dlNum" name="dlNum" value="<?php echo $results['drv_driving_license_no']; ?>" >
                        </div>
                        <div class="col-sm-2">
                           <input  type="text" class="datepicker" placeholder="Issued Date"  id="issueDate" name="issueDate" value="<?php echo $results['drv_dl_issue_date']; ?>" >
                        </div>
                        <div class="col-sm-2">
                           <input  type="text" class="datepicker" placeholder="Expiry Date"  id="expiryDate" name="expiryDate" value="<?php echo $results['drv_dl_expiry_date']; ?>" >
                        </div>
                        <div class="col-sm-3">
                          <input type="file" class="fl" id="driverLicence" name="driverLicence">
                          <input type="hidden" id="driverLicence1" name="driverLicence1" value="<?php echo $documentRow['edocs_dl_copy']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_dl_copy'],"uploads/documents/"); ?></small>
                          <p id="drvng"></p>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Mobile Number :</label>
                        <div class="col-sm-5">
                          <div class="col-sm-6 padLEFT0">
                            <input type="text" class="form-control" id="contact1" name="contact1" placeholder="Enter a Mobile No.1" onkeypress="return isNumber(event)" maxlength="12" value="<?php echo $results['drv_contact_no']; ?>" >
                          </div>
                          <div class="col-sm-6 padRIGHT0">
                            <input type="text" class="form-control" id="contact2" name="contact2" placeholder="Enter a Mobile No.2" onkeypress="return isNumber(event)" maxlength="12" value="<?php echo $results['drv_alt_contact_no']; ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Address :</label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter a Address 1" value="<?php echo $results['drv_address']; ?>" >
                          <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter a Address 2" value="<?php echo $results['drv_address2']; ?>" >
                          <div class="col-sm-6 padLEFT0">
                            <input type="text" class="form-control" id="city" name="city" placeholder="Enter a City" value="<?php echo $results['drv_city']; ?>" >
                          </div>
                          <div class="col-sm-6 padRIGHT0">
                            <input type="text" class="form-control" id="state" name="state" placeholder="Enter a PO Box" value="<?php echo $results['drv_state']; ?>" >
                          </div>
                          <div class="col-sm-6 padLEFT0">
                            <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Enter a Postal Code" value="<?php echo $results['drv_zipcode']; ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Email :</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="email" name="email" placeholder="Enter a Email ID" value="<?php echo $results['drv_email']; ?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                          <input type="file" id="driverImg" name="driverImg">
                          <input type="hidden" id="driverImg1" name="driverImg1" value="<?php echo $results['drv_img']; ?>">

                          <img src="<?php echo $results['drv_img']; ?>" class="img-responsive" id="img" name="img" style="width: 100px;"/>
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="nssf" name="nssf" placeholder="Enter a NSSF" onkeypress="return isNumber(event);" value="<?php echo $results['empdt_nssfNo']; ?>" >
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="nhifNo" name="nhifNo" placeholder="Enter a NHIF" onkeypress="return isNumber(event);" value="<?php echo $results['empdt_nhif']; ?>" >
                        </div>
                      </div>
                      <h4>Employment Details</h4>                      
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Employee Id :</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="empId" id="empId" placeholder="Employee Id" value="<?php echo $results['empdt_empID']; ?>" >

                            <input type="hidden" class="form-control" name="oldempId" id="oldempId" value="<?php echo $results['empdt_empID']; ?>">
                            <!-- <input type="hidden" class="form-control" name="updateEmpQuery" id="updateEmpQuery" value="no">   --> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Contract :</label>
                        <div class="col-sm-3">
                        <label for="inputPassword3" class="control-label">From</label>
                           <input  type="text" class="datepicker" placeholder="Select Date"  id="startDate" name="startDate" value="<?php echo $results['empdt_contract_start']; ?>" >
                        </div>
                        <div class="col-sm-3">
                        <label for="inputPassword3" class=" control-label">To</label>
                           <input  type="text" class="datepicker" placeholder="Select Date"  id="endDate" name="endDate" value="<?php echo $results['empdt_contract_end']; ?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Branch :</label>
                        <div class="col-sm-3 load_branch">
                          <select name="deptId" id="deptId" class="form-control chosen-select" >
                            <option default selected disabled="disabled" value="0">Select Branch</option>
                            <?php
                              while($dept = $department->fetch()){
                                $selectedDpt = "";
                                if($dept['dept_id'] == $results['empdt_dept_id']){
                                  $selectedDpt = "selected";
                                }
                                else{
                                  $selectedDpt = "";
                                }
                                echo "<option value='".$dept['dept_id']."'".$selectedDpt.">".$dept['dept_name']."</option>";
                              }
                            ?>
                          </select>
                         <!--  <a class="btn btn-default add-branch" href="#" role="button">Add</a> -->
                        </div>
                      </div>                     

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Annual Leave Days:</label>
                        <div class="col-sm-1">
                          <input type="text" class="form-control" id="leaveAllowance" name="leaveAllowance" value="<?php echo $results['empdt_annual_leaves']; ?>" onkeypress="return isNumber(event);" >
                        </div>
                      </div>

                      <h4>Employment Documents</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Curriculum Vitae :</label>
                        <div class="col-sm-6">
                          <input type="file" id="cv" name="cv">
                          <input type="hidden" id="cv1" name="cv1" value="<?php echo $documentRow['edocs_cv']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_cv'],"uploads/documents/"); ?></small>
                          <p id="cvv"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Application Letter :</label>
                        <div class="col-sm-6">
                          <input type="file" id="appLetter" name="appLetter">
                          <input type="hidden" id="appLetter1" name="appLetter1" value="<?php echo $documentRow['edocs_app_letter']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_app_letter'],"uploads/documents/"); ?></small>
                          <p id="app"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Professional & Academic qualifications :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="qualification" name="qualification" value="<?php echo $results['empdt_qualification']; ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="file" id="qualificationD" name="qualificationD">
                          <input type="hidden" id="qualificationD1" name="qualificationD1" value="<?php echo $documentRow['edocs_qualification']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_qualification'],"uploads/documents/"); ?></small>
                          <p id="qulfy"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">ID Card copy :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="idCard" name="idCard" value="<?php echo $results['empdt_Idcopy']; ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="file" id="idCardD" name="idCardD">
                          <input type="hidden" id="idCardD1" name="idCardD1" value="<?php echo $documentRow['edocs_id_card']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_id_card'],"uploads/documents/"); ?></small>
                          <p id="icard"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">KRA PIN copy :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="pin" name="pin" value="<?php echo $results['empdt_pinCert']; ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="file" id="pinD" name="pinD">
                          <input type="hidden" id="pinD1" name="pinD1" value="<?php echo $documentRow['edocs_pin_cert']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_pin_cert'],"uploads/documents/"); ?></small>
                          <p id="pinn"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="nssf1" name="nssf1" readonly="readonly" value="<?php echo $results['empdt_nssfNo']; ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="file" id="nssfD" name="nssfD">
                          <input type="hidden" id="nssfD1" name="nssfD1" value="<?php echo $documentRow['edocs_nssf']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_nssf'],"uploads/documents/"); ?></small>
                          <p id="nssfid"></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="nhifNo1" name="nhifNo1" readonly="readonly" value="<?php echo $results['empdt_nhif']; ?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="file" id="nhifD" name="nhifD">
                          <input type="hidden" id="nhifD1" name="nhifD1" value="<?php echo $documentRow['edocs_nhif']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_nhif'],"uploads/documents/"); ?></small>
                          <p id="nhifid"></p>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Certificate of Good Conduct :</label>
                        <div class="col-sm-6">
                          <input type="file" class="bonafideCert" id="bonafideCert" name="bonafideCert">
                          <input type="hidden" id="bonafideCert1" name="bonafideCert1" value="<?php echo $documentRow['edocs_conduct_cert']; ?>">

                          <small><?php echo ltrim($documentRow['edocs_conduct_cert'],"uploads/documents/"); ?></small>
                          <p id="bonaid"></p>
                        </div>
                      </div>
                      <h4>Salary</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Gross Salary :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="grossSal" name="grossSal" class="gSal" value="<?php echo $salaryRow['sal_gross_salary']; ?>">

                          <!-- <input type="hidden" class="form-control" name="updatesalary" id="updatesalary" value="no"> -->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Tax Slab :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="taxSlab" name="taxSlab" value="<?php echo $salaryRow['sal_tax_slab']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Personal Relief :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="perRelief" onchange="calSal();" name="perRelief" value="<?php echo $salaryRow['sal_personal_relief']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Insurance Relief :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="insReleif" onchange="calSal();" name="insReleif" value="<?php echo $salaryRow['sal_insurance_relief']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Allowable Pension Fund Contribution :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="apfundContri" onchange="calSal();" name="apfundContri" value="<?php echo $salaryRow['sal_all_pension_fund']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Allowable NSSF Contribution :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="hosp" name="hosp" value="<?php echo $salaryRow['sal_all_hosp_contri']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Owner Occupier Interest :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="oInts" onchange="calSal();" name="oInts" value="<?php echo $salaryRow['sal_occupier_interest']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">NHIF :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="nhif" name="nhif" value="<?php echo $salaryRow['sal_nhif']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Net Salary :</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="netsal" name="netsal" readonly="readonly" style="border:none;box-shadow:none;background-color:#fff;" value="<?php echo $salaryRow['sal_net_salary']; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                          <button type="button" onclick = "getFormData();" id="submit" name="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </form>
                 </div>
              <div class="msg">
                  <p>sorry these data already exists.kindky update from drivers page.</p>
              </div>
          </div>
      </div>
    </div>
  </div>
  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script src="js/typeahead.min.js"></script>
<script src="../js/datepicker.js"></script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function () {
    $(".msg").hide();

      $('#grossSal').change(function () {
        var value = $("#grossSal").val();
        var comp = $("#compId").val();
       
        //console.log("Hi this is new box: "+value);
        if(value!=null && value!=0)
        {
          $.ajax({
            method:"GET",
            url: "functions/searchSalary.php?addSalary="+value+"&compID="+comp
          })
          .done(function(dataa) {
              //$( this ).addClass( "done" );
              console.log(dataa);
              dataa = JSON.parse(dataa);

              var pec = dataa.logis_tax['tax_percentage'];
              var totalpec = (value*pec)/100;
       
              var nssf = dataa.logis_nssf['nssf_rate'];
              var nhif = dataa.logis_nhif['nhif_deduction'];

              var addpec_nssf = parseInt(totalpec) + parseInt(nssf);
              var addnhif = parseInt(addpec_nssf) + parseInt(nhif);

              var subgross = value - addnhif;

              if(dataa.logis_tax==false && dataa.logis_nhif==false && dataa.logis_nssf==false)
              {
                console.log("The TaxSlab is new "+dataa);
                 $("#taxSlab").val('').attr('readonly',false);
                 $("#nhif").val('').attr('readonly',false);
                 $("#hosp").val('').attr('readonly',false);
              }
              else if(dataa.logis_tax==false && dataa.logis_nhif==false && dataa.logis_nssf!=false)
              {
                console.log("The TaxSlab is new "+dataa);
                 $("#taxSlab").val('').attr('readonly',false);
                 $("#nhif").val('').attr('readonly',false);
                 $("#hosp").val('').attr('readonly',false);
              }
              else{
                    $("#taxSlab").val(totalpec).attr('readonly',true);
                    $("#nhif").val(dataa.logis_nhif['nhif_deduction']).attr('readonly',true);
                    $("#hosp").val(dataa.logis_nssf['nssf_rate']).attr('readonly',true);
                    $("#netsal").val(subgross);
                   //alert("Input driver values"+data.drv_NSN);
              }
              console.log("value of data "+dataa);
          });
        }
        else{
          alert("Please enter the Gross Salary to proceed");
        }
      });

      $('#empId').change(function () {
        var value = $("#empId").val();      

        if(value!=null)
        {
          $.ajax({
            method:"GET",
            url: "functions/searchDriver.php?chkempId="+value
          })
          .done(function(empdata) {
              empdata = JSON.parse(empdata);
              var e = empdata.empdt_empID;

              if(empdata!=false)
              {
                console.log("The Employee ID is Check"+empdata);
                 alert("Employee ID is aleardy exist Please enter a new Id.");
                 $("#empId").val('');
              }
              console.log("value of data="+empdata);
          });
        }
        else{
          alert("Please enter the Employee Id to proceed");
        }
      });

    var url = window.location.href;
    var msg = url.substring(url.indexOf('?')+5);
    console.log(msg);
      if(msg=="success"){
        $("#alertBox").removeClass("hidden");
        $("#alertBox").addClass("alert-success");
        $("#alertBox").html("Driver Details submitted succesfully");
        window.location.href ='drivers.php';
      }
      else if(msg=="error"){  
        $("#alertBox").removeClass("hidden");
        $("#alertBox").addClass("alert-danger");
        $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
      }
      else{
      	$("#alertBox").addClass("hidden");
        $("#alertBox").removeClass("alert-success");
        $("#alertBox").removeClass("alert-danger");
        $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
      }

      $('#startDate').datepicker({
          format: "yyyy/mm/dd"
      }).on('changeDate', function(ev){
       // alert("working");
         var date1 = $('#startDate').datepicker('getDate');
         var date2 = date1.setDate(date1.getDate()+364)
        $('#endDate').datepicker('setDate',date1);
      });

      $('#endDate').datepicker({
          format: "yyyy/mm/dd",
      });

      $('#issueDate').datepicker({
          format: "yyyy/mm/dd" 
      }).on('changeDate', function(ev){
        $("#expiryDate").val("");
      }); 

      $('#expiryDate').datepicker({
          format: "yyyy/mm/dd"
      }).on('changeDate', function(ev){
       // alert("working");
         var date1 = $('#issueDate').datepicker('getDate');
         var date2 = $('#expiryDate').datepicker('getDate');
         if(date1 > date2)
         {
          alert("Date cannot be less than issue Date");
          $('#expiryDate').datepicker('setDate',date1);
         }
        
      });
  });

function calSal(){
  var netsal;

  var grossSal = $("#grossSal").val();
  var tax = $("#taxSlab").val();
  var nssf = $("#hosp").val();
  var nhif = $("#nhif").val();  

  var rel = $("#perRelief").val();
  if(rel == ""){
    rel = 0;
  }

  var ins = $("#insReleif").val();
  if(ins == ""){
    ins = 0;
  }

  var fund = $("#apfundContri").val();
  if(fund == ""){
    fund = 0;
  }

  var oInts = $("#oInts").val();
  if(oInts == ""){
    oInts = 0;
  }
 
  var total = parseInt(tax) + parseInt(nhif) + parseInt(nssf) + parseInt(rel) + parseInt(ins) + parseInt(fund) + parseInt(oInts);

 var netsal = grossSal - total;
 $("#netsal").val(netsal);
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function() {
  $('#nssf').keyup(function(e) {
    var txtVal = $(this).val();
    txtVal2 = txtVal;
    $('#nssf1').val(txtVal);
  });

   $('#nhifNo').keyup(function(e) {
    var txtVal = $(this).val();
    txtVal2 = txtVal;
    $('#nhifNo1').val(txtVal);
  });
});
</script>

<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script> 
<script>
$( "#edit_form" ).validate({
  rules: {
    email: {
      email: true
    },
   }
});
</script>


<script>
// function getFormData(){

// var a = $("#bonafideCert").val();
// alert(a);

//   $.ajax({
//     type : "post",
//     url : "functions/edit-drivers-forms.php",
//     data : $("#edit_form").serialize()+"&bonafideCert="+bonafideCert,

//     success:function(msg){
//       alert(msg);
//     }
//   })
// }

 function getFormData(){
    var file_data = $("#bonafideCert").prop("files")[0];   // Getting the properties of file from file field
    alert(file_data);

    var data = new FormData();                  // Creating object of FormData class
    data.append("file",file_data)               // Appending parameter named file with properties of file_field to form_data
    
    $.ajax({
      url: "functions/edit-drivers-forms.php",
      dataType: 'script',
      cache: false,
      contentType: false,
      processData: false,
      data: data,                         // Setting the data attribute of ajax with file_data
      type: 'post',

      success : function(msg){
        alert(msg);
      }
  })
}

</script>

<?php include'footer.php'; ?>