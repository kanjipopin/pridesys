<?php include'header.php'; ?>

      <div class="col-sm-9 mrgnTOP15">
          <div class="drivers-detail payrollDRIVER-page">
              <div class="heading">
                <h4>Payroll for month of Jan 2017</h4>

                <div class="filters">
                      <div class="form-inline">
                        <!-- <button type="button" class="btn btn-default">Add</button> -->
                        <a href="payroll.php" type="button" class="btn btn-default">Back</a>
                        <a href="#" type="button" class="btn btn-default">Download PDF</a>
                        <a href="#" type="button" class="btn btn-default">Create Salary Slip</a>
                      </div>
                </div>

              </div>

              <div class="table-responsive detail-table">
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>Driver ID</th>
                          <th>Driver Name</th>
                          <th>Gross Salary (KSh)</th>
                          <th>Paye (KSh)</th>
                          <th>NSSF (KSh)</th>
                          <th>NHIF (KSh)</th>
                          <th>Leaves</th>
                          <th>Advance (KSh)</th>
                          <th>Net Salary (KSh)</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>DID321</td>
                          <td>James Vance</td>
                          <td>10000</td>
                          <td>500</td>
                          <td>500</td>
                          <td>400</td>
                          <td>1</td>
                          <td>00</td>
                          <td>8600</td>
                          <td><img src="images/edit.png"></td>
                      </tr>
                      <tr>
                          <td>DID322</td>
                          <td>Max Carr</td>
                          <td>9000</td>
                          <td>500</td>
                          <td>500</td>
                          <td>400</td>
                          <td>3</td>
                          <td>500</td>
                          <td>7100</td>
                          <td><img src="images/edit.png"></td>
                      </tr>
                      <tr>
                          <td>DID323</td>
                          <td>Dan Young</td>
                          <td>11000</td>
                          <td>600</td>
                          <td>400</td>
                          <td>500</td>
                          <td>0</td>
                          <td>00</td>
                          <td>9500</td>
                          <td><img src="images/edit.png"></td>
                      </tr>
                      <tr>
                          <td>DID324</td>
                          <td>Brandon Turner</td>
                          <td>10000</td>
                          <td>600</td>
                          <td>400</td>
                          <td>500</td>
                          <td>1</td>
                          <td>300</td>
                          <td>8200</td>
                          <td><img src="images/edit.png"></td>
                      </tr>
                  </tbody>
              </table>
              </div>
          </div>
      </div>
    </div>
  </div>
  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});
</script>


<?php include'footer.php';?>