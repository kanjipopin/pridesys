<?php include'header.php'; ?>

      
          <div class="wht-bg">
              <div class="heading heading-tabs">
                <ul class="nav nav-tabs ">
            				<li class="active">
            					<a href="#tab_default_1" data-toggle="tab">Tax Slab</a>
            				</li>
            				<li>
            					<a href="#tab_default_2" data-toggle="tab">NHIF and NSSF</a>
            				</li>
                    <li>
                      <a href="#tab_default_3" data-toggle="tab">Current Departments</a>
                    </li>
                    <li>
                      <a href="#tab_default_4" data-toggle="tab">Vehicle Class</a>
                    </li>
            			</ul>
              </div>
              <div class="master-page">
                	<div class="tab-content">
      						  <div class="tab-pane active" id="tab_default_1">
                        <div class="table-responsive detail-table">
                         
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Monthly Taxable Pay (Ksh)</th>
                                        <th>Annual Taxable Pay (Ksh)</th>
                                        <th>Rate of Tax (%)</th>
                                    </tr>
                                </thead>
                                <tbody id="showtax">
                                  <?php 
                                    foreach($resultsss as $fddd){
                                  ?>
                                    <tr>
                                        <td id="tax_amt"><?php echo $fddd['tax_mamt_from']." - ".$fddd['tax_mamt_to']; ?></td>
                                        <td id="tax_mmt"><?php echo $fddd['tax_aamt_from']." - ".$fddd['tax_aamt_to']; ?></td>
                                        <td id="tax_pec"><?php echo $fddd['tax_percentage']; ?></td>
                                     </tr>
                                  <?php } ?> 
                                </tbody>
                            </table>
                        </div>
      						  </div>

      						  <div class="tab-pane NHIF-tab" id="tab_default_2">
                        <div class="row">
                        <div class="table-responsive detail-table col-sm-6">
                            <a href="#" type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addNHIF" data-backdrop="static">Add</a><br/><br/>
                            <!-- addNHIF Modal Popup -->
                              <div class="modal" id="addNHIF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Add NHIF</h4>
                                    </div>
                                    <form class="form-horizontal" id="adddNHIF" name="adddNHIF" action="functions/addmaster.php" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Gross Pay</label>
                                        <div class="col-sm-8">
                                          <input type="hidden" class="form-control" name="hideNHIF" id="hideNHIF" value="hideNHIF">

                                          <input type="text" class="form-control" id="gross_pay" name="gross_pay">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Deduction </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="deduction" name="deduction">

                                          <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id = $_SESSION['comp_id']; ?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="addNHIFF" id="addNHIFF" class="btn btn-default" onclick="getNHIF();">Submit</button>
                                      </div>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            <!-- End addNHIF Modal Popup -->
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="45%">Gross Pay (Ksh)</th>
                                        <th width="35%">Deduction (Ksh)</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="showNHIF">
                                <?php 
                                $ii = 0;
                                foreach($resultss as $fdd) {
                                $ii++;
                                ?>
                                    <tr>
                                        <td id="nhif_amt<?php echo $ii;?>"><?php echo $fdd['nhif_amt_from']." - ".$fdd['nhif_amt_to'];?></td>
                                         <input type="hidden" class="form-control" name="nhif_id<?php echo $ii;?>" id="nhif_id<?php echo $ii;?>" value="<?php echo $fdd['nhif_id']; ?>"> 
                                        <td id="nhif_dction<?php echo $ii;?>"><?php echo $fdd['nhif_deduction'];?></td>
                                        <td>
                                          <a href="#" data-toggle="modal" onclick="geteditNHIF(<?php echo $ii;?>);" data-target="#editNHIF" data-backdrop="static" style="display: inline-block;">
                                              <img src="images/edit.png">
                                          </a>
                                          <a href="#" onclick="getdeleteNHIF(<?php echo $ii;?>);" data-target="#deleteNHIF" style="display: inline-block;">
                                              <img src="images/remove.png">
                                          </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                          </div>

                          <div class="modal" id="editNHIF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update NHIF</h4>
                                    </div>
                                    <form class="form-horizontal" id="editNHIFF" name="editNHIFF" action="functions/addmaster.php" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Gross Pay</label>
                                        <div class="col-sm-8">
                                          <input type="hidden" class="form-control" name="hideeditNHIF" id="hideeditNHIF" value="yes">

                                          <input type="hidden" class="form-control" name="NHIF_id" id="NHIF_id">

                                          <input type="text" class="form-control" id="NHIF_gross_pay" name="NHIF_gross_pay">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Deduction </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="NHIF_Deduction" name="NHIF_Deduction">

                                          <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id = $_SESSION['comp_id']; ?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="edittNHIF" id="edittNHIF" class="btn btn-default" onclick="geteditmodalNHIF();">Submit</button>
                                      </div>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            <!-- End addNHIF Modal Popup -->

                          <div class="nssf-block col-sm-6">
                              <form class="form-inline" name="editnssf" id="editnssf" method="post">
                                <div class="form-group">
                                  <label for="exampleInputEmail2">NSSF : </label>
                                  <p style="display: inline-block;"><?php echo $fnssfrow['nssf_rate']; ?></p>
                                </div>
                              </form>
                          </div>
                        </div>
      						  </div>

                    <div class="tab-pane" id="tab_default_3">
                        <div class="table-responsive detail-table">
                            <a href="#" type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addDept" data-backdrop="static">Add</a><br/><br/>
                            <!-- addTaxSlab Modal Popup -->
                              <div class="modal" id="addDept" tabindex="-1" role="dialog" aria-labelledby="addDeptModal">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="addDeptModal">Add Department</h4>
                                    </div>
                                    
                                    <form class="form-horizontal" class="addDeptt" id="addDeptt" name="addDeptt" action="functions/addmaster.php" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Department Name</label>
                                        <div class="col-sm-6">
                                          <input type="hidden" class="form-control" name="hideDept" id="hideDept" value="hideDept">

                                          <input type="text" class="form-control" id="Dept" name="Dept" placeholder="Department">

                                          <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id = $_SESSION['comp_id']; ?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="adddepart" id="adddepart" class="btn btn-default" onclick="getDept();">Submit</button>
                                      </div>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            <!-- End addTaxSlab Modal Popup -->
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Department Name</th>  
                                        <th>Action</th>                                     
                                    </tr>
                                </thead>
                                <tbody id="showDept">
                                <?php 
                                $i = 0;
                                foreach($results as $fd) { 
                                $i++;
                                ?>
                                <tr>
                                    <td id="deptName<?php echo $i;?>"><?php echo $fd['dept_name'];?></td>
                                     <input type="hidden" class="form-control" name="Deptt_id<?php echo $i;?>" id="Deptt_id<?php echo $i;?>" value="<?php echo $fd['dept_id']; ?>"> 
                                    <td>
                                        <a href="#" data-toggle="modal" onclick="geteditDept(<?php echo $i;?>);" data-target="#editDept" data-backdrop="static" style="display: inline-block;">
                                          <img src="images/edit.png">
                                        </a>
                                        <a href="#" onclick="getdeleteDept(<?php echo $i;?>);" data-target="#deleteDept" style="display: inline-block;">
                                          <img src="images/remove.png">
                                        </a>
                                    </td>
                                </tr>
                            <!-- End Edit Dept Modal Popup -->
                                <?php } ?>  
                                </tbody>
                            </table>
                          </div>
                          <div class="modal" id="editDept" tabindex="-1" role="dialog" aria-labelledby="addDeptModal">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="addDeptModal">Update Department</h4>
                                    </div>
                                    
                                    <form class="form-horizontal" id="editDeptt" name="editDeptt" action="functions/addmaster.php" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Department Name</label>
                                        <div class="col-sm-6">
                                          <input type="hidden" class="form-control" name="hideeditDept" id="hideeditDept" value="yes">

                                          <input type="hidden" class="form-control" name="Dept_id" id="Dept_id">

                                          <input type="text" class="form-control" id="Deptt" name="Deptt" placeholder="Department">

                                          <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id = $_SESSION['comp_id']; ?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="editdepart" id="editdepart" class="btn btn-default" onclick="geteditmodalDept();">Submit</button>
                                      </div>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                    </div>

                      <div class="tab-pane" id="tab_default_4">
                        <div class="table-responsive detail-table">
                            <a href="#" type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addvehicle" data-backdrop="static">Add</a><br/><br/>
                            <!-- addvehicle Modal Popup -->
                            <div class="modal" id="addvehicle" tabindex="-1" role="dialog" aria-labelledby="addvehicleModal">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="addvehicleModal">Vehicle Class</h4>
                                  </div>
                                  
                                  <form class="form-horizontal" class="addvehi" id="addvehi" name="addvehi" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Vehicle Name</label>
                                        <div class="col-sm-6">
                                         <input type="hidden" class="form-control" name="hidevehicle" id="hidevehicle" value="hidevehicle">

                                         <input type="text" class="form-control" id="vehicle" name="vehicle" placeholder="Vehicle">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="adddvehicle" id="adddvehicle" class="btn btn-default" onclick="getVehicle();">Submit</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!-- End addTaxSlab Modal Popup -->
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Vehicle Name</th>  
                                        <th>Action</th>                                     
                                    </tr>
                                </thead>
                                <tbody id="showVehicle">
                                <?php 
                                $i = 0;
                                foreach($vehiclerow as $fd){
                                $i++;
                                ?>
                                <tr>
                                    <td id="vehicleName<?php echo $i;?>"><?php echo $fd['logis_vehicle_name'];?></td>
                                     <input type="hidden" class="form-control" name="vehicle_id<?php echo $i;?>" id="vehicle_id<?php echo $i;?>" value="<?php echo $fd['logis_vehicle_id']; ?>"> 
                                    <td>
                                        <a href="#" data-toggle="modal" onclick="geteditvehicle(<?php echo $i;?>);" data-target="#editvehicle" data-backdrop="static" style="display: inline-block;">
                                          <img src="images/edit.png">
                                        </a>
                                        <a href="#" onclick="getdeleteVehicle(<?php echo $i;?>);" data-target="#deletevehicle" style="display: inline-block;">
                                          <img src="images/remove.png">
                                        </a>
                                    </td>
                                </tr>
                            <!-- End Edit Dept Modal Popup -->
                                <?php } ?>  
                                </tbody>
                            </table>
                          </div>
                            <div class="modal" id="editvehicle" tabindex="-1" role="dialog" aria-labelledby="addvehicleModal">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="addvehicleModal">Update Department</h4>
                                    </div>
                                    
                                    <form class="form-horizontal" id="editvehicles" name="editvehicles" action="functions/addmaster.php" method="POST">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Department Name</label>
                                        <div class="col-sm-6">
                                          <input type="hidden" class="form-control" name="hideeditvehicle" id="hideeditvehicle" value="hideeditvehicle">

                                          <input type="hidden" class="form-control" name="evehicle_id" id="evehicle_id">

                                          <input type="text" class="form-control" id="evehicle" name="evehicle" placeholder="Vehicle">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <div>
                                        <button type="button" name="editvehic" id="editvehic" class="btn btn-default" onclick="geteditmodalVehicle();">Submit</button>
                                      </div>
                                    </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                          </div>
      					     </div>
              </div>
          </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="js/jquery.popupoverlay.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script>
$(document).ready(function () {
  $('#addDeptForm').submit(function(){
  });

    $('#fade').popup({
      transition: 'all 0.3s',
      scrolllock: true
    });
});

function getDept(){
  var validate_Dept = $("#Dept").val();
  if(validate_Dept == ""){
    alert("Field is required.");
  }

  else if(validate_Dept != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#addDeptt').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showDept").html(msg);
        $('#addDept').modal('hide');
        $('#addDept').find('#Dept').val('');
      },
    });
  }
}

  function getNHIF(){
    var valdate_grosspay = $("#gross_pay").val();
    var valdate_deduction = $("#deduction").val();
    if(valdate_grosspay == "" || valdate_deduction == ""){
       alert("All Field is required.");
    }

    else if(valdate_grosspay != "" || valdate_deduction != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#adddNHIF').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showNHIF").html(msg);
        $('#addNHIF').modal('hide');
        $('#addNHIF').find('#gross_pay').val('');
        $('#addNHIF').find('#deduction').val('');
      },
    });
  }
 }

  function getnssf(){
    var validate_nssf = $("#nssf").val();
    if(validate_nssf == ""){
      alert("NSSF Field is required");
    }

    else if(validate_nssf != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#editnssf').serialize(),
      success: function(msg){
       // alert(msg);
        $("#nssf").html(msg);
        alert("NSSF rate upadate successfully.")
      },
    });
  }
}

function getTax(){
    var validate_monthly = $("#monthly_taxable_pay").val();
    var validate_annual = $("#annual_taxable_pay").val();
    var validate_rate = $("#rate_of_tax").val();
    if(validate_monthly == "" || validate_annual == "" || validate_rate == ""){
        alert("All Field is required");
    }

    else if(validate_monthly != "" || validate_annual != "" || validate_rate != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#addTax').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showtax").html(msg);
        $('#addTaxSlab').modal('hide');
        $('#addTaxSlab').find('#monthly_taxable_pay').val('');
        $('#addTaxSlab').find('#annual_taxable_pay').val('');
        $('#addTaxSlab').find('#rate_of_tax').val('');
      },
    });
  }
}


function getVehicle(){
  var validate_vehicle = $("#vehicle").val();
  if(validate_vehicle == ""){
    alert("Field is required.");
  }

  else if(validate_vehicle != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#addvehi').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showVehicle").html(msg);
        $('#addvehicle').modal('hide');
        $('#addvehicle').find('#vehicle').val('');
      },
    });
  }
}

// EDIT Department data using modal popup
  function geteditDept(modalId){

   var id = modalId;
   var i = $("#deptName"+id).text();
   var j = $("#Deptt_id"+id).val();
   // alert(i);
   // alert(j);
   
     $("#Deptt").val(i);
     $("#Dept_id").val(j);
     $("#editDept").show();
  }

  function geteditmodalDept(){
  var validate_Dept = $("#Deptt").val();
  if(validate_Dept == ""){
    alert("Field is required.");
  }

  else if(validate_Dept != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#editDeptt').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showDept").html(msg);
        $('#editDept').modal('hide');
      },
    });
  }
}

// EDIT NHIF data using modal popup
  function geteditNHIF(modalNHIFId){

   var id = modalNHIFId;
   var i = $("#nhif_amt"+id).text();
   var j = $("#nhif_dction"+id).text();
   var k = $("#nhif_id"+id).val();
   // alert(i);
   // alert(j);
   // alert(k);
      
     $("#NHIF_gross_pay").val(i);
     $("#NHIF_id").val(k);
     $("#NHIF_Deduction").val(j);
     $("#editNHIF").show();
  }

  function geteditmodalNHIF(){
    var valdate_grosspay = $("#NHIF_gross_pay").val();
    var valdate_deduction = $("#NHIF_Deduction").val();
    if(valdate_grosspay == "" || valdate_deduction == ""){
       alert("All Field is required.");
    }

    else if(valdate_grosspay != "" || valdate_deduction != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#editNHIFF').serialize(),
      success: function(msg){
        // alert(msg);
        $("#showNHIF").html(msg);
        $('#editNHIF').modal('hide');
      },
    });
  }
}

  // EDIT Tax data using modal popup
  function getedittax(modalTAXId){

   var id = modalTAXId;
   var i = $("#tax_amt"+id).text();
   var j = $("#tax_mmt"+id).text();
   var k = $("#tax_pec"+id).text();
   var l = $("#tax_id"+id).val();
      
     $("#monthly_tax").val(i);
     $("#annual_tax").val(j);
     $("#rate_tax").val(k);
     $("#tax_id").val(l);
     $("#edittax").show();
  }

function geteditmodaltax(){
    var validate_monthly = $("#monthly_tax").val();
    var validate_annual = $("#annual_tax").val();
    var validate_rate = $("#rate_tax").val();
    if(validate_monthly == "" || validate_annual == "" || validate_rate == ""){
        alert("All Field is required");
    }

    else if(validate_monthly != "" || validate_annual != "" || validate_rate != ""){
    $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: $('#editTaxslab').serialize(),
      success: function(msg){
       // alert(msg);
        $("#showtax").html(msg);
        $('#edittax').modal('hide');
      },
    });
  }
}


// EDIT vehicle data using modal popup
function geteditvehicle(modalVehicle){

   var id = modalVehicle;
   var i = $("#vehicle_id"+id).val();
   var j = $("#vehicleName"+id).text();
      
    // alert(i);
    // alert(j);

  $("#evehicle_id").val(i);
  $("#evehicle").val(j);
}

  function geteditmodalVehicle(){
      var vehicleName = $("#vehicleName").val();

      if(vehicleName == ""){
          alert("All Field is required");
      }

      else if(vehicleName != ""){
      $.ajax({
        type: "POST",
        url:"functions/ajaxaddmaster.php",
        data: $('#editvehicles').serialize(),
        success: function(msg){
          // alert(msg);
          $("#showVehicle").html(msg);
          $('#editvehicle').modal('hide');
        },
      });
    }
  }


////////////////////
//  DELETE DATA  //
///////////////////
function getdeleteDept(modalId){
  var confirmDelete = confirm("Are you sure want to delete?");

  if(confirmDelete == true){
  var id = modalId;
  var idd = $("#Deptt_id"+id).val();
  
  $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: {"type":"deleteDept","idd":idd},
      success: function(msg){
      // alert(msg);
      $("#showDept").html(msg);
      },
    });
  }
}

function getdeleteNHIF(modalId){
  var confirmDelete = confirm("Are you sure want to delete?");

  if(confirmDelete == true){
  var id = modalId;
  var idd = $("#nhif_id"+id).val();
  
  $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: {"type":"deleteNHIF","idd":idd},
      success: function(msg){
      // alert(msg);
      $("#showNHIF").html(msg);
      },
    });
  }
}

function getdeletetax(modalId){
  var confirmDelete = confirm("Are you sure want to delete?");

  if(confirmDelete == true){
  var id = modalId;
  var idd = $("#tax_id"+id).val();
  
  $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: {"type":"deletetax","idd":idd},
      success: function(msg){
      // alert(msg);
      $("#showtax").html(msg);
      },
    });
  }
}

function getdeleteVehicle(modalId){
  var confirmDelete = confirm("Are you sure want to delete?");

  if(confirmDelete == true){
  var id = modalId;
  var idd = $("#vehicle_id"+id).val();
  
  $.ajax({
      type: "POST",
      url:"functions/ajaxaddmaster.php",
      data: {"type":"deletevehicle","idd":idd},
      success: function(msg){
      // alert(msg);
      $("#showVehicle").html(msg);
      },
    });
  }
}
</script>
<?php include'footer.php'; ?>