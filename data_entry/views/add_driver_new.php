<?php
	session_start();
	require_once('../config/db.php');
	@$Email = $_SESSION['Email'];

	if(isset($_POST['submit'])){

		// Driver Details
		$compId = $_POST['compId'];
		$fname = $_POST['fname'];
		$mname = $_POST['mname'];
		$lname = $_POST['lname'];
		$gender = $_POST['gender'];
		$NSN = $_POST['NSN'];
		$implodeVehicle = implode(",",$_POST['vehiclelist']);
		$vehicle = isset($implodeVehicle) ? $implodeVehicle : "";
		$insuranceType = isset($_POST['insuranceType']) ? $_POST['insuranceType'] : "";
		$insuranceRefNum = isset($_POST['insuranceRefNum']) ? $_POST['insuranceRefNum'] : "";
		$insuranceExpDate = isset($_POST['insuranceExpDate']) ? $_POST['insuranceExpDate'] : "";
		$dlNum = $_POST['dlNum'];
		$contactNo = $_POST['contact1'];
		$altContactNo = $_POST['contact2'];
		$address = $_POST['address1'];
		$address2= $_POST['address2'];
		$city = isset($_POST['city']) ? $_POST['city'] : "";
		$pobox = $_POST['pobox'];
		$postalcode = $_POST['postalcode'];
		$email = $_POST['email'];
		$grossSal = $_POST['grossSal'];
		$CreatedDate = date("Y-m-d");

		$issueDate = date("Y/m/d",strtotime($_POST['issueDate']));
		if($issueDate == ""){
			$issueDate = "NULL";
		}
		elseif($issueDate != ""){
			$issueDate;
		}

		$expiryDate = date("Y/m/d",strtotime($_POST['expiryDate']));
		if($expiryDate == ""){
			$expiryDate = "NULL";
		}
		elseif($expiryDate != ""){
			$expiryDate;
		}

		//Employment Details
		$empId = $_POST['empId'];
		$deptId = $_POST['deptId'];
		$leave = $_POST['leave'];

		$startDate = date("Y/m/d",strtotime($_POST['startDate']));
		if($startDate == ""){
			$startDate = "NULL";
		}
		elseif($startDate != ""){
			$startDate;
		}

		$endDate = date("Y/m/d",strtotime($_POST['endDate']));
		if($endDate == ""){
			$endDate = "NULL";
		}
		elseif($endDate != ""){
			$endDate;
		}

		//Kin data
		$kinname = $_POST['kinname'];
		$kinnsn = $_POST['kinnsn'];
		$kinadd = $_POST['kinadd'];
		$kincontact = $_POST['kincontact'];
		$kinrelation = $_POST['kinrelation'];
		// $emrg_contact = $_POST['emrg_contact'];

		//Employee data
		$qualification = $_POST['qualification'];
		$idCard = $_POST['idCard'];
		$pin = $_POST['pin'];
		$nssf = $_POST['nssf'];
		$nhifNo = $_POST['nhifNo'];


		$uploaddir = '../uploads/company'.$_POST["compId"];
		if(!file_exists($uploaddir)){
			mkdir($uploaddir,0755);
		}
		$imgfolder = $uploaddir."/img";
		if(!file_exists($imgfolder)){
			mkdir($imgfolder,0755);
		}
		$imgFile="";
		if($_FILES['driverImg']['name']){
			$type = substr($_FILES['driverImg']['name'],strrpos($_FILES['driverImg']['name'],'.')+1);

			$imgName = date("d-m-Y")."-".time()."image.".$type;
			$uploadImg = $imgfolder."/".$imgName;

			if (move_uploaded_file($_FILES['driverImg']['tmp_name'], $uploadImg)){
				$imgFile = $imgfolder."/".$imgName;
			} else {
				$err='er-up-img';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}


		//Check & upload CV
		if(!file_exists($uploaddir)){
			mkdir($uploaddir,0755);
		}
		$docfolder = $uploaddir."/document";
		if(!file_exists($docfolder)){
			mkdir($docfolder,0755);
		}

		//Check & upload driving licence
		$driverLicence = "";
		if($_FILES['driverLicence']['name']){
			$type = substr($_FILES['driverLicence']['name'],strrpos($_FILES['driverLicence']['name'],'.')+1);

			$uploadDL = date("d-m-Y")."-".time()."driverLicence.".$type;
			$uploadDoc = $docfolder."/".$uploadDL;

			if (move_uploaded_file($_FILES['driverLicence']['tmp_name'], $uploadDoc))
			{		
				$driverLicence = $docfolder."/".$uploadDL;
			} else {
				$err='er-up-dl';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		$cv="";
		if($_FILES['cv']['name']){
			$type = substr($_FILES['cv']['name'],strrpos($_FILES['cv']['name'],'.')+1);

			$uploadCV = date("d-m-Y")."-".time()."cv.".$type;
			$uploadDoc = $docfolder."/".$uploadCV;

			if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadDoc)) 
			{
				$cv =  $docfolder."/".$uploadCV;
			} else {
				$err='er-up-cv';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload application letter
		$appLetter="";
		if($_FILES['appLetter']['name']){
			$type = substr($_FILES['appLetter']['name'],strrpos($_FILES['appLetter']['name'],'.')+1);

			$uploadAppLetter = date("d-m-Y")."-".time()."appLetter.".$type;
			$uploadDoc = $docfolder."/".$uploadAppLetter;

			if (move_uploaded_file($_FILES['appLetter']['tmp_name'], $uploadDoc)) 
			{
				$appLetter = $docfolder."/".$uploadAppLetter;
			} else {
				$err='er-up-al';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload qualification documents
		$qualificationD="";
		if($_FILES['qualificationD']['name']){
			$type = substr($_FILES['qualificationD']['name'],strrpos($_FILES['qualificationD']['name'],'.')+1);

			$uploadQ = date("d-m-Y")."-".time()."qualificationD.".$type;
			$uploadDoc = $docfolder."/".$uploadQ;

			if (move_uploaded_file($_FILES['qualificationD']['tmp_name'], $uploadDoc))
			{		
				$qualificationD = $docfolder."/".$uploadQ;
			} else {
				$err='er-up-qa';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload ad card certificate
		$idCardD="";
		if($_FILES['idCardD']['name']){
			$type = substr($_FILES['idCardD']['name'],strrpos($_FILES['idCardD']['name'],'.')+1);

			$uploadId = date("d-m-Y")."-".time()."idCardD.".$type;
			$uploadDoc = $docfolder."/".$uploadId;

			if (move_uploaded_file($_FILES['idCardD']['tmp_name'], $uploadDoc))
			{			
				$idCardD = $docfolder."/".$uploadId;
			} else {
				$err='er-up-id';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload pin certificate
		$pinD="";
		if($_FILES['pinD']['name']){
			$type = substr($_FILES['pinD']['name'],strrpos($_FILES['pinD']['name'],'.')+1);

			$uploadPin = date("d-m-Y")."-".time()."pinD.".$type;
			$uploadDoc = $docfolder."/".$uploadPin;

			if(move_uploaded_file($_FILES['pinD']['tmp_name'], $uploadDoc)) 
			{
				$pinD = $docfolder."/".$uploadPin;
			} else {
				$err='er-up-pin';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload nssf document
		$nssfD="";
		if($_FILES['nssfD']['name']){
			$type = substr($_FILES['nssfD']['name'],strrpos($_FILES['nssfD']['name'],'.')+1);

			$uploadNSSF = date("d-m-Y")."-".time()."nssfD.".$type;
			$uploadDoc = $docfolder."/".$uploadNSSF;

			if(move_uploaded_file($_FILES['nssfD']['tmp_name'], $uploadDoc)) 
			{			
				$nssfD = $docfolder."/".$uploadNSSF;
			} else {
				$err='er-up-ns';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload nhif
		$nhifD="";
		if($_FILES['nhifD']['name']){
			$type = substr($_FILES['nhifD']['name'],strrpos($_FILES['nhifD']['name'],'.')+1);

			$uploadNH = date("d-m-Y")."-".time()."nhifD.".$type;
			$uploadDoc = $docfolder."/".$uploadNH;

			if(move_uploaded_file($_FILES['nhifD']['tmp_name'], $uploadDoc)) 
			{			
				$nhifD = $docfolder."/".$uploadNH;
			} else {
				$err='er-up-NH';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}

		//Check & upload character certificate
		$bonafideCert = "";
		if($_FILES['bonafideCert']['name']){
			$type = substr($_FILES['bonafideCert']['name'],strrpos($_FILES['bonafideCert']['name'],'.')+1);

			$uploadBC = date("d-m-Y")."-".time()."bonafideCert.".$type;
			$uploadDoc = $docfolder."/".$uploadBC;

			if(move_uploaded_file($_FILES['bonafideCert']['tmp_name'], $uploadDoc)){
				$bonafideCert = $docfolder."/".$uploadBC;
			} else {
				$err='er-up-bonCert';
				$connection->redirect("../add-drivers.php?err=".$err);
			}
		}


		$driverCols = array("drv_NSN"=>$NSN,"drv_fname"=>$fname,"drv_lname"=>$lname,"gender"=>$gender,"drv_mname"=>$mname,"drv_img"=>$imgFile,"drv_contact_no"=>$contactNo,"drv_alt_contact_no"=>$altContactNo, "drv_address"=>$address,"drv_address2"=>$address2, "drv_city"=>$city,"drv_pobox"=>$pobox,"drv_postal"=>$postalcode,"drv_email"=>$email,"logis_drivers_vehicle_class"=>$vehicle,"drv_driving_license_no"=>$dlNum,"drv_curr_comp_id"=>$compId,"drv_isActive"=>"Active","gross_salary"=>$grossSal,"drv_dl_issue_date"=>$issueDate,"drv_dl_expiry_date"=>$expiryDate,"drv_created_on"=>$CreatedDate,"driver_work_status"=>"working","insurance_type"=>$insuranceType,"insurance_ref_number"=>$insuranceRefNum,"insurance_exp_date"=>$insuranceExpDate,'delete_flag'=>'No');
		$driverTable = "logis_drivers_master";
		$insertDriver = $connection->InsertQuery($driverTable,$driverCols);


		//Insert Data into Kin table
		$kinCols = array("drivers_kin_name"=>$kinname,"drv_nsnId"=>$NSN,"drv_comp_id"=>$compId,"drivers_kin_nsn_id"=>$kinnsn,"drivers_kin_contact"=>$kincontact,"drivers_kin_address"=>$kinadd,"drivers_kin_relation"=>$kinrelation);
		$kinTable = "logis_drivers_kin";
		$insertKin = $connection->InsertQuery($kinTable,$kinCols);


		//Insert data in Employee Details table
		$empCols = array("empdt_empID"=>$empId, "empdt_NSN"=>$NSN,"empdt_emp_name"=>$fname.' '.$mname.' '.$lname,"empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate,"empdt_dept_id"=>$deptId, "empdt_comp_id"=>$compId,"empdt_Idcopy"=>$idCard,"empdt_pinCert"=>$pin,"empdt_nssfNo"=>$nssf,"empdt_nhif"=>$nhifNo,"empdt_isValid"=>"Active","empdt_annual_leaves"=>$leave,"empdt_qualification"=>$qualification);
		$empTable ="logis_employee_details";
		$insertEmp = $connection->InsertQuery($empTable,$empCols);


		//Insert data in Employee Documents table
		$docCols = array("edocs_nsn"=>$NSN,"edocs_comp_id"=>$compId,"edocs_cv"=>$cv,"edocs_app_letter"=>$appLetter,
					  "edocs_qualification"=>$qualificationD, "edocs_id_card"=>$idCardD,"edocs_pin_cert"=>$pinD,
					  "edocs_nssf"=>$nssfD,"edocs_nhif"=>$nhifD,"edocs_dl_copy"=>$driverLicence,"edocs_conduct_cert"=>$bonafideCert);
		$docTable ="logis_emp_docs";
		$insertDocs = $connection->InsertQuery($docTable,$docCols);


		$getLastDriverDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
		$getLastDriverDetails->execute();
		$getLastDriverDetailsRow = $getLastDriverDetails->fetch();

		$driver_limit = $getLastDriverDetailsRow['driver_limit'] - 1;
		$working_driver = $getLastDriverDetailsRow['working_driver'] + 1;


		// UPDATE DRIVER LIMITS
		$updateDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$driver_limit',working_driver='$working_driver' where Email = '$Email'");
		$updateDriverLimit->execute();

		if($insertDriver == "success" && $insertEmp == "success" && $insertDocs == "success" && $insertKin == "success"){
			$connection->redirect("../drivers.php");
		}
		else {
			$connection->redirect("../add_driver.php?ref=error");
		}
	}
?>