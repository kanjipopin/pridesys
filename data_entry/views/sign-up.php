<?php
  include("config/db.php");

  $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
  $msg = "";

  if($type == "exist"){
    $msg = "You Company Pin or Email is already registered with Dereva Enterprise.";
  }
  elseif ($type == "error") {
    $msg = "We are getting a server error please try again later.";
  }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Dereva Portal</title>
	  <!-- Meta Tags -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!-- Favicon -->
	    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
      <!-- Bootstrap -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/bootstrap-theme.min.css">
	    <!-- Css -->
      <link rel="stylesheet" href="css/portal.css">
      <link rel="stylesheet" href="css/portal-responsive.css">
      <link rel="stylesheet" href="css/showup.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- datepicker -->
      <link rel="stylesheet" href="css/wbn-datepicker.css">

      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>

  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-4 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
            </div>

            <div class="col-sm-8 col-md-9">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="about-us.php">About Us</a></li>
                  <li><a class="btn btn-default header-btn" href="login.php" role="button" style="margin-right: 15px;padding: 10px 10px;">Login</a></li>
              </ul>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <div class="enterprise-signUP">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <small style="text-align: center;color: red;font-size: 15px;font-weight: 600;display: block;margin-bottom: 20px;"><?php if(!empty($msg)){ echo $msg; } ?></small>

            <form enctype="multipart/form-data" method="POST" action="functions/sign-up.php" id="login_form" name="login_form">
              <h4>Sign up</h4>
                <div class="input-group">
                  <input type="hidden" name="latitude" id="latitude">
                  <input type="hidden" name="longitude" id="longitude">

                  <div class="input-group-addon"><i class="fa fa-building"></i></div>
                    <input type="text" name="comp_name" id="comp_name" placeholder="Company Name *" class="form-control" required>
                </div>

                <!-- <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                    <input type="text" name="comp_position" id="comp_position" placeholder="Position *" class="form-control" required>
                </div> -->

                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-id-badge"></i></div>
                  <input type="text" name="comp_pin" id="comp_pin" placeholder="Tax Indetification Number" class="form-control" style="margin-bottom: 0px;" required>
                </div>
                  <small class="PinAlertMsg" style="color: red;font-size: 14px;font-weight: 600;"></small>


                <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></div>
                    <select name="location" id="location" class="form-control" required>
                      <option value="" selected disabled>Select a City</option>
                      <?php
                        $city = $conn->prepare("SELECT * from kenya_city");
                        $city->execute();
                        while($cityRow = $city->fetch()){
                      ?>
                        <option value = "<?php echo $cityRow['city_name']; ?>"><?php echo $cityRow['city_name']; ?></option>
                      <?php } ?>
                    </select>
                </div>

                <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                  <!-- <textarea name="comp_address" id="comp_address" placeholder="1st Floor, ABC Building, Road/Street *" class="form-control" required></textarea> -->
                  <input type="text" name="address1" id="address1" placeholder="Address line 1" class="form-control" required>
                </div>

                <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                  <input type="text" name="address2" id="address2" placeholder="Address line 2" class="form-control" required>
                </div>

                <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                  <input type="text" name="po_box" id="po_box" placeholder="P.O.Box *" class="form-control" onkeypress="return isNumber(event)" maxlength="5" required>
                </div>

                <div class="input-group">
                  <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                  <input type="text" name="po_box1" id="po_box1" placeholder="Post Code *" class="form-control" onkeypress="return isNumber(event)" maxlength="5" required>
                </div>

               <!--  <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-id-card"></i></div>
                    <input type="text" name="comp_NSN" id="comp_NSN" placeholder="National ID *" class="form-control" onkeypress="return isNumber(event)" style="margin-bottom: 0px;" required>
                </div> -->

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                  <input type="text" name="comp_fname" id="comp_fname" placeholder="First Name *" class="form-control required">
              </div> -->

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                  <input type="text" name="comp_mname" id="comp_mname" placeholder="Middle Name (optional)" class="form-control">
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                  <input type="text" name="comp_lname" id="comp_lname" placeholder="Last Name *" class="form-control" required>
              </div> -->

              <!-- <div class="input-group">
                <div class="btn-group" role="group" aria-label="...">
                  <button type="button" class="btn btn-default active-btn maleClass" value="male" onclick="genderClass(event);">Male</button>
                  <button type="button" class="btn btn-default femaleClass" value="female" onclick="genderClass(event);">Female</button>

                  <input type="hidden" name="genderVal" id="genderVal" value="male">
                </div>
              </div> -->

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                  <input type="text" id="comp_dob" name="comp_dob" class="form-control wbn-datepicker" data-min="1960-01-01" data-max="2000-01-01" required/>
              </div> -->

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div>
                  <input style="z-index: 0;" type="email" name="email" id="email" placeholder="Email ID *" class="form-control" onchange="chekEmail();" required>
              </div>
              <p class="emailAlert" style="color: red;font-size: 14px;font-weight: 500;"></p>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
                <select style="z-index: 0;" name="contactExtn" id="contactExtn" class="form-control">
                  <option value="" disabled>Select Country Code</option>
                  <?php
                    $slctCode = "254";
                    $getContryCode = $conn->prepare("SELECT * from country");
                    $getContryCode->execute();
                    while($getContryCodeRow = $getContryCode->fetch()){
                      $code = $getContryCodeRow['phonecode'];
                  ?>
                  <option value="<?php echo $code; ?>"<?php if($slctCode == $code){ echo "selected"; } ?>><?php echo $getContryCodeRow['nicename']." (+".$code.")"; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
                <input style="z-index: 0;" type="text" class="form-control" id="contact" name="contact" placeholder="Contact Number" onkeypress="return isNumber(event);" required>
              </div>

              <p id="terms_tag">* indicates required fields</p>
              <p id="terms_tag">By creating a Dereva account you agree to our <a href="../terms-of-service.php" style="color: #ff0200">Terms of Service</a></p>

              <div class="clearfix"></div>
              <input type="submit" id="submit" name="submit" class="btn btn-default form-btn" value="Submit" disabled>
            </form>
          </div>
        </div>
      </div>
    </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>
  
  <footer class="new-footer">
    <div class="container-fluid hidden-xs">
      <div class="col-sm-4">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-4">
        <p>&copy; Dereva.com | All rights reserved.</p>
        <a href="terms-of-service.php">Terms of Service</a>
      </div>

      <div class="col-sm-4">
        <ul style="float: right;">
          <li>Follow Us</li><br>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
        </ul>
      </div>
    </div>

    <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>
      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; Dereva.com | All rights reserved.</p>
      </div>
    </div>
  </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
if(navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function (p) {
      var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);

      var lat = p.coords.latitude;
      var long = p.coords.longitude;

      $("#latitude").val(lat);
      $("#longitude").val(long);
  });
}
</script>

<script src="js/wbn-datepicker.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('.wbn-datepicker').datepicker()
  })

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
</script>

<script type="text/javascript">
  function genderClass(event){
    var getVal = $(event.currentTarget).val();
    $("#genderVal").val(getVal);

    if(getVal == "male"){
      $(".maleClass").addClass("active-btn");
      $(".femaleClass").removeClass("active-btn");
    } 
    else if(getVal == "female") {
      $(".femaleClass").addClass("active-btn");
      $(".maleClass").removeClass("active-btn");
    }
  }
</script>


<script>
  function checkPIN(){
    var pin = $("#comp_pin").val();

    if(pin == ""){
      $(".PinAlertMsg").show().text("Please enter your registered company pin number.");
    } else {
      $(".PinAlertMsg").hide();

      $.ajax({
        type : "post",
        url : "functions/ajax.php",
        data : {"type":"checkpin","pin":pin},

        success : function(msg){
          if(msg > 0){
            $(".PinAlertMsg").show().text("Your Company PIN number is already registered in Dereva Enterprise.");
          } else {
            $(".PinAlertMsg").hide();
          }
        }
      });
    }
  }

  function confirmPIN(){
    var pwd = $("#comp_pin").val();
    var Cpwd = $("#confm_comp_pin").val();

    if(Cpwd != ""){
      if(pwd != Cpwd){
        $("#submit").attr("disabled",true);
        $("#comp_pin").css("border-color","red");
        $("#confm_comp_pin").css("border-color","red");
        $("#chk_pass").show();
        $(".PinconfirmMsg").text("Your PIN Number an Confirm PIN are not matched.");
      }
      else {
        $("#submit").attr("disabled",false);
        $("#comp_pin").css("border-color","#e8e8e8");
        $("#confm_comp_pin").css("border-color","#e8e8e8");
        $(".PinconfirmMsg").hide();
      }
    }
  }

  function chekEmail(){
    var email = $("#email").val();
    // alert(email);

    $.ajax({
      type : "post",
      url : "functions/ajax.php",
      data : {"type":"checkEmail","email":email},

      success : function(msg){
        // alert(msg);

        if(msg > 0){
          $(".emailAlert").text("Your Email address is already registered in Dereve Enterprise.");
          $("#submit").attr("disabled",true);
        } else {
          $(".emailAlert").text("");
          $("#submit").attr("disabled",false);
        }
      }
    });
  }


// var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

// $(document).ready(function(){
//   var url = window.location.href;
//   var msg = url.substring(url.indexOf('?')+5);
//   console.log(msg);
//   if(msg=="success"){
//     $("#alertBox").removeClass("hidden");
//     $("#alertBox").addClass("alert-success");
//     $("#alertBox").html("Your form has been succesfully submitted");
//   }
//   else{    
//     $("#alertBox").addClass("hidden");
//     $("#alertBox").removeClass("alert-success");
//     $("#alertBox").html("");
//   }
// });

  // $("#frm_sign_up").submit(function(){
  //   $("#frm_sign_up input").blur(function()
  //   { 
  //     //alert("hi");
  //       if( !$(this).val()) {              
  //             $("#alertBox").removeClass("hidden");
  //             $("#alertBox").addClass("alert-danger");
  //             $("#alertBox").html("Please enter the highlighted fields");
  //             $(this).addClass('focus');
  //             event.preventDefault();
  //       }
  //   });

    //   var fname=$('#fname').val();
    //   var lname=$('#lname').val();
    //   var compName = $('#compName').val();
    //   var phone = $('#phone').val();
    //   var email = $('#email').val();
    //   var pwd =$('#pwd').val();
    //   if(fname!==""  && lname!=="")
    //   { 
    //     if(compName!=="")
    //     { 
    //       if(phone!=="" && email!=="")
    //       {
    //         if(pwd==""){
    //           //window.location.href='functions/sign-up.php';
    //           $("#alertBox").html("please enter a valid password");
              
    //         }
            
    //       }
    //       else{
    //         $("#alertBox").removeClass("hidden");
    //         $("#alertBox").addClass("alert-danger");
    //         $("#alertBox").html("please enter contact info");
    //         event.preventDefault();
    //       }
            
    //     }
    //     else {
    //       $("#alertBox").removeClass("hidden");
    //       $("#alertBox").addClass("alert-danger");
    //       $("#alertBox").html("please enter Organisation name");
    //       event.preventDefault();
    //     }
    //   }
    //   else {
    //     $("#alertBox").removeClass("hidden");
    //     $("#alertBox").addClass("alert-danger");
    //     $("#alertBox").html("Please enter both name fields");
    //     event.preventDefault();
    //   }
    // });


  // $("#alertBoxs").hide();
  // $("#alertBoxSuccess").hide();
  // $("#alertemailSuccess").hide();

  // function delayedRedirect(){
  //   window.location = "http://markweb.in/muranga-portal/index.php";
  // }

  // function getForm(){
  //   var name = $("#fname").val();
  //   var comp = $("#compName").val();
  //   var city = $("#city").val();
  //   var phone = $("#phone").val();
  //   var email = $("#email").val();
  //   var pwd = $("#pwd").val();

  //   if(name == "" || comp == "" || city == "" || phone == "" || email == "" || pwd == ""){
  //     alert("All fields is required.");
  //   }

  //   else if(name != "" || comp != "" || city != "" || phone != "" || email != "" || pwd != ""){
  //     $.ajax({
  //       type: "POST",
  //       url:"functions/sign-up.php",
  //       data: $('#frm_sign_up').serialize() + "&type=" + "signupform",
      
  //       success : function(msg){
  //         // alert(msg);
  //         if(msg == "congragulations - login succesful."){
            
  //           $("#alertBoxSuccess").text(msg);
  //           $("#alertBoxSuccess").show();
  //           $("#alertemailSuccess").text("An confirmation email has been sent to your email address with your account details.");
  //           $("#alertemailSuccess").show();
  //           setTimeout('delayedRedirect()', 5000);
  //         }
  //         else if(msg == "account already registered."){
  //           $("#alertBoxs").text(msg);
  //           $("#alertBoxs").show();
  //         }
  //       }
  //       // error : function(){
  //       //   $("#alertBoxs").text("account already exists.");
  //       //   $("#alertBoxs").show().delay(5000).fadeOut();
  //       // }
  //     });
  //   }
  // }
</script>

<!-- <script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>   -->
<script>
  // $( "#frm_sign_up" ).validate({
  //   rules: {
  //     email: {
  //     email: true
  //     },
  //    }
  // });
</script>

</body>
</html>