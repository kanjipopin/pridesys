<?php include'header.php'; ?>

  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <!-- Accordion styles -->
	<link rel="stylesheet" href="css/smk-accordion.css" />

	<div class="page-rightWidth">
      <div class="drivers">
          <div>
              <div class="driver-detail-heading">
					<h4>Vehicle Details</h4>

					<a href="vehicles.php" type="button" class="btn btn-default pull-right"  style="margin-left:10px">< Back</a>

					<a href="edit-vehicle.php?id=<?php echo base64_encode($getVehicleData['VehicleReg']); ?>" type="button" class="btn btn-default pull-right"  style="margin-left:10px">Edit Vehicle</a>

						<a href="<?php echo 'functions/ajax.php?type=delete_vehicle&id='.base64_encode($getVehicleData['VehicleReg']); ?>" type="button" class="btn btn-default pull-right">Delete Vehicle</a>
						<div class="clearfix"></div>


         	  </div>

			  <div class="verifying-driver-page driver-detail-page">
              	<div class="row">
              		<div class="col-sm-8 hidden-xs">
              			<div class="table-responsive">
              				<table class="table table-striped">
              					<tbody>
              						<tr>
              							<td>Registration:</td>
              							<td align="right"><?php echo $getVehicleData['VehicleReg']; ?></td>
              						</tr>
              						<tr>
              							<td>Manufacturer:</td>
              							<td align="right"><?php echo $getManufacturerData['Manufacturer']; ?></td>
              						</tr>
              						<tr>
              							<td>Model:</td>
              							<td align="right"><?php echo $getModelData['Model']; ?></td>
              						</tr>
              						<tr>
              							<td>Body Type:</td>
              							<td align="right"><?php echo $getBodyTypeData['BodyType']; ?></td>
              						</tr>

              						<tr>
              							<td>Color:</td>
              							<td align="right"><?php echo $getColorData['Color']; ?></td>
              						</tr>
              						<tr>
              							<td>Year:</td>
              							<td align="right"><?php echo $getYearData['VehicleYear']; ?></td>
              						</tr>

              						<tr>
              							<td>Fuel:</td>
              							<td align="right"><?php echo $getFuelData['Fuel']; ?></td>
              						</tr>

                                    <tr>
                                        <td>Transmission:</td>
                                        <td align="right"><?php echo $getTransmissionData['Transmission']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Fuel economy:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleFuelEconomy']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Maximum passengers:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleMaxPassengers']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Engine capacity:</td>
                                        <td align="right"><?php echo $getEngineCapacityData['EngineCapacity']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Doors:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleDoors']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Mileage Limit:</td>
                                        <td align="right"><?php echo $getMileageData['Mileage']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Lease preparation time:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleLeasePreparation']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Add ons:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleAddOns']; ?></td>
                                    </tr>

                                        <td>Inventory location:</td>
                                        <td align="right"><?php echo $getInventoryLocationData['InventoryLocation']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Price per day:</td>
                                        <td align="right"><?php echo $getVehicleData['VehicleDailyPrice']; ?></td>
                                    </tr>

              					</tbody>
              				</table>
              			</div>
              		</div>
              		<div class="col-sm-4">
              			<img src="../<?php echo @$img = ltrim($getVehicleData['VehicleImg'],"../"); ?>" class="img-responsive driver-img">
              			<p class="belowImgTxt"><span style="font-weight: 500;">More features:</span><span style="float: right;">

                                <?php

                                    $additional_features = explode(",",$getVehicleData['VehicleMoreFeatures']);


                                    foreach($additional_features as $key => $row) {

                                        if($additional_features > 0) {

                                        $vehicle = $conn->prepare("SELECT * from vehicle_more_features_master WHERE ID='{$row}' ");
                                        $vehicle->execute();
                                        $getVehicleMoreFeaturesData = $vehicle->fetch(PDO::FETCH_ASSOC);


                                ?>


                               <ul style="list-style-type:none;">
                                   <li><img src="../images/doc_right.png" alt="Delete Icon"><?php echo $getVehicleMoreFeaturesData['MoreFeatures']; ?></li>
                               </ul>


                               <?php } } ?>

                            </span></p>

              		</div>
              		<div class="visible-xs">
	              		<div class="col-sm-8 driver-detail-resp">
	              			<h4><span class="span1">Name:</span><span class="span2"><?php echo $getVehicleData['drv_fname']." ".$getVehicleData['drv_mname']." ".$getVehicleData['drv_lname']; ?></span></h4>
	              			<h4><span class="span1">ID:</span><span class="span2"><?php echo $getVehicleData['drv_NSN']; ?></span></h4>
	              			<h4><span class="span1">Contact:</span><span class="span2"><?php echo $getVehicleData['drv_contact_no']; ?></span></h4>
	              			<h4><span class="span1">Vehicle:</span><span class="span2"><?php echo $getVehicleData['vehiPlatNumber']; ?></span></h4>
	              			<h4><span class="span1">Driving Licence Number:</span><span class="span2"><?php echo $getVehicleData['drv_driving_license_no']; ?></span></h4>
	              			<h4><span class="span1">Driver Port Pass Number:</span><span class="span2"><?php echo $getVehicleData['driver_port_number']; ?>, Valid till : <?php echo $getVehicleData['driver_port_expire_date']; ?></span></h4>
	              			<h4><span class="span1">Vehicle Port Pass Number:</span><span class="span2"><?php echo $getVehicleData['vehicle_port_number']; ?>, Valid till : <?php echo $getVehicleData['vehicle_port_expire_date']; ?></span></h4>
	              			<h4><span class="span1">Branch:</span><span class="span2"><?php echo $getVehicleData['empdt_dept_id']; ?></span></h4>
	              			<h4><span class="span1">Date of Birth:</span><span class="span2">31-12-1980</span></h4>
	              			<h4><span class="span1">Email:</span><span class="span2"><?php echo $getVehicleData['drv_email']; ?></span></h4>
	              		</div>
              		</div>
              	</div>


            </div>


          </div>

         </div>

         <div class="row visible-xs" style="margin: 0;">
	        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
	          <div class="sidebar-nav navbar-collapse">
	            <ul class="nav" id="side-menu">
	              <li>
	                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
	              </li>
	              <li>
	                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Vehicles</a>
	              </li>
	            </ul>
	          </div>
	        <!-- /.sidebar-collapse -->
	        </div>
	      </div>
      </div>
  </div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="js/rate/jquery.rateyo.min.css"/>
<script type="text/javascript" src="js/rate/jquery.min.js"></script>
<script type="text/javascript" src="js/rate/jquery.rateyo.js"></script>

<script src="js/jquery.popupoverlay.js"></script>
<script src="../js/datepicker.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script>
	function getleaves(){
		$.ajax({
			type: "POST",
      		url:"functions/ajaxaddrate.php",
      		data : $("#form_leav").serialize(),
      		success: function(msg){
		     	// $("#addLeaves").modal();
		     	// $("#fetch_leave").html(msg);
		     	window.location.reload();
		    },
		});
	}

// function getadvanced(){
// 		var l_name = $("#drv_name").val();
// 		var l_nsn = $("#drv_nsn").val();

// 		$.ajax({
// 			type: "POST",
//       		url:"functions/ajaxaddrate.php",
//       		data : $("#form_leav").serialize()+"&name="+l_name+"&nsn="+l_nsn,
//       		success: function(msg){
// 		     	// alert(msg);
// 		     	alert("Added Successfully");
// 		     	$("#fetch_leave").html(msg);
// 		     	$('#addLeaves').modal('hide');
// 		    },
// 		});
// 	}


	$(document).ready(function() {

		$("#redflagDiv").hide();
		$('.notworking').click(function() {
		 	$('#redflagDiv').show();
		});
		$('.working').click(function() {
		 	$('#redflagDiv').hide();
		});


	    $('#fade').popup({
	      transition: 'all 0.3s',
	      scrolllock: true
	    });


		$("#renewC").hide();
		$("#Cyes").click(function(){
		 	$("#renewC").show("slow");
		});
		$("#Cno").click(function(){
		 	$("#renewC").hide("slow");
		});


		$('#startDate').datepicker({
	          format: "yyyy/mm/dd"
	    }).on('changeDate', function(ev){
	       	// alert("working");
	         var date1 = $('#startDate').datepicker('getDate');
	         var date2 = date1.setDate(date1.getDate()+364);
	        $('#endDate').datepicker('setDate',date1);
	    });

	    $('#endDate').datepicker({
	        format: "yyyy/mm/dd",
	    });

	  	var f_leaves = $('#from_leaves').datepicker({
					        format: "yyyy/mm/dd",
					    });
		var t_leaves = $('#to_leaves').datepicker({
					        format: "yyyy/mm/dd",
					    });

		$("#to_leaves").change(function (){
			var fl = $("#from_leaves").datepicker('getDate');
			var tl = $("#to_leaves").datepicker('getDate');
			
			var t = Math.floor((tl - fl) / (1000 * 60 * 60 * 24));
			var tt = parseInt(t) + 1;
			
			$("#total_leaves").val(tt);
		});
	});
		
  	function getRenew(){
    	$.ajax({
      		type: "POST",
      		url:"functions/ajaxaddrate.php",
      		data: $('#addrenew').serialize()+"type=sc",
      
      		success: function(msg){
	    		// alert("Contract Renew Successfully.");
	    		window.location.reload();
      		},
    	});
  	}

    // $(function(){
    //     var rating = $("#rt").val();
    //     if(rating == ""){
    //   	  rating = 0;        	
    //     }

    //     console.log("On rating Rate"+rating);

    //     $(".counter").text(rating);

    //     $("#rateYo1").on("rateyo.init", function () { console.log("rateyo.init"); });

    //     $("#rateYo1").rateYo({
    //       rating: rating,
    //       numStars: 5,
    //       precision: 2,
    //       starWidth: "64px",
    //       spacing: "5px",
	   //    rtl: true,
    //       multiColor: {
    //         startColor: "#000000",
    //         endColor  : "#ffffff"
    //       },
    //       onInit: function () {
    //         console.log("On Init");
    //       },
    //       onSet: function () {
    //         console.log("On Set");
    //       }
    //     }).on("rateyo.set", function () { console.log("rateyo.set"); })
    //       .on("rateyo.change", function () { console.log("rateyo.change"); });

    //     $(".rateyo").rateYo();

    //     $(".rateyo-readonly-widg").rateYo({
    //       rating: rating,
    //       numStars: 5,
    //       precision: 2,
    //       minValue: 1,
    //       maxValue: 5
    //     }).on("rateyo.change", function (e, data) {
    //       $("#rt").val(data.rating);
    //       console.log(data.rating);
    //     });
    //   });


	function UpdateWorkStatus(){

	    $.ajax({
	      type: "POST",
	      url: "functions/ajaxaddrate.php",
	      data: $('#workingForm').serialize(),

	      success: function(msg){
	      	$(".workUpdateMsg").text("Work Status updated Successfully.");
	    	window.location.reload();
	     	},
		});
	}

	// function removeRedfalg(){
	// 	var removeRed = $("#redflagNo").val();
	// 	var drv_nsn = $("#drv_nsn").val();
	// 	$.ajax({
	// 		type: "POST",
	//      	url: "functions/ajaxaddrate.php",
	//      	data: {"type":"removeRedFlag","removeRed":removeRed,"drv_nsn":drv_nsn},

	//      	success: function(msg){
	//     		window.location.reload();
	//      	},
	// 	})
	// }
</script>
<script type="text/javascript" src="js/smk-accordion.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$(".accordion_example").smk_Accordion({
				closeAble: true, //boolean
			});			
	});
</script>

<?php include'footer.php'; ?>