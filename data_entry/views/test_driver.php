<?php
	// Driver Details
	$compId = $lastCompID;
	$fname = "John";
	$mname = "Mark";
	$lname = "Doe";
	$gender = "male";
	$NSN = "123456";
	@$implodeVehicle = implode(",", '3,7');
	@$vehicle = isset($implodeVehicle) ? $implodeVehicle : "";
	$insuranceType = "private";
	$insuranceRefNum = "798456132";
	$insuranceExpDate = "2019-07-03";
	$dlNum = "JHM030121";
	$contactNo = "07894561230";
	$altContactNo = "9878456120";
	$address = "MOI AVENUE 23";
	$address2 = "MOMBASA NORTH";
	$city = "16";
	$pobox = "45634";
	$postalcode = "123457";
	$email = "johndoe@gmail.com";
	$grossSal = "150000";
	$CreatedDate = date("Y-m-d");
	$vehiPlatNumber = "abc456";
	$vehicleInspection = "Test";
	$department = "Test";
	$inspectionExpDate = '2019-07-24';
	$issueDate = '2019-07-01';
	$expiryDate = '2019-11-08';
	$deptId = 'Nairobi';
	$leave = 'NULL';
	$commenced_startDate = '2019-04-17';
	$startDate = '2019-07-01';
	$endDate = '2020-06-29';

	@$kinname = implode(",", 'MOHAMED SAID');
	@$kincontact = implode(",", '123456');
	@$kin_email = implode(",", 'mohamed@gmail.com');
	@$kinrelation = implode(",", 'Friend');
	@$kinnsn = implode(",", '456789123');
	@$kinadd = implode(",", 'Nivina Towers');

	@$expkinname = explode(",", $kinname);
	@$expkincontact = explode(",", $kincontact);
	@$expkin_email = explode(",", $kin_email);
	@$expkinrelation = explode(",", $kinrelation);
	@$expkinnsn = explode(",", $kinnsn);
	@$expkinadd = explode(",", $kinadd);

	$qualification ='456789456';
	$idCard = '123456';
	$pin = '123456';
	$imgFile = "../uploads/test_company/testImg.png";
	$vehicleInspectionFile = "../uploads/test_company/testDocs.pdf";
	$ContractFile = "../uploads/test_company/testDocs.pdf";
	$insuranceImg = "../uploads/test_company/testDocs.pdf";
	$driverLicence = "../uploads/test_company/testDocs.pdf";
	$cv = "../uploads/test_company/testDocs.pdf";
	$appLetter="../uploads/test_company/testDocs.pdf";
	$qualificationD="../uploads/test_company/testDocs.pdf";
	$idCardD="../uploads/test_company/testDocs.pdf";
	$pinD="../uploads/test_company/testDocs.pdf";
	$nssfD="../uploads/test_company/testDocs.pdf";
	$nhifD="../uploads/test_company/testDocs.pdf";
	$bonafideCert = "../uploads/test_company/testDocs.pdf";


	$driverCols = array("drv_NSN"=>$NSN,"drv_fname"=>$fname,"drv_lname"=>$lname,"gender"=>$gender,"drv_mname"=>$mname,"drv_img"=>$imgFile,"drv_contact_no"=>$contactNo,"drv_alt_contact_no"=>$altContactNo, "drv_address"=>$address,"drv_address2"=>$address2, "drv_city"=>$city,"drv_pobox"=>$pobox,"drv_postal"=>$postalcode,"drv_email"=>$email,"logis_drivers_vehicle_class"=>$vehicle,"drv_driving_license_no"=>$dlNum,"drv_curr_comp_id"=>$compId,"drv_isActive"=>"Active","gross_salary"=>$grossSal,"drv_dl_issue_date"=>$issueDate,"drv_dl_expiry_date"=>$expiryDate,"drv_created_on"=>$CreatedDate,"driver_work_status"=>"working","insurance_type"=>$insuranceType,"insurance_ref_number"=>$insuranceRefNum,"insurance_exp_date"=>$insuranceExpDate,'delete_flag'=>'No','vehiPlatNumber'=>$vehiPlatNumber,'vehicleInspection'=>$vehicleInspection,'vehicleInspectionFile'=>$vehicleInspectionFile,"vehicleInspection_date"=>$inspectionExpDate,"insurance_img"=>$insuranceImg);
	$driverTable = "logis_drivers_master";
	$insertDriver = $connection->InsertQuery($driverTable,$driverCols);


	//Insert Data into Kin table
	for($i=0; $i<count($expkinname); $i++){

		$expkinname1 = $expkinname[$i];
		$expkincontact1 = $expkincontact[$i];
		$expkin_email1 = $expkin_email[$i];
		$expkinrelation1 = $expkinrelation[$i];
		$expkinnsn1 = $expkinnsn[$i];
		$expkinadd1 = $expkinadd[$i];

		if($expkinname1 != ""){

			$kinCols = array("drivers_kin_name"=>$expkinname1,"drv_nsnId"=>$NSN,"drv_comp_id"=>$compId,"drivers_kin_email"=>$expkin_email1,"drivers_kin_contact"=>$expkincontact1,"drivers_kin_nsn"=>$expkinnsn1,"drivers_kin_relation"=>$expkinrelation1,"drivers_kin_address"=>$expkinadd1);
			$kinTable = "logis_drivers_kin";
			$insertKin = $connection->InsertQuery($kinTable,$kinCols);
		}
	}


	//Insert data in Employee Details table
	$empCols = array("empdt_NSN"=>$NSN,"empdt_emp_name"=>$fname.' '.$mname.' '.$lname,"empdt_commenced_date"=>$commenced_startDate,"empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate,"empdt_dept_id"=>$deptId, "empdt_comp_id"=>$compId,"empdt_Idcopy"=>$idCard,"empdt_pinCert"=>$pin,"empdt_isValid"=>"Active","empdt_annual_leaves"=>$leave,"empdt_qualification"=>$qualification,"department"=>$department,"contract_file"=>$ContractFile);
	$empTable ="logis_employee_details";
	$insertEmp = $connection->InsertQuery($empTable,$empCols);


	//Insert data in Employee Documents table
	$docCols = array("edocs_nsn"=>$NSN,"edocs_comp_id"=>$compId,"edocs_cv"=>$cv,"edocs_app_letter"=>$appLetter,
				  "edocs_qualification"=>$qualificationD, "edocs_id_card"=>$idCardD,"edocs_pin_cert"=>$pinD,
				  "edocs_nssf"=>$nssfD,"edocs_nhif"=>$nhifD,"edocs_dl_copy"=>$driverLicence,"edocs_conduct_cert"=>$bonafideCert);
	$docTable ="logis_emp_docs";
	$insertDocs = $connection->InsertQuery($docTable,$docCols);
?>