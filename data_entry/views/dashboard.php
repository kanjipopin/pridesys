<?php 
  include 'header.php'; 

  $prvsDay = date("Y-m-d", strtotime("- 30 days"));
  $nextDay = date("Y-m-d", strtotime("+ 30 days"));

  $getDriver = $conn->prepare("SELECT * from logis_drivers_master where drv_dl_expiry_date > '$prvsDay' and drv_dl_expiry_date < '$nextDay' and driver_work_status = 'working' and delete_flag = 'No' ");
  $getDriver->execute();
  $getDriverCount = $getDriver->rowCount();

  $getDriver1 = $conn->prepare("SELECT * from logis_drivers_master as dm INNER JOIN logis_employee_details as ed ON ed.empdt_NSN = dm.drv_NSN
    where ed.empdt_contract_start > '$prvsDay' and ed.empdt_contract_end < '$nextDay' and dm.driver_work_status = 'working' and dm.delete_flag = 'No' ");
  $getDriver1->execute();
  $getDriver1Count = $getDriver1->rowCount();

  $getDriver2 = $conn->prepare("SELECT * from logis_drivers_master where insurance_exp_date > '$prvsDay' and insurance_exp_date < '$nextDay' and driver_work_status = 'working' and delete_flag = 'No' ");
  $getDriver2->execute();
  $getDriver2Count = $getDriver2->rowCount();

  $getDriver3 = $conn->prepare("SELECT * from logis_drivers_master where vehicleInspection_date > '$prvsDay' and vehicleInspection_date < '$nextDay' and driver_work_status = 'working' and delete_flag = 'No' ");
  $getDriver3->execute();
  $getDriver3Count = $getDriver3->rowCount();
?>
    
   <div class="page-rightWidth"> 
  <div class="dashboard-page">
    <h3>Overview </h3>
    <div class="row">
      <div class="col-sm-6 col-md-6 col-lg-3">
        
        <div class="boxes drivers-box">
          <h4>Driving Licences Expiring</h4>
          <h5><span class="counter">
            <?php echo $getDriverCount; ?>
          </span></h5>
          <a class="hidden-xs" href = "drivers.php"><p>View All</p></a>
          <a class="visible-xs" href = "driving_licences_expiring.php"><p>View All</p></a>
          <span class="glyphicon glyphicon glyphicon-menu-right visible-xs" aria-hidden="true"></span>
          
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="boxes drivers-box">
          <h4>Driver Contracts Expiring</h4>
          <h5><span class="counter">
            <?php echo $getDriver1Count; ?>
          </span></h5>
          <a class="hidden-xs" href = "drivers.php"><p>View All</p></a>
          <a class="visible-xs" href = "driving_contract_expiring.php"><p>View All</p></a>
          <span class="glyphicon glyphicon glyphicon-menu-right visible-xs" aria-hidden="true"></span>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="boxes drivers-box">
          <h4>Vehicle Insurance Expiring</h4>
          <h5><span class="counter">
            <?php echo $getDriver2Count; ?>
          </span></h5>
          <a class="hidden-xs" href = "drivers.php"><p>View All</p></a>
          <a class="visible-xs" href = "driving_insurance_expiring.php"><p>View All</p></a>
          <span class="glyphicon glyphicon glyphicon-menu-right visible-xs" aria-hidden="true"></span>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="boxes drivers-box">
          <h4>Vehicle Inspection Expiring</h4>
          <h5><span class="counter">
            <?php echo $getDriver3Count; ?>
          </span></h5>
          <a class="hidden-xs" href = "drivers.php"><p>View All</p></a>
          <a class="visible-xs" href = "driving_inspection_expiring.php"><p>View All</p></a>
          <span class="glyphicon glyphicon glyphicon-menu-right visible-xs" aria-hidden="true"></span>
        </div>
      </div>
      <div class="clearfix"></div>
      </div>

      <div class="notification-block hidden-xs">
         <div class="heading-tabs">
           <ul class="nav nav-tabs ">
              <li class="active"><a href="#tab_default_1" data-toggle="tab">Driving Licences Expiring</a></li>
              <li><a href="#tab_default_2" data-toggle="tab">Driver Contracts Expiring</a></li>
              <li><a href="#tab_default_3" data-toggle="tab">Vehicle Insurance Expiring</a></li>
              <li><a href="#tab_default_4" data-toggle="tab">Vehicle Inspection Expiring</a></li>
           </ul>
          </div>

          <div class="driver-detail-tab">
             <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Driver Name</th>
                          <th>Contact</th>
                          <th>DL Ref Number</th>
                          <th>License Expires On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        if($getDriverCount > 0){
                          while($getDriverRow = $getDriver->fetch()){
                      ?>
                        <tr>
                          <td><?php echo $getDriverRow['drv_fname']; ?></td>
                          <td><?php echo $getDriverRow['drv_contact_no']; ?></td>
                          <td><?php echo $getDriverRow['drv_driving_license_no']; ?></td>
                          <td><?php echo date("d M y", strtotime($getDriverRow['drv_dl_expiry_date'])); ?></td>
                        </tr>
                      <?php } } else { ?>
                        <tr>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="tab_default_2">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Driver Name</th>
                          <th>Contact</th>
                          <th>DL Ref Number</th>
                          <th>Contract Expires On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        if($getDriver1Count > 0){
                          while($getDriver1Row = $getDriver1->fetch()){
                      ?>
                        <tr>
                          <td><?php echo $getDriver1Row['drv_fname']; ?></td>
                          <td><?php echo $getDriver1Row['drv_contact_no']; ?></td>
                          <td><?php echo $getDriver1Row['drv_driving_license_no']; ?></td>
                          <td><?php echo date("d M y", strtotime($getDriver1Row['empdt_contract_end'])); ?></td>
                        </tr>
                      <?php } } else { ?>
                        <tr>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="tab_default_3">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Vehicle Registration Number</th>
                          <th>Driver Name</th>
                          <th>Contact</th>
                          <th>Insurance Expires On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        if($getDriver2Count > 0){
                          while($getDriver2Row = $getDriver2->fetch()){
                      ?>
                        <tr>
                          <td><?php echo $getDriver2Row['vehiPlatNumber']; ?></td>
                          <td><?php echo $getDriver2Row['drv_fname']; ?></td>
                          <td><?php echo $getDriver2Row['drv_contact_no']; ?></td>
                          <td><?php echo date("d M y", strtotime($getDriver2Row['insurance_exp_date'])); ?></td>
                        </tr>
                      <?php } } else { ?>
                        <tr>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="tab_default_4">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Vehicle Registration Number</th>
                          <th>Driver Name</th>
                          <th>Contact</th>
                          <th>Certificate Expires On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        if($getDriver3Count > 0){
                          while($getDriver3Row = $getDriver3->fetch()){
                      ?>
                        <tr>
                          <td><?php echo $getDriver3Row['vehiPlatNumber']; ?></td>
                          <td><?php echo $getDriver3Row['drv_fname']; ?></td>
                          <td><?php echo $getDriver3Row['drv_contact_no']; ?></td>
                          <td><?php echo date("d M y", strtotime($getDriver3Row['vehicleInspection_date'])); ?></td>
                        </tr>
                      <?php } } else { ?>
                        <tr>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
             </div>
          </div>

      </div>
    </div>
  </div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a class="active-class" href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

</div>
</div>
   
<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/metisMenu.min.js"></script>
<script src="../js/sb-admin-2.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});
</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="../js/jquery.counterup.min.js"></script>
<script>
  $(document).ready(function() {
    $('.counter').counterUp({
        delay: 8,
        time: 1000
    });

  if($('#contoday1').val()!==null){

    var bfortotal = daydiff(parseDate($('#contoday1').val()), parseDate($('#conexpiry1').val()));
  }

  var tostr = bfortotal.toString();
  var remove = tostr.replace("-","");

  //$("#remain_contractday1").text("expired "+remove+" days ago");

  var bfortotals = daydiff(parseDate($('#today1').val()), parseDate($('#expiry1').val()));
  // var bfortotals = "-4";

  var tostr = bfortotals.toString();
  var remove = tostr.replace("-","");

 // $("#remain_day1").text("expiried "+remove+" days ago");

});

/////////////////
//  DL EXPIRY //
////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(today, expiry) {
    return Math.round((expiry - today)/(1000*60*60*24));
}
var total = daydiff(parseDate($('#today').val()), parseDate($('#expiry').val()));
$("#remain_day").text(total+" days left");


///////////////////
//  DL EXPIRIED //
//////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(today1, expiry1) {
    return Math.round((expiry1 - today1)/(1000*60*60*24));
}

$(document).ready(function() {
});



///////////////////////
//  CONTARCT EXPIRY //
//////////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(contoday, conexpiry) {
    return Math.round((conexpiry - contoday)/(1000*60*60*24));
}
var total = daydiff(parseDate($('#contoday').val()), parseDate($('#conexpiry').val()));
$("#remain_contractday").text(total+" days left");


///////////////////
//  DL EXPIRIED //
//////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(contoday1, conexpiry1) {
    return Math.round((conexpiry1 - contoday1)/(1000*60*60*24));
}
</script>
<script type="text/javascript">
  

function sticky_relocate() {
    var footer_top = $("#footer").offset().top;
    
    if (footer_top)
        $('.sidebar').removeClass('stick');    
    else {
        $('.sidebar').addClass('stick');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
</script>

<?php include'footer.php'; ?>