<?php include'header.php'; ?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>Vehicle more features (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_vehicle_more_features_master.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="vehicle_more_features_master.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Feature</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {

                $id = base64_encode($fd['ID']);
            ?>
              <tr>
                <td><?php echo $fd['MoreFeatures']; ?></td>

                <td>

                  <a class="editBtn" href="<?php echo 'edit_vehicle_more_features_master.php?id='.$id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>

                    <a class="editBtn dltDriver" data-id="<?php echo 'functions/ajax.php?type=delete_vehicle_more_features_master&id='.$id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Delete
                    </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {

            $id = base64_encode($fd1['ID']);
        ?>
          <div class="resp-my-driver-block">

            <div class="block-2">
              <h5><?php echo $fd1['MoreFeatures']; ?></h5>
              <h5>
                  <a class="editBtn" href="<?php echo 'edit_vehicle_more_features_master.php?id='.$id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>

              </h5>
              <h5>
                  <a class="editBtn dltDriver" data-id="<?php echo 'functions/ajax.php?type=delete_vehicle_more_features_master&id='.$id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Delete
                  </a>
              </h5>

            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });




  $(".dltDriver").click(function (event){

    var deletedvr = confirm("Are you sure want to delete this feature?");

    if(deletedvr == true){
      var dataid = $(event.currentTarget).attr('data-id');
      window.location = dataid;
    }
  });


  </script>

<?php include'footer.php'; ?>