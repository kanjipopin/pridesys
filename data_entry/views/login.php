<?php
  require_once('../config/db.php');
  session_start();


  $action = isset($_GET['action']) ? $_GET['action'] : "";
  $actions = isset($_GET['actions']) ? $_GET['actions'] : "";

  if ($action == "wrong_credential") {
    $loginMsg = "Wrong Credential Provided.";
  }

  if(isset($_REQUEST['loginBtn'])){
    $Email = $_POST['Email'];
    $passwd = $_POST['passwd'];

    $getLogin = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}' and comp_pwd = '{$passwd}'");
    $getLogin->execute();
    $getLoginCount =$getLogin->rowCount();
    $getLoginRow = $getLogin->fetch();

    //print_r($getLoginRow);

    if($getLoginCount > 0){

      $_SESSION['Email'] = $Email;

      
          // header("location: check_order_status.php");
      header("location: dashboard.php");

    }
    else {
      unset($_SESSION['Email']);
      header("location: login.php?action=wrong_credential");
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Data entry</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>
  <body>
    <header>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header col-sm-4 col-md-3">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="images/prime_logo_sizable.png" alt="Pride drive" title="Pride drive" style="width: 160px; height: 60px;"></a>
          </div>

          <div class="col-sm-8 col-md-9">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <!--<li><a href="about-us.php">About Us</a></li>
                <li><a class="btn btn-default header-btn" href="sign-up.php" role="button" style="padding: 10px 10px;">Sign Up</a></li>-->
            </ul>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <div class="enterprise-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <form enctype="multipart/form-data" method="POST" id="login_form" name="login_form">
              <h4>Log in</h4>
              <p style="color: red;font-size: 15px;font-weight: 500;"><?php if(!empty($loginMsg)) { echo $loginMsg; } ?></p>
              <p style="color: red;font-size: 15px;font-weight: 500;"><?php if(!empty($msg)) { echo $msg; } ?></p>
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="text" class="form-control" name="Email" id="Email" placeholder="Email Address" required>
              </div>
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="passwd" id="passwd" placeholder="Password" required>
              </div>
              <small><a style="display: block;text-align: left;color: #909090;" href="email_confirmation.php">Forgot your Password?</a></small>

              <div class="clearfix"></div>
              <button type="submit" id="loginBtn" name="loginBtn" class="btn btn-default">Sign in</button>

              <div class="clearfix"></div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <footer class="new-footer">
      <div class="container-fluid hidden-xs">
        <div class="col-sm-4">
          <p style="color: #fff;">
              8 Chiromo Lane, NRB <br>
            <a href="tel: +254714001177" target="_blank" style="color: #fff;">
                0714-001-177 / 0780-001-177</a> <br>
            <a href="mailto: carhire@pridedrive.com" target="_blank" style="color: #fff;">carhire@pridedrive.com </a>
          </p>
        </div>

        <div class="col-sm-4">
          <p>&copy; pridedrive.com | All rights reserved.</p>
          <a href="terms-of-service.php">Terms of Service</a>
        </div>

        <div class="col-sm-4">
          <ul style="float: right;">
            <li>Follow Us</li><br>
            <li><a href="https://www.facebook.com/" target="_blank"><img src="images/fb.png"></a></li>
            <li><a href="https://twitter.com/" target="_blank"><img src="images/twitter.png"></a></li>
            <li><a href="https://www.linkedin.com/company/" target="_blank"><img src="images/linkedin.png"></a></li>
            <li><a href="https://www.instagram.com/" target="_blank"><img src="images/insta.png"></a></li>
          </ul>
        </div>
      </div>

      <div class="container-fluid visible-xs" style="text-align: center;">
        <div class="col-sm-3">
          <p style="color: #fff;">8 Chiromo Lane, NRB<br>
              <a href="tel: +254714001177" target="_blank" style="color: #fff;">
                  0714-001-177 / 0780-001-177</a> <br>
              <a href="mailto: carhire@pridedrive.com" target="_blank" style="color: #fff;">carhire@pridedrive.com </a>
          </p>
        </div>
        <div class="col-sm-3">
          <ul>
              <li><a href="https://www.facebook.com/" target="_blank"><img src="images/fb.png"></a></li>
              <li><a href="https://twitter.com/" target="_blank"><img src="images/twitter.png"></a></li>
              <li><a href="https://www.linkedin.com/company/" target="_blank"><img src="images/linkedin.png"></a></li>
              <li><a href="https://www.instagram.com/" target="_blank"><img src="images/insta.png"></a></li>
          </ul>
        </div>

        <div class="col-sm-3">
          <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; pridedrive.com | All rights reserved.</p>
        </div>
      </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
  </body>
</html>