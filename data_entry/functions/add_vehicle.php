<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$Email = $_SESSION['Email'];

if(isset($_POST['submit'])){


    // Vehicle Details
    $compId = isset($_POST['compId']) ? $_POST['compId'] : "";
    $VehicleReg = isset($_POST['VehicleReg']) ? $_POST['VehicleReg'] : "";
    $VehicleManufacturer = isset($_POST['VehicleManufacturer']) ? $_POST['VehicleManufacturer'] : "";
    $VehicleModel = isset($_POST['VehicleModel']) ? $_POST['VehicleModel'] : "";
    $VehicleBodyType = isset($_POST['VehicleBodyType']) ? $_POST['VehicleBodyType'] : "";
    $VehicleColor = isset($_POST['VehicleColor']) ? $_POST['VehicleColor'] : "";
    $VehicleYear = isset($_POST['VehicleYear']) ? $_POST['VehicleYear'] : "";
    $VehicleFuel = isset($_POST['VehicleFuel']) ? $_POST['VehicleFuel'] : "";
    $VehicleTransmission = isset($_POST['VehicleTransmission']) ? $_POST['VehicleTransmission'] : "";
    $VehicleFuelEconomy = isset($_POST['VehicleFuelEconomy']) ? $_POST['VehicleFuelEconomy'] : "";
    $VehicleMaxPassengers = isset($_POST['VehicleMaxPassengers']) ? $_POST['VehicleMaxPassengers'] : "";
    $VehicleEngineCapacity = isset($_POST['VehicleEngineCapacity']) ? $_POST['VehicleEngineCapacity'] : "";
    $VehicleDoors = isset($_POST['VehicleDoors']) ? $_POST['VehicleDoors'] : "";
    $VehicleMileageLimit = isset($_POST['VehicleMileageLimit']) ? $_POST['VehicleMileageLimit'] : "";
    $VehicleLeasePreparation = isset($_POST['VehicleLeasePreparation']) ? $_POST['VehicleLeasePreparation'] : "";

    @$implodeVehicleMoreFeatures = implode(",", $_POST['VehicleMoreFeatures']);
    @$VehicleMoreFeatures = isset($implodeVehicleMoreFeatures) ? $implodeVehicleMoreFeatures : "";

    //$VehicleMoreFeatures = isset($_POST['VehicleMoreFeatures']) ? $_POST['VehicleMoreFeatures'] : "";
    $VehicleAddOns = isset($_POST['VehicleAddOns']) ? $_POST['VehicleAddOns'] : "";
    $VehicleInventoryLocation = isset($_POST['VehicleInventoryLocation']) ? $_POST['VehicleInventoryLocation'] : "";
    $VehicleDailyPrice = isset($_POST['VehicleDailyPrice']) ? $_POST['VehicleDailyPrice'] : "";

    $VehicleRegDate = $database->now_date_only;
    $VehicleRegTime = $database->now_time_only;

    $uploaddir = '../../uploads/company'.$_POST["compId"];
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $imgfolder = $uploaddir."/img";
    if(!file_exists($imgfolder)){
        mkdir($imgfolder,0755);
    }

    $vehicleImg="";
    if($_FILES['vehicleImg']['name']){
        $type = substr($_FILES['vehicleImg']['name'],strrpos($_FILES['vehicleImg']['name'],'.')+1);

        $imgName = str_replace(" ","_",$_POST['VehicleReg'])."image.".$type;
        $uploadImg = $imgfolder."/".$imgName;

        if (move_uploaded_file($_FILES['vehicleImg']['tmp_name'], $uploadImg)){
            $vehicleImg = $imgfolder."/".$imgName;
        } else {
            $err='er-up-img';
            $connection->redirect("../add-vehicles.php?err=".$err);
        }
    }


    $vehiclesCols = array("CompanyId"=>$compId,"VehicleReg"=>$VehicleReg,"VehicleManufacturer"=>$VehicleManufacturer,"VehicleModel"=>$VehicleModel,"VehicleBodyType"=>$VehicleBodyType,"VehicleColor"=>$VehicleColor,"VehicleYear"=>$VehicleYear,"VehicleFuel"=>$VehicleFuel,"VehicleTransmission"=>$VehicleTransmission,"VehicleFuelEconomy"=>$VehicleFuelEconomy,"VehicleMaxPassengers"=>$VehicleMaxPassengers,"VehicleEngineCapacity"=>$VehicleEngineCapacity,"VehicleDoors"=>$VehicleDoors,"VehicleMileageLimit"=>$VehicleMileageLimit,"VehicleLeasePreparation"=>$VehicleLeasePreparation,"VehicleMoreFeatures"=>$VehicleMoreFeatures,"VehicleAddOns"=>$VehicleAddOns,"VehicleInventoryLocation"=>$VehicleInventoryLocation,"VehicleDailyPrice"=>$VehicleDailyPrice,"VehicleImg"=>$vehicleImg,"RegDate"=>$VehicleRegDate,"RegTime"=>$VehicleRegTime);
    $vehiclesTable = "pridedrive_vehicles";
    $insertVehicles = $connection->InsertQuery($vehiclesTable,$vehiclesCols);


    if($insertVehicles == "success"){
        $connection->redirect("../vehicles.php");
    }
    else {
        $connection->redirect("../vehicles.php");
    }
}
?>