<?php
ob_start();
require_once('../config/db.php');

$imgFile="";
$cv="";
$appLetter="";
$qualificationD="";
$idCardD="";
$pinD="";
$nssfD="";
$nhifD="";
$driverLicence="";
$bonafideCert="";

if(isset($_POST['submit'])){
    //Upload the image and documents to the uploads folder.
    $img_name = $_POST['driverImg1'];

    $uploaddir = '../../uploads/company'.$_POST["compId"];
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $imgfolder = $uploaddir."/img";
    if(!file_exists($imgfolder)){
        mkdir($imgfolder,0755);
    }

    if($_FILES['driverImg']['name'])
    {

        $type = substr($_FILES['driverImg']['name'],strrpos($_FILES['driverImg']['name'],'.')+1);

        //$imgName = date("d-m-Y")."-".time()."image.".$type;
        $imgName = $_POST['NSN']."image.".$type;
        $uploadImg = $imgfolder."/".$imgName;

        if (move_uploaded_file($_FILES['driverImg']['tmp_name'], $uploadImg))
        {
            $imgFile = $imgfolder."/".$imgName;
            //echo "Img File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-img';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }
    else if($_FILES['driverImg']['name'] == "" && $img_name != ""){
        $imgFile = $img_name;
    }

    else if($_FILES['driverImg']['name'] == "" && $img_name == ""){
        $imgFile = "";
    }



    //Check & upload CV
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $docfolder = $uploaddir."/document";
    if(!file_exists($docfolder)){
        mkdir($docfolder,0755);
    }

    $vehicleInspection_name = "";
    $vehicleInspectionFile = "";

    /*if($_FILES['vehicleInspectionFile']['name'])
    {
        $type = substr($_FILES['vehicleInspectionFile']['name'],strrpos($_FILES['vehicleInspectionFile']['name'],'.')+1);

        $uploadCV = date("d-m-Y")."-".time()."vehicleInspectionFile.".$type;
        $uploadDoc = $docfolder."/".$uploadCV;

        if (move_uploaded_file($_FILES['vehicleInspectionFile']['tmp_name'], $uploadDoc))
        {
            $vehicleInspectionFile =  $docfolder."/".$uploadCV;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-cv';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }
    else if($_FILES['vehicleInspectionFile']['name'] == "" && $vehicleInspection_name != ""){
        $vehicleInspectionFile = $vehicleInspection_name;
    }

    else if($_FILES['vehicleInspectionFile']['name'] == "" && $vehicleInspection_name == ""){
        $vehicleInspectionFile = "";
    }*/


    //Check & upload CV
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $docfolder = $uploaddir."/document";
    if(!file_exists($docfolder)){
        mkdir($docfolder,0755);
    }

    $cv_name = $_POST['cv1'];
    if($_FILES['cv']['name'])
    {
        $type = substr($_FILES['cv']['name'],strrpos($_FILES['cv']['name'],'.')+1);

        $uploadCV = $_POST['NSN']."cv.".$type;
        $uploadDoc = $docfolder."/".$uploadCV;

        if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadDoc))
        {
            $cv =  $docfolder."/".$uploadCV;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-cv';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['cv']['name'] == "" && $cv_name != ""){
        $cv = $cv_name;
    }

    else if($_FILES['cv']['name'] == "" && $cv_name == ""){
        $cv = "";
    }


    //Check & upload CV
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $docfolder = $uploaddir."/document";
    if(!file_exists($docfolder)){
        mkdir($docfolder,0755);
    }

    $ContractFile_name = $_POST['ContractFile1'];
    if($_FILES['ContractFile']['name'])
    {
        $type = substr($_FILES['ContractFile']['name'],strrpos($_FILES['ContractFile']['name'],'.')+1);

        $uploadContractFile = $_POST['NSN']."ContractFile.".$type;
        $uploadDoc = $docfolder."/".$uploadContractFile;

        if (move_uploaded_file($_FILES['ContractFile']['tmp_name'], $uploadDoc))
        {
            $ContractFile =  $docfolder."/".$uploadContractFile;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-ContractFile';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['ContractFile']['name'] == "" && $ContractFile_name != ""){
        $ContractFile = $ContractFile_name;
    }

    else if($_FILES['ContractFile']['name'] == "" && $ContractFile_name == ""){
        $ContractFile = "";
    }


    //Check & upload CV
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $docfolder = $uploaddir."/document";
    if(!file_exists($docfolder)){
        mkdir($docfolder,0755);
    }

    $insuranceImg_name = "";
    $insuranceImg = "";



    //Check & upload application letter
    $appLetter_name = $_POST['appLetter1'];
    if($_FILES['appLetter']['name'])
    {
        $type = substr($_FILES['appLetter']['name'],strrpos($_FILES['appLetter']['name'],'.')+1);

        $uploadAppLetter = $_POST['NSN']."appLetter.".$type;
        $uploadDoc = $docfolder."/".$uploadAppLetter;

        if (move_uploaded_file($_FILES['appLetter']['tmp_name'], $uploadDoc))
        {
            $appLetter = $docfolder."/".$uploadAppLetter;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-al';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['appLetter']['name'] == "" && $appLetter_name != ""){
        $appLetter = $appLetter_name;
    }

    else if($_FILES['appLetter']['name'] == "" && $appLetter_name == ""){
        $appLetter = "";
    }


    //Check & upload qualification documents
    $qualificationD_name = $_POST['qualificationD1'];

    if($_FILES['qualificationD']['name'])
    {
        $type = substr($_FILES['qualificationD']['name'],strrpos($_FILES['qualificationD']['name'],'.')+1);

        $uploadQ = $_POST['NSN']."qualificationD.".$type;
        $uploadDoc = $docfolder."/".$uploadQ;

        if (move_uploaded_file($_FILES['qualificationD']['tmp_name'], $uploadDoc))
        {
            $qualificationD = $docfolder."/".$uploadQ;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-qa';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['qualificationD']['name'] == "" && $qualificationD_name != ""){
        $qualificationD = $qualificationD_name;
    }

    else if($_FILES['qualificationD']['name'] == "" && $qualificationD_name == ""){
        $qualificationD = "";
    }


    //Check & upload ad card certificate
    $idCardD_name = $_POST['idCardD1'];

    if($_FILES['idCardD']['name'])
    {
        $type = substr($_FILES['idCardD']['name'],strrpos($_FILES['idCardD']['name'],'.')+1);

        $uploadId = $_POST['NSN']."idCardD.".$type;
        $uploadDoc = $docfolder."/".$uploadId;

        if (move_uploaded_file($_FILES['idCardD']['tmp_name'], $uploadDoc))
        {
            $idCardD = $docfolder."/".$uploadId;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-id';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['idCardD']['name'] == "" && $idCardD_name != ""){
        $idCardD = $idCardD_name;
    }

    else if($_FILES['idCardD']['name'] == "" && $idCardD_name == ""){
        $idCardD = "";
    }


    //Check & upload pin certificate
    $pinD_name = $_POST['pinD1'];

    if($_FILES['pinD']['name'])
    {
        $type = substr($_FILES['pinD']['name'],strrpos($_FILES['pinD']['name'],'.')+1);

        $uploadPin = $_POST['NSN']."pinD.".$type;
        $uploadDoc = $docfolder."/".$uploadPin;

        if(move_uploaded_file($_FILES['pinD']['tmp_name'], $uploadDoc))
        {
            $pinD = $docfolder."/".$uploadPin;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-pin';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['pinD']['name'] == "" && $pinD_name != ""){
        $pinD = $pinD_name;
    }

    else if($_FILES['pinD']['name'] == "" && $pinD_name == ""){
        $pinD = "";
    }


    //Check & upload nssf document
    $nssfD_name = $_POST['nssfD1'];

    if($_FILES['nssfD']['name'])
    {
        $type = substr($_FILES['nssfD']['name'],strrpos($_FILES['nssfD']['name'],'.')+1);

        $uploadNSSF = $_POST['NSN']."nssfD.".$type;
        $uploadDoc = $docfolder."/".$uploadNSSF;

        if(move_uploaded_file($_FILES['nssfD']['tmp_name'], $uploadDoc))
        {
            $nssfD = $docfolder."/".$uploadNSSF;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-ns';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['nssfD']['name'] == "" && $nssfD_name != ""){
        $nssfD = $nssfD_name;
    }

    else if($_FILES['nssfD']['name'] == "" && $nssfD_name == ""){
        $nssfD = "";
    }


    //Check & upload nhif
    $nhifD_name = $_POST['nhifD1'];

    if($_FILES['nhifD']['name'])
    {
        $type = substr($_FILES['nhifD']['name'],strrpos($_FILES['nhifD']['name'],'.')+1);

        $uploadNH = $_POST['NSN']."nhifD.".$type;
        $uploadDoc = $docfolder."/".$uploadNH;

        if(move_uploaded_file($_FILES['nhifD']['tmp_name'], $uploadDoc))
        {
            $nhifD = $docfolder."/".$uploadNH;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-NH';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['nhifD']['name'] == "" && $nhifD_name != ""){
        $nhifD = $nhifD_name;
    }

    else if($_FILES['nhifD']['name'] == "" && $nhifD_name == ""){
        $nhifD = "";
    }


    //Check & upload driving licence
    $licence_name = $_POST['driverLicence1'];

    if($_FILES['driverLicence']['name'])
    {
        $type = substr($_FILES['driverLicence']['name'],strrpos($_FILES['driverLicence']['name'],'.')+1);

        $uploadDL = $_POST['NSN']."driverLicence.".$type;
        $uploadDoc = $docfolder."/".$uploadDL;

        if (move_uploaded_file($_FILES['driverLicence']['tmp_name'], $uploadDoc))
        {
            $driverLicence = $docfolder."/".$uploadDL;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-dl';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['driverLicence']['name'] == "" && $licence_name != ""){
        $driverLicence = $licence_name;
    }

    else if($_FILES['driverLicence']['name'] == "" && $licence_name == ""){
        $driverLicence = "";
    }


    //Check & upload character certificate
    $bonafideCert_name = $_POST['bonafideCert1'];

    if($_FILES['bonafideCert']['name'])
    {
        $type = substr($_FILES['bonafideCert']['name'],strrpos($_FILES['bonafideCert']['name'],'.')+1);

        $uploadBC = $_POST['NSN']."bonafideCert.".$type;
        $uploadDoc = $docfolder."/".$uploadBC;

        if (move_uploaded_file($_FILES['bonafideCert']['tmp_name'], $uploadDoc))
        {
            $bonafideCert = $docfolder."/".$uploadBC;
            //echo "File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-bonCert';
            $connection->redirect("../edit-drivers.php?err=".$err);
        }
    }
    else if($_FILES['bonafideCert']['name'] == "" && $bonafideCert_name != ""){
        $bonafideCert = $bonafideCert_name;
    }

    else if($_FILES['bonafideCert']['name'] == "" && $bonafideCert_name == ""){
        $bonafideCert = "";
    }

    // Driver Details
    $edocs_id = $_POST['edocs_id'];
    $driver_id = $_POST['driver_id'];
    $compId = $_POST['compId'];
    $fname = $_POST['fname'];
    $mname = $_POST['mname'];
    $lname = $_POST['lname'];
    $working_status = $_POST['workingStatus'];
    $gender = $_POST['gender'];
    $NSN = $_POST['NSN'];
    $implodeVehicle = implode(",",$_POST['vehiclelist']);
    $vehicle = isset($implodeVehicle) ? $implodeVehicle : "";

    /*$insuranceType = isset($_POST['insuranceType']) ? $_POST['insuranceType'] : "";
    $insuranceRefNum = isset($_POST['insuranceRefNum']) ? $_POST['insuranceRefNum'] : "";
    $insuranceExpDate = $_POST['insuranceExpDate'];*/

    $insuranceType = "";
    $insuranceRefNum = "";
    $insuranceExpDate = "";

    $dlNum = $_POST['dlNum'];
    $contactNo = $_POST['contact1'];
    $altContactNo = $_POST['contact2'];
    $address = $_POST['address1'];
    $address2= $_POST['address2'];
    $city = $_POST['city'];
    $pobox = $_POST['pobox'];
    $postalcode = $_POST['postalcode'];
    $email = $_POST['email'];
    $grossSal = $_POST['grossSal'];
    $CreatedDate = date("Y-m-d");

    $isLicensePSV = isset($_POST['isLicensePSV']) ? $_POST['isLicensePSV'] : "";
    $vehiType = isset($_POST['Vehicle_Type']) ? $_POST['Vehicle_Type'] : "";
    $vehiPlatNumber = $_POST['vehiPlatNumber'];
    /*$vehicleInspection = isset($_POST['vehicleInspection']) ? $_POST['vehicleInspection'] : "";*/
    $vehicleInspection = "";
    $department = $_POST['department'];

    /*$inspectionExpDate = $_POST['inspectionExpDate'];*/
    $inspectionExpDate = "";
    $issueDate = $_POST['issueDate'];
    $expiryDate = $_POST['expiryDate'];

    // $inspectionExpDate = date("Y/m/d",strtotime($_POST['inspectionExpDate']));
    // if($inspectionExpDate == ""){
    // 	$inspectionExpDate = "NULL";
    // }
    // elseif($inspectionExpDate != ""){
    // 	$inspectionExpDate;
    // }

    // $issueDate = date("Y/m/d",strtotime($_POST['issueDate']));
    // if($_POST['issueDate'] == ""){
    // 	$issueDate = "NULL";
    // }
    // elseif($_POST['issueDate'] != ""){
    // 	$issueDate;
    // }
    // $expiryDate = date("Y/m/d",strtotime($_POST['expiryDate']));
    // if($_POST['expiryDate'] == ""){
    // 	$expiryDate = "NULL";
    // }
    // elseif($_POST['expiryDate'] != ""){
    // 	$expiryDate;
    // }

    //Employment Details
    $empdt_id = $_POST['empdt_id'];
    $empId = $_POST['empId'];
    $deptId = $_POST['deptId'];
    $leave = $_POST['leave'];

    $commenced_startDate = $_POST['commenced_startDate'];
    $startDate = $_POST['startDate'];
    //$endDate = $_POST['endDate'];

    $end_date = new DateTime($startDate);
    $end_date->modify("+365 day");
    $endDate = $end_date->format("Y-m-d");

    // $commenced_startDate = date("Y/m/d",strtotime($_POST['commenced_startDate']));
    // if($_POST['commenced_startDate'] == ""){
    // 	$commenced_startDate = "NULL";
    // }
    // elseif($_POST['commenced_startDate'] != ""){
    // 	$commenced_startDate;
    // }
    // $startDate = date("Y/m/d",strtotime($_POST['startDate']));
    // if($_POST['startDate'] == ""){
    // 	$startDate = "NULL";
    // }
    // elseif($_POST['startDate'] != ""){
    // 	$startDate;
    // }
    // $endDate = date("Y/m/d",strtotime($_POST['endDate']));
    // if($_POST['endDate'] == ""){
    // 	$endDate = "NULL";
    // }
    // elseif($_POST['endDate'] != ""){
    // 	$endDate;
    // }

    // Driver Port Pass
    /*$driver_port_number = isset($_POST['driver_port_number']) ? $_POST['driver_port_number'] : "";
    $driver_port_expire_date = $_POST['driver_port_expire_date'];*/

    $driver_port_number = "";
    $driver_port_expire_date = "";


    // Vehicle Port Pass
    /*$vehicle_port_number = isset($_POST['vehicle_port_number']) ? $_POST['vehicle_port_number'] : "";
    $vehicle_port_expire_date = $_POST['vehicle_port_expire_date'];*/

    $vehicle_port_number = "";
    $vehicle_port_expire_date = "";

    //Kin data
    $kinname = implode(",", $_POST['kinname']);
    $kincontact = implode(",", $_POST['kincontact']);
    $kin_email = implode(",", $_POST['kin_email']);
    $kinrelation = implode(",", $_POST['kinrelation']);
    $kinnsn = implode(",", $_POST['kinnsn']);
    $kinadd = implode(",", $_POST['kinadd']);

    $expkinname = explode(",", $kinname);
    $expkincontact = explode(",", $kincontact);
    $expkin_email = explode(",", $kin_email);
    $expkinrelation = explode(",", $kinrelation);
    $expkinnsn = explode(",", $kinnsn);
    $expkinadd = explode(",", $kinadd);


    //Employee data
    $qualification = $_POST['qualification'];
    $idCard = $_POST['idCard'];
    $pin = $_POST['pin'];
    // $nssf = $_POST['nssf'];
    // $nhifNo = $_POST['nhifNo'];


    //Update data in Drivers table
    $driverCols = array("drv_NSN"=>$NSN,"drv_fname"=>$fname,"drv_lname"=>$lname,"gender"=>$gender,"drv_mname"=>$mname,"drv_img"=>$imgFile,"drv_contact_no"=>$contactNo,"drv_alt_contact_no"=>$altContactNo, "drv_address"=>$address,"drv_address2"=>$address2, "drv_city"=>$city,"drv_pobox"=>$pobox,"drv_postal"=>$postalcode,"drv_email"=>$email,"logis_drivers_vehicle_class"=>$vehicle,"drv_driving_license_no"=>$dlNum,"drv_curr_comp_id"=>$compId,"drv_isActive"=>"Active","gross_salary"=>$grossSal,"drv_dl_issue_date"=>$issueDate,"drv_dl_expiry_date"=>$expiryDate,"drv_created_on"=>$CreatedDate,"driver_work_status"=>$working_status,"insurance_type"=>$insuranceType,"insurance_ref_number"=>$insuranceRefNum,"insurance_exp_date"=>$insuranceExpDate,'isLicensePSV'=>$isLicensePSV,'vehicleType'=>$vehiType,'vehiPlatNumber'=>$vehiPlatNumber,'vehicleInspection'=>$vehicleInspection,'vehicleInspectionFile'=>$vehicleInspectionFile,"vehicleInspection_date"=>$inspectionExpDate,"insurance_img"=>$insuranceImg,"driver_port_number"=>$driver_port_number,"driver_port_expire_date"=>$driver_port_expire_date,"vehicle_port_number"=>$vehicle_port_number,"vehicle_port_expire_date"=>$vehicle_port_expire_date);
    $driverTable = "logis_drivers_master";
    $condition = "drv_id='".$driver_id."'";
    $updateDriver = $connection->UpdateQuery($driverTable,$driverCols,$condition);


    //Insert Data into Kin table
    $deleKin=$conn->prepare("DELETE from logis_drivers_kin where drv_nsnId = '$NSN'");
    $deleKin->execute();

    for($i=0; $i<count($expkinname); $i++){

        $expkinname1 = $expkinname[$i];
        $expkincontact1 = $expkincontact[$i];
        $expkin_email1 = $expkin_email[$i];
        $expkinrelation1 = $expkinrelation[$i];
        $expkinnsn1 = $expkinnsn[$i];
        $expkinadd1 = $expkinadd[$i];

        if($expkinname1 != ""){

            $kinCols = array("drivers_kin_name"=>$expkinname1,"drv_nsnId"=>$NSN,"drv_comp_id"=>$compId,"drivers_kin_email"=>$expkin_email1,"drivers_kin_contact"=>$expkincontact1,"drivers_kin_nsn"=>$expkinnsn1,"drivers_kin_relation"=>$expkinrelation1,"drivers_kin_address"=>$expkinadd1);
            $kinTable = "logis_drivers_kin";
            $insertKin = $connection->InsertQuery($kinTable,$kinCols);
        }
    }

    //Insert data in Employee Details table
    $empCols = array("empdt_NSN"=>$NSN,"empdt_emp_name"=>$fname.' '.$mname.' '.$lname,"empdt_commenced_date"=>$commenced_startDate,"empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate,"empdt_dept_id"=>$deptId, "empdt_comp_id"=>$compId,"empdt_Idcopy"=>$idCard,"empdt_pinCert"=>$pin,"empdt_isValid"=>"Active","empdt_annual_leaves"=>$leave,"empdt_qualification"=>$qualification,"department"=>$department,"contract_file"=>$ContractFile);
    $empTable = "logis_employee_details";
    $condition = "empdt_id='".$empdt_id."'";
    $updateEmp = $connection->UpdateQuery($empTable,$empCols,$condition);


    //Insert data in Employee Documents table
    $docCols = array("edocs_nsn"=>$NSN,"edocs_comp_id"=>$compId,"edocs_cv"=>$cv,"edocs_app_letter"=>$appLetter,
        "edocs_qualification"=>$qualificationD, "edocs_id_card"=>$idCardD,"edocs_pin_cert"=>$pinD,
        "edocs_nssf"=>$nssfD,"edocs_nhif"=>$nhifD,"edocs_dl_copy"=>$driverLicence,"edocs_conduct_cert"=>$bonafideCert);
    $docTable = "logis_emp_docs";
    $condition = "edocs_id='".$edocs_id."'";
    $updateDoc = $connection->UpdateQuery($docTable,$docCols,$condition);


    if($updateDriver=="success" && $updateEmp=="success" && $updateDoc=="success"){
        $connection->redirect("../drivers.php?ref=success");
    }
    else {
        $connection->redirect("../edit-drivers.php?ref=error");
    }
}