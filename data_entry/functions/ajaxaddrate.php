<?php
	require_once('../config/db.php');
	session_start();

	@$Email = $_SESSION['Email'];
	if($Email == ""){
		$connection->redirect('../../index.php');
	}

	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] :"";

	//	UPDATE WORK EXP.  //
	if($type == "updateWorkingStatus"){
		$working_status = $_REQUEST['working_status'];
		$drv_id = $_REQUEST['drv_id'];


		$checkPrvStatus = $conn->prepare("SELECT * from logis_drivers_master where drv_id = '{$drv_id}'");
		$checkPrvStatus->execute();
		$checkPrvStatusRow= $checkPrvStatus->fetch();

		if($checkPrvStatusRow['driver_work_status'] != $working_status){

			$lastCols = array("driver_work_status"=>$working_status);
			$lastTable = "logis_drivers_master";
			$condition = "drv_id='".$drv_id."'";
			$updatework = $connection->UpdateQuery($lastTable,$lastCols,$condition);


			if($working_status == "notworking"){

				$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
				$getCompDetails->execute();
				$getCompDetailsRow = $getCompDetails->fetch();

				$driver_limit = $getCompDetailsRow['driver_limit'] + 1;
				$working_driver = $getCompDetailsRow['working_driver'] - 1;


				$updateWorkDrivLimit = $conn->prepare("UPDATE logis_company_subadmin set 
					driver_limit = '$driver_limit',working_driver = '$working_driver' where Email = '{$Email}'");
				$updateWorkDrivLimit->execute();
			}
			elseif($working_status == "working"){

				$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
				$getCompDetails->execute();
				$getCompDetailsRow = $getCompDetails->fetch();

				$driver_limit = $getCompDetailsRow['driver_limit'] - 1;
				$working_driver = $getCompDetailsRow['working_driver'] + 1;


				$updateWorkDrivLimit = $conn->prepare("UPDATE logis_company_subadmin set 
					driver_limit = '$driver_limit',working_driver = '$working_driver' where Email = '{$Email}'");
				$updateWorkDrivLimit->execute();
			}
		}
	}


	//	CONTRACT RENEW OPTION  //
	if($type == "sc"){

		$nsn_id = isset($_POST['nsn_id']) ? $_POST['nsn_id'] : "";
		$empdt_id = isset($_POST['empdt_id']) ? $_POST['empdt_id'] : "";
		$comp_id = isset($_POST['comp_id']) ? $_POST['comp_id'] : "";
		$startDate = isset($_POST['startDate']) ? $_POST['startDate'] : "";
		$endDate = isset($_POST['endDate']) ? $_POST['endDate'] : "";
		// $sc = isset($_POST['sc']) ? $_POST['sc'] : "";

		// $stmt = $conn->prepare("SELECT * from logis_employee_details where empdt_NSN = '{$nsn_id}' and empdt_comp_id = '{$comp_id}' order by empdt_id desc");
		// $stmt->execute();
		// $nsnresult = $stmt->fetch(PDO::FETCH_ASSOC);

		// $empdt_id = $nsnresult['empdt_id'];
		// $empID = $nsnresult['empdt_empID'];
		// $empNSN = $nsnresult['empdt_NSN'];
		// $empName = $nsnresult['empdt_emp_name'];
		// $empDpt = $nsnresult['empdt_dept_id'];
		// $empWork = $nsnresult['empdt_work_status'];
		// $empLeaves = $nsnresult['empdt_annual_leaves'];
		// $empComp = $nsnresult['empdt_comp_id'];


		$lastCols = array("empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate);
		$lastTable = "logis_employee_details";
		$condition = "empdt_id='".$empdt_id."'";
		$updateContract = $connection->UpdateQuery($lastTable,$lastCols,$condition);

		// if($empNSN == $nsn_id && $empComp == $comp_id){
		// 	$empCols = array("empdt_empID"=>$empID,"empdt_NSN"=>$nsn_id,"empdt_emp_name"=>$empName,"empdt_dept_id"=>$empDpt,"empdt_work_status"=>$empWork,"empdt_annual_leaves"=>$empLeaves,"empdt_comp_id"=>$comp_id,"empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate,"empdt_isValid"=>"Active","empdt_current_data"=>"Yes");
		// 	$empTable = " logis_employee_details";
		// 	$empFaill=$connection->InsertQuery($empTable,$empCols);
		// }	
	}


	//////////////////////
	//	RATING OPTION  //
	/////////////////////
	// $working = isset($_POST['working_status']) ? $_POST['working_status'] : "";
	// $redflag = isset($_POST['redflag']) ? $_POST['redflag'] : "No";
	// $drv_name = isset($_POST["drv_name"]) ? $_POST['drv_name'] : "";
	// $drv_nsn = isset($_POST['drv_nsn']) ? $_POST['drv_nsn'] : "";
	// $drv_lic = isset($_POST['drv_lic']) ? $_POST['drv_lic'] : "";
	// $comp_id = isset($_POST['comp_id']) ? $_POST['comp_id'] : "";
	// $comp_name = isset($_POST['comp_name']) ? $_POST['comp_name'] : "";
	// $rate = isset($_POST['rt']) ? $_POST['rt'] : "";
	// $hide_rate = isset($_POST['hide_rate']) ? $_POST['hide_rate'] : "";
	// $comment = isset($_POST['comment']) ? $_POST['comment'] : "";

	// if($hide_rate == "hide_rate"){

	// $stmt = $conn->prepare("SELECT * from logis_ratings where rate_drv_NSN = '{$drv_nsn}' and rate_comp_id = '{$comp_id}'");
	// $stmt->execute();
	// $count = $stmt->rowCount();
	// $rateresult = $stmt->fetch(PDO::FETCH_ASSOC);

	// $nsn = $rateresult['rate_drv_NSN'];
	// $com = $rateresult['rate_comp_id'];

	// if($count != 0){
	// 	$rateCols = array("rate_comp_id"=>$comp_id,"rate_comp_name"=>$comp_name,"rate_drv_name"=>$drv_name,"rate_status"=>$working,"rate_dlNum"=>$drv_lic,"rate_ratings"=>$rate,"rate_comments"=>$comment);
	// 	$rateTable = "logis_ratings";
	// 	$condition = "rate_drv_NSN='".$drv_nsn."'";
	// 	$updaterate = $connection->UpdateQuery($rateTable,$rateCols,$condition);
	// }
	// elseif($count == 0){
	// 	$rateCols = array("rate_comp_id"=>$comp_id,"rate_comp_name"=>$comp_name,"rate_drv_name"=>$drv_name,"rate_status"=>$working,"rate_drv_NSN"=>$drv_nsn,"rate_dlNum"=>$drv_lic,"rate_ratings"=>$rate,"rate_comments"=>$comment);
	// 	$rateTable = "logis_ratings";
	// 	$rateFaill=$connection->InsertQuery($rateTable,$rateCols);
	// }

	// 	$redflagcols = array("drv_red_flag"=>$redflag);
	// 	$redflagTable = "logis_drivers_master";
	// 	$condition = "drv_nsn='".$drv_nsn."'";
	// 	$redflagFaill = $connection->UpdateQuery($redflagTable,$redflagcols,$condition);

	// 	$workCols = array("empdt_work_status"=>$working);
	// 	$workTable = "logis_employee_details";
	// 	$condition = "empdt_NSN='".$drv_nsn."'";
	// 	$updatework = $connection->UpdateQuery($workTable,$workCols,$condition);
	
	// 	echo "Rated Successfully";
	// }


	// LEAVES OPTION //
	if($type == "leav"){
		$leavesNSN = isset($_POST['leavesNSN']) ? $_POST['leavesNSN'] : "";
		$leaves_drv_comp = isset($_POST['leavesCompId']) ? $_POST['leavesCompId'] : "";
		$from_leaves = isset($_POST['from_leaves']) ? $_POST['from_leaves'] : "";
		$to_leaves = isset($_POST['to_leaves']) ? $_POST['to_leaves'] : "";
		$total_leaves = isset($_POST['total_leaves']) ? $_POST['total_leaves'] : "";
		$leave_comment = isset($_POST['leave_comment']) ? $_POST['leave_comment'] : "";

			$leaveCols = array("leaves_nsn"=>$leavesNSN,"leaves_drv_comp"=>$leaves_drv_comp,"leaves_from_date"=>$from_leaves,"leaves_to_date"=>$to_leaves,"leaves_comment"=>$leave_comment,"leaves_total"=>$total_leaves);
			$leaveTable = " logis_leaves";
			$LeaveInsert = $connection->InsertQuery($leaveTable,$leaveCols);
		}
	?>
<?php

// if($type == "removeRedFlag"){
// 	$removeRed = $_POST['removeRed'];
// 	$drv_nsn = $_POST['drv_nsn'];

// 	$removeredcols = array("drv_red_flag"=>$removeRed);
// 	$removeredTable = "logis_drivers_master";
// 	$condition = "drv_nsn='".$drv_nsn."'";
// 	$removeredFaill = $connection->UpdateQuery($removeredTable,$removeredcols,$condition);
// }

?>