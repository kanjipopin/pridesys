<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$Email = $_SESSION['Email'];


if(isset($_POST['submit'])){

    // Driver Details
    $compId = isset($_POST['compId']) ? $_POST['compId'] : "";
    $fname = isset($_POST['fname']) ? $_POST['fname'] : "";
    $mname = isset($_POST['mname']) ? $_POST['mname'] : "";
    $lname = isset($_POST['lname']) ? $_POST['lname'] : "";
    $gender = isset($_POST['gender']) ? $_POST['gender'] : "";
    $NSN = isset($_POST['NSN']) ? $_POST['NSN'] : "";
    @$implodeVehicle = implode(",", $_POST['vehiclelist']);
    @$vehicle = isset($implodeVehicle) ? $implodeVehicle : "";
    /*$insuranceType = isset($_POST['insuranceType']) ? $_POST['insuranceType'] : "";
    $insuranceRefNum = isset($_POST['insuranceRefNum']) ? $_POST['insuranceRefNum'] : "";
    $insuranceExpDate = $_POST['insuranceExpDate'];*/

    $insuranceType = "";
    $insuranceRefNum = "";
    $insuranceExpDate = "";

    $dlNum = isset($_POST['dlNum']) ? $_POST['dlNum'] : "";
    $contactNo = isset($_POST['contact1']) ? $_POST['contact1'] : "";
    $altContactNo = isset($_POST['contact2']) ? $_POST['contact2'] : "";
    $address = isset($_POST['address1']) ? $_POST['address1'] : "";
    $address2= isset($_POST['address2']) ? $_POST['address2'] : "";
    $city = isset($_POST['city']) ? $_POST['city'] : "";
    $pobox = isset($_POST['pobox']) ? $_POST['pobox'] : "";
    $postalcode = isset($_POST['postalcode']) ? $_POST['postalcode'] : "";
    $email = isset($_POST['email']) ? $_POST['email'] : "";
    $grossSal = isset($_POST['grossSal']) ? $_POST['grossSal'] : "";
    $CreatedDate = date("Y-m-d");


    $isLicensePSV = isset($_POST['isLicensePSV']) ? $_POST['isLicensePSV'] : "";
    $vehiType = isset($_POST['Vehicle_Type']) ? $_POST['Vehicle_Type'] : "";
    $vehiPlatNumber = isset($_POST['vehiPlatNumber']) ? $_POST['vehiPlatNumber'] : "";
    /*$vehicleInspection = isset($_POST['vehicleInspection']) ? $_POST['vehicleInspection'] : "";*/
    $vehicleInspection = "";
    $department = isset($_POST['department']) ? $_POST['department'] : "";


    /*$inspectionExpDate = $_POST['inspectionExpDate'];*/
    $inspectionExpDate = "";
    $issueDate = $_POST['issueDate'];
    $expiryDate = $_POST['expiryDate'];


    //Employment Details
    $empId = $_POST['empId'];
    $deptId = $_POST['deptId'];
    $leave = $_POST['leave'];

    $commenced_startDate = $_POST['commenced_startDate'];
    $startDate = $_POST['startDate'];
    //$eDate = $_POST['endDate'];

    $end_date = new DateTime($startDate);
    $end_date->modify("+365 day");
    $endDate = $end_date->format("Y-m-d");


    // Driver Port Pass
    /*$driver_port_number = isset($_POST['driver_port_number']) ? $_POST['driver_port_number'] : "";
    $driver_port_expire_date = $_POST['driver_port_expire_date'];*/

    $driver_port_number = "";
    $driver_port_expire_date = "";


    // Vehicle Port Pass
    /*$vehicle_port_number = isset($_POST['vehicle_port_number']) ? $_POST['vehicle_port_number'] : "";
    $vehicle_port_expire_date = $_POST['vehicle_port_expire_date'];*/

    $vehicle_port_number = "";
    $vehicle_port_expire_date = "";

    //Kin data
    @$kinname = implode(",", $_POST['kinname']);
    @$kincontact = implode(",", $_POST['kincontact']);
    @$kin_email = implode(",", $_POST['kin_email']);
    @$kinrelation = implode(",", $_POST['kinrelation']);
    @$kinnsn = implode(",", $_POST['kinnsn']);
    @$kinadd = implode(",", $_POST['kinadd']);

    @$expkinname = explode(",", $kinname);
    @$expkincontact = explode(",", $kincontact);
    @$expkin_email = explode(",", $kin_email);
    @$expkinrelation = explode(",", $kinrelation);
    @$expkinnsn = explode(",", $kinnsn);
    @$expkinadd = explode(",", $kinadd);

    //Employee data
    $qualification = $_POST['qualification'];
    $idCard = $_POST['idCard'];
    $pin = $_POST['pin'];
    // $nssf = $_POST['nssf'];
    // $nhifNo = $_POST['nhifNo'];


    $uploaddir = '../../uploads/company'.$_POST["compId"];
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $imgfolder = $uploaddir."/img";
    if(!file_exists($imgfolder)){
        mkdir($imgfolder,0755);
    }

    $imgFile="";
    if($_FILES['driverImg']['name']){
        $type = substr($_FILES['driverImg']['name'],strrpos($_FILES['driverImg']['name'],'.')+1);

        $imgName = $_POST['NSN']."image.".$type;
        $uploadImg = $imgfolder."/".$imgName;

        if (move_uploaded_file($_FILES['driverImg']['tmp_name'], $uploadImg)){
            $imgFile = $imgfolder."/".$imgName;
        } else {
            $err='er-up-img';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }


    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $docfolder = $uploaddir."/document";
    if(!file_exists($docfolder)){
        mkdir($docfolder,0755);
    }

    //Check & upload vehicle Inspection
    $vehicleInspectionFile = "";
    /*if($_FILES['vehicleInspectionFile']['name']){
        $type = substr($_FILES['vehicleInspectionFile']['name'],strrpos($_FILES['vehicleInspectionFile']['name'],'.')+1);

        $uploadDL = date("d-m-Y")."-".time()."vehicleInspectionFile.".$type;
        $uploadDoc = $docfolder."/".$uploadDL;

        if (move_uploaded_file($_FILES['vehicleInspectionFile']['tmp_name'], $uploadDoc))
        {
            $vehicleInspectionFile = $docfolder."/".$uploadDL;
        } else {
            $err='er-up-dl';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }*/

    //Check & upload Insurance Image
    $ContractFile = "";
    if($_FILES['ContractFile']['name']){
        $type = substr($_FILES['ContractFile']['name'],strrpos($_FILES['ContractFile']['name'],'.')+1);

        $uploadDL = $_POST['NSN']."ContractFile.".$type;
        $uploadDoc = $docfolder."/".$uploadDL;

        if (move_uploaded_file($_FILES['ContractFile']['tmp_name'], $uploadDoc))
        {
            $ContractFile = $docfolder."/".$uploadDL;
        } else {
            $err='er-up-dl';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload Insurance Image
    $insuranceImg = "";
    if($_FILES['insuranceImg']['name']){
        $type = substr($_FILES['insuranceImg']['name'],strrpos($_FILES['insuranceImg']['name'],'.')+1);

        $uploadDL = $_POST['NSN']."insuranceImg.".$type;
        $uploadDoc = $docfolder."/".$uploadDL;

        if (move_uploaded_file($_FILES['insuranceImg']['tmp_name'], $uploadDoc))
        {
            $insuranceImg = $docfolder."/".$uploadDL;
        } else {
            $err='er-up-dl';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload driving licence
    $driverLicence = "";
    if($_FILES['driverLicence']['name']){
        $type = substr($_FILES['driverLicence']['name'],strrpos($_FILES['driverLicence']['name'],'.')+1);

        $uploadDL = $_POST['NSN']."driverLicence.".$type;
        $uploadDoc = $docfolder."/".$uploadDL;

        if (move_uploaded_file($_FILES['driverLicence']['tmp_name'], $uploadDoc))
        {
            $driverLicence = $docfolder."/".$uploadDL;
        } else {
            $err='er-up-dl';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload CV
    $cv = "";
    if($_FILES['cv']['name']){
        $type = substr($_FILES['cv']['name'],strrpos($_FILES['cv']['name'],'.')+1);

        $uploadCV = $_POST['NSN']."cv.".$type;
        $uploadDoc = $docfolder."/".$uploadCV;

        if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadDoc))
        {
            $cv =  $docfolder."/".$uploadCV;
        } else {
            $err='er-up-cv';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload application letter
    $appLetter="";
    if($_FILES['appLetter']['name']){
        $type = substr($_FILES['appLetter']['name'],strrpos($_FILES['appLetter']['name'],'.')+1);

        $uploadAppLetter = $_POST['NSN']."appLetter.".$type;
        $uploadDoc = $docfolder."/".$uploadAppLetter;

        if (move_uploaded_file($_FILES['appLetter']['tmp_name'], $uploadDoc))
        {
            $appLetter = $docfolder."/".$uploadAppLetter;
        } else {
            $err='er-up-al';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload qualification documents
    $qualificationD="";
    if($_FILES['qualificationD']['name']){
        $type = substr($_FILES['qualificationD']['name'],strrpos($_FILES['qualificationD']['name'],'.')+1);

        $uploadQ = $_POST['NSN']."qualificationD.".$type;
        $uploadDoc = $docfolder."/".$uploadQ;

        if (move_uploaded_file($_FILES['qualificationD']['tmp_name'], $uploadDoc))
        {
            $qualificationD = $docfolder."/".$uploadQ;
        } else {
            $err='er-up-qa';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload ad card certificate
    $idCardD="";
    if($_FILES['idCardD']['name']){
        $type = substr($_FILES['idCardD']['name'],strrpos($_FILES['idCardD']['name'],'.')+1);

        $uploadId = $_POST['NSN']."idCardD.".$type;
        $uploadDoc = $docfolder."/".$uploadId;

        if (move_uploaded_file($_FILES['idCardD']['tmp_name'], $uploadDoc))
        {
            $idCardD = $docfolder."/".$uploadId;
        } else {
            $err='er-up-id';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload pin certificate
    $pinD="";
    if($_FILES['pinD']['name']){
        $type = substr($_FILES['pinD']['name'],strrpos($_FILES['pinD']['name'],'.')+1);

        $uploadPin = $_POST['NSN']."pinD.".$type;
        $uploadDoc = $docfolder."/".$uploadPin;

        if(move_uploaded_file($_FILES['pinD']['tmp_name'], $uploadDoc))
        {
            $pinD = $docfolder."/".$uploadPin;
        } else {
            $err='er-up-pin';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload nssf document
    $nssfD="";
    if($_FILES['nssfD']['name']){
        $type = substr($_FILES['nssfD']['name'],strrpos($_FILES['nssfD']['name'],'.')+1);

        $uploadNSSF = $_POST['NSN']."nssfD.".$type;
        $uploadDoc = $docfolder."/".$uploadNSSF;

        if(move_uploaded_file($_FILES['nssfD']['tmp_name'], $uploadDoc))
        {
            $nssfD = $docfolder."/".$uploadNSSF;
        } else {
            $err='er-up-ns';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload nhif
    $nhifD="";
    if($_FILES['nhifD']['name']){
        $type = substr($_FILES['nhifD']['name'],strrpos($_FILES['nhifD']['name'],'.')+1);

        $uploadNH = $_POST['NSN']."nhifD.".$type;
        $uploadDoc = $docfolder."/".$uploadNH;

        if(move_uploaded_file($_FILES['nhifD']['tmp_name'], $uploadDoc))
        {
            $nhifD = $docfolder."/".$uploadNH;
        } else {
            $err='er-up-NH';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }

    //Check & upload character certificate
    $bonafideCert = "";
    if($_FILES['bonafideCert']['name']){
        $type = substr($_FILES['bonafideCert']['name'],strrpos($_FILES['bonafideCert']['name'],'.')+1);

        $uploadBC = $_POST['NSN']."bonafideCert.".$type;
        $uploadDoc = $docfolder."/".$uploadBC;

        if(move_uploaded_file($_FILES['bonafideCert']['tmp_name'], $uploadDoc)){
            $bonafideCert = $docfolder."/".$uploadBC;
        } else {
            $err='er-up-bonCert';
            $connection->redirect("../add-drivers.php?err=".$err);
        }
    }


    $driverCols = array("drv_NSN"=>$NSN,"drv_fname"=>$fname,"drv_lname"=>$lname,"gender"=>$gender,"drv_mname"=>$mname,"drv_img"=>$imgFile,"drv_contact_no"=>$contactNo,"drv_alt_contact_no"=>$altContactNo, "drv_address"=>$address,"drv_address2"=>$address2, "drv_city"=>$city,"drv_pobox"=>$pobox,"drv_postal"=>$postalcode,"drv_email"=>$email,"logis_drivers_vehicle_class"=>$vehicle,"drv_driving_license_no"=>$dlNum,"drv_curr_comp_id"=>$compId,"drv_isActive"=>"Active","gross_salary"=>$grossSal,"drv_dl_issue_date"=>$issueDate,"drv_dl_expiry_date"=>$expiryDate,"drv_created_on"=>$CreatedDate,"driver_work_status"=>"working","insurance_type"=>$insuranceType,"insurance_ref_number"=>$insuranceRefNum,"insurance_exp_date"=>$insuranceExpDate,'delete_flag'=>'No','isLicensePSV'=>$isLicensePSV,'vehicleType'=>$vehiType,'vehiPlatNumber'=>$vehiPlatNumber,'vehicleInspection'=>$vehicleInspection,'vehicleInspectionFile'=>$vehicleInspectionFile,"vehicleInspection_date"=>$inspectionExpDate,"insurance_img"=>$insuranceImg,"driver_port_number"=>$driver_port_number,"driver_port_expire_date"=>$driver_port_expire_date,"vehicle_port_number"=>$vehicle_port_number,"vehicle_port_expire_date"=>$vehicle_port_expire_date);
    $driverTable = "logis_drivers_master";
    $insertDriver = $connection->InsertQuery($driverTable,$driverCols);


    //Insert Data into Kin table
    for($i=0; $i<count($expkinname); $i++){

        $expkinname1 = $expkinname[$i];
        $expkincontact1 = $expkincontact[$i];
        $expkin_email1 = $expkin_email[$i];
        $expkinrelation1 = $expkinrelation[$i];
        $expkinnsn1 = $expkinnsn[$i];
        $expkinadd1 = $expkinadd[$i];

        if($expkinname1 != ""){

            $kinCols = array("drivers_kin_name"=>$expkinname1,"drv_nsnId"=>$NSN,"drv_comp_id"=>$compId,"drivers_kin_email"=>$expkin_email1,"drivers_kin_contact"=>$expkincontact1,"drivers_kin_nsn"=>$expkinnsn1,"drivers_kin_relation"=>$expkinrelation1,"drivers_kin_address"=>$expkinadd1);
            $kinTable = "logis_drivers_kin";
            $insertKin = $connection->InsertQuery($kinTable,$kinCols);
        }
    }


    //Insert data in Employee Details table
    $empCols = array("empdt_NSN"=>$NSN,"empdt_emp_name"=>$fname.' '.$mname.' '.$lname,"empdt_commenced_date"=>$commenced_startDate,"empdt_contract_start"=>$startDate,"empdt_contract_end"=>$endDate,"empdt_dept_id"=>$deptId, "empdt_comp_id"=>$compId,"empdt_Idcopy"=>$idCard,"empdt_pinCert"=>$pin,"empdt_isValid"=>"Active","empdt_annual_leaves"=>$leave,"empdt_qualification"=>$qualification,"department"=>$department,"contract_file"=>$ContractFile);
    $empTable ="logis_employee_details";
    $insertEmp = $connection->InsertQuery($empTable,$empCols);


    //Insert data in Employee Documents table
    $docCols = array("edocs_nsn"=>$NSN,"edocs_comp_id"=>$compId,"edocs_cv"=>$cv,"edocs_app_letter"=>$appLetter,
        "edocs_qualification"=>$qualificationD, "edocs_id_card"=>$idCardD,"edocs_pin_cert"=>$pinD,
        "edocs_nssf"=>$nssfD,"edocs_nhif"=>$nhifD,"edocs_dl_copy"=>$driverLicence,"edocs_conduct_cert"=>$bonafideCert);
    $docTable ="logis_emp_docs";
    $insertDocs = $connection->InsertQuery($docTable,$docCols);


    $getLastDriverDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
    $getLastDriverDetails->execute();
    $getLastDriverDetailsRow = $getLastDriverDetails->fetch();

    $driver_limit = $getLastDriverDetailsRow['driver_limit'] - 1;
    $working_driver = $getLastDriverDetailsRow['working_driver'] + 1;

    if($insertDriver == "success" && $insertEmp == "success" && $insertDocs == "success" && $insertKin == "success"){
        $connection->redirect("../drivers.php");
    }
    else {
        $connection->redirect("../drivers.php");
        // $connection->redirect("../add_driver.php?ref=error");
    }
}
?>