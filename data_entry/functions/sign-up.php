<?php
	include ('../config/db.php');
	include("../php-mailer/class.phpmailer.php");

	/*Get user ip address*/
	$ip_address = $_SERVER['REMOTE_ADDR'];
	/*Get user ip address details with geoplugin.net*/
	$geopluginURL = 'http://www.geoplugin.net/php.gp?ip='.$ip_address;
	$addrDetailsArr = unserialize(file_get_contents($geopluginURL));

	$ipcity = $addrDetailsArr['geoplugin_city'];
	$ipcountry = $addrDetailsArr['geoplugin_countryName'];

	// USER GET BROWSER DETAILS
	$browser = $_SERVER['HTTP_USER_AGENT'];

	if(isset($_POST['submit'])){

		$comp_name = $_POST["comp_name"];
		// $comp_position = $_POST["comp_position"];
		$comp_pin = $_POST["comp_pin"];
		$location = $_POST["location"];
		$address1 = $_POST["address1"];
		$address2 = $_POST["address2"];
		$po_box = $_POST["po_box"]."-".$_POST["po_box1"];
		// $comp_NSN = $_POST["comp_NSN"];
		// $comp_fname = $_POST["comp_fname"];
		// $comp_mname = $_POST["comp_mname"];
		// $comp_lname = $_POST["comp_lname"];
		// $genderVal = $_POST["genderVal"];
		// $comp_dob = $_POST["comp_dob"];
		$contactExtn = $_POST["contactExtn"];
		$contact = $_POST["contact"];
		$email = $_POST["email"];
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  		$random_num = substr(str_shuffle( $chars ),0, 40 );
  		$password = substr(md5(microtime()),rand(0,26),6);
  		$latitude = isset($_POST['latitude']) ? $_POST['latitude'] : "";
		$longitude = isset($_POST['longitude']) ? $_POST['longitude'] : "";
		$createdDate = date("Y-m-d H:i:s");
		$admin_status = "Approved";


		$checkPin = $conn->prepare("SELECT * from logis_company_subadmin where comp_pin = '{$comp_pin}' and Email = '{$email}'");
		$checkPin->execute();
		$checkPinCount = $checkPin->rowCount();

		if($checkPinCount > 0){
			header("location: ../sign-up.php?type=exist");
		}
		else {
			$smtpQuery = $connection->OneRow("*","smtp_settings");
			$adminEmailQuery = $connection->OneRow("*","email_settings");

			$adminemailQuery =$connection->OneRowCondition("*","email_content","email_type='mail_enterprise_emp_approved'");
			if($adminemailQuery['email_bcc'] != ""){
				$adminbcc = explode(",", $adminemailQuery['email_bcc']);
			} else {
				$adminbcc = "";
			}
			$adminMsg = "";
			$adminMsg .= "<html><body>
			<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Enterprise' title='Dereva Enterprise'><br><br>";
			$adminMsg .= "<p>Dear Administrator,</p><p>You have a new company registration in dereva.</p>";
			$adminMsg .= "<p>Company Name : ".$comp_name."</p>";
			$adminMsg .= "<p>Company PIN : ".$comp_pin."</p>";
			$adminMsg .= "<p>Email : ".$email."</p>";	
			$adminMsg .= "<p>Contact : ".$contactExtn."-".$contact."</p>";
			$adminMsg .="</td></tr><tr> <td height='25' valign='middle'>".$adminemailQuery['email_signature']."</td></tr></table></td></tr></table></body></html>";


			$useremailQuery = $connection->OneRowCondition("*","email_content","email_type='mail_enterprise_emp_approved'");
			if($useremailQuery['email_bcc'] != ""){
				$userbcc = explode(",", $useremailQuery['email_bcc']);
			} else {
				$userbcc = "";
			}
			$userMsg = "";
			$userMsg .= "<html><body>
			<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Enterprise' title='Dereva Enterprise'><br><br>";
			$userMsg .= "<p>Dear ".$comp_name.",</p>";
			$userMsg .= "<p>You have successfully registered to Dereva Enterprise. Please use the credentials below to access your account.</p>";
			$userMsg .= "<p>Username : ".$email."</p>";
			$userMsg .= "<p>Password : ".$password."</p>";
			$userMsg .="<p>".$useremailQuery['email_signature']."</p></body></html>";


			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug  = 1;
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = $smtpQuery['smtp_secure'];
			$mail->Host = $smtpQuery['smtp_host']; 
			$mail->Port = $smtpQuery['smtp_port']; 
			$mail->Username = $smtpQuery['smtp_username']; 
			$mail->Password = $smtpQuery['smtp_password']; 
			$mail->From = $smtpQuery['smtp_username'];
			$mail->FromName = $adminemailQuery['email_name'];
			$mail->Subject = "Company Registration";
			$mail->MsgHTML($adminMsg);
			$mail->AddAddress($adminEmailQuery['email_address']);
			if($adminbcc != ""){
				foreach($adminbcc as $addressbcc) {
					$mail->AddBCC($addressbcc);
				}
			}

			$mail2 = new PHPMailer();
			$mail2->IsSMTP(); 
			$mail2->SMTPDebug = 1; 
			$mail2->SMTPAuth = true; 
			$mail2->SMTPSecure = $smtpQuery['smtp_secure'];
			$mail2->Host = $smtpQuery['smtp_host']; 
			$mail2->Port = $smtpQuery['smtp_port']; 
			$mail2->Username = $smtpQuery['smtp_username']; 
			$mail2->Password = $smtpQuery['smtp_password']; 
			$mail2->From = $smtpQuery['smtp_username'];
			$mail2->FromName = $useremailQuery['email_name'];
			$mail2->Subject = "Company Registration";
			$mail2->MsgHTML($userMsg);
			$mail2->AddAddress($email, $comp_fname);
			if($userbcc != ""){
				foreach($userbcc as $addressbcc) {
					$mail2->AddBCC($addressbcc);
				}
			}

			if($mail->send() && $mail2->send()){

				$insertData = $conn->prepare("INSERT into logis_company_subadmin 
					(comp_name,comp_pin,Email,comp_pwd,contact_code,contact_num,location,address1,address2,po_box,random_num,created_date,admin_status,ip_address,latitude,longitude,city,country,browser,first_login)
					values ('$comp_name','$comp_pin','$email','$password','$contactExtn','$contact','$location','$address1','$address2','$po_box','$random_num','$createdDate','$admin_status','$ip_address','$latitude','$longitude','$ipcity','$ipcountry','$browser','Yes')");
				$insertData->execute();

				$lastCompID = $conn->lastInsertId();

				include("../views/test_driver.php");

				header("location: ../registration_thank.php");
			}
			elseif(!$mail->send() && !$mail2->send()) {
				header("location: ../sign-up.php?type=error");
			}
		}
	}
?>