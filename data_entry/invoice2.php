<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['Email'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $action = isset($_GET['action']) ? $_GET['action'] : "";

  if($action == "not_wroking"){
    $msg = "These driver is currently not working with your company.";
  }
  elseif($action == "no_driver"){
    $msg = "Please check driver National ID.";
  }
  elseif($action == "success"){
    $msg = "You are successfully flaged to this driver.";
  }
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <div class="col-sm-12">
    <div class="wht-bg">
      <div class="heading">
        <h4>Invoice</h4>
        <div class="filters">
          <div class="form-inline">
            
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="functions/flag_driver.php" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group" style="padding: 10px 30px;">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th scope="col">Plan Name</th>
                  <th scope="col">Pricing</th>
                  <th scope="col">Drivers</th>
                  <th scope="col">Purchase Date</th>
                  <th scope="col">End Date</th>
                  <th scope="col">View</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <?php
                $detail = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id DESC");
                $detail->execute();
                while($detail1 = $detail->fetch()){
                  if($detail1['package_id'] == "freetrail"){
                    $plan_name = "30 days trial";
                    $plan_price = "0";
                  }else{
                    $plan = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$detail1['package_id']}' ");
                    $plan->execute();
                    $plan1= $plan->fetch();
                    $plan_name = $plan1['plan_name'];
                    $plan_price = $plan1['plan_price'];
                 }   
              ?>
                <tr>
                  <td><?php echo $plan_name; ?></td>
                  <td>$ <?php echo $plan_price; ?></td>
                  <td><?php echo $detail1['driver_limit']; ?></td>
                  <td><?php echo $detail1['package_start_date']; ?></td>
                  <td><?php echo $detail1['package_end_date']; ?></td>
                  <td><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>

         
        </form>
      </div>
    </div>
  </div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  function checkNSN(){
    var NSN = $("#NSN").val();
    var compId = $("#compId").val();

    $.ajax({
      type : "post",
      url : "functions/ajax.php",
      data : { "type":"checkFlagDriverNSN","NSN":NSN,"compId":compId },

      success : function(msg){
        if(msg == "not working"){
          $(".nsnMsg").text("These driver is currently not working with your company.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "no driver"){
          $(".nsnMsg").text("Please check driver National ID.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "success"){
          $(".nsnMsg").text('');
          $("#submitFlag").attr("disabled",false);
        }
        
      }
    });
  }
</script>

<?php include'footer.php'; ?>