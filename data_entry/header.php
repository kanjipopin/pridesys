<?php
  ob_start();
  
  if(!isset($_SESSION)) {
    session_start();
  } 

  require_once('../config/db.php');
  require_once('../config/includes/initialise.php');

  $Email = $_SESSION['Email'];

  $adminQry = $conn->prepare("SELECT * from logis_company_subadmin Where Email = '$Email' ");
  $adminQry->execute();
  $adminRes = $adminQry->fetch(PDO::FETCH_ASSOC);
  $adminName = $adminRes['Name'];

  $orderDetails = 'true';


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Data entry</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="../../images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">

      <!-- Flaticon CSS -->
      <link rel="stylesheet" href="../fonts/flaticon.css">
    <!-- Css -->
    <link rel="stylesheet" href="../css/portal.css">
    <link rel="stylesheet" href="../css/chat.css">
    <link rel="stylesheet" href="../css/portal-responsive.css">
    <link rel="stylesheet" href="../css/showup.css">
    <link rel="stylesheet" href="../css/datepicker.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="../css/tabs-style.css">
    <link href="../css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />


    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.php"><img src="../images/prime_logo_sizable.png"  alt="Pride drive" title="Pride drive" style="width: 160px; height: 60px;"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <!--  <div style="margin-top: 30px;display: inline-block;float: right;">
                <img src = "../images/notification1.png">
            </div> -->
            <ul class="nav navbar-nav navbar-right">
              <!-- <li><a href = "#"><img src = "../images/notification1.png"><span class="notify">0</span></a></li> -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                <?php echo $adminName; ?>
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
        				  <li><a href="dashboard.php">Dashboard</a></li>
                  <!--<li><a href="invoice.php">Billing</a></li>-->
                  <!--<li><a href="pricing_details.php">Pricing</a></li>-->
                  <li><a href="profile.php">Profile</a></li>
                  <!--<li><a href="about-us.php">About Us</a></li>-->
                  <li><a href="functions/logout.php">Log Out</a></li> 
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <div class="bg-color">
      <div class="container-fluid">
        <div class="sidebar stick hidden-xs" role="navigation">
          <div class="sidebar-nav navbar-collapse">


              <ul class="nav nav-sidebar-menu sidebar-toggle-view" id="side-menu">
                  <li class="nav-item sidebar-nav-item">

                      <a href="dashboard.php" class="<?php if(@$activeClass == "Dashboard") { echo "active-class"; } ?>"><img src="../images/dashboard-icon1.png"/>Dashboard</a>

                  </li>

                  <li class="nav-item sidebar-nav-item">
                      <a href="#" class="nav-link

                        <?php if(@$activeClass == "VehicleBodyType" || @$activeClass == "VehicleColor" || @$activeClass == "VehicleEngineCapacity" ||
                          @$activeClass == "VehicleFuel" || @$activeClass == "VehicleInventoryLocation" || @$activeClass == "VehicleManufacturer" ||
                          @$activeClass == "VehicleMileage" || @$activeClass == "VehicleModel" || @$activeClass == "VehicleMoreFeatures" ||
                          @$activeClass == "VehicleTransmission" || @$activeClass == "VehicleYear"
                                    ) { echo "active-class"; } ?>">

                          <img src="../images/drivers-icon1.png"/>Masters</a>
                      <ul class="nav sub-group-menu" >

                          <li class="nav-item">
                              <a href="vehicle_body_type_master.php" class="nav-link" style="font-size:13px;"><i class="glyphicon glyphicon-chevron-right"></i>Body type master</a>
                          </li>
                          <li class="nav-item">
                              <a href="vehicle_color_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Color master</a>
                          </li>
                          <li class="nav-item">
                              <a href="vehicle_engine_capacity_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Engine capacity master</a>
                          </li>
                          <li class="nav-item">
                              <a href="vehicle_fuel_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Fuel master</a>
                          </li>
                          <li class="nav-item">
                              <a href="vehicle_inventory_location_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Inventory location master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_manufacturer_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Manufacturer master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_mileage_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Mileage master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_model_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Model master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_more_features_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>More features master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_transmission_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Transmission master</a>
                          </li>

                          <li class="nav-item">
                              <a href="vehicle_year_master.php" class="nav-link" style="font-size:13px;"><i
                                          class="glyphicon glyphicon-chevron-right"></i>Year master</a>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item sidebar-nav-item">
                      <a href="drivers.php" class="<?php if(@$activeClass == "Drivers") { echo "active-class"; } ?>"><img src="../images/drivers-icon1.png"/>My Drivers</a>
                  </li>
                  <li class="nav-item sidebar-nav-item">
                      <a href="vehicles.php" class="<?php if(@$activeClass == "Vehicles") { echo "active-class"; } ?>"><img src="../images/assign-vehicles.png">My Vehicles</a>
                  </li>

                  <li class="nav-item sidebar-nav-item">
                      <a href="reports.php" class="<?php if(@$activeClass == "Reports") { echo "active-class"; } ?>"><img src="../images/reports.svg">Reports</a>
                  </li>

                  <li class="nav-item sidebar-nav-item">
                      <a href="add_reports_performance.php" class="<?php if(@$activeClass == "Performance") { echo "active-class"; } ?>"><img src="../images/performance.svg">Performance</a>
                  </li>


              </ul>

          </div>
          <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->