<?php
	//set timezone
	date_default_timezone_set("Africa/Nairobi");

	//database connect info
	define('DBHOST','localhost');
	define('DBUSER','root');
	define('DBPASS','');
	define('DBNAME','pridesys');

	// @define('DIR','http://enterprise.dereva.com/');
	//$prefix = "";

	//Connect to the database
	try {
		$conn = new PDO("mysql:host=".DBHOST.";dbname=".DBNAME, DBUSER, DBPASS);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//echo "Connected successfully";
    }
	catch(PDOException $e) {
		echo "Connection failed: " . $e->getMessage();
    }

	//Select database for a mysql connection.
	include_once 'connect.php';
	$connection = new Connect($conn);

?>