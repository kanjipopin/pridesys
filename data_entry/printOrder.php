<?php
  if(!isset($order)) die();
  require_once ("config/db.php");

  $order = $conn->prepare("SELECT * from enterprise_order_details where order_id = '{$order['orderId']}'");
  $order->execute();
  $orderrow = $order->fetch();

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$orderrow['comp_email']}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $plan = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$orderrow['package_id']}'");
  $plan->execute();
  $planrow = $plan->fetch();
?>

  <style type="text/css">
    /*.invoiceDetails h3 { text-align: center; }*/
    /*.invoiceDetails h4 { color: #575757;font-weight: 500;border-bottom: 1px solid #e0e0e0;padding: 13px 0;margin-bottom: 15px;text-transform: uppercase;font-size: 18px; }*/
    /*.billing { float: left; }*/
    /*.issuer { float: right; }*/
    /*.invoiceDetails p { font-size: 18px;text-transform: capitalize;margin-bottom: 5px; }*/
    /*.OrderDetails { margin-top: 20px; }*/
    /*.btn { float: right;background: #ec2226;box-shadow: none;color: #fff;border-radius: 0px;margin-right: 10px; }*/
  </style>

  <div class="form-group invoiceDetails" style="padding: 10px 30px;">
    <h3 style="text-align: center;margin-bottom: 30px;font-size: 20px;">Thank you for your order!</h3>

    <div class="billing" style="float: left;">
      <h4 style="color: #575757;font-weight: 500;border-bottom: 1px solid #e0e0e0;padding: 13px 0;margin-bottom: 15px;text-transform: uppercase;font-size: 18px;">Billing Details</h4>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;"><?php echo $comprow['fname']." ".$comprow['lname']; ?></p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;"><?php echo $comprow['comp_name']; ?></p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;"><?php echo $comprow['location'].", ".$comprow['po_box']; ?></p>
    </div>

    <div class="issuer" style="float: right;margin-left: 430px">
      <h4 style="color: #575757;font-weight: 500;border-bottom: 1px solid #e0e0e0;padding: 13px 0;margin-bottom: 15px;text-transform: uppercase;font-size: 18px;">Issuer</h4>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">CLIFFORD TECHNOLOGIES LTD</p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">P051639270T</p>
      <p style="font-size: 18px;text-transform: lowercase;margin-bottom: 5px;">info@clifford.co.ke</p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">+254(0)753000888</p>
    </div>

    <div class="OrderDetails" style="margin-top: 330px;">
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">Order Details :</p>
      <table border="1" cellspacing="0" cellpadding="0" style="border-color: #000;" width="99%">
      <thead>
        <tr>
            <th style="width:30%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;padding: 5px 0;color:#000;">Package Name</th>
            <!-- <th style="width:20%;font-size: 14px;text-align:center;font-weight: 700;border-color: #000;color:#000;">Order ID</th> -->
            <th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Driver Limit</th>
            <th style="width:15%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Start Date</th>
            <th style="width:15%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">End Date</th>
            <th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Price</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td style="text-align:center;border-color: #000;color: #000;font-size: 13px;"><?php echo "Driver (30 days)"; ?></td>
          <!-- <td style="text-align:center;border-color: #000;color: #000;padding:15px 15px;font-size: 15px;"><?php //echo $orderrow['refrence_id']; ?></td> -->
          <td style="text-align:center;border-color: #000;font-size: 13px;color: #000;"><?php echo $orderrow['driver_limit']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $orderrow['package_start_date']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $orderrow['package_end_date']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;">KES <?php echo $orderrow['amount']; ?></td>
        </tr>
      </tbody>
    </table>
    <div>
    <small>please note - payment once made are not refundable</small>
  </div>