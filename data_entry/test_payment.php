<?php
	ob_start();

	$ServiceDate = date("Y-m-d H:i:s");

	$headers = array(
	    'Content-Type: application/xml',
	    'Accept-Charset: UTF-8'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://secure.3gdirectpay.com/API/v6/");

	$msgParameter = '<?xml version="1.0" encoding="utf-8"?>
		<API3G>
		<CompanyToken>843E1DDF-36C2-4D2D-A83D-FDCC23E1A6DF</CompanyToken>
		<Request>createToken</Request>
		<Transaction>
		<PaymentAmount>5000.00</PaymentAmount>
		<PaymentCurrency>KES</PaymentCurrency>
		<CompanyRef>DERE</CompanyRef>
		<RedirectURL>https://enterprise.dereva.com/payment_success.php</RedirectURL>
		<BackURL>https://enterprise.dereva.com/payment_cancel.php</BackURL>
		<CompanyRefUnique>0</CompanyRefUnique>
		<PTL>5</PTL>
		<customerFirstName>NIrav</customerFirstName>
	    <customerLastName>Soni</customerLastName>
	    <customerEmail>nirav.soni@sujaltechnologies.com</customerEmail>
	    <customerAddress>Address</customerAddress>
	    <customerZip>123465</customerZip>
	    <customerPhone>1234657890</customerPhone>
	    <customerCity>Diani Beach</customerCity>
		</Transaction>
		<Services>
		  <Service>
		    <ServiceType>23254</ServiceType>
		    <ServiceDescription>Plan 1 to 99 Driver</ServiceDescription>
		    <ServiceDate>2018/11/29 19:00</ServiceDate>
		  </Service>
		</Services>
		</API3G>';

	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $msgParameter);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($ch);
	$err = curl_error($ch);
	curl_close($ch);

	if($err){
		echo "Error response = ".$err;
	} else {
		$xml_string = $response;

		$xml = simplexml_load_string($xml_string);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);

		// echo $array['Result'];

		if($array['Result'] == "000" && $array['TransToken'] != ""){
			header('location: https://secure.3gdirectpay.com/pay.asp?ID='.$array['TransToken']);
		}
		else {
			header("location: https://enterprise.dereva.com/payment_cancel.php");
		}
	}
?>