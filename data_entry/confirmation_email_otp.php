<?php
  require_once('../config/db.php');
  session_start();

  @$Email = $_SESSION['Email'];
  if($Email == ""){
    $connection->redirect('../index.php');
  }

  $getDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $getDetails->execute();
  $getDetailsRow = $getDetails->fetch();
  $companyName = $getDetailsRow['comp_name'];

  $new_email_add = $_SESSION['new_email_add'];
  $OTPNum = $_SESSION['OTPNum'];

  $action = isset($_GET['action']) ? $_GET['action'] : "";
  $msg = "";
  if($action == "wrongOtp"){
    $msg = "Your OTP Number are not matched.";
  }

  if(isset($_REQUEST['confrmOtpBtn'])){

    $otp_number = $_REQUEST['otp_number'];

    if($otp_number != $OTPNum){
      header("location: confirmation_email_otp.php?action=wrongOtp");
    }
    elseif($otp_number == $OTPNum) {

      $updateEmail = $conn->prepare("UPDATE logis_company_subadmin set Email='{$new_email_add}' where comp_id = '{$getDetailsRow['comp_id']}'");
      $updateEmail->execute();

      usnet($_SESSION['Email']);

      header("location: index.php?actions=emailUpdated");
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Dereva Portal</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>
  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-3 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="dashboard.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                  <?php echo $companyName; ?>
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="about-us.php">About Us</a></li>
                    <li><a href="pricing_table.php">Pricing</a></li>
                    <li><a href="invoice.php">Billing</a></li>
                    <li><a href="functions/logout.php">Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->

          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <div class="enterprise-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <p style="color: red;font-size: 15px;font-weight: 500;"><?php if(!empty($msg)) { echo $msg; } ?></p>
            <form enctype="multipart/form-data" method="POST" id="login_form" name="login_form">
              <h4>Confirm OTP Number</h4>
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="text" class="form-control" name="otp_number" id="otp_number" placeholder="OTP Number" required maxlength="5">
              </div>
              <small>Please check your updated email address and confirm OTP Number.</small>

              <div class="clearfix"></div>
              <button type="submit" id="confrmOtpBtn" name="confrmOtpBtn" class="btn btn-default">Confirm</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <footer class="new-footer">
      <div class="container-fluid hidden-xs">
      <div class="col-sm-4">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-4">
        <p>&copy; dereva.com | All rights reserved.</p>
        <a href="terms-of-service.php">Terms of Service</a>
      </div>

      <div class="col-sm-4">
        <ul style="float: right;">
          <li>Follow Us</li><br>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
        </ul>
      </div>
    </div>

    <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; dereva.com | All rights reserved.</p>
      </div>
    </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
  </body>
</html>