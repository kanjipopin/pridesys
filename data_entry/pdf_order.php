<?php
use Dompdf\Adapter\CPDF;
use Dompdf\Dompdf;
use Dompdf\Exception;
use Dompdf\Options;
	
	//use Dompdf\Dompdf;
	$type = $_REQUEST['type'];

	switch ($type) {
	 	case 'pdf':
	 	$id = base64_decode($_REQUEST['payId']);

	 		$order = array("orderId" => $id);

	 		ob_start();			
			require_once("printOrder.php");

			$template = ob_get_clean();

			require_once("vendor/autoload.php");

			require_once ("vendor/dompdf/autoload.inc.php");

			$options = new Options();
			$options->set('defaultFont', 'Courier');
			$options->set('isRemoteEnabled', TRUE);
			$options->set('debugKeepTemp', TRUE);
			$options->set('isHtml5ParserEnabled', true);
			$dompdf = new Dompdf($options);

			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($template);
			$dompdf->setPaper('A4', 'potrait');
			$dompdf->render();
			file_put_contents("uploads/pdf/order-" . mt_rand(0,99999) . ".pdf",  $dompdf->output());

			$dompdf->stream("DE-order".$id);
	 	break;
	 } 
?>