<?php
	@session_start();
  	require_once('../config/db.php');
  	@include 'pricing_header.php';
  	include_once('config/OAuth.php');

  	@$Email = $_SESSION['Email'];
	if($Email == ""){
		$connection->redirect('../index.php');
	}

	$plan = $_REQUEST['plan'];

	$_SESSION['plan'] = $plan;

	$getPlanDetails = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$plan}'");
	$getPlanDetails->execute();
	$getPlanDetailsRow = $getPlanDetails->fetch();

	$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompDetails->execute();
	$getCompDetailsRow = $getCompDetails->fetch();
		$comp_name = $getCompDetailsRow['comp_name'];
		$fname = $getCompDetailsRow['fname'];
		$lname = $getCompDetailsRow['lname'];


	$getOrderDetails = $conn->prepare("SELECT * from enterprise_order_details order by order_id desc");
	$getOrderDetails->execute();
	$getOrderDetailsRow = $getOrderDetails->fetch();

	$refrence_id = $getOrderDetailsRow['refrence_id'];
	if($refrence_id == ""){
		$new_refrence_id = "DE-10001";
	}
	else {
		$explode = explode("-", $refrence_id);
		$create_refrence_id = $explode[1] + 1;
		$new_refrence_id = "DE-".$create_refrence_id;
	}

	//get form details
	$amount = $getPlanDetailsRow['plan_price'];
	$amount = number_format($amount, 2);
	// $currency = "USD";

	$desc = 'Dereva enterprise';
	$type = 'MERCHANT';
	$reference = $new_refrence_id;
	$first_name = $fname;
	$last_name = $lname;
	$email = $Email;
	$phonenumber = $getCompDetailsRow['contact_num'];


	$token = $params = NULL;
	// FOR LIVE MODE
	$consumer_key = 'N62u08gpoT8RfVejeD9WoqdNvCMdJhOG';
	$consumer_secret = 'JlM+U22xL3pL0H9M2yltx8eElfU=';
	$signature_method = new OAuthSignatureMethod_HMAC_SHA1();     
	$iframelink = 'https://www.pesapal.com/API/PostPesapalDirectOrderV4';

	// FOR TEST MODE
	// $consumer_key = 'vZqXOE2/q0ySTN0uye3EKCMGTCT2FB1a';
	// $consumer_secret = 'wgn/j/nKfc0Bznsbi1WEVume1s8=';
	// $signature_method = new OAuthSignatureMethod_HMAC_SHA1();     
	// $iframelink = 'https://demo.pesapal.com/api/PostPesapalDirectOrderV4';

	$callback_url = 'https://enterprise.dereva.com/payment_ipn_success.php';

	$post_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><PesapalDirectOrderInfo xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Amount=\"".$amount."\" Description=\"".$desc."\" Type=\"".$type."\" Reference=\"".$reference."\" FirstName=\"".$first_name."\" LastName=\"".$last_name."\" Email=\"".$email."\" PhoneNumber=\"".$phonenumber."\" xmlns=\"http://www.pesapal.com\" />";
	$post_xml = htmlentities($post_xml);

	$consumer = new OAuthConsumer($consumer_key, $consumer_secret);

	//post transaction to pesapal
	$iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $iframelink, $params);
	$iframe_src->set_parameter("oauth_callback", $callback_url);
	$iframe_src->set_parameter("pesapal_request_data", $post_xml);
	$iframe_src->sign_request($signature_method, $consumer, $token);


	// $insertOrderData = $conn->prepare("INSERT into enterprise_order_details 
	// 	(comp_name,comp_email,emp_fname,emp_lname,package_id,refrence_id,package_start_date,package_end_date,package_status,package_activation_date)
	// 	values ('$comp_name','$Email','$fname','$lname','$plan','$new_refrence_id','$lname')") 
?>

<div class="col-sm-12">
    <div class="wht-bg">
		<iframe src="<?php echo $iframe_src; ?>" width="100%" height="700px"  scrolling="no" frameBorder="0">
			<p>Browser unable to load iFrame</p>
		</iframe>
	</div>
</div>
<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<?php include'footer.php'; ?>