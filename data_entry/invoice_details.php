<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['Email'];
  $id = base64_decode($_GET['id']);

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $order = $conn->prepare("SELECT * from enterprise_order_details where order_id = '{$id}'");
  $order->execute();
  $orderrow = $order->fetch();

  $plan = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$orderrow['package_id']}'");
  $plan->execute();
  $planrow = $plan->fetch();
?>

<style type="text/css">
	.invoiceDetails h3 { text-align: center; }
	.invoiceDetails h4 { color: #575757;font-weight: 500;border-bottom: 1px solid #e0e0e0;padding: 13px 0;margin-bottom: 15px;text-transform: uppercase;font-size: 18px; }
	.billing { float: left; }
	.issuer { float: right; }
	.invoiceDetails p { font-size: 18px;text-transform: capitalize;margin-bottom: 5px; }
	.OrderDetails { margin-top: 20px; }
	.btn { float: right;background: #ec2226;box-shadow: none;color: #fff;border-radius: 0px;margin-right: 10px; }
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="page-rightWidth">
  <div class="col-sm-12">
    <div class="invoice-page">
      <div class="heading">
        <h4>Invoice</h4>
        <div class="filters">
          <div class="form-inline">
          </div>
        </div>
      </div>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        
          <div class="form-group invoiceDetails" style="padding-bottom: 30px;">
          	<h3 style="text-align: center;">Thank you for your order!</h3>

          	<div class="billing">
          		<h4>Billing Details</h4>
          		<p><?php echo $comprow['fname']." ".$comprow['lname']; ?></p>
          		<p><?php echo $comprow['comp_name']; ?></p>
          		<p><?php echo $comprow['location'].", ".$comprow['po_box']; ?></p>
          	</div>

          	<div class="issuer">
          		<h4>Issuer</h4>
          		<p>CLIFFORD TECHNOLOGIES LTD</p>
          		<p>P051639270T</p>
          		<p style="text-transform: lowercase;">info@clifford.co.ke</p>
          		<p>+254(0)753000888</p>
          	</div>

          	<div class="clearfix"></div>
          	<div class="OrderDetails">
          		<p>Order Details :</p>
              <div class="table-responsive">
  	          	<table border="1" class="table" cellspacing="0" cellpadding="0" style="border-color: #000;" width="99%">
  		          <thead>
  		            <tr>
  		              	<th style="width:30%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;padding: 5px 0;color:#000;">Package Name</th>
  		              	<!-- <th style="width:20%;font-size: 14px;text-align:center;font-weight: 700;border-color: #000;color:#000;">Order ID</th> -->
  		              	<th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Driver Limit</th>
  		              	<th style="width:15%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Start Date</th>
  		              	<th style="width:15%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">End Date</th>
  		              	<th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Price</th>
  		            </tr>
  		          </thead>

  		          <tbody>
  		            <tr>
  		              <td style="text-align:center;border-color: #000;color: #000;font-size: 13px;"><?php echo "Driver (30 days)"; ?></td>
  		              <!-- <td style="text-align:center;border-color: #000;color: #000;padding:15px 15px;font-size: 15px;"><?php //echo $orderrow['refrence_id']; ?></td> -->
  		              <td style="text-align:center;border-color: #000;font-size: 13px;color: #000;"><?php echo $orderrow['driver_limit']; ?></td>
  		              <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $orderrow['package_start_date']; ?></td>
  		              <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $orderrow['package_end_date']; ?></td>
  		              <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;">KES <?php echo $orderrow['amount']; ?></td>
  		            </tr>
  		          </tbody>
  		        </table>
            </div>

          	<div>
          	 <small>Please note that any payment once made is non-refundable.</small>
            </div>

          <a href="pdf_order.php?type=pdf&payId=<?php echo base64_encode($orderrow['order_id']); ?>"><button class="btn">PDF Order</button></a>
      </div>
    </div>
  </div>
</div>

<div class="row visible-xs" style="margin: 0;">
  <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
          <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
        </li>
        <li>
          <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
        </li>
        <li>
          <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
        </li>
      </ul>
    </div>
  <!-- /.sidebar-collapse -->
  </div>
</div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<?php include'footer.php'; ?>