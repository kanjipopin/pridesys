<?php
  require_once('../config/db.php');
  session_start();

  @$Email = $_SESSION['Email'];
  if($Email == ""){
    $connection->redirect('../index.php');
  }

  $getDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $getDetails->execute();
  $getDetailsRow = $getDetails->fetch();
  $companyName = $getDetailsRow['comp_name'];

  if(isset($_REQUEST['chngPassBtn'])){
    $new_password = $_REQUEST['new_password'];

    $UpdatePass = $conn->prepare("UPDATE logis_company_subadmin set comp_pwd='$new_password' where Email = '{$Email}'");
    $UpdatePass->execute();
      header("location: dashboard.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dereva Portal</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>
  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-3 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                  <?php echo $companyName; ?>
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="about-us.php">About Us</a></li>
                    <li><a href="pricing_table.php">Pricing</a></li>
                    <li><a href="invoice.php">Billing</a></li>
                    <li><a href="functions/logout.php">Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <div class="enterprise-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <form enctype="multipart/form-data" method="POST" id="password_form" name="password_form">
              <h4>Change Password</h4>
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password" onchange="getOldPassword();" required>
              </div>
                <small style="float:left;font-size: 13px;display: none;color: red;" id="oldPass">Your Old password are not matched.</small>

              <div class="input-group" style="margin-top: 50px;">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password" onchange="getpassword();" required>
              </div>
              <small style="float:left;font-size: 11px;display: block;" id="six_letter">Minimum 6 character required.</small>

              <div class="input-group" style="margin-top: 50px;">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                <input type="password" class="form-control" name="confm_password" id="confm_password" placeholder="Confirm Password" onchange="getpassword();" required>
              </div>
              <small style="float: left;color: #ff0202;" id="chk_pass"></small>

              <div class="clearfix"></div>
              <button type="submit" id="chngPassBtn" name="chngPassBtn" class="btn btn-default" disabled>Change Password</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <footer class="new-footer">
      <div class="container-fluid hidden-xs">
        <div class="col-sm-4">
          <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
            <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
            <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
          </p>
        </div>

        <div class="col-sm-4">
          <p>&copy; Dereva.com | All rights reserved.</p>
          <a href="terms-of-service.php">Terms of Service</a>
        </div>

        <div class="col-sm-4">
          <ul style="float: right;">
            <li>Follow Us</li><br>
            <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
            <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
            <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
            <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
          </ul>
        </div>
      </div>

      <div class="container-fluid visible-xs" style="text-align: center;">
        <div class="col-sm-3">
          <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
            <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
            <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
          </p>
        </div>

        <div class="col-sm-3">
          <ul>
            <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
            <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
            <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
            <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
          </ul>
        </div>

        <div class="col-sm-3">
          <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; Dereva.com | All rights reserved.</p>
        </div>
      </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>

    <script type="text/javascript">
      function init() {
        // Clear forms here
        document.getElementById("old_password").value = "";
      }
      window.onload = init;

      function getpassword(){
        var pwd = $("#new_password").val();
        var Cpwd = $("#confm_password").val();

        if((pwd).length < 6 && (Cpwd).length < 6){
          $("#six_letter").css("color","red");
        } else {
          $("#six_letter").css("color","#333");
        }

      if(Cpwd != ""){
          if(pwd != Cpwd){
            $("#chngPassBtn").attr("disabled",true);
            $("#new_password").css("border-color","red");
            $("#confm_password").css("border-color","red");
            $("#chk_pass").show();
            $("#chk_pass").text("Confirm password and password are not matched.");
          }
          else {
            $("#chngPassBtn").attr("disabled",false);
            $("#new_password").css("border-color","#e8e8e8");
            $("#confm_password").css("border-color","#e8e8e8");
            $("#chk_pass").hide();
          }
        }
      }

      function getOldPassword(){
        var old_password = $("#old_password").val();

        $.ajax({
          type : "post",
          url : "functions/ajax.php",
          data : {"old_password":old_password,"type":"checkOldPassword"},

          success : function(msg){

            if(msg == 0){
              $("#oldPass").show();
            } else {
              $("#oldPass").hide();
              $("#chngPassBtn").attr("disabled",false);
            }
          }
        })
      }
    </script>
  </body>
</html>