<?php
  @session_start();
  @$Email = $_SESSION['Email'];

  if($Email == ""){
    $connection->redirect('../index.php');
  }

  if($_SERVER['HTTPS'] != "on"){ 
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    header('Location:'. $redirect);
  }

  $companyQry = $conn->prepare("SELECT * from logis_company_subadmin Where Email = '{$Email}' AND admin_status = 'Approved'");
  $companyQry->execute();
  $companyRes = $companyQry->fetch(PDO::FETCH_ASSOC);
  $companyName = $companyRes['comp_name'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dereva Enterprise</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/chat.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="css/tabs-style.css">
    <link href="css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.php"><img src="images/logo.svg" alt="Dereva Enterprise" title="Dereva Enterprise" style="width: 270px;"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <!--  <div style="margin-top: 30px;display: inline-block;float: right;">
                <img src = "images/notification1.png">
            </div> -->
            <ul class="nav navbar-nav navbar-right">
              <!-- <li><a href = "#"><img src = "images/notification1.png"><span class="notify">0</span></a></li> -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                <?php echo $companyName; ?>
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="dashboard.php">Dashboard</a></li>
                  <li><a href="invoice.php">Billing</a></li>
                  <li><a href="pricing_table.php">Pricing</a></li>
                  <li><a href="profile.php">Profile</a></li>
                  <li><a href="about-us.php">About Us</a></li>
                  <li><a href="functions/logout.php">Log Out</a></li> 
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <div class="bg-color">
      <div class="container-fluid">
        <div class="sidebar stick hidden-xs" role="navigation">
          <div class="sidebar-nav navbar-collapse">
              <ul class="nav" id="side-menu">
                <li>
                <?php
                  if($action != "expire"){
                ?>
                  <a href="pricing_details.php" class="<?php if(@$activeClass == "Pricing") { echo "active-class"; } ?>"><img src="images/pricing.png">Pricing</a>
                <?php } else { ?>
                  <a href="pricing_details.php?action=expire" class="<?php if(@$activeClass == "Pricing") { echo "active-class"; } ?>"><img src="images/pricing.png">Pricing</a>
                <?php } ?>
                </li>
              </ul>
          </div>
          <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->