<?php
  ob_start();
  session_start();
  date_default_timezone_set("Africa/Nairobi");
  include("config/db.php");

  @$Email = $_SESSION['Email'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <head>
    <title>Dereva Enterprise</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/chat.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <!-- WOW Effect -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="css/tabs-style.css">
    <link href="css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
  </head>

  <body class="bodyDisbld">
  <header>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-sm-3 col-md-3">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php if($Email == ""){ ?>
            <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
          <?php } else { ?>
            <a class="navbar-brand" href="dashboard.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
          <?php } ?>
        </div>

        <div class="col-sm-8 col-md-9">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <?php if($Email == ""){ ?>
            <ul class="nav navbar-nav navbar-right">
              <li><a class="btn btn-default header-btn" href="login.php" role="button" style="margin-right: 15px;padding: 10px 10px;">Login</a></li>
              <li><a class="btn btn-default header-btn" href="sign-up.php" role="button" style="padding: 10px 10px;">Sign Up</a></li>
            </ul>

          <?php } else { ?>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="profile.php">Profile</a></li>
              <li><a href="about-us.php">About Us</a></li>
              <li><a href="pricing_table.php">Pricing</a></li>
              <li><a href="invoice.php">Billing</a></li>
              <li><a href="functions/logout.php">Log Out</a></li>
            </ul>
          <?php } ?>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </nav>
  </header>

  <section class="terms-of-conditions">
      <div class="container-fluid">
        <h3>Terms of Use</h3>
        <p>Please read these terms of use ("Terms of Use" or "Agreement") carefully before using the 'Dereva'. By accessing or using the 'Dereva', you agree that you have read, understood and agreed to be bound by the terms and conditions of this Agreement and any and all applicable laws, rules and regulations. If you do not agree to these Terms of Use, please do not proceed to use or access Dereva.</p>
        <h4>Introduction</h4>
        <ul>
          <li>This constitutes a legal agreement between you ("User" or "You") and Clifford Technologies Limited (the “Company”) the owner and operator of the Application ‘Dereva’- who maintains this Dereva web site (the "Site").</li>
          <li>The information presented herein is for informative and interactive purposes only. Permitted users may visit the Site to access, download and copy information and materials (collectively, "Materials") from the Site for User’s personal use, without any right to resell or redistribute them or to compile derivative works therefrom, subject to the terms and conditions outlined below, and also subject to more specific restrictions that may apply to specific Material within this Site.</li>
          <li>In addition to the terms defined elsewhere in this Agreement, the following definitions apply:
            <ol>
              <li><strong>"Application"</strong> means the web-based software application developed, owned, controlled, managed, maintained, hosted, licensed and/or designed by the Company (or its affiliated company) to run on smartphones, computers, and/or other devices on which the Dereva service is made available;</li>
              <li><strong>"Dereva"</strong> includes the on-demand service through the Site that is made available by or on behalf of the Company that permits a User to upload a Driver Profile, apply for any job vacancies listed on the Application and accept and reject requests for scheduled interviews; or upload job vacancies, view Driver Profiles for the drivers that have applied for the job vacancies and schedule interviews with the drivers;</li>
              <li><strong>"Driver Profile"</strong> means data uploaded by a User from time to time indicating his driving qualifications, experience and skillset;</li>
              <li><strong>"Site"</strong> means the www.dereva.com</li>
            </ol>
          </li>
        </ul>

        <h4>Information about us</h4>
        <p>The Company duly incorporated in the Republic of Kenya, under registration number PVT-AAAEOW2, with its registered office situated in Nairobi. The Company is the owner of the registered trademark "Dereva".</p>

        <h4>Disclaimers</h4>
        <ul>
          <li>Materials provided on this Site are “as is”, without warranty of any kind, either express or implied, including, without limitation, warranties of competence, fitness for a particular purpose and non-infringement. The Company specifically does not make any warranties or representations as to the accuracy or completeness of any such Materials. The Company periodically adds, changes, improves or updates the Materials on this Site without notice. Under no circumstances shall the Company be liable for any loss, damage, liability or expense incurred or suffered that is claimed to have resulted from the use of this Site, including, without limitation, any fault, error, omission, interruption or delay with respect thereto. The use of this Site is at the User’s sole risk. Under no circumstances, including but not limited to negligence, shall the Company or its affiliates be liable for any direct, indirect, incidental, special or consequential damages, even if the Company has been advised of the possibility of such damages.</li>
          <li>The User specifically acknowledges and agrees that the Company is not liable for any conduct of any User.</li>
          <li>This Site may contain advice, opinions and statements of various information providers. The Company does not represent or endorse the accuracy or reliability of any advice, opinion, statement or other information provided by any information provider, any User of this Site or any other person or entity. Reliance upon any such advice, opinion, statement, or other information shall also be at the User’s own risk. Neither the Company nor its affiliates, nor any of their respective agents, employees, information providers or content providers, shall be liable to any User or anyone else for any inaccuracy, error, omission, interruption, deletion, defect, alteration of or use of any content herein, or for its timeliness or completeness, nor shall they be liable for any failure of performance, computer virus or communication line failure, regardless of cause, or for any damages resulting therefrom.</li>
          <li>As a condition of use of this Site, the User agrees to indemnify the Company and its affiliates from and against any and all actions, claims, losses, damages, liabilities and expenses (including reasonable attorneys’ fees) arising out of the User’s use of this Site, including, without limitation, any claims alleging facts that if true would constitute a breach by the User of these Terms and Conditions. If the User is dissatisfied with any Material on this Site or with any of its Terms and Conditions of Use, the User’s sole and exclusive remedy is to discontinue using the Site.</li>
          <li>This Site may contain links and references to third-party web sites. The linked sites are not under the control of the Company, and the Company is not responsible for the content of any linked site or any link contained in a linked site. The Company provides these links only as a convenience, and the inclusion of a link or reference does not imply the endorsement of the linked site by the Company.</li>
          <li>If this Site contains bulletin boards, chat rooms, access to mailing lists or other message or communication facilities (collectively, “Forums”), the User agrees to use the Forums only to send and receive messages and materials that are proper and related to the particular Forum. By way of example and not as a limitation, the User agrees that when using a Forum, he or she shall not do any of the following:
            <ol>
              <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others;</li>
              <li>Publish, post, distribute or disseminate any defamatory, infringing, obscene, indecent or unlawful material or information;</li>
              <li>Upload or attach files that contain software or other material protected by intellectual property laws (or by rights of privacy and publicity) unless the User owns or controls the rights thereto or has received all consents therefor as may be required by law;</li>
              <li>Upload or attach files that contain viruses, corrupted files or any other similar software or programs that may damage the operation of another’s computer;</li>
              <li>Delete any author attributions, legal notices or proprietary designations or labels in any file that is uploaded;</li>
              <li>Falsify the origin or source of software or other material contained in a file that is uploaded;</li>
              <li>Advertise or offer to sell any goods or services, or conduct or forward surveys, contests or chain letters, or download any file posted by another user of a Forum that the User knows, or reasonably should know, cannot be legally distributed in such manner.</li>
            </ol>
          </li>
          <li>The User acknowledges that all Forums and discussion groups are public and not private communications. Further, the User acknowledges that chats, postings, conferences, e-mails and other communications by other Users are not endorsed by the Company, and that such communications shall not be considered to have been reviewed, screened or approved by the Company. The Company reserves the right to remove, for any reason and without notice, any content of the Forums received from Users, including, without limitation, e-mail and bulletin board postings.</li>
        </ul>

        <h4>Eligibility</h4>
        <ul>
          <li>Dereva is intended for legal persons and for natural persons aged eighteen (18) years or older.</li>
          <li>By agreeing to this Agreement, you represent and warrant to the Company:
            <ol>
              <li>That you are at least eighteen (18) years of age; and </li>
              <li>that your registration and use of the Dereva service is in compliance with any and all applicable laws and regulations in force in the Republic of Kenya. If you are using the Dereva on behalf of an entity, organization, or company, you represent and warrant that you have the authority to bind such organization to this Agreement and you agree to be bound by this Agreement on behalf of such organization.</li>
            </ol>
          </li>
        </ul>

        <h4>Registration</h4>
        <ul>
          <li>You will be required to set up an account (“Account”) in order to use the Dereva.</li>
          <li>You are solely responsible for maintaining the confidentiality and security of Your Account and associated credentials. You must take appropriate steps to keep Your information secure by not using an obvious login name and ensuring that You keep your password confidential and change it regularly. </li>
          <li>You should not reveal your Account information to anyone else or use anyone else’s Account. </li>
          <li>You are entirely responsible for all activities that occur on or through Your Account; and</li>
          <li>You agree to immediately notify the Company of any unauthorized use of Your Account or any other breach of security.</li>
          <li>The Company is not responsible for any losses arising out of the unauthorized use of Your Account. The Company reserves the right to terminate, disable or exclude any features of Your Account if it suspects or becomes aware of any unauthorized or unpermitted use of such Account or any part of the Dereva.</li>
        </ul>

        <h4>Collection of Your Information</h4>
        <ul>
          <li>Your content, materials, information, Driver Profile, data, opinions, photos, profiles, messages, notes, website links, text information, designs, graphics, that You and/or other service users post or otherwise make publicly available on or through Dereva, except to the extent the content is owned by Dereva (“User Created Content”) shall not be treated as confidential or proprietary. You grant, affirm, represent, and/or warrant that:
            <ol>
              <li>You have the right to and accordingly agree to grant to the Company a non-exclusive, non-revocable, worldwide, transferable, royalty-free, perpetual right to use Your User Created Content in any manner now or later developed, for any purpose, commercial, advertising, or otherwise, including the right to translate, display, reproduce, modify, create derivative works, sublicense, distribute, assign and commercialize such User Created Content without any payment due to You or additional rights required from You; and </li>
              <li>You have the express consent, release, and/or permission of each and every identifiable individual person in the User Created Content to use the name or likeness of each and every such identifiable individual person to enable inclusion and use of the User Created Content in the manner contemplated by the Dereva service and these Terms of Use.</li>
              <li>There may be additional information the Company may collect in connection with Your use of Dereva and such information will be treated in accordance with this Terms of Use.</li>
            </ol>
          </li>
        </ul>

        <h4>User Created Content</h4>
        <ul>
          <li>The Company does not pre-screen User Created Content. Accordingly, the Company shall not be responsible for the accuracy or content of User Created Content. </li>
          <li>The Company does not guarantee that the Dereva/Site will be free from User Created Content that is inaccurate, incomplete, out of date, deceptive, offensive, threatening, defamatory, unlawful, infringing of third party rights or otherwise objectionable. </li>
          <li>Dereva is merely a passive channel for the distribution of such User Created Content and does not endorse any User Created Content or undertake any obligation or liability relating to any User Created Content or for the activities of any and all users of Dereva. </li>
          <li>In the event the Company chooses to monitor any User Created Content, the Company assumes no responsibility for, or any obligation to monitor or remove, such User Created Content.</li>
          <li>You must ensure that Your User Created Content and any communications that You exchange with other Users of Dereva does not:
            <ol>
              <li>Contain "spam" or any unsolicited marketing;</li>
              <li>Infringe the rights of any third party, including rights of publicity, privacy and any intellectual property or any other proprietary rights;</li>
              <li>Contain viruses, Trojans, worms or other technologically harmful materials;</li>
              <li>Violate any applicable law or regulation; </li>
              <li>Contain content that is obscene or pornographic; or</li>
              <li>Contain content that is defamatory, deceptive, abusive, threatening or harassing, harmful to minors, or otherwise offensive or inappropriate. </li>
            </ol>
          </li>
          <li>The Company reserves the right to edit, remove, or refuse to post any User Created Content or to terminate Your Account for any failure to comply with this Section, or where the Company considers, at its reasonable discretion, that it is in its interests to do so.</li>
        </ul>

        <h4>Your use of Dereva </h4>
        <ul>
          <li>Subject to your acceptance of these Terms of Use, the Company grants to you a non-exclusive, non-transferable right to view and use Dereva in accordance with these Terms of Use. You are not permitted to use Dereva beyond the scope of this right and in particular you agree not to:
            <ol>
              <li>Modify, copy, reproduce or retransmit any content that you may access through Dereva, except as authorized by applicable law;</li>
              <li>Disassemble, reverse engineer or otherwise decompile your Dereva and any upgrades or updates made available to you through Dereva, except as authorized by applicable law or license terms;</li>
              <li>Save for the intended intent, use Dereva for any business or commercial purpose without first obtaining a licence to do so from the Company or it’s licensors; or</li>
              <li>Access or use Dereva for any unlawful purpose.</li>
            </ol>
          </li>
          <li>Dereva reserves the right to terminate Your Account for any failure to comply with this section. </li>
        </ul>

        <h4>Consent to Disclosure</h4>
        <p>You agree and give the Company unconditional right, without liability to You, to disclose Your User Created Content to law enforcement authorities, government officials and other third parties, as the Company believes is reasonably necessary or appropriate to enforce and verify Your compliance with these Terms of Use and/or ensure compliance with, or assist in the investigation of, compliance with applicable law (including, but not limited to, the Company’s right to cooperate with any legal process in relation to Your use of Dereva and any third party claim that Your use of Dereva is unlawful or infringes such third party's rights, etc.).</p>

        <h4>Intellectual Property</h4>
        <p>All intellectual property in Dereva is owned by the Company or its licensors, which includes materials protected by copyright, trademark, or patent laws and other intellectual property laws and treaties applicable in Kenya or globally. All trademarks, service marks and trade names used on or in connection with Dereva are owned, registered and/or licensed by the Company. All content, data, information, etc., on Dereva (except for User Created Content), including but not limited to text, software, scripts, code, designs, graphics, photos, sounds, music, videos, applications, interactive features and any and all other content, data information, etc., and all intellectual property rights therein, are the proprietary property of the Company and/or its licensors; all rights reserved.</p>

        <h4>Indemnification</h4>
        <p>To the maximum extent permitted by law, You agree to defend and compensate in full the Company, its directors, officers, employees, affiliates, and agents in respect of any and all claims, damages and other losses suffered by the Company arising out of Your breach of this Agreement. </p>

        <h4>Representation and Warranties</h4>
        <ul>
          <li>The Company shall endeavour to make reasonable efforts to provide access to Dereva on a continuous and high quality basis. However, You expressly acknowledge and agree that use of Dereva is at Your sole risk and that access to the Dereva Service is provided to You on an “as is” and “as available” basis and without any warranty of any kind or nature.</li>
          <li>The Company does not warrant that use of Dereva service/Site will be uninterrupted or error free and the Company does not accept any responsibility if for any reason the Dereva service or any part of it is unavailable at any time or for any period. </li>
          <li>To the maximum extent permitted by applicable law, the Company expressly disclaims any implied or statutory terms, conditions, representations or warranties which may apply to Dereva or any content on it, including warranties of merchantability, fitness for a particular purpose, title, accuracy, correspondence with description, satisfactory quality and non-infringement. </li>
        </ul>

        <h4>Limitation of Liability</h4>
        <ul>
          <li>The Company will not be liable for any damages of any kind arising out of or relating to the use or the inability to use Dereva and its content or links, including but not limited to damages caused by or related to errors, omissions, interruptions, defects, delay in operation or transmission, virus, line failure, and all other direct, indirect, special, incidental, exemplary or consequential damages even if the Company has been advised of the possibility of such damages.</li>
          <li>Nothing in these terms of use excludes or limits a User’s legal rights as a consumer or the Company’s liability for death or personal injury arising from the Company’s negligence, for fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by applicable law.</li>
          <li>The Company makes no guarantees, warranties or representations as to the actions or conduct of any User using Dereva. Responsibility for the decisions a User makes regarding the Dereva service rests solely with that User. The User agrees that is the User’s responsibility to take reasonable precautions in all actions and interactions with any third party the User interacts with through Dereva/Site.</li>
          <li>Your interaction and use of Dereva pursuant to this Agreement is fully and entirely Your responsibility. The Company does not screen or otherwise evaluate potential drivers/employers that have signed up on Dereva. You understand therefore that by using the Application and Dereva, You may be introduced to third parties that may be potentially dangerous, and that You use the Application and Dereva at Your own risk.</li>
          <li>The Company will not assess the suitability, legality or ability of any such third parties and You expressly waive and release the Company from any and all liability, claims, causes of action, or damages arising from Your use of the Application or Dereva, or in any way related to the third parties introduced to You by the Application or Dereva.</li>
        </ul>

        <h4>Changes to Terms of Use</h4>
        <ul>
          <li>The Company may change or amend our Terms of Use from time to time to correct editorial defects or phrase it more clearly without notice.</li>
          <li>The Company shall notify Users, through the Site, at least 30 days prior (“Notification Period”) to affecting any material changes to the Terms of Use.</li>
          <li>Your continued to use of Dereva after expiry the Notification Period will deemed to mean Your acceptance of the changes. </li>
          <li>You are free to terminate Your agreement to these Terms of Use at any time should You choose not to accept the revised Terms of Use.</li>
        </ul>

        <h4>Governing Law and Dispute Resolution</h4>
        <p>This Agreement shall be governed by the Laws of the Republic of Kenya. Any dispute arising out of or in connection with this Terms of Use, including any question regarding its existence, validity or termination, shall be resolved under provisions of the Arbitration Act [Cap.49, Laws of Kenya] [as amended from time to time]. The parties shall mutually appoint one arbitrator failing which such arbitrator shall be appointed by the Chairperson for the time being of the Institute of Arbitrators [Kenya Branch] upon the application of either party who shall not be of less than 5 years standing as an Advocate of the High Court of Kenya. The place and seat of arbitration shall be Nairobi and the language of arbitration shall be English. The award of the arbitration tribunal shall be final and binding upon the parties to the extent permitted by law and either party may apply to a court of competent jurisdiction for enforcement of such an award. The award of the arbitral tribunal may take the form of an order to pay an amount or to perform or to prohibit certain activities.</p>

        <h4>Termination</h4>
        <ul>
          <li>You may choose to stop using Dereva or to close Your Account at any time.</li>
          <li>This Agreement is effective until terminated by You or by the Company. This Agreement and Your rights under this Agreement will terminate immediately upon notice from the Company if You fail to comply with any term(s) or conditions of this Agreement, You have clearly expressed or demonstrated (regardless of whether directly or through your actions or statements or otherwise) that You do not intend to comply with these Terms of Use, or in the event that Your Account is closed by You or the Company for any reason.</li>
          <li>Upon termination of this Agreement, You shall cease all use of Dereva, and destroy all copies, full or partial, in relation to Dereva, including any accompanying documentation.</li>
          <li>Furthermore, the Company reserves the right to terminate these Terms of Use at any time if;
            <ol>
              <li>The Company or any of our partners/affiliates providing or connected with Dereva resolve to terminate Dereva, in which or any parts thereof, or if any of our partner decides to terminate the entire relationship with us, regardless of the reason of such termination, including where we or any of our partner are of the opinion that the provision of Dereva service or parts thereof to You or to us or together with us are no longer commercially feasible; and </li>
              <li>The Company or any of our partner providing the Dereva service are required to terminate the provision of the Dereva service or any parts thereof due to changes in laws, regulations or by order of a court of competent jurisdiction.</li>
            </ol>
          </li>
        </ul>

        <h4>Miscellaneous</h4>
        <ul>
          <li>(a)Unless expressly stated otherwise, these Terms of Use and the documents referred to in them contain the entire agreement between the Company and You relating to Your use of Dereva and supersede any previous agreements, arrangements, undertakings or proposals, written or oral.</li>
          <li>(b)The Company may assign, sub-contract or otherwise transfer any or all of our rights and obligations under these Terms of Use to any company, firm or person. You may not assign, sub-contract or otherwise transfer Your rights or obligations under these Terms to anyone else.</li>
          <li>(c)If the Company decides not to exercise or enforce any right that it may have against You at a particular time, then this does not prevent the Company from exercising or enforcing that right later.</li>
          <li>(d)If any part of these Terms of Use is found to be illegal, invalid or otherwise unenforceable by a court or regulator, then, where required, that part shall be deleted and the remaining parts of the Terms of Use will continue to be enforceable.</li>
          <li>(e)These Terms of Use do not create or infer any rights that are enforceable by any person who is not party to them.</li>
          <li>The Company shall not be liable or responsible for delay in performance of, or any failure to perform any of its obligations under these Terms of Use that is caused by events outside its reasonable control (“Force Majeure”), in particular (but not limited to)
            <ol>
              <li>Acts, decrees, legislation, regulations or restrictions;</li>
              <li>Unavailability of public or private telecommunication networks; or</li>
              <li>Strikes, lock-outs or other industrial action, civil commotion, riot, invasion, terrorist attacks or threats of terrorist attacks, war (whether declared or not) or any natural disaster. The Company’s performance under these Terms of Use is deemed to be suspended for the period that Force Majeure continues.</li>
            </ol>
          </li>
        </ul>

        <h4>Acceptance</h4>
        <p>By clicking “I accept”, You expressly acknowledge and agree to be bound by the terms and conditions of this Agreement.</p>
      </div>
    </section>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <footer class="new-footer">
    <div class="container-fluid hidden-xs">
      <div class="col-sm-4">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-4">
        <p>&copy; Dereva.com | All rights reserved.</p>
        <a href="terms-of-service.php">Terms of Service</a>
      </div>

      <div class="col-sm-4">
        <ul style="float: right;">
          <li>Follow Us</li><br>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
        </ul>
      </div>
    </div>

    <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; Dereva.com | All rights reserved.</p>
      </div>
    </div>
  </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/showup.js"></script>
<script src="js/wow.js"></script>