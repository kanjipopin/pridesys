<?php
	ob_start();
	session_start();
  	require_once('../config/db.php');
  	include("php-mailer/class.phpmailer.php");
  	// include 'config/checkStatus.php';
  	// include 'config/OAuth.php';

  	$refrer = $_SERVER['HTTP_REFERER'];
  	if($refrer == "https://enterprise.dereva.com/payment_success.php"){
  		header("location : https://enterprise.dereva.com/payment_success.php");
  	}

  	$todatedate = date("Y-m-d");
  	$activeClass = "Pricing";
  	@$Email = $_SESSION['Email'];
  	@$driversLimit = $_SESSION['driversLimit'];
  	@$amount = $_SESSION['amount'];
  	@$time = $_SESSION['time'];

  	$fname = '';
	$lname = '';
	$plan = '';
	$pesapalMerchantReference = '';

  	// GET COMPANY DETAILS
  	$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompDetails->execute();
	$getCompDetailsRow = $getCompDetails->fetch();
		$comp_name = $getCompDetailsRow['comp_name'];
		

		$_SESSION['comp_name'] = $comp_name;

	// GET PLAN DETAILS
	// $getPlanDetails = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$plan}'");
	// $getPlanDetails->execute();
	// $getPlanDetailsRow = $getPlanDetails->fetch();
		$driver_limit = $driversLimit;


	// // DATA FORM PESAPAL RESPONSE
	// $pesapalMerchantReference = null;
	// $pesapalTrackingId = null;
	// $checkStatus = new pesapalCheckStatus();

	// if(isset($_GET['pesapal_merchant_reference'])){
	// 	$pesapalMerchantReference = $_GET['pesapal_merchant_reference'];
	// }

	// if(isset($_GET['pesapal_transaction_tracking_id'])){
	// 	$pesapalTrackingId = $_GET['pesapal_transaction_tracking_id'];
	// }

	// $status = $checkStatus->checkStatusUsingTrackingIdandMerchantRef($pesapalMerchantReference,$pesapalTrackingId);

	// $_SESSION['status'] = $status;

	// if($status == "COMPLETED"){

	$TransID = $_GET['TransID'];
	$CCDapproval = $_GET['CCDapproval'];
	$PnrID = $_GET['PnrID'];


	$checkPrevPak = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id desc limit 1");
	$checkPrevPak->execute();
	$checkPrevPakCount = $checkPrevPak->rowCount();
	$checkPrevPakRow = $checkPrevPak->fetch();

		$prves_package_id = $checkPrevPakRow['order_id'];
		$prves_refrence_id = $checkPrevPakRow['refrence_id'];
		$prves_Pack_EndDate = $checkPrevPakRow['package_end_date'];
		$prves_package_status = $checkPrevPakRow['package_status'];
		$prves_package_Drv_Limit = $checkPrevPakRow['driver_limit'];
		$package_activation_date = date("Y-m-d H:i:s");

	if($checkPrevPakCount > 0){

		if($prves_package_status == "completed"){

			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+ ".$time." days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}


			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,CCDapproval,PnrID,package_start_date,package_end_date,package_status,package_activation_date,amount,days) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$pesapalMerchantReference','$TransID','$CCDapproval','$PnrID','$package_start_date','$package_end_date','current','$package_activation_date','$amount','$time')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit',payment_success_status='completed' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
		}

		elseif($prves_package_status == "current" && $getCompDetailsRow['driver_limit'] == 0){

			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+ ".$time." days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}


			$updatePrevious_pack = $conn->prepare("UPDATE enterprise_order_details set package_status = 'completed' where order_id = '{$prves_package_id}'");
			$updatePrevious_pack->execute();

			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,CCDapproval,PnrID,package_start_date,package_end_date,package_status,package_activation_date,amount,days) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$pesapalMerchantReference','$TransID','$CCDapproval','$PnrID','$package_start_date','$package_end_date','current','$package_activation_date','$amount','$time')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit',payment_success_status='completed' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
		}

		elseif($prves_package_status == "current" && $getCompDetailsRow['driver_limit'] > 0){

			// PACKAGE IS NOT CARRY FORWARD
			$package_start_date = date("Y-m-d");
			$package_end_date = date("Y-m-d", strtotime("+ ".$time." days"));
			// $day_30 = date("Y-m-d", strtotime($lastdate."- 30 days"));

			$newPackDriverLimit = $driver_limit;

			$NewUpdateDriverLimit = "";
			if($prves_package_Drv_Limit == $newPackDriverLimit){
				$NewUpdateDriverLimit = $getCompDetailsRow['driver_limit'];
			}
			elseif($prves_package_Drv_Limit < $newPackDriverLimit){
				$NewUpdateDriverLimit = $newPackDriverLimit - $getCompDetailsRow['working_driver'];
			}

			$updatePrevious_pack = $conn->prepare("UPDATE enterprise_order_details set package_status = 'completed' where order_id = '{$prves_package_id}'");
			$updatePrevious_pack->execute();

			$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,CCDapproval,PnrID,package_start_date,package_end_date,package_status,package_activation_date,amount,days) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$pesapalMerchantReference','$TransID','$CCDapproval','$PnrID','$package_start_date','$package_end_date','current','$package_activation_date','$amount','$time')");
			$insertTrailPack->execute();

			$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$NewUpdateDriverLimit',payment_success_status='completed' where Email='{$Email}'");
			$updateCompDriverLimit->execute();
		}
	}
	else {
		$package_start_date = date("Y-m-d");
		$package_end_date = date('Y-m-d', strtotime("+ ".$time." days"));

		$insertTrailPack = $conn->prepare("INSERT into enterprise_order_details 
			(comp_name,comp_email,emp_fname,emp_lname,package_id,driver_limit,refrence_id,tracking_id,CCDapproval,PnrID,package_start_date,package_end_date,package_status,package_activation_date,amount,days) 
			values ('$comp_name','$Email','$fname','$lname','$plan','$driver_limit','$pesapalMerchantReference','$TransID','$CCDapproval','$PnrID','$package_start_date','$package_end_date','current','$package_activation_date','$amount','$time')");
		$insertTrailPack->execute();

		$updateCompDriverLimit = $conn->prepare("UPDATE logis_company_subadmin set driver_limit='$driver_limit',working_driver='0',payment_success_status='completed' where Email='{$Email}'");
		$updateCompDriverLimit->execute();
	}

		$smtpQuery = $connection->OneRow("*","smtp_settings");

		$adminMsg = "<html><body>
		<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
		$adminMsg .= "<p>Dear Admin</p>";
		$adminMsg .= "<p>".$comp_name." has made payment of ".$amount." Kes. please log into pesapal admin panel to verify the payment</p>";
		$adminMsg .="<p>The Enterprise Team</p></body></html>";

		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug  = 0;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = $smtpQuery['smtp_secure'];
		$mail->Host = $smtpQuery['smtp_host'];
		$mail->Port = $smtpQuery['smtp_port'];
		$mail->Username = $smtpQuery['smtp_username'];
		$mail->Password = $smtpQuery['smtp_password'];
		$mail->From = $smtpQuery['smtp_username'];
		$mail->FromName = "Dereva Enterprise";
		$mail->Subject = "Payment Status Confirmation";
		$mail->MsgHTML($adminMsg);
		$mail->AddAddress("enterprise@dereva.com", "Dereva Enterprise");
		$mail->send();


		$userMsg = "<html><body>
		<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='The Enterprise Team' title='The Enterprise Team'><br><br>";
		$userMsg .= "<p>Dear ".$comp_name.",</p>";
		$userMsg .= "<p>Your Payment has been verified and you have successfully subscribed to the Enterprise Plan package and your subscription will expire on ".$package_end_date.".</p>";
		$userMsg .= "<p>To access your Dashboard please log in with your password : https://www.enterprise.dereva.com</p>";
		$userMsg .="</td></tr><tr><td height='25' valign='middle'><p>The Enterprise Team</p></td></tr></table></td></tr></table></body></html>";

		$mail2 = new PHPMailer();
		$mail2->IsSMTP();
		$mail2->SMTPDebug  = 0;
		$mail2->SMTPAuth   = true;
		$mail2->SMTPSecure = $smtpQuery['smtp_secure'];
		$mail2->Host = $smtpQuery['smtp_host']; 
		$mail2->Port = $smtpQuery['smtp_port']; 
		$mail2->Username = $smtpQuery['smtp_username']; 
		$mail2->Password = $smtpQuery['smtp_password']; 
		$mail2->From = $smtpQuery['smtp_username'];
		$mail2->FromName = "Dereva Enterprise";
		$mail2->Subject = "Payment Status Confirmation";
		$mail2->MsgHTML($userMsg);
		$mail2->AddAddress($Email, $comp_name);
		$mail2->send();

		header("location: payment_success.php");
?>