<?php
  require_once('../config/db.php');
  include("php-mailer/class.phpmailer.php");
  session_start();

  $activeClass = "Drivers";
  @$Email = $_SESSION['Email'];
  if($Email == ""){
    $connection->redirect('../index.php');
  }

  $getDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $getDetails->execute();
  $getDetailsRow = $getDetails->fetch();
  $companyName = $getDetailsRow['comp_name'];

  if(isset($_REQUEST['chngEmailBtn'])){

    $new_email_add = $_REQUEST['new_email_add'];
    $OTPchars = "0123456789";
    $OTPNum = substr(str_shuffle($OTPchars), 0, 5);

    $_SESSION['new_email_add'] = $new_email_add;
    $_SESSION['OTPNum'] = $OTPNum;


    $smtpQuery = $connection->OneRow("*","smtp_settings");

    $userMsg = "<html><body>
    <img src='https://dereva.com/images/logo.jpg' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
    $userMsg .= "<p>Dear ".$getDetailsRow['comp_name'].",</p><p>Please find below OTP Number for confirm your email address.</p>";
    $userMsg .= "<p>Your New Email : ".$new_email_add."</p>";
    $userMsg .= "<p>OTP Number : ".$OTPNum."</p>";
    $userMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Enterprise Team</p></td></tr></table></td></tr></table></body></html>";


    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = $smtpQuery['smtp_secure'];
    $mail->Host = $smtpQuery['smtp_host']; 
    $mail->Port = $smtpQuery['smtp_port']; 
    $mail->Username = $smtpQuery['smtp_username']; 
    $mail->Password = $smtpQuery['smtp_password']; 
    $mail->From = $smtpQuery['smtp_username'];
    $mail->FromName = "Dereva Enterprise";
    $mail->Subject = "Change Email Address";
    $mail->MsgHTML($userMsg);
    $mail->AddAddress($new_email_add, $getDetailsRow['comp_name']);
    $mail->send();

      header("location: confirmation_email_otp.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Dereva Portal</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
	</head>
  <body>
    <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-sm-3 col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva" title="Dereva"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                  <?php echo $companyName; ?>
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="about-us.php">About Us</a></li>
                    <li><a href="pricing_table.php">Pricing</a></li>
                    <li><a href="invoice.php">Billing</a></li>
                    <li><a href="functions/logout.php">Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </header>

    <div class="enterprise-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <form enctype="multipart/form-data" method="POST" id="login_form" name="login_form">
              <h4>Change Email</h4>
              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="email" class="form-control" name="new_email_add" id="new_email_add" placeholder="New Email Address" required>
              </div>

              <div class="clearfix"></div>
              <button type="submit" id="chngEmailBtn" name="chngEmailBtn" class="btn btn-default">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <footer class="new-footer">
      <div class="container-fluid hidden-xs">
        <div class="col-sm-4">
          <p>Western Heights, Karuna Road <br>
            <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
            <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
          </p>
        </div>

        <div class="col-sm-4">
          <p>&copy; dereva.com | All rights reserved.</p>
          <a href="terms-of-service.php">Terms of Service</a>
        </div>

        <div class="col-sm-4">
          <ul style="float: right;">
            <li>Follow Us</li><br>
            <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
            <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
            <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
            <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
          </ul>
        </div>
      </div>

      <div class="container-fluid visible-xs" style="text-align: center;">
        <div class="col-sm-3">
          <p>Western Heights, Karuna Road <br>
            <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
            <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
          </p>
        </div>

        <div class="col-sm-3">
          <ul>
            <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
            <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
            <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
            <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
          </ul>
        </div>

        <div class="col-sm-3">
          <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; dereva.com | All rights reserved.</p>
        </div>
      </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
  </body>
</html>