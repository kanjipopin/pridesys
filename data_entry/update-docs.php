<?php

	include("config/db.php");

	$id = base64_decode($_REQUEST['id']);
	$docs = isset($_REQUEST['docs']) ? $_REQUEST['docs'] : "";

	if($docs == "nsn"){
		$docColumn = "edocs_id_card";
	}
	elseif($docs == "dl"){
		$docColumn = "edocs_dl_copy";
	}
	elseif($docs == "app"){
		$docColumn = "edocs_app_letter";
	}
	elseif($docs == "cv"){
		$docColumn = "edocs_cv";
	}
	elseif($docs == "qualification"){
		$docColumn = "edocs_qualification";
	}
	elseif($docs == "conduct"){
		$docColumn = "edocs_conduct_cert";
	}
	elseif($docs == "pin"){
		$docColumn = " edocs_pin_cert ";
	}
	elseif($docs == "nssf"){
		$docColumn = "edocs_nssf";
	}
	elseif($docs == "nhif"){
		$docColumn = " edocs_nhif";
	}
	elseif($docs == "contract"){
		$docColumn = " contract_file";
	}

	$updateDocs = $conn->prepare("UPDATE logis_emp_docs set ".$docColumn." = '' where edocs_nsn = '{$id}'");
	$updateDocs->execute();

	$updateContract = $conn->prepare("UPDATE logis_employee_details set ".$docColumn." = '' where empdt_NSN = '{$id}'");
	$updateContract->execute();

	header("location: driver-detail.php?id=".$_REQUEST['id']);

?>