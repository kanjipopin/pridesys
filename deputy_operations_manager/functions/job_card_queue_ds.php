<?php

/**
 * This is class to handle the jobs to be done on the job card
 */

class job_card_queue_ds
{


    public $job_card_queue = array();


    public function jobs_available(): int {

        return count($this->job_card_queue);

    }

    public function is_jobs_to_done_empty() {

        if($this->jobs_available === 0) {
            return true;
        } else {
            return false;
        }

    }

    public function enqueue($element) {

        $this->job_card_queue[] = $element;

    }

    public function dequeue() {

        if(!$this->is_jobs_to_done_empty()) {

            array_shift($this->job_card_queue);

        }

    }

    public function return_jobs_to_be_done(): array
    {

        return $this->job_card_queue;

    }

}


?>