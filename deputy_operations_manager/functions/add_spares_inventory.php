<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$deputy_operations_manager_email_add = $_SESSION['deputy_operations_manager_email_add'];

if(isset($_POST['submit'])){

    $spares_category = $_POST['spares_category'];
    $spares_name = $_POST['spares_name'];
    $spares_stock = $_POST['spares_stock'];

    $SparesCols = array("SpareName"=>$spares_name,"SpareCategory"=>$spares_category,"SpareStock"=>$spares_stock);
    $SparesTable = "spares_inventory";
    $insertToSparesTable = $connection->InsertQuery($SparesTable,$SparesCols);

    if($insertToSparesTable == "success"){

            $connection->redirect("../spares_inventory.php");

    }
    else {

        echo "failed to insert to spares inventory";

    } 

    
}
?>