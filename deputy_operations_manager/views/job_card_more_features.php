<?php 

include'header.php'; 

if(isset($_POST['delete_job_card_feature'])) {

  $job_card_feature_id = base64_decode($_POST['del_job_card_feature_id']);


  $database->fetch_in_and_out_record_column_name($job_card_feature_id);

  //$database->drop_record_in_table_column($database->record_in_out_field_name);
          

  if($database->drop_record_in_table_column($database->record_in_out_field_name) === "True"){

    if($database->drop_record_out_table_column($database->record_in_out_field_name) === "True") {
  
          $JobCardFeatureField = "ID";
          $JobCardFeatureTable = "job_card_more_features_master";
          $deleteJobCardFeature = $connection->DeleteRow($JobCardFeatureTable,$JobCardFeatureField,$job_card_feature_id);
        
          if($deleteJobCardFeature=="success"){
              $connection->redirect("job_card_more_features.php?ref=success");
          }
          else {
              $connection->redirect("job_card_more_features.php?ref=error");
          } 

      } else {
        $connection->redirect("job_card_more_features.php?ref=error_delete_out_record");
    }
  
  } else {
      $connection->redirect("job_card_more_features.php?ref=error_delete_in_record");
  }


}

if(isset($_POST['edit_job_card_feature'])) {

  $job_card_feature_id = base64_decode($_POST['edit_job_card_feature_id']);

  $job_card_feature_name_plain = $_POST['edit_job_card_feature_name'];

  $job_card_feature_name = str_replace(" ","_",$job_card_feature_name_plain);


  $database->fetch_in_and_out_record_column_name($job_card_feature_id);
                

  if($database->edit_record_in_table_column($database->record_in_out_field_name, $job_card_feature_name) === "True"){

    if($database->edit_record_out_table_column($database->record_in_out_field_name, $job_card_feature_name) === "True") {
  
        
          $editCols = array("JobCardMoreFeatures"=>$job_card_feature_name_plain);
          $condition = "ID='".$job_card_feature_id."'";
          $editTable = "job_card_more_features_master";
          $updateJobCardFeatures = $connection->UpdateQuery($editTable,$editCols,$condition);

          if($updateJobCardFeatures=="success"){
              $connection->redirect("job_card_more_features.php?ref=success");
          }
          else {
              $connection->redirect("job_card_more_features.php?ref=error");
          } 

      } else {
        $connection->redirect("job_card_more_features.php?ref=error_delete_out_record");
    }
  
  } else {
      $connection->redirect("job_card_more_features.php?ref=error_delete_in_record");
  } 


}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>More Features (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)
           
      </h4>

        
        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_job_card_more_features.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="job_card_more_features.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Feature</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = base64_encode($fd['ID']);

                  $job_card_more_feature = $fd['JobCardMoreFeatures'];


            ?>
              <tr>
                <td><?php echo $job_card_more_feature; ?></td>

                <td>

                  <a href="#editJobCardMoreFeaturesDialog" class="openEditDialog" data-toggle="modal" data-editname="<?php echo $job_card_more_feature; ?>" data-editid="<?php echo $id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>
                  <a href="#deleteJobCardMoreFeaturesDialog" class="openDeleteDialog" data-toggle="modal" data-deleteid="<?php echo $id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Delete
                  </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd1['ID']);

                  $job_card_more_feature = $fd1['JobCardMoreFeatures'];
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $job_card_more_feature; ?>
            </div>
            <div class="block-2">

              <a href="<?php echo 'job-card-detail.php?id='.$id; ?>" class="view-profile">View</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="vehicles.php"><img src="images/drivers-icon1.svg">My Vehicles(<?php echo $rowCount; ?>)</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteJobCardMoreFeaturesDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Delete job card feature</h2>
                
                <p class="delete-p">Are you sure you want to delete this job card feature?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myDeleteJobCardFeatureId" class="form-control" name="del_job_card_feature_id" placeholder="JobCardFeatureId">

                  <input type="submit" style="float: right; " name="delete_job_card_feature" value="Delete" class="btn btn-danger">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="editJobCardMoreFeaturesDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Edit job card feature</h2>
                

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myEditJobCardFeatureId" class="form-control" name="edit_job_card_feature_id" placeholder="JobCardFeatureId">

                  <input type="text" id="myEditJobCardFeatureName" class="form-control" name="edit_job_card_feature_name" placeholder="Feature name">

                  <input type="submit" style="float: right; " name="edit_job_card_feature" value="Update" class="btn btn-primary">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });

    $("#filterForm").on('click',function(e){
      var data="";
      e.preventDefault();
      var drvName = $("#drvName").val();
      var drvLicenseNum = $("#dln").val();
      var contactNo = $("#contact").val();
      //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

      $.ajax({
        type : "POST",
        url  : "functions/drivers.php",
        data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
        success : function(msg){
           // var msg = msg;
           //alert("this message:"+msg);
           $("#showDriverData").html("");
           $("#showDriverData").html(msg);
        }
      });


    });

    $(document).on("click", ".openDeleteDialog", function () {
            var myJobCardFeatureId = $(this).data('deleteid');
            $("#myDeleteJobCardFeatureId").attr("value", myJobCardFeatureId);
    });

    $(document).on("click", ".openEditDialog", function () {
            var myJobCardFeatureId = $(this).data('editid');
            var myJobCardFeatureName = $(this).data('editname');
            $("#myEditJobCardFeatureId").attr("value", myJobCardFeatureId);
            $("#myEditJobCardFeatureName").attr("value", myJobCardFeatureName);
    });


  </script>

<?php include'footer.php'; ?>