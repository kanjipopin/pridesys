<?php
@$deputy_operations_manager_email_add = $_SESSION['deputy_operations_manager_email_add'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$deputy_operations_manager_email_add}'");
$comp->execute();
$comprow = $comp->fetch();

$user_id = $comprow['Id'];

	$user_name = $comprow['Name'];

  $user_dept = $comprow['Department'];

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}


include'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Job card</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="job_card.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/edit-job-card-form.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Job card information</h4>

            <div class="form-group">
              <label for="inputdeputy_operations_manager_email_add3" class="col-sm-3 control-label">Job card number :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="Job_Card_No" id="Job_Card_No" value="<?php echo $results['Job_Card_No'];?>" readonly>

              </div>

            </div>

            <div class="form-group">
              <label for="inputdeputy_operations_manager_email_add3" class="col-sm-3 control-label">Driver's Name: :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="Job_Card_DriversName" id="Job_Card_DriversName" value="<?php echo $results['Job_Card_Drivers_Name']; ?>" placeholder="Driver's name *" >

              </div>

               <div class="col-sm-3">

                  <select class="form-control" id="Job_Card_Vehicle_Reg" name="Job_Card_Vehicle_Reg" onchange="select_vehicle_model()">
                    <option value="" selected disabled>Select vehicle reg no</option>

                    <?php

                      $value = $conn->prepare("SELECT * from pridedrive_vehicles");
                      $value->execute();
                      $chkMaster = "";
                      while($masterRow = $value->fetch()){
                          if($masterRow['VehicleId'] === $results['Job_Card_Vehicle_Reg']){
                              $chkMaster = "selected";
                          }
                          else{
                              $chkMaster = "";
                          }
                          ?>
                          <option value = "<?php echo $masterRow['VehicleId']; ?>" <?php echo $chkMaster; ?>> <?php echo $masterRow['VehicleReg']; ?></option>
                    <?php } ?>

                  </select>

              </div>
              <div class="col-sm-3">

                    <?php

                      $job_card_vehicle_reg = $results['Job_Card_Vehicle_Reg'];

                      $vehicle_master = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$job_card_vehicle_reg' ");
                      $vehicle_master->execute();
                      $getVehicleData = $vehicle_master->fetch(PDO::FETCH_ASSOC);

                      $vehicle_model = $getVehicleData['VehicleModel'];

                      $value = $conn->prepare("SELECT * from model_master WHERE ID='$vehicle_model' ");
                      $value->execute();
                      $getVehicleModelData = $value->fetch(PDO::FETCH_ASSOC);
                    
                    ?>

                    <input type="text" class="form-control" id="Job_Card_Vehicle_Model" name="Job_Card_Vehicle_Model" value="<?php echo $getVehicleModelData['Model'];?>" disbaled>      


              </div>
            </div>

            <div class="form-group">
              <label for="inputdeputy_operations_manager_email_add3" class="col-sm-3 control-label">From (Client) :</label>
              <div class="col-sm-3">

              <select class="form-control" id="Job_Card_ClientsName" name="Job_Card_ClientsName" >
              <option value="" selected disabled>Select client</option>
              <?php

                $value = $conn->prepare("SELECT * from logis_company_sales_rep_clients");
                $value->execute();
                $chkMaster = "";
                while($masterRow = $value->fetch()){
                    if($masterRow['Id'] === $results['Job_Card_Clients_Name']){
                        $chkMaster = "selected";
                    }
                    else{
                        $chkMaster = "";
                    }
                    ?>
                    <option value = "<?php echo $masterRow['Id']; ?>" <?php echo $chkMaster; ?>> <?php echo $masterRow['ClientsName']; ?></option>
              <?php } ?>
              </select>

              </div>

              <div class="col-sm-3">
                
                <input type="text" class="form-control" name="Job_Card_Opened_By_Display" id="Job_Card_Opened_By_Display" value="<?php echo $user_name;?>" disabled>
                <input type="hidden" class="form-control" name="Job_Card_Opened_By" id="Job_Card_Opened_By" value="<?php echo $user_id;?>" required>

              </div>

              <div class="col-sm-3">
                
               <input type="text" class="form-control" name="Job_Card_Department" id="Job_Card_Department" value="<?php echo $user_dept;?>" readonly>

              </div>

            </div>

            <div class="form-group">
            <label for="inputdeputy_operations_manager_email_add3" class="col-sm-3 control-label">When vehicle in :</label>
              <div class="col-sm-3">

                <input type="datetime-local" class="form-control" name="Job_Card_DateTimeIn" id="Job_Card_DateTimeIn" placeholder="Date and time in" value="<?php echo $results['Job_Card_Date_Time_In']; ?>" >

              </div>

              <div class="col-sm-3">
                
                <input type="text" class="form-control" name="Job_Card_FuelIn" id="Job_Card_FuelIn" placeholder="Fuel in" value="<?php echo $results['Job_Card_Fuel_In']; ?>">

              </div>

              <div class="col-sm-3">
                
                <input type="text" class="form-control" name="Job_Card_MileageIn" id="Job_Card_MileageIn" placeholder="Mileage in" value="<?php echo $results['Job_Card_Mileage_In']; ?>">

              </div>

            </div>

            <div class="form-group">
            <label for="inputdeputy_operations_manager_email_add3" class="col-sm-3 control-label">When vehicle out :</label>
              <div class="col-sm-3">

                <input type="datetime-local" class="form-control" name="Job_Card_DateTimeOut" id="Job_Card_DateTimeOut" placeholder="Date and time out" value="<?php echo $results['Job_Card_Date_Time_Out']; ?>" >

              </div>

              <div class="col-sm-3">
                
                <input type="text" class="form-control" name="Job_Card_FuelOut" id="Job_Card_FuelOut" placeholder="Fuel out" value="<?php echo $results['Job_Card_Fuel_Out']; ?>">

              </div>

              <div class="col-sm-3">
                
                <input type="text" class="form-control" name="Job_Card_MileageOut" id="Job_Card_MileageOut" placeholder="Mileage out" value="<?php echo $results['Job_Card_Mileage_Out']; ?>">

              </div>

            </div>

          </span>


         <span class="show_next_kin">
            <div class="job_to_be_done">
              <h4>Jobs to be done</h4>

              <?php

                    $jobs = explode(",",$results['Jobs_To_Be_Done']);

                    foreach($jobs as $key=>$value){
                        
              ?>

              <div class="form-group" id="new_jobs_to_be_done_loop<?php echo $key;?>">
                <label class="col-sm-3 control-label">Job :</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="job_to_be_done[]" placeholder="Job to be done" value="<?php echo $value;?>">
                </div>
                <div class="col-sm-2">
                    <a href="javascript:remove_jobs_to_be_done('new_jobs_to_be_done_loop<?php echo $key;?>')">Remove</a>
                </div>
              </div>

              <?php } ?>

              <div class="form-group" id="new_jobs_to_be_done">
                
              </div>
              
              <div class="form-group" id="new_jobs_to_be_done_template" style="display:none;">
                <label class="col-sm-3 control-label">Job :</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="job_to_be_done[]" placeholder="Job to be done" >
                </div>
              </div>

              <a href="javascript:add_jobs_to_be_done_field()">Add job to be done</a>

            </div>

          </span>

          <span class="show_next_kin">

              <div class="col-sm-6">

                      <table class="table">
                        <thead>
                          <tr>
                            <th>Feature</th>
                            <th>Vehicle In</th>
                            <th>Vehicle Out</th>
                          </tr>
                        </thead>

                        <tbody>

                        <?php
                          $value = $conn->prepare("SELECT * from job_card_more_features_master ");
                          $value->execute();

                          while($valueRow = $value->fetch()){

                                $more_features_field = str_replace(" ","_",$valueRow['JobCardMoreFeatures']);
                                //echo $more_features_field;
                            
                        ?>
                          <tr>
                            <td><?php echo $valueRow['JobCardMoreFeatures']; ?></td>
                            <td>
                              <input type="checkbox" class="form-control" name="JobCardMoreFeaturesCheck[]" value="<?php echo str_replace(" ","_",$valueRow['JobCardMoreFeatures']); ?>" <?php echo $database->select_specific_table_column($more_features_field,$job_card_no); ?> disabled>
                            </td>
                            <td>
                              <input type="checkbox" class="form-control" name="JobCardMoreFeaturesCheckAfter[]" value="<?php echo str_replace(" ","_",$valueRow['JobCardMoreFeatures']); ?>" <?php echo $database->select_specific_out_table_column($more_features_field,$job_card_no); ?> >
                            </td>
                          </tr>

                          <?php } ?>  

                        </tbody>
                      </table>

              </div>

          </span>

          <span class="show_salary">

            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Submit</button>
              </div>
            </div>
          </span>

        </form>


            </div>

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your deputy_operations_manager_email_add for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
            //   $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#inspectionExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#commenced_startDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#driver_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
            //   $('#vehicle_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function checkNSN(){
            var NSN = $("#NSN").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkDuplicateNSN","NSN":NSN,"compId":compId },

                success : function(msg){

                    if(msg == "same"){
                        $(".CheckNSNMsg").text("Driver already exists in your account.");
                        $("#submit").attr("disabled",true);
                    } else if(msg == "different") {
                        $(".CheckNSNMsg").text("Driver already exists on another account.");
                        $("#submit").attr("disabled",true);
                    } else {
                        $(".CheckNSNMsg").text("");
                        $("#submit").attr("disabled",false);
                    }
                }
            });
        }

        function flagNSN(){
            var NSN = $("#NSN").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkflagNSN","NSN":NSN,"compId":compId },

                success : function(msg){
                    if(msg > 0){
                        // $(".CheckFlagNSNMsg").text("This driver was Flaged. Please check your deputy_operations_manager_email_add address for more details about driver.");

                        $("#delivery_popup").modal("show");
                    } else {
                        $("#delivery_popup").modal("hide");
                    }
                }
            });
        }

        function getDept(){
            $(".load_branch").hide();
            $.ajax({
                type: "POST",
                url:"functions/ajaxaddmaster.php",
                data: $('#addDeptt').serialize(),
                success: function(msg){
                    // alert(msg);
                    $(".old_branch").hide();
                    $(".load_branch").show();
                    $(".load_branch").html(msg);
                    $('#addbranch').modal('hide');
                },
            });
        }



        $(document).ready(function() {
            $('#nssf').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nssf1').val(txtVal);
            });


            $('input[type=radio][name=Vehicle_Type]').change(function() {

                if(this.value === "Other") {

                    document.getElementById("toggle_Plat_Number").style.display = "none";

                } else {

                    document.getElementById("toggle_Plat_Number").style.display = "block";

                }

            });

            $('#nhifNo').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nhifNo1').val(txtVal);
            });
        });


        function getSecondKin(){
            $(".kinTwo").show();
            $("#secondKinBtn").hide();
        }


    </script>



    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        
        function add_jobs_to_be_done_field()
        {

          var div1 = document.createElement('div');

          div1.innerHTML = document.getElementById('new_jobs_to_be_done_template').innerHTML;

          document.getElementById('new_jobs_to_be_done').appendChild(div1);

        }

        function remove_jobs_to_be_done(remove_id)
        {

          document.getElementById(remove_id).remove();
          //console.log(remove_id);

        }

        function select_vehicle_model()
        {

          var vehicle_reg = document.getElementById('Job_Card_Vehicle_Reg').value;

          //console.log(vehicle_reg);

           $.ajax({
                type: "POST",
                url:"functions/ajax.php",
                data: { "type":"checkVehicleModel", "vehicle_reg":vehicle_reg },
                success: function(msg){

                    document.getElementById('Job_Card_Vehicle_Model').value = msg;


                },
            }); 

        }

    </script>

<?php include'footer.php'; ?>