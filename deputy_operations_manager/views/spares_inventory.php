<?php 

include'header.php'; 

if(isset($_POST['delete_spare_inventory'])) {

  $spare_id = $_POST['spare_id'];

  $spare_column = "Id";
  $SpareTable = "spares_inventory";
  $deleteSpareInventory = $connection->DeleteRow($SpareTable,$spare_column,$spare_id);

  if($deleteSpareInventory=="success"){
      $connection->redirect("spares_inventory.php?ref=success");
  }
  else {
      $connection->redirect("spares_inventory.php?ref=error");
  }

}

if(isset($_POST['edit_spare_inventory'])) {

  $spare_id = $_POST['edit_spares_id'];
  $spares_category = $_POST['edit_spares_category'];
  $spares_name = $_POST['edit_spares_name'];
  $spares_stock = $_POST['edit_spares_stock'];

    $SpareCols = array("SpareName"=>$spares_name,"SpareCategory"=>$spares_category,"SpareStock"=>$spares_stock);
    $SpareTable = "spares_inventory";
    $condition = "Id='".$spare_id."'";
    $updateSparesTable = $connection->UpdateQuery($SpareTable,$SpareCols,$condition);

    if($updateSparesTable=="success"){
      $connection->redirect("spares_inventory.php?ref=success");
    }
    else {
        $connection->redirect("spares_inventory.php?ref=error");
    }

}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>Spares inventory (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_spares_inventory.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="spares_inventory.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Category</th>
                <th>Spare name</th>
                <th>Stock remaining</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = $fd['Id'];

                  $spare_name = $fd['SpareName'];
                  $spare_category = $fd['SpareCategory'];
                  $spare_stock = $fd['SpareStock'];

            ?>
              <tr>
                <td><?php echo $spare_category; ?></td>
                <td><?php echo $spare_name; ?></td>
                <td><?php echo $spare_stock; ?></td>
                <td>
                  <a href="#editDialog" class="openEditDialog" data-toggle="modal" data-spares_id="<?php echo $id; ?>"
                    data-spares_name="<?php echo $spare_name; ?>" data-spares_category="<?php echo $spare_category; ?>" 
                    data-spares_stock="<?php echo $spare_stock; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Edit
                  </a>
                  <a href="#deleteJobCardDialog" class="openDeleteDialog" data-toggle="modal" data-spareid="<?php echo $id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Delete
                    </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = $fd['Id'];

                  $spare_name = $fd['SpareName'];
                  $spare_category = $fd['SpareCategory'];
                  $spare_stock = $fd['SpareStock'];
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $spare_category; ?>
            </div>
            <div class="block-2">
              <h5><?php echo $spare_name; ?></h5>
              <h5><?php echo $spare_stock; ?></h5>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="vehicles.php"><img src="images/drivers-icon1.svg">My Vehicles(<?php echo $rowCount; ?>)</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteJobCardDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Delete spare inventory</h2>
                
                <p class="delete-p">Are you sure you want to delete this spare part?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="spare_id" class="form-control" name="spare_id" >

                  <input type="submit" style="float: right; " name="delete_spare_inventory" value="Delete" class="btn btn-danger">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="editDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Edit spare</h2>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="edit_spares_id" class="form-control" name="edit_spares_id" >

                      <label for="edit_spares_category" class="col-sm-3 control-label">Spare's category</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="edit_spares_category" id="edit_spares_category" >

                      </div>
  

                      <div class="form-group">
                        <label for="edit_spares_name" class="col-sm-3 control-label">Spare's Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="edit_spares_name" id="edit_spares_name" >

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="edit_spares_stock" class="col-sm-3 control-label">Spare's Stock</label>
                        <div class="col-sm-9">
                          
                            <input type="text" class="form-control" name="edit_spares_stock" id="edit_spares_stock" >

                        </div>

                      </div>

                      <input type="submit" style="float: right; " name="edit_spare_inventory" value="Update" class="btn btn-primary">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });


    $(document).on("click", ".openDeleteDialog", function () {
            var spare_id = $(this).data('spareid');
            $("#spare_id").attr("value", spare_id);
    });

    $(document).on("click", ".openEditDialog", function () {
            var edit_spares_id = $(this).data('spares_id');
            var edit_spares_category = $(this).data('spares_category');
            var edit_spares_name = $(this).data('spares_name');
            var edit_spares_stock = $(this).data('spares_stock');

            $("#edit_spares_id").attr("value", edit_spares_id);
            $("#edit_spares_category").attr("value", edit_spares_category);
            $("#edit_spares_name").attr("value", edit_spares_name);
            $("#edit_spares_stock").attr("value", edit_spares_stock);

    });


  </script>

<?php include'footer.php'; ?>