<?php
ob_start();
session_start();
include '../../config/db.php';
include '../../config/includes/initialise.php';

@$sales_email_add = $_SESSION['sales_email_add'];

if(isset($_POST['submit'])){


    // Sales rep Details

    $SalesRepName = isset($_POST['SalesRepName']) ? $_POST['SalesRepName'] : "";
    $SalesRepEmail = isset($_POST['SalesRepEmail']) ? $_POST['SalesRepEmail'] : "";
    $SalesRepPhone = isset($_POST['SalesRepPhone']) ? $_POST['SalesRepPhone'] : "";

    $SalesRepPassword = $_POST['SalesRepPassword'];
    $SalesRepConfirmPassword = $_POST['SalesRepConfirmPassword'];

    if($SalesRepPassword == $SalesRepConfirmPassword) {

        $database->set_password($SalesRepPassword);

        $password = $database->password;

        $RegDate = $database->now_date_only;

        $RegTime = $database->now_time_only;


        $userCols = array("Name"=>$SalesRepName,"Email"=>$SalesRepEmail,"PhoneNumber"=>$SalesRepPhone, "Password"=>$password,"RegDate"=>$RegDate,"RegTime"=>$RegTime);
        $userTable = "logis_company_sales_rep";
        $insertUser = $connection->InsertQuery($userTable,$userCols);


        if($insertUser == "success"){
            $connection->redirect("../sales_rep.php");
        }
        else {
            $connection->redirect("../add_sales_rep.php?error_msg=Failed to register sales representative");
        }

    } else {

        $connection->redirect("../add_sales_rep.php?error_msg=Passwords do not match");

    }

}
?>