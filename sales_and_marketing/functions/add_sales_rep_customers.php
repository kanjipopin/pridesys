<?php
ob_start();
session_start();
include '../../config/db.php';
include '../../config/includes/initialise.php';

@$sales_email_add = $_SESSION['sales_email_add'];

if(isset($_POST['submit'])){


    // Sales rep Details

    $SalesRepId = isset($_POST['SalesRepId']) ? $_POST['SalesRepId'] : "";
    $ClientsName = isset($_POST['ClientsName']) ? $_POST['ClientsName'] : "";
    $ClientsContactPerson = isset($_POST['ClientsContactPerson']) ? $_POST['ClientsContactPerson'] : "";
    $ClientsEmail = isset($_POST['ClientsEmail']) ? $_POST['ClientsEmail'] : "";
    $ClientsPhone = isset($_POST['ClientsPhone']) ? $_POST['ClientsPhone'] : "";
    $ClientsLocation = isset($_POST['ClientsLocation']) ? $_POST['ClientsLocation'] : "";

    $Approval = "1";

    $SalesRepIdEncoded = base64_encode($SalesRepId);

    $RegDate = $database->now_date_only;

    $RegTime = $database->now_time_only;


        $clientsCols = array("SalesRepId"=>$SalesRepId,"ClientsName"=>$ClientsName,"ClientsContactPerson"=>$ClientsContactPerson,"ClientsEmail"=>$ClientsEmail,"ClientsPhone"=>$ClientsPhone,"ClientsLocation"=>$ClientsLocation, "Approval"=>$Approval,"RegDate"=>$RegDate,"RegTime"=>$RegTime);
        $clientsTable = "logis_company_sales_rep_clients";
        $insertClient = $connection->InsertQuery($clientsTable,$clientsCols);


        if($insertClient == "success"){
            $connection->redirect("../sales_rep_customers_details.php?id=".$SalesRepIdEncoded);
        }
        else {
            $connection->redirect("../add_sales_rep_customers.php?id=".$SalesRepIdEncoded."&error_msg=Failed to register sales representative client");
        }

}
?>