<?php include'header.php'; 


?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>Active bookings (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="booking_calendar.php"><input type="button" class="form-control" id="viewCalendarBtn" name="viewCalendarBtn" value="View on calendar" style="border-radius: 0px;width: 150px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="all_pending_bookings.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Booking ID</th>
                <th>Vehicle reg</th>
                <th>Client's name</th>
                <th>Driver's name</th>
                <th>Rent from</th>
                <th>Expected return</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              
              foreach($results as $fd) {

                $id = $fd['BookingId'];

                if($searchTxt != ""){

                  $VehicleRegID = $fd['VehicleId'];

                } else {

                  $VehicleRegID = $fd['VehicleReg'];

                }

                $stmt_vehicles = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$VehicleRegID' ");
                $stmt_vehicles->execute();
                $results_vehicle = $stmt_vehicles->fetch(PDO::FETCH_ASSOC);

                //vehicle details
                $vehicle_reg = $results_vehicle['VehicleReg'];

                
                $Driver_Type = $fd['DriverType'];

                if($Driver_Type == "Our_Driver") {

                    $driver_id = $fd['DriverId'];

                    $stmt_driver = $conn->prepare("SELECT * from logis_drivers_master WHERE drv_NSN = '{$driver_id}' ");
                    $stmt_driver->execute();
                    $results_driver = $stmt_driver->fetch();

                    $driver_type = "Our driver";
                    $driver_name = $results_driver['drv_fname']." ".$results_driver['drv_mname']." ".$results_driver['drv_lname'];
                    $driver_phone = $results_driver['drv_contact_no'];

                } else if($Driver_Type == "Clients_Driver") {

                    $driver_id = "";
                    $driver_type = "Client's driver";
                    $driver_name = $fd['DriverName'];
                    $driver_phone = $fd['DriverPhone'];

                }

                $Client_Type = $fd['ClientType'];

                if($Client_Type == "Existing") {

                    $client_id = $fd['ClientId'];

                    $stmt_client = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE Id = '{$client_id}' ");
                    $stmt_client->execute();
                    $results_client = $stmt_client->fetch();

                    $client_type = "Our client";
                    $client_name = $results_client["ClientsName"];
                    $client_phone = $results_client["ClientsPhone"];
                    $client_email = $results_client["ClientsEmail"];

                } else if($Client_Type == "Walk_In") {

                    $client_id = "";
                    $client_type = "Walk in client";
                    $client_name = $fd['ClientName'];
                    $client_phone = $fd['ClientPhone'];
                    $client_email = $fd['ClientEmail'];

                }

                $rent_from_date = $fd['RentFrom'];
                $expected_return_date = $fd['ExpectedReturnDate'];
                $booking_additional_notes = $fd['AdditionalNotes'];
                

            ?>
              <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $vehicle_reg; ?></td>
                <td><?php echo $client_name; ?></td>
                <td><?php echo $driver_name; ?></td>
                <td><?php echo $fd['RentFrom']; ?></td>
                <td><?php echo $fd['ExpectedReturnDate']; ?></td>

                <td>

                   <a href="<?php echo 'book_thankyou.php?id='.$id; ?>" class="btn btn-dark" style="display:block;color: white;font-weight: 400;">Download report
                  </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
            @$img = ltrim($fd1['drv_img'], "../");
            $id = base64_encode($fd1['empdt_NSN']);
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <img src="<?php echo $img; ?>" class="img-responsive">
            </div>
            <div class="block-2">
              <h5><?php echo $fd1['drv_fname'].' '.$fd1['drv_lname']; ?></h5>
              <h5><?php echo $fd1['drv_driving_license_no']; ?></h5>
              <h5><?php echo $fd1['drv_contact_no']; ?></h5>
              <a href="<?php echo 'driver-detail.php?id='.$id; ?>" class="view-profile">View Profile</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers(<?php echo $rowCount; ?>)</a>
              </li>
              <li>
                <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="js/metisMenu.min.js"></script>
  <script src="js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>


  </script>

<?php include'footer.php'; ?>