<?php

ob_start();

include'header.php'; 

@$Email = $_SESSION['sales_account_manager_email_add'];


$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
$error_msg = isset($_GET['error_msg']) ? $_GET['error_msg'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}

$getCompID = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$getCompID->execute();
$getCompIDRow = $getCompID->fetch();

$sale_rep_id = $getCompIDRow['Id'];

$sales_rep_name = $getCompIDRow['Name'];

//$booking_id = $_POST['id'];
$booking_id = $_GET['id'];

$stmt = $conn->prepare("SELECT * from booking_table WHERE BookingId = '{$booking_id}' ");
$stmt->execute();
$results = $stmt->fetch();

$VehicleRegID = $results['VehicleReg'];

$stmt_vehicles = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$VehicleRegID' ");
$stmt_vehicles->execute();
$results_vehicle = $stmt_vehicles->fetch(PDO::FETCH_ASSOC);

//vehicle details
$vehicle_reg = $results_vehicle['VehicleReg'];
$color_master = $results_vehicle['VehicleColor'];
$vehicle_model_master = $results_vehicle['VehicleModel'];

$stmt_vehicles_color = $conn->prepare("SELECT * from color_master WHERE ID='$color_master' ");
$stmt_vehicles_color->execute();
$results_vehicle_color = $stmt_vehicles_color->fetch(PDO::FETCH_ASSOC);

$stmt_vehicles_model = $conn->prepare("SELECT * from model_master WHERE ID='$vehicle_model_master' ");
$stmt_vehicles_model->execute();
$results_vehicle_model = $stmt_vehicles_model->fetch(PDO::FETCH_ASSOC);

$vehicle_color = $results_vehicle_color['Color'];
$vehicle_model = $results_vehicle_model['Model'];

    
$Driver_Type = $results['DriverType'];

if($Driver_Type == "Our_Driver") {

    $driver_id = $results['DriverId'];

    $stmt_driver = $conn->prepare("SELECT * from logis_drivers_master WHERE drv_NSN = '{$driver_id}' ");
    $stmt_driver->execute();
    $results_driver = $stmt_driver->fetch();

    $driver_type = "Our driver";
    $driver_name = $results_driver['drv_fname']." ".$results_driver['drv_mname']." ".$results_driver['drv_lname'];
    $driver_phone = $results_driver['drv_contact_no'];

} else if($Driver_Type == "Clients_Driver") {

    $driver_id = "";
    $driver_type = "Client's driver";
    $driver_name = $results['DriverName'];
    $driver_phone = $results['DriverPhone'];

}


$Client_Type = $results['ClientType'];

    if($Client_Type == "Existing") {

        $client_id = $results['ClientId'];

        $stmt_client = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE Id = '{$client_id}' ");
        $stmt_client->execute();
        $results_client = $stmt_client->fetch();

        $client_type = "Our client";
        $client_name = $results_client["ClientsName"];
        $client_phone = $results_client["ClientsPhone"];
        $client_email = $results_client["ClientsEmail"];

    } else if($Client_Type == "Walk_In") {

        $client_id = "";
        $client_type = "Walk in client";
        $client_name = $results['ClientName'];
        $client_phone = $results['ClientPhone'];
        $client_email = $results['ClientEmail'];

    }

    $rent_from_date = $results['RentFrom'];
    $expected_return_date = $results['ExpectedReturnDate'];
    
    $availability = $results["Availability"];

    if($availability == "0") {

        $booking_status = "Booked";

    } else {

        $returned_date = $results["ReturnedDate"];
        $booking_status = "Retuned on ".$returned_date;

    }

    $booking_additional_notes = $results['AdditionalNotes'];

    $PNG_TEMP_DIR = "..".DIRECTORY_SEPARATOR.'Tickets'.DIRECTORY_SEPARATOR.'QR'.DIRECTORY_SEPARATOR;
                                                            
    $filename = $PNG_TEMP_DIR.$booking_id.".png";
    
    $append_path = "..".DIRECTORY_SEPARATOR.'Tickets'.DIRECTORY_SEPARATOR.'QR'.DIRECTORY_SEPARATOR;

    /*if(file_exists($filename)) {
        unlink($filename);
    }

    if(file_exists("../Tickets/Rented/".$booking_id)) {
        unlink("../Tickets/Rented/".$booking_id);
    }*/
    
    QRcode::png($booking_id, $filename, 'L', $matrixPointSize = '9', 2); 


    $pdf->set_append_path($append_path);
    
    $pdf->set_qr_code($booking_id);
    
    $pdf->create_booking_ticket($booking_id,$vehicle_reg,$vehicle_model,$vehicle_color,$driver_type,$driver_name,$driver_phone,$client_type,$client_name,$client_phone,$client_email,$rent_from_date,$expected_return_date,$booking_status,$booking_additional_notes);

    //$pdf->Output("../Tickets/Rented/".$booking_id.".pdf","F");

    $pdf->Output("D","".$booking_id.".pdf", true);
    
    ob_end_flush();

?>
