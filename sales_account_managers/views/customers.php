<?php 

include'header.php'; 


if(isset($_POST['delete_client'])) {

  $client_id = $_POST['del_client_id'];
  $sales_rep_id = $_POST['del_sales_rep_id'];

  $clientsField = "Id";
  $clientsTable = "logis_company_sales_rep_clients";
  $deleteClient = $connection->DeleteRow($clientsTable,$clientsField,$client_id);

  if($deleteClient=="success"){
      $connection->redirect("customers.php?ref=success");
  }
  else {
      $connection->redirect("customers.php?ref=error");
  }

}

if(isset($_POST['update_client'])) {

  $client_id = $_POST['edit_client_id'];
  $edit_client_name = $_POST['edit_client_name'];
  $edit_client_contact_person = $_POST['edit_client_contact_person'];
  $edit_client_email = $_POST['edit_client_email'];
  $edit_client_phone = $_POST['edit_client_phone'];
  $edit_client_location = $_POST['edit_client_location'];

  $clientsCols = array("ClientsName"=>$edit_client_name,"ClientsContactPerson"=>$edit_client_contact_person,"ClientsEmail"=>$edit_client_email,"ClientsPhone"=>$edit_client_phone,"ClientsLocation"=>$edit_client_location);
  $condition = "Id='".$client_id."'";
  $clientsTable = "logis_company_sales_rep_clients";
  $updateClient = $connection->UpdateQuery($clientsTable,$clientsCols,$condition);

  if($updateClient=="success"){
      $connection->redirect("customers.php?ref=success");
  }
  else {
      $connection->redirect("customers.php?ref=error");
  }

}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4><?php echo $sales_rep_name;?> clients (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="hidden" class="search-query form-control" name="id" value="<?php echo $_GET['id'];?>" />
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <input type="hidden" class="search-query form-control" name="id" value="<?php echo $_GET['id'];?>" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

      <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_customers.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="sales_rep_customers_details.php?id=<?php echo $_GET['id'];?>">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <?php
          
            if($success_message != "") {

          ?>

          <h6><?php echo $success_message;?></h6>

          <?php } ?>

          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Account name</th>
                <th>Contact person</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {

                $id = base64_encode($fd['SalesRepId']);

                  $ClientsId = $fd['Id'];
                  $ClientsName = $fd['ClientsName'];
                  $ClientsContactPerson = $fd['ClientsContactPerson'];
                  $ClientsEmail = $fd['ClientsEmail'];
                  $ClientsPhone = $fd['ClientsPhone'];
                  $ClientsLocation = $fd['ClientsLocation'];

                  
                  if($fd['Approval'] == "0") {
                    $approval_status = "Pending";
                    $approval_color = "Grey";
                  } else if($fd['Approval'] == "1") {
                    $approval_status = "Approved";
                    $approval_color = "Green";
                  } else {
                    $approval_status = "Rejected";
                    $approval_color = "Red";
                  }



            ?>
              <tr>
                <td><?php echo $ClientsName; ?></td>
                <td><?php echo $ClientsContactPerson; ?></td>
                <td><?php echo $ClientsEmail; ?></td>
                <td><?php echo $ClientsPhone; ?></td>
                <td><?php echo $ClientsLocation; ?></td>
                <td style="color: <?php echo $approval_color;?>;"><?php echo $approval_status; ?></td>
                <td>


                  <?php
                  
                    if($fd['Approval'] == "0") {
                  
                  ?>

                    <a href="#editClientDialog" data-clientid="<?php echo $ClientsId; ?>" data-clientname="<?php echo $ClientsName; ?>" data-clientcontactperson="<?php echo $ClientsContactPerson; ?>" data-clientemail="<?php echo $ClientsEmail; ?>" data-clientphone="<?php echo $ClientsPhone; ?>" data-clientlocation="<?php echo $ClientsLocation; ?>" data-salesrepid="<?php echo $id; ?>" class="openEditDialog btn btn-success" data-toggle="modal" style="display:block;color: white;font-weight: 400;">Edit client</a>
                    <a href="#deleteClientDialog" data-clientid="<?php echo $ClientsId; ?>" data-salesrepid="<?php echo $id; ?>" class="openDeleteDialog btn btn-dark" data-toggle="modal" style="display:block;color: white;font-weight: 400;">Delete client</a>

                  <?php } ?>  

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd1['Id']);

              $ClientsName = $fd['ClientsName'];
              $ClientsContactPerson = $fd['ClientsContactPerson'];
              $ClientsEmail = $fd['ClientsEmail'];
              $ClientsPhone = $fd['ClientsPhone'];
              $ClientsLocation = $fd['ClientsLocation'];

        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $ClientsName; ?>
            </div>
            <div class="block-2">
              <h5><?php echo $ClientsContactPerson; ?></h5>
              <a href="<?php echo 'sales_rep_specific_customers.php?id='.$id; ?>" class="view-profile">View</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="sales_rep.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Sales rep</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteClientDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Delete client</h2>
                
                <p class="delete-p">Are you sure you want to delete this client?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myDeleteClientId" class="form-control" name="del_client_id" placeholder="ClientId">
                    
                  <input type="hidden" id="deleteSalesRepId" class="form-control" name="del_sales_rep_id" placeholder="SalesRepId">

                  <input type="submit" style="float: right; " name="delete_client" value="Delete" class="btn btn-danger">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="editClientDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h4 class="delete-title">Edit client</h4>
                

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >


                  <input type="hidden" id="myEditClientId" class="form-control" name="edit_client_id" placeholder="Clients Id">
                  
                  <label for="myEditClientName">Clients name</label>
                  <input type="text" id="myEditClientName" class="form-control" name="edit_client_name" placeholder="Clients name">

                  <br/>
                  <label for="myEditClientContactPerson">Contact person</label>
                  <input type="text" id="myEditClientContactPerson" class="form-control" name="edit_client_contact_person" placeholder="Contact phone">

                  <br/>
                  <label for="myEditClientEmail">Email address</label>
                  <input type="text" id="myEditClientEmail" class="form-control" name="edit_client_email" placeholder="Clients email">

                  <br/>
                  <label for="myEditClientPhone">Phone number</label>
                  <input type="text" id="myEditClientPhone" class="form-control" name="edit_client_phone" placeholder="Clients phone">

                  <br/>
                  <label for="myEditClientLocation">Location</label>
                  <input type="text" id="myEditClientLocation" class="form-control" name="edit_client_location" placeholder="Clients location">

                  <input type="submit" style="float: right; " name="update_client" value="Update" class="btn btn-primary">
                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="js/metisMenu.min.js"></script>
  <script src="js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });

    $("#filterForm").on('click',function(e){
      var data="";
      e.preventDefault();
      var drvName = $("#drvName").val();
      var drvLicenseNum = $("#dln").val();
      var contactNo = $("#contact").val();
      //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

      $.ajax({
        type : "POST",
        url  : "functions/drivers.php",
        data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
        success : function(msg){
           // var msg = msg;
           //alert("this message:"+msg);
           $("#showDriverData").html("");
           $("#showDriverData").html(msg);
        }
      });


    });


  $(".dltDriver").click(function (event){

    var deletedvr = confirm("Are you sure want to delete these job card?");

    if(deletedvr == true){
      var dataid = $(event.currentTarget).attr('data-id');
      window.location = dataid;
    }
  });

  $(document).on("click", ".openDeleteDialog", function () {
            var myClientId = $(this).data('clientid');
            var salesRepId = $(this).data('salesrepid');
            $("#myDeleteClientId").attr("value", myClientId);
            $("#deleteSalesRepId").attr("value", salesRepId);
  });

  $(document).on("click", ".openEditDialog", function () {
            var myClientId = $(this).data('clientid');
            var myClientName = $(this).data('clientname');
            var myClientContactPerson = $(this).data('clientcontactperson');
            var myClientEmail = $(this).data('clientemail');
            var myClientPhone = $(this).data('clientphone');
            var myClientLocation = $(this).data('clientlocation'); 
            $("#myEditClientId").attr("value", myClientId);
            $("#myEditClientName").attr("value", myClientName);
            $("#myEditClientContactPerson").attr("value", myClientContactPerson);
            $("#myEditClientEmail").attr("value", myClientEmail);
            $("#myEditClientPhone").attr("value", myClientPhone);
            $("#myEditClientLocation").attr("value", myClientLocation);
  });

  </script>

<?php include'footer.php'; ?>