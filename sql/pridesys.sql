-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2021 at 11:06 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pridesys`
--

-- --------------------------------------------------------

--
-- Table structure for table `body_type_master`
--

CREATE TABLE `body_type_master` (
  `ID` int(11) NOT NULL,
  `BodyType` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `body_type_master`
--

INSERT INTO `body_type_master` (`ID`, `BodyType`) VALUES
(1, 'Bus T'),
(2, 'Coupe'),
(3, 'WAGON');

-- --------------------------------------------------------

--
-- Table structure for table `booking_table`
--

CREATE TABLE `booking_table` (
  `BookingId` varchar(255) NOT NULL,
  `VehicleReg` varchar(255) NOT NULL,
  `DriverType` varchar(255) NOT NULL,
  `DriverId` varchar(255) DEFAULT NULL,
  `DriverName` varchar(255) DEFAULT NULL,
  `DriverPhone` varchar(50) DEFAULT NULL,
  `ClientType` varchar(255) NOT NULL,
  `ClientId` varchar(255) DEFAULT NULL,
  `ClientName` varchar(255) DEFAULT NULL,
  `ClientPhone` varchar(50) DEFAULT NULL,
  `ClientEmail` varchar(500) DEFAULT NULL,
  `AdditionalNotes` text DEFAULT NULL,
  `RentFrom` date NOT NULL,
  `ExpectedReturnDate` date NOT NULL,
  `ReturnedDate` date NOT NULL,
  `Availability` int(1) NOT NULL,
  `RecordDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_table`
--

INSERT INTO `booking_table` (`BookingId`, `VehicleReg`, `DriverType`, `DriverId`, `DriverName`, `DriverPhone`, `ClientType`, `ClientId`, `ClientName`, `ClientPhone`, `ClientEmail`, `AdditionalNotes`, `RentFrom`, `ExpectedReturnDate`, `ReturnedDate`, `Availability`, `RecordDateTime`) VALUES
('A7770', '5', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-31', '2021-09-01', '0000-00-00', 0, '2021-08-31 10:34:57'),
('AC8581', '3', 'Our_Driver', '12345', '', '', 'Existing', '2', '', '', '', '', '2021-08-25', '2021-08-26', '2021-08-27', 1, '2021-08-25 14:07:42'),
('AH3301', '6', 'Our_Driver', '33084686', '', '', 'Existing', '2', '', '', '', '', '2021-09-07', '2021-09-09', '0000-00-00', 0, '2021-08-31 10:36:04'),
('AJ8625', '6', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13'),
('AJ8626', '3', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13'),
('AJ8627', '4', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13'),
('AJ8628', '5', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13'),
('AJ8629', '4', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13'),
('AJ86299', '5', 'Our_Driver', '2345', '', '', 'Existing', '3', '', '', '', '', '2021-08-27', '2021-08-28', '0000-00-00', 0, '2021-08-27 15:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `color_master`
--

CREATE TABLE `color_master` (
  `ID` int(11) NOT NULL,
  `Color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color_master`
--

INSERT INTO `color_master` (`ID`, `Color`) VALUES
(1, 'BLACK');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `engine_capacity_master`
--

CREATE TABLE `engine_capacity_master` (
  `ID` int(11) NOT NULL,
  `EngineCapacity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `engine_capacity_master`
--

INSERT INTO `engine_capacity_master` (`ID`, `EngineCapacity`) VALUES
(1, '35000 cc'),
(2, '1500cc'),
(3, '2500 cc');

-- --------------------------------------------------------

--
-- Table structure for table `fuel_master`
--

CREATE TABLE `fuel_master` (
  `ID` int(11) NOT NULL,
  `Fuel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fuel_master`
--

INSERT INTO `fuel_master` (`ID`, `Fuel`) VALUES
(1, 'Petrol'),
(2, 'Diesel'),
(3, 'Hybrid');

-- --------------------------------------------------------

--
-- Table structure for table `ibooq_services`
--

CREATE TABLE `ibooq_services` (
  `no` int(11) NOT NULL,
  `id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `category` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ibooq_services`
--

INSERT INTO `ibooq_services` (`no`, `id`, `name`, `category`) VALUES
(7, '1', 'Bentley', 'Coupe'),
(8, 'CAR100T', 'RAM', 'Truck'),
(9, 'CAR101T', 'BMW X16', 'Coupe');

-- --------------------------------------------------------

--
-- Table structure for table `job_card`
--

CREATE TABLE `job_card` (
  `Job_Card_No` varchar(255) NOT NULL,
  `Job_Card_Drivers_Name` varchar(255) NOT NULL,
  `Job_Card_Vehicle_Reg` varchar(255) NOT NULL,
  `Job_Card_Clients_Name` varchar(255) NOT NULL,
  `Job_Card_Opened_By` varchar(255) NOT NULL,
  `Job_Card_Department` varchar(255) NOT NULL,
  `Jobs_To_Be_Done` text NOT NULL,
  `Job_Card_Date_Time_In` varchar(255) NOT NULL,
  `Job_Card_Date_Time_Out` varchar(255) DEFAULT NULL,
  `Job_Card_Fuel_In` varchar(255) NOT NULL,
  `Job_Card_Fuel_Out` varchar(255) DEFAULT NULL,
  `Job_Card_Mileage_In` varchar(255) NOT NULL,
  `Job_Card_Mileage_Out` varchar(255) DEFAULT NULL,
  `Record_Date` date NOT NULL,
  `Record_Time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_card`
--

INSERT INTO `job_card` (`Job_Card_No`, `Job_Card_Drivers_Name`, `Job_Card_Vehicle_Reg`, `Job_Card_Clients_Name`, `Job_Card_Opened_By`, `Job_Card_Department`, `Jobs_To_Be_Done`, `Job_Card_Date_Time_In`, `Job_Card_Date_Time_Out`, `Job_Card_Fuel_In`, `Job_Card_Fuel_Out`, `Job_Card_Mileage_In`, `Job_Card_Mileage_Out`, `Record_Date`, `Record_Time`) VALUES
('AH7777', 'Wakorach', '3', '8', '3', 'operations', 'Fix battery', '2021-08-13T10:38', '2021-08-13T11:30', '200 cc', '3000 cc', '1300', '5000', '2021-08-13', '11:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `job_card_more_features_master`
--

CREATE TABLE `job_card_more_features_master` (
  `ID` int(11) NOT NULL,
  `JobCardMoreFeatures` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_card_more_features_master`
--

INSERT INTO `job_card_more_features_master` (`ID`, `JobCardMoreFeatures`) VALUES
(1, 'Engine starts'),
(2, 'Battery charging'),
(3, 'Battery clamp'),
(4, 'Dashboard lights'),
(5, 'Horn bill'),
(6, 'Windscreen two'),
(8, 'Tyre'),
(9, 'Trunk');

-- --------------------------------------------------------

--
-- Table structure for table `job_card_more_features_record`
--

CREATE TABLE `job_card_more_features_record` (
  `ID` int(10) NOT NULL,
  `JobCardNo` varchar(255) NOT NULL,
  `JCFeaturesStatus` varchar(255) NOT NULL,
  `Engine_starts` varchar(255) NOT NULL,
  `Battery_charging` varchar(255) NOT NULL,
  `Battery_clamp` varchar(255) NOT NULL,
  `Dashboard_lights` varchar(255) NOT NULL,
  `Horn_bill` varchar(255) NOT NULL,
  `Windscreen_two` varchar(255) NOT NULL,
  `Tyre` varchar(255) NOT NULL,
  `Trunk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_card_more_features_record`
--

INSERT INTO `job_card_more_features_record` (`ID`, `JobCardNo`, `JCFeaturesStatus`, `Engine_starts`, `Battery_charging`, `Battery_clamp`, `Dashboard_lights`, `Horn_bill`, `Windscreen_two`, `Tyre`, `Trunk`) VALUES
(1, '254', 'In', '1', '0', '1', '', '', '', '', ''),
(2, '420344ARAH', 'In', '1', '0', '0', '1', '', '', '', ''),
(3, '86225AGFy', 'In', '0', '1', '0', '1', '1', '', '', ''),
(4, '114043AkGp', 'In', '1', '0', '0', '1', '0', '0', '', ''),
(5, '663609AjNn', 'In', '1', '1', '0', '0', '0', '0', '', ''),
(6, 'AY2940', 'In', '1', '0', '0', '0', '0', '0', '', ''),
(7, 'AK8920', 'In', '1', '0', '0', '0', '0', '0', '', ''),
(8, 'AH7777', 'In', '1', '0', '0', '0', '0', '0', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `job_card_more_features_record_out`
--

CREATE TABLE `job_card_more_features_record_out` (
  `ID` int(10) NOT NULL,
  `JobCardNo` varchar(255) NOT NULL,
  `JCFeaturesStatus` varchar(255) NOT NULL,
  `Engine_starts` varchar(255) NOT NULL,
  `Battery_charging` varchar(255) NOT NULL,
  `Battery_clamp` varchar(255) NOT NULL,
  `Dashboard_lights` varchar(255) NOT NULL,
  `Horn_bill` varchar(255) NOT NULL,
  `Windscreen_two` varchar(255) NOT NULL,
  `Tyre` varchar(255) NOT NULL,
  `Trunk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_card_more_features_record_out`
--

INSERT INTO `job_card_more_features_record_out` (`ID`, `JobCardNo`, `JCFeaturesStatus`, `Engine_starts`, `Battery_charging`, `Battery_clamp`, `Dashboard_lights`, `Horn_bill`, `Windscreen_two`, `Tyre`, `Trunk`) VALUES
(2, '420344ARAH', 'Out', '1', '1', '1', '1', '1', '1', '', ''),
(5, '86225AGFy', 'Out', '1', '1', '1', '1', '1', '', '', ''),
(6, '114043AkGp', 'Out', '1', '1', '1', '1', '1', '1', '', ''),
(7, 'AH7777', 'Out', '0', '0', '1', '0', '0', '0', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kenya_city`
--

CREATE TABLE `kenya_city` (
  `city_id` int(255) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kenya_city`
--

INSERT INTO `kenya_city` (`city_id`, `city_name`) VALUES
(1, 'Baragoi'),
(2, 'Bungoma'),
(4, 'Busia'),
(5, 'Butere'),
(9, 'Diani Beach'),
(8, 'Dadaab'),
(10, 'Eldoret'),
(11, 'Emali'),
(12, 'Embu'),
(13, 'Garissa'),
(14, 'Gede'),
(15, 'Hola'),
(16, 'Homa Bay'),
(17, 'Isiolo'),
(18, 'Kitui'),
(19, 'Kibwezi'),
(20, 'Makindu'),
(21, 'Wote'),
(22, 'Mutomo'),
(23, 'Kajiado'),
(24, 'Kakamega'),
(25, 'Kakuma'),
(26, 'Kapenguria'),
(27, 'Kericho'),
(28, 'Kiambu'),
(29, 'Kilifi'),
(30, 'Kisii'),
(31, 'Kisumu'),
(32, 'Kitale'),
(33, 'Lamu'),
(34, 'Langata'),
(35, 'Litein'),
(36, 'Lodwar'),
(37, 'Lokichoggio'),
(38, 'Londiani'),
(39, 'Loyangalani'),
(40, 'Machakos'),
(41, 'Malindi'),
(42, 'Mandera'),
(43, 'Maralal'),
(44, 'Marsabit'),
(45, 'Meru'),
(46, 'Mombasa'),
(47, 'Moyale'),
(48, 'Mumias'),
(49, 'Muranga'),
(50, 'Nairobi'),
(51, 'Naivasha'),
(52, 'Nakuru'),
(53, 'Garissa'),
(54, 'Gede'),
(55, 'Hola'),
(56, 'Homa Bay'),
(57, 'Isiolo'),
(58, 'Kitui'),
(59, 'Kibwezi'),
(60, 'Makindu'),
(61, 'Wote'),
(62, 'Mutomo'),
(63, 'Kajiado'),
(64, 'Kakamega'),
(65, 'Kakuma'),
(66, 'Kapenguria'),
(67, 'Kericho'),
(68, 'Kiambu'),
(69, 'Kilifi'),
(70, 'Kisii'),
(71, 'Kisumu'),
(72, 'Kitale'),
(73, 'Lamu'),
(74, 'Langata'),
(75, 'Litein'),
(76, 'Lodwar'),
(77, 'Lokichoggio'),
(78, 'Londiani'),
(79, 'Loyangalani'),
(80, 'Machakos'),
(81, 'Malindi'),
(82, 'Mandera'),
(83, 'Maralal'),
(84, 'Marsabit'),
(85, 'Meru'),
(86, 'Mombasa'),
(87, 'Moyale'),
(88, 'Mumias'),
(89, 'Muranga'),
(90, 'Nairobi'),
(91, 'Naivasha'),
(92, 'Nakuru'),
(93, 'Namanga'),
(94, 'Nanyuki'),
(95, 'Naro Moru'),
(96, 'Narok'),
(97, 'Nyahururu'),
(98, 'Nyeri'),
(99, 'Ruiru'),
(100, 'Shimoni'),
(101, 'Takaungu'),
(102, 'Thika'),
(103, 'Vihiga'),
(104, 'Voi'),
(105, 'Wajir'),
(106, 'Watamu'),
(107, 'Webuye'),
(108, 'Wundanyi');

-- --------------------------------------------------------

--
-- Table structure for table `logis_company_master`
--

CREATE TABLE `logis_company_master` (
  `comp_id` int(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `comp_name` varchar(255) NOT NULL,
  `comp_pin` varchar(255) NOT NULL,
  `emp_nsn` varchar(255) NOT NULL,
  `email_add` varchar(255) NOT NULL,
  `comp_pwd` varchar(255) NOT NULL,
  `first_login` enum('Yes','No') NOT NULL,
  `contact_code` varchar(255) NOT NULL,
  `random_num` varchar(255) NOT NULL,
  `contact_num` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `address1` longtext NOT NULL,
  `address2` varchar(255) NOT NULL,
  `po_box` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `emp_dob` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `admin_status` enum('Pending','Approved','Disapproved') NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `disapproved_comment` longtext NOT NULL,
  `driver_limit` varchar(255) NOT NULL,
  `working_driver` varchar(255) NOT NULL,
  `payment_success_status` enum('completed','pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_company_master`
--

INSERT INTO `logis_company_master` (`comp_id`, `fname`, `mname`, `lname`, `comp_name`, `comp_pin`, `emp_nsn`, `email_add`, `comp_pwd`, `first_login`, `contact_code`, `random_num`, `contact_num`, `location`, `address1`, `address2`, `po_box`, `position`, `emp_dob`, `gender`, `created_date`, `admin_status`, `ip_address`, `latitude`, `longitude`, `city`, `country`, `browser`, `disapproved_comment`, `driver_limit`, `working_driver`, `payment_success_status`) VALUES
(28, '', '', '', 'PRIDE DRIVE', 'P123456', '', 'dataentry@pridedrive.com', 'pridedrive123', 'No', '254', 'PhUBnlHivoQ1XgJdTbpYzGVxKAW2MOqEcfkRD9Sj', '780766667', 'Nairobi', 'Address line 1', 'Address line 2', '55555-80100', '', '0000-00-00', '', '2019-09-26 10:05:38', 'Approved', '123.136.149.168', '19.4563697', '72.79244969999999', 'Nairobi', 'Kenya', 'Mozilla/5.0 (Windows NT 6.1; rv:69.0) Gecko/20100101 Firefox/69.0', '', '9990', '10', 'completed');

-- --------------------------------------------------------

--
-- Table structure for table `logis_company_sales_rep`
--

CREATE TABLE `logis_company_sales_rep` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `PhoneNumber` varchar(50) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logis_company_sales_rep`
--

INSERT INTO `logis_company_sales_rep` (`Id`, `Name`, `Email`, `PhoneNumber`, `Password`, `RegDate`, `RegTime`) VALUES
(1, 'c', 's', '1', '', '2021-08-04', '15:04:30'),
(2, 'Mwangi', 'mwangi@gmail.com', '0789', '', '2021-08-05', '10:09:15'),
(3, 'Jongo', 'j@gmail.com', '123', '$2y$10$AR2s.vwkR6LH8k0okDLKvu20Ki/lOlD1501748OYQ68G5YBMmrO02', '2021-08-05', '13:40:41'),
(4, 'Kanji', 'kanji@gmail.com', '11', '$2y$10$ooib1hH78gsL3fZw09fm7uAZ4t0j7tXmVxGysfeHSrIrGLX2BQl8.', '2021-08-10', '13:15:24');

-- --------------------------------------------------------

--
-- Table structure for table `logis_company_sales_rep_clients`
--

CREATE TABLE `logis_company_sales_rep_clients` (
  `Id` int(11) NOT NULL,
  `SalesRepId` varchar(255) NOT NULL,
  `ClientsName` varchar(255) NOT NULL,
  `ClientsContactPerson` varchar(255) NOT NULL,
  `ClientsEmail` varchar(4000) NOT NULL,
  `ClientsPhone` varchar(1000) NOT NULL,
  `ClientsLocation` varchar(1000) NOT NULL,
  `Approval` int(1) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logis_company_sales_rep_clients`
--

INSERT INTO `logis_company_sales_rep_clients` (`Id`, `SalesRepId`, `ClientsName`, `ClientsContactPerson`, `ClientsEmail`, `ClientsPhone`, `ClientsLocation`, `Approval`, `RegDate`, `RegTime`) VALUES
(2, '2', 'FAO', 'Zipporah Otieno', 'zipporahotieno@fao.org', '0702', 'Gigiri', 1, '2021-08-05', '10:58:39'),
(3, '3', 'Twiga foods', 'Kanji', 'kanjianto@gmail.com, kanjiantonio@gmail.com', '0705972361, 0101190329', 'Limuru', 1, '2021-08-05', '13:45:09'),
(8, '4', 'KCB Bank A', 'Java', 'javae@gmail.com', '011', 'Kisumu', 2, '2021-08-10', '13:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `logis_company_subadmin`
--

CREATE TABLE `logis_company_subadmin` (
  `Id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Position` varchar(255) NOT NULL,
  `Department` varchar(255) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logis_company_subadmin`
--

INSERT INTO `logis_company_subadmin` (`Id`, `comp_id`, `Name`, `Email`, `Position`, `Department`, `Password`, `RegDate`, `RegTime`) VALUES
(1, 28, 'd', 'k@gmail.com', 'head_of_sales_and_marketing', 'sales_and_marketing', '$2y$10$X7hrVODwrJLS6jz/if3atesDLfDIAxS.GLFXMvdcANAIApW9H3GHS', '2021-08-04', '11:41:42'),
(2, 28, 'Esther', 'dataentry@pridedrive.com', 'data_entry', 'data_entry', '$2y$10$je15Ihl.Q20sU/qrl6I7WutbT.lDvlzfz9qvhStcZ20RHw.mjg6Za', '2021-08-04', '11:41:42'),
(3, 28, 'Mr. Mungatia', 'mungatia@gmail.com', 'deputy_operations_manager', 'operations', '$2y$10$.ylfx/3xNin4fplPIFd8Cez21dS0xwE1ZE/HWFntH9FRFX8cjdeKK', '2021-08-10', '15:02:22'),
(4, 28, 'ksanta', 'ksanta@gmail.com', 'sales_account_managers', 'sales_and_marketing', '$2y$10$14rc/0Au/Kt6bvLqK7bqSuIl2v5EieGRgkxcF2penyUWvQt2sCqUy', '2021-08-11', '09:32:40'),
(5, 0, 'ksanta', 's@gmail.com', 'data_entry', 'data_entry', '$2y$10$MkRIwtGSQ99vY39p4q6rSeTAk9sGCU9udoHi1v8HMMZoW8ASxnUae', '2021-08-13', '14:30:14'),
(6, 0, 'Kanji Branch', 'kanjibranch@gmail.com', 'branch_manager', 'branch_managers', '$2y$10$qoEmnDbHckRQFne5lKoBaO/J.CYxuk/KgQMhF0vs7INMqe2Fq1t5e', '2021-08-13', '14:49:11'),
(7, 28, 'om', 'om@pridedrive.com', 'operations_manager', 'operations', '$2y$10$fQTyXj11qXnHmjYSk9mWJOAWZ.T8eMbdGktDYTmfSfC9y08pszzOa', '2021-08-17', '10:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `logis_company_superadmin_master`
--

CREATE TABLE `logis_company_superadmin_master` (
  `user_id` int(255) NOT NULL,
  `comp_id` int(2) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `superadmin_email_add` varchar(255) NOT NULL,
  `user_pwd` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_company_superadmin_master`
--

INSERT INTO `logis_company_superadmin_master` (`user_id`, `comp_id`, `user_name`, `superadmin_email_add`, `user_pwd`, `created_date`) VALUES
(1, 28, 'SUPER ADMIN', 'od@pridedrive.com', 'admin_pridedrive123', '2019-09-26 10:05:38');

-- --------------------------------------------------------

--
-- Table structure for table `logis_drivers_kin`
--

CREATE TABLE `logis_drivers_kin` (
  `drivers_kin_id` int(255) NOT NULL,
  `drivers_kin_name` varchar(255) NOT NULL,
  `drv_nsnId` varchar(255) NOT NULL,
  `drv_comp_id` varchar(255) NOT NULL,
  `drivers_kin_email` varchar(255) NOT NULL,
  `drivers_kin_contact` varchar(255) NOT NULL,
  `drivers_kin_nsn` varchar(255) NOT NULL,
  `drivers_kin_address` longtext NOT NULL,
  `drivers_kin_relation` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_drivers_kin`
--

INSERT INTO `logis_drivers_kin` (`drivers_kin_id`, `drivers_kin_name`, `drv_nsnId`, `drv_comp_id`, `drivers_kin_email`, `drivers_kin_contact`, `drivers_kin_nsn`, `drivers_kin_address`, `drivers_kin_relation`) VALUES
(105, 'GRACE WAMBUI MURIAI', '21997276', '28', '', '0789485283', '', 'SIGONA', 'WIFE'),
(119, 'Alex wambua', '12345', '28', 'alexwambua@gmail.com', '0721397287', '123333', '26660', 'cousin'),
(94, 'JENTRINE MUKUA', '2345', '28', '', '0711235650', '123333', '65', 'WIFE'),
(91, 'Nicholas Indusa', '33084686', '28', 'nikindusa@gmail.com', '0711675834', '473562', '23 Limuru', 'Brother'),
(95, 'HEZINAH KERUBO MACHINI', '23214120', '28', '', '0727801308', '', '4931 NAIVASHA', 'WIFE'),
(90, 'Kanji Jakob', '33084686', '28', 'jkanji@gmail.com', '0704282828', '2499863', '23 Limuru', 'Father'),
(110, 'A.Nyambura', '25144389', '28', '', '0729340845', '', 'NYERI', 'MOTHER'),
(104, 'EDITH NJERI KINUTHIA', '24894292', '28', '', '0720618926', '', 'NAIROBI', 'WIFE'),
(103, 'VERONICA HETA MBUGUA', '29965470', '28', 'Hetakariuki24@gmail.com', '', '32724287', '', 'WIFE'),
(108, 'TERESA NYAMBURA', '2474974', '28', '', '0727605950', '', '41309 NGUMO', 'WIFE'),
(109, 'CONSOLATA MEMBA SHIMWATI', '5630306', '28', '', '0720840566', '', '104 LUNGALUNGA', 'WIFE'),
(111, 'EVANCE LEWIS OWINO', '10577533', '28', '', '0757371515', '', 'MOMBASA', 'SON');

-- --------------------------------------------------------

--
-- Table structure for table `logis_drivers_master`
--

CREATE TABLE `logis_drivers_master` (
  `drv_id` int(255) NOT NULL,
  `drv_NSN` varchar(255) NOT NULL,
  `drv_fname` varchar(250) DEFAULT NULL,
  `drv_mname` varchar(255) NOT NULL,
  `drv_lname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `drv_img` varchar(255) DEFAULT NULL,
  `drv_contact_no` varchar(255) DEFAULT NULL,
  `drv_alt_contact_no` varchar(255) DEFAULT NULL,
  `drv_address` varchar(250) DEFAULT NULL,
  `drv_address2` varchar(255) DEFAULT NULL,
  `drv_city` varchar(250) DEFAULT NULL,
  `drv_pobox` varchar(250) DEFAULT NULL,
  `drv_postal` varchar(250) DEFAULT NULL,
  `drv_email` varchar(250) DEFAULT NULL,
  `logis_drivers_vehicle_class` varchar(255) DEFAULT NULL,
  `drv_driving_license_no` varchar(250) DEFAULT NULL,
  `isLicensePSV` varchar(255) DEFAULT NULL,
  `vehicleType` varchar(255) DEFAULT NULL,
  `vehiPlatNumber` varchar(255) NOT NULL,
  `vehicleInspection` varchar(255) NOT NULL,
  `vehicleInspectionFile` varchar(255) NOT NULL,
  `vehicleInspection_date` date NOT NULL,
  `insurance_type` varchar(255) NOT NULL,
  `insurance_ref_number` varchar(255) NOT NULL,
  `insurance_exp_date` date NOT NULL,
  `insurance_img` varchar(255) NOT NULL,
  `gross_salary` varchar(255) NOT NULL,
  `drv_dl_issue_date` date DEFAULT NULL,
  `drv_dl_expiry_date` date DEFAULT NULL,
  `driver_port_number` varchar(255) NOT NULL,
  `driver_port_expire_date` date NOT NULL,
  `vehicle_port_number` varchar(255) NOT NULL,
  `vehicle_port_expire_date` date NOT NULL,
  `drv_curr_comp_id` varchar(250) DEFAULT NULL,
  `drv_created_on` date DEFAULT NULL,
  `driver_work_status` enum('working','notworking') NOT NULL,
  `drv_isActive` enum('Active','inActive') DEFAULT NULL,
  `delete_flag` enum('Yes','No') NOT NULL,
  `driving_license_cron` enum('remain','days_remain','expired') NOT NULL,
  `driver_contract_cron` enum('remain','days_remain','expired') NOT NULL,
  `vehicle_inspection_cron` enum('remain','days_remain','expired') NOT NULL,
  `vehicle_insurance_cron` enum('remain','days_remain','expired') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_drivers_master`
--

INSERT INTO `logis_drivers_master` (`drv_id`, `drv_NSN`, `drv_fname`, `drv_mname`, `drv_lname`, `gender`, `drv_img`, `drv_contact_no`, `drv_alt_contact_no`, `drv_address`, `drv_address2`, `drv_city`, `drv_pobox`, `drv_postal`, `drv_email`, `logis_drivers_vehicle_class`, `drv_driving_license_no`, `isLicensePSV`, `vehicleType`, `vehiPlatNumber`, `vehicleInspection`, `vehicleInspectionFile`, `vehicleInspection_date`, `insurance_type`, `insurance_ref_number`, `insurance_exp_date`, `insurance_img`, `gross_salary`, `drv_dl_issue_date`, `drv_dl_expiry_date`, `driver_port_number`, `driver_port_expire_date`, `vehicle_port_number`, `vehicle_port_expire_date`, `drv_curr_comp_id`, `drv_created_on`, `driver_work_status`, `drv_isActive`, `delete_flag`, `driving_license_cron`, `driver_contract_cron`, `vehicle_inspection_cron`, `vehicle_insurance_cron`) VALUES
(121, '33084686', 'Kanji', 'Antony', 'Ondere', 'male', '../uploads/company28/img/06-07-2021-1625571679image.png', '0705972361', '', '23', '', '28', '23', '00217', 'kanjianto@gmail.com', '1,6', 'I20/0770/2015', NULL, 'PSV', 'KAM 234R', '', '', '0000-00-00', '', '', '0000-00-00', '', '300000', '2021-03-01', '2022-01-31', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-06', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(122, '33084687', 'Grace', 'Mumbi', 'K', 'female', '../uploads/company28/img/06-07-2021-1625571816image.jpg', '12345', '', '', '', '12', '45', '00345', 'grace@gmail.com', '1', 'E23/1782/2015', 'No', 'Other', 'KBS 123R', '', '', '0000-00-00', '', '', '0000-00-00', '', '10000', '2021-07-01', '2021-11-30', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-06', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(123, '145', 'K', 'K', 'K', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-06', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(124, '678', 'D', 'D', 'D', 'male', '', '', '', '', '', NULL, '', '', '', '', '', 'No', 'Other', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-06', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(125, '12345', 'CHARLES', 'MUEMA', 'A', 'male', '../../uploads/company28/img/12345image.png', '0716749428', '', '23', '', '18', '23', '00207', 'Charlesmuema@gmail.com', '1,2,3,4,6,7,8', 'GEB191', 'Yes', 'PSV', 'KCD 544L', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2021-03-01', '2022-03-03', '', '0000-00-00', '', '0000-00-00', '28', '2021-08-17', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(126, '2345', 'DAVIS', 'OENGA', 'MUKUA', 'male', '../uploads/company28/img/07-07-2021-1625652292image.jpg', '0723568963', '', '65', '', '30', '65', '00123', 'Davismukua@gmail.com', '2,3,4', 'CB0021', 'No', 'Other', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '20000', '2021-02-06', '2022-02-02', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-07', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(127, '23214120', 'TIMOTHY', 'MAKORO', 'MOKAYA', 'male', '../uploads/company28/img/08-07-2021-1625725803image.pdf', '0720889938', '', '54274', '', '90', '54274', '', 'Mokayatimothy@gmail.com', '1,2,8', 'RLE1810000', 'Yes', 'PSV', 'KCF 833Q', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2019-06-24', '2022-06-24', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-08', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(128, '21997276', 'DAVID ', 'NYOIKE', 'NGONYO', 'male', '../uploads/company28/img/08-07-2021-1625732409image.jpg', '0719275361', '', 'SIGONA', '', '28', '433', '00902', 'Davidnyoike@yahoo.com', '1,2,7,8', 'RKJ119', 'Yes', 'PSV', 'KCX 154M', '', '', '0000-00-00', '', '', '0000-00-00', '', '25,000', '2020-12-02', '2021-12-02', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-08', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(129, '25144389', 'DAVID', 'MUCHEMI', 'NYAMBURA', 'male', '../uploads/company28/img/08-07-2021-1625733245image.jpg', '0720028689', '', 'EASTLANDS', '', '50', '1399 NYERI', '', 'Davidmuchemi4@gmail.com', '1,2,7,8', 'WWH135', 'Yes', 'PSV', 'KCD 596N', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2021-03-10', '2022-03-10', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-09', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(130, '24894292', 'MOSES', 'NJOROGE', 'WAINAINA', 'male', '../uploads/company28/img/08-07-2021-1625735177image.jpg', '0726950081', '', '', '', NULL, '', '', 'Mosesn.wainaina@gmail.com', '1,2,7,8', 'MPF111', 'Yes', 'PSV', 'KCD 875J', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2021-06-22', '2022-06-22', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-08', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(131, '29965470', 'JOHN', 'KARIUKI', 'NJOKI', 'male', '../uploads/company28/img/08-07-2021-1625734962image.jpg', '0715037566', '', '', '', NULL, '', '', 'johnkarisnjoki@gmail.com', '1,2,7,8', 'YMD185', 'Yes', 'PSV', 'KCX153M', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2019-05-28', '2022-05-29', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-08', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(132, '2474974', 'STEPHEN', 'NKONGE', 'MURIANKI', 'male', '../uploads/company28/img/09-07-2021-1625816391image.jpg', '0706220720', '', 'LANGATA', '', '50', '41309', '', '', '1,2,8', 'DDD166', 'No', 'Other', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '20000', '2020-01-31', '2023-01-31', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-09', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(133, '5630306', 'MOSES', 'SHIMWATI', 'ALUVALA', 'male', '../uploads/company28/img/09-07-2021-1625821946image.png', '0722234908', '', 'UTAWALA', '', '50', '10222', '', '', '1,2,7,8', 'CWU133', 'Yes', 'PSV', 'KCF 837Q', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-09', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(134, '10876549', 'STEPHEN', 'GACHERU', 'TIBI', 'male', '', '0722919059', '', '62 KIKUYU', '', '28', '62', '', '', '1,2,7,8', 'DDD166', 'Yes', 'PSV', 'KCX 668W', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-09', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(135, '10577533', 'AUGUSTINO', 'OWINO', '', 'male', '../uploads/company28/img/10-07-2021-1625900021image.png', '0703661162', '0786321210', '50639', '', '46', '50639', '00100', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '20000', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-10', 'working', 'Active', 'No', 'remain', 'remain', 'remain', 'remain'),
(136, '', 'FIDELIS', 'MUNYWOKI', 'MUSYOKI', 'male', '', '', '', '', '', '', '', '', '', '1,2,7,8', 'QTR1200000', 'Yes', 'PSV', 'KCF 835Q', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2020-03-13', '2023-03-13', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-13', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(137, '', 'JOHN', 'MUIYORO', 'KIBE', 'male', '', '', '', '', '', '', '', '', '', '1,2,7,8', 'UVP023M', 'Yes', 'PSV', 'KCF 836Q', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2020-12-28', '2023-12-28', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-13', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(138, '', 'PAUL', 'AYIENDA', 'OYARO', 'male', '', '', '', '', '', '', '', '', '', '1,2,7,8', 'XVQ016', '', 'PSV', 'KCF 834Q', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2021-01-26', '2022-01-26', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-13', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(139, '', 'HARDLY', 'ROBIN', 'MALESI', 'male', '', '', '', '', '', '', '', '', '', '1,2,7,8', 'IQC172', 'Yes', 'PSV', 'KCF 252S', '', '', '0000-00-00', '', '', '0000-00-00', '', '25000', '2021-04-22', '2022-04-22', '', '0000-00-00', '', '0000-00-00', '28', '2021-07-13', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain'),
(140, '3308', 'Kanji', 'Antony', 'O', 'male', '../uploads/company28/img/3308image.png', '', '', '', '', NULL, '', '', '', '1', '', 'No', 'Other', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '28', '2021-08-14', 'working', 'Active', 'Yes', 'remain', 'remain', 'remain', 'remain');

-- --------------------------------------------------------

--
-- Table structure for table `logis_employee_details`
--

CREATE TABLE `logis_employee_details` (
  `empdt_id` int(255) NOT NULL,
  `empdt_empID` varchar(255) NOT NULL,
  `empdt_NSN` varchar(250) DEFAULT NULL,
  `empdt_emp_name` varchar(250) DEFAULT NULL,
  `empdt_commenced_date` date NOT NULL,
  `empdt_contract_start` date DEFAULT NULL,
  `empdt_contract_end` date DEFAULT NULL,
  `contract_file` varchar(255) NOT NULL,
  `empdt_dept_id` varchar(255) DEFAULT NULL,
  `department` varchar(255) NOT NULL,
  `empdt_comp_id` varchar(255) DEFAULT NULL,
  `empdt_qualification` varchar(255) NOT NULL,
  `empdt_Idcopy` varchar(255) NOT NULL,
  `empdt_pinCert` varchar(255) DEFAULT NULL,
  `empdt_nssfNo` varchar(255) DEFAULT NULL,
  `empdt_nhif` varchar(255) DEFAULT NULL,
  `empdt_annual_leaves` varchar(250) DEFAULT NULL,
  `empdt_current_data` enum('Yes','No') NOT NULL,
  `empdt_isValid` enum('Active','inActive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_employee_details`
--

INSERT INTO `logis_employee_details` (`empdt_id`, `empdt_empID`, `empdt_NSN`, `empdt_emp_name`, `empdt_commenced_date`, `empdt_contract_start`, `empdt_contract_end`, `contract_file`, `empdt_dept_id`, `department`, `empdt_comp_id`, `empdt_qualification`, `empdt_Idcopy`, `empdt_pinCert`, `empdt_nssfNo`, `empdt_nhif`, `empdt_annual_leaves`, `empdt_current_data`, `empdt_isValid`) VALUES
(211, '', '33084686', 'Kanji Antony Ondere', '0000-00-00', '2021-07-01', '2022-07-01', '../uploads/company28/document/06-07-2021-1625571679ContractFile.jpg', 'Nairobi', 'Operations', '28', 'College', '33084686', 'AX7171OX', NULL, NULL, NULL, 'Yes', 'Active'),
(212, '', '33084687', 'Grace Mumbi K', '0000-00-00', '2021-06-30', '2022-06-30', '', 'Nairobi', 'Operations', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(213, '', '145', 'K K K', '0000-00-00', '0000-00-00', '2022-07-06', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(214, '', '678', 'D D D', '0000-00-00', '0000-00-00', '2022-07-06', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(215, '', '12345', 'CHARLES MUEMA A', '0000-00-00', '2015-05-06', '2016-05-05', '../uploads/company28/document/125ContractFile.jpg', 'NAIROBI', 'OPERATIONS', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(216, '', '2345', 'DAVIS OENGA MUKUA', '0000-00-00', '2017-07-05', '2018-07-05', '', 'NAIROBI', 'OPERATIONS', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(217, '', '23214120', 'TIMOTHY MAKORO MOKAYA', '0000-00-00', '2021-01-15', '2022-01-15', '../uploads/company28/document/08-07-2021-1625725803ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', 'HIGH SCHOOL', '23214120', 'A003594148Y', NULL, NULL, NULL, 'Yes', 'Active'),
(218, '', '21997276', 'DAVID  NYOIKE NGONYO', '0000-00-00', '2015-09-11', '2016-09-10', '../uploads/company28/document/08-07-2021-1625730480ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', '', '21997276', 'A006049997P', NULL, NULL, NULL, 'Yes', 'Active'),
(219, '', '25144389', 'DAVID MUCHEMI NYAMBURA', '0000-00-00', '2015-10-05', '2016-10-04', '../uploads/company28/document/09-07-2021-1625833182ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', '', '', 'A008479938P', NULL, NULL, NULL, 'Yes', 'Active'),
(220, '', '24894292', 'MOSES NJOROGE WAINAINA', '0000-00-00', '0000-00-00', '2022-07-08', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(221, '', '29965470', 'JOHN KARIUKI NJOKI', '0000-00-00', '2019-12-13', '2020-12-12', '../uploads/company28/document/08-07-2021-1625734748ContractFile.jpg', 'NAIROBI', 'OPERATIONS', '28', '', '29965470', 'A007838869A', NULL, NULL, NULL, 'Yes', 'Active'),
(222, '', '2474974', 'STEPHEN NKONGE MURIANKI', '0000-00-00', '2016-06-24', '2017-06-24', '../uploads/company28/document/09-07-2021-1625816391ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', 'COLLEGE', '2474974', 'A002418299J', NULL, NULL, NULL, 'Yes', 'Active'),
(223, '', '5630306', 'MOSES SHIMWATI ALUVALA', '0000-00-00', '2015-11-20', '2016-11-19', '../uploads/company28/document/09-07-2021-1625821946ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', '', '5630306', 'A00231966M', NULL, NULL, NULL, 'Yes', 'Active'),
(224, '', '10876549', 'STEPHEN GACHERU TIBI', '0000-00-00', '2015-05-15', '2016-05-14', '../uploads/company28/document/09-07-2021-1625823819ContractFile.pdf', 'NAIROBI', 'OPERATIONS', '28', 'HIGH SCHOOL', '10876549', 'A003013007S', NULL, NULL, NULL, 'Yes', 'Active'),
(225, '', '10577533', 'AUGUSTINO OWINO ', '0000-00-00', '2021-06-12', '2022-06-12', '', 'NAIROBI', 'OPERATIONS', '28', 'HIGH SCHOOL', '10577533', 'A002297299Y', NULL, NULL, NULL, 'Yes', 'Active'),
(226, '', '', 'FIDELIS MUNYWOKI MUSYOKI', '0000-00-00', '0000-00-00', '2022-07-13', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(227, '', '', 'JOHN MUIYORO KIBE', '0000-00-00', '0000-00-00', '2022-07-13', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(228, '', '', 'PAUL AYIENDA OYARO', '0000-00-00', '0000-00-00', '2022-07-13', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(229, '', '', 'HARDLY ROBIN MALESI', '0000-00-00', '0000-00-00', '2022-07-13', '', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active'),
(230, '', '3308', 'Kanji Antony O', '0000-00-00', '0000-00-00', '2022-08-14', '../uploads/company28/document/3308ContractFile.png', '', '', '28', '', '', '', NULL, NULL, NULL, 'Yes', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `logis_emp_docs`
--

CREATE TABLE `logis_emp_docs` (
  `edocs_id` int(11) NOT NULL,
  `docs_isValid` enum('Active','inActive') NOT NULL DEFAULT 'Active',
  `edocs_nsn` varchar(255) DEFAULT NULL,
  `edocs_comp_id` varchar(255) DEFAULT NULL,
  `edocs_cv` varchar(255) DEFAULT NULL,
  `edocs_app_letter` varchar(255) DEFAULT NULL,
  `edocs_qualification` varchar(255) DEFAULT NULL,
  `edocs_id_card` varchar(255) DEFAULT NULL,
  `edocs_pin_cert` varchar(255) DEFAULT NULL,
  `edocs_nssf` varchar(255) DEFAULT NULL,
  `edocs_nhif` varchar(255) DEFAULT NULL,
  `edocs_dl_copy` varchar(255) DEFAULT NULL,
  `edocs_conduct_cert` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_emp_docs`
--

INSERT INTO `logis_emp_docs` (`edocs_id`, `docs_isValid`, `edocs_nsn`, `edocs_comp_id`, `edocs_cv`, `edocs_app_letter`, `edocs_qualification`, `edocs_id_card`, `edocs_pin_cert`, `edocs_nssf`, `edocs_nhif`, `edocs_dl_copy`, `edocs_conduct_cert`) VALUES
(142, 'Active', '33084686', '28', '../uploads/company28/document/06-07-2021-1625571679cv.docx', '../uploads/company28/document/06-07-2021-1625571679appLetter.docx', '../uploads/company28/document/06-07-2021-1625571679qualificationD.docx', '../uploads/company28/document/06-07-2021-1625571679idCardD.png', '../uploads/company28/document/06-07-2021-1625571679pinD.png', '../uploads/company28/document/06-07-2021-1625571679nssfD.png', '../uploads/company28/document/06-07-2021-1625571679nhifD.jpg', '../uploads/company28/document/06-07-2021-1625571679driverLicence.png', '../uploads/company28/document/06-07-2021-1625571679bonafideCert.jpg'),
(143, 'Active', '33084687', '28', '', '', '', '', '', '', '', '../uploads/company28/document/06-07-2021-1625572028driverLicence.jpg', ''),
(144, 'Active', '145', '28', '', '', '', '', '', '', '', '', ''),
(145, 'Active', '678', '28', '', '', '', '', '', '', '', '', ''),
(146, 'Active', '12345', '28', '', '', '', '', '', '', '', '../../uploads/company28/document/12345driverLicence.png', ''),
(147, 'Active', '2345', '28', '', '', '', '', '', '', '', '../uploads/company28/document/07-07-2021-1625652292driverLicence.jpg', ''),
(148, 'Active', '23214120', '28', '../uploads/company28/document/08-07-2021-1625725803cv.pdf', '../uploads/company28/document/08-07-2021-1625725803appLetter.pdf', '', '../uploads/company28/document/08-07-2021-1625725803idCardD.pdf', '../uploads/company28/document/08-07-2021-1625725803pinD.pdf', '', '', '../uploads/company28/document/08-07-2021-1625725803driverLicence.pdf', ''),
(149, 'Active', '21997276', '28', '../uploads/company28/document/08-07-2021-1625738816cv.pdf', '../uploads/company28/document/08-07-2021-1625738816appLetter.pdf', '../uploads/company28/document/08-07-2021-1625738816qualificationD.pdf', '../uploads/company28/document/08-07-2021-1625730480idCardD.pdf', '../uploads/company28/document/08-07-2021-1625738816pinD.pdf', '../uploads/company28/document/08-07-2021-1625738816nssfD.pdf', '../uploads/company28/document/08-07-2021-1625738816nhifD.pdf', '../uploads/company28/document/08-07-2021-1625730480driverLicence.pdf', ''),
(150, 'Active', '25144389', '28', '../uploads/company28/document/09-07-2021-1625833182cv.pdf', '../uploads/company28/document/09-07-2021-1625833182appLetter.pdf', '', '../uploads/company28/document/08-07-2021-1625732073idCardD.pdf', '', '', '', '../uploads/company28/document/08-07-2021-1625732073driverLicence.pdf', ''),
(151, 'Active', '24894292', '28', '', '', '', '../uploads/company28/document/08-07-2021-1625733136idCardD.pdf', '', '', '', '../uploads/company28/document/08-07-2021-1625733136driverLicence.pdf', ''),
(152, 'Active', '29965470', '28', '', '', '', '../uploads/company28/document/08-07-2021-1625734748idCardD.pdf', '', '', '', '../uploads/company28/document/08-07-2021-1625734748driverLicence.pdf', ''),
(153, 'Active', '2474974', '28', '../uploads/company28/document/09-07-2021-1625816809cv.pdf', '', '../uploads/company28/document/09-07-2021-1625816391qualificationD.pdf', '../uploads/company28/document/09-07-2021-1625816391idCardD.pdf', '', '', '', '../uploads/company28/document/09-07-2021-1625816391driverLicence.pdf', ''),
(154, 'Active', '5630306', '28', '', '../uploads/company28/document/09-07-2021-1625821946appLetter.pdf', '', '../uploads/company28/document/09-07-2021-1625821946idCardD.pdf', '../uploads/company28/document/09-07-2021-1625821946pinD.pdf', '../uploads/company28/document/09-07-2021-1625821946nssfD.pdf', '../uploads/company28/document/09-07-2021-1625821946nhifD.pdf', '', ''),
(155, 'Active', '10876549', '28', '../uploads/company28/document/09-07-2021-1625823819cv.pdf', '../uploads/company28/document/09-07-2021-1625823819appLetter.pdf', '../uploads/company28/document/09-07-2021-1625823819qualificationD.pdf', '../uploads/company28/document/09-07-2021-1625823819idCardD.pdf', '../uploads/company28/document/09-07-2021-1625823819pinD.pdf', '../uploads/company28/document/09-07-2021-1625823819nssfD.pdf', '../uploads/company28/document/09-07-2021-1625823819nhifD.pdf', '', ''),
(156, 'Active', '10577533', '28', '', '', '', '', '', '', '', '', ''),
(157, 'Active', '', '28', '', '', '', '', '', '', '', '../uploads/company28/document/13-07-2021-1626155483driverLicence.pdf', ''),
(158, 'Active', '', '28', '', '', '', '', '', '', '', '../uploads/company28/document/13-07-2021-1626156158driverLicence.pdf', ''),
(159, 'Active', '', '28', '', '', '', '', '', '', '', '../uploads/company28/document/13-07-2021-1626158013driverLicence.pdf', ''),
(160, 'Active', '', '28', '', '', '', '', '', '', '', '../uploads/company28/document/13-07-2021-1626158376driverLicence.pdf', ''),
(161, 'Active', '3308', '28', '../uploads/company28/document/3308cv.png', '../uploads/company28/document/3308appLetter.docx', '../uploads/company28/document/3308qualificationD.docx', '../uploads/company28/document/3308idCardD.docx', '../uploads/company28/document/3308pinD.docx', '../uploads/company28/document/3308nssfD.jpg', '../uploads/company28/document/3308nhifD.png', '../uploads/company28/document/3308driverLicence.png', '../uploads/company28/document/3308bonafideCert.png');

-- --------------------------------------------------------

--
-- Table structure for table `logis_flag_driver`
--

CREATE TABLE `logis_flag_driver` (
  `flag_drv_id` int(255) NOT NULL,
  `flag_drv_compid` varchar(255) NOT NULL,
  `flag_drv_name` varchar(255) NOT NULL,
  `flag_drv_nsn` varchar(255) NOT NULL,
  `flag_drv_comment` longtext NOT NULL,
  `flag_drv_date` datetime NOT NULL,
  `flag_drv_status` enum('Active','Deactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logis_nhif_master`
--

CREATE TABLE `logis_nhif_master` (
  `nhif_id` int(11) NOT NULL,
  `nhif_comp_id` varchar(255) DEFAULT NULL,
  `nhif_amt_from` varchar(255) DEFAULT NULL,
  `nhif_amt_to` varchar(255) DEFAULT NULL,
  `nhif_deduction` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logis_nssf_master`
--

CREATE TABLE `logis_nssf_master` (
  `nssf_id` int(255) NOT NULL,
  `nssf_comp_id` varchar(255) NOT NULL,
  `nssf_rate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logis_reports`
--

CREATE TABLE `logis_reports` (
  `report_id` int(255) NOT NULL,
  `driver_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vehicle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `report_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `report_file` longtext COLLATE utf8_unicode_ci NOT NULL,
  `report_abstract_file` longtext COLLATE utf8_unicode_ci NOT NULL,
  `report_details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `report_status` longtext COLLATE utf8_unicode_ci NOT NULL,
  `report_date` date NOT NULL,
  `report_update` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logis_reports`
--

INSERT INTO `logis_reports` (`report_id`, `driver_name`, `company_id`, `vehicle`, `report_type`, `report_file`, `report_abstract_file`, `report_details`, `report_status`, `report_date`, `report_update`) VALUES
(21, '125', '28', 'KCD 544L', 'Incidents', 'uploads/company28/report/07-07-2021-1625651656790Report.docx', 'uploads/company28/report/07-07-2021-1625651656807Report.pdf', 'POLICE FINE OVER SPEEDING', '', '2021-07-07', '2021-07-07');

-- --------------------------------------------------------

--
-- Table structure for table `logis_report_performance`
--

CREATE TABLE `logis_report_performance` (
  `report_id` int(255) NOT NULL,
  `driver_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driver_sign` longtext COLLATE utf8_unicode_ci NOT NULL,
  `employer_sign` longtext COLLATE utf8_unicode_ci NOT NULL,
  `report_start_date` date NOT NULL,
  `report_end_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logis_report_performance`
--

INSERT INTO `logis_report_performance` (`report_id`, `driver_id`, `company_id`, `rating`, `confirmed`, `driver_sign`, `employer_sign`, `report_start_date`, `report_end_date`) VALUES
(14, '125', '28', '7', 'confirmed', '', '', '2021-07-01', '2021-07-31');

-- --------------------------------------------------------

--
-- Table structure for table `logis_users`
--

CREATE TABLE `logis_users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(250) DEFAULT NULL,
  `user_password` varchar(250) DEFAULT NULL,
  `user_type` varchar(250) DEFAULT NULL,
  `user_status` enum('Active','Deactive') DEFAULT NULL,
  `user_email` varchar(250) DEFAULT NULL,
  `user_mobile` int(11) DEFAULT NULL,
  `user_data_list_type` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_users`
--

INSERT INTO `logis_users` (`user_id`, `user_name`, `user_password`, `user_type`, `user_status`, `user_email`, `user_mobile`, `user_data_list_type`) VALUES
(1, 'admin', 'Rahul.766667', 'super_admin', 'Active', 'dereva.Admin', 1234567890, 'yes'),
(2, 'Nirav', '888', 'Employee', 'Active', 'nirav.soni@sujaltechnologies.com', 1234567890, '');

-- --------------------------------------------------------

--
-- Table structure for table `logis_vehicle`
--

CREATE TABLE `logis_vehicle` (
  `logis_vehicle_id` int(255) NOT NULL,
  `logis_vehicle_name` varchar(255) NOT NULL,
  `logis_vehicle_icon` varchar(255) NOT NULL,
  `logis_vehicle_status` enum('Active','inActive') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logis_vehicle`
--

INSERT INTO `logis_vehicle` (`logis_vehicle_id`, `logis_vehicle_name`, `logis_vehicle_icon`, `logis_vehicle_status`) VALUES
(1, 'B - heavy commercial vehicles', '../Driver/images/vehicleClass//27-11-2017-1511777839B.png', 'Active'),
(2, 'C - commercial vehicles exceeding 4000lbs', '../Driver/images/vehicleClass//27-11-2017-1511777981C.png', 'Active'),
(3, 'G - motor  cycles', '../Driver/images/vehicleClass//27-11-2017-1511778029G.png', 'Active'),
(4, 'I - special type (specific)', '', 'Active'),
(6, 'D - tractors', '../Driver/images/vehicleClass//27-11-2017-1511778010D.png', 'Active'),
(7, 'A - motor/omnibuses', '../Driver/images/vehicleClass//27-11-2017-1511777776A.png', 'Active'),
(8, 'E - commercial vehicles not exceeding 4000lbs', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer_master`
--

CREATE TABLE `manufacturer_master` (
  `ID` int(11) NOT NULL,
  `Manufacturer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manufacturer_master`
--

INSERT INTO `manufacturer_master` (`ID`, `Manufacturer`) VALUES
(1, 'TOYOTA '),
(2, 'BMW');

-- --------------------------------------------------------

--
-- Table structure for table `mileage_master`
--

CREATE TABLE `mileage_master` (
  `ID` int(11) NOT NULL,
  `Mileage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mileage_master`
--

INSERT INTO `mileage_master` (`ID`, `Mileage`) VALUES
(1, '0 - 10000'),
(2, 'Unlimited');

-- --------------------------------------------------------

--
-- Table structure for table `model_master`
--

CREATE TABLE `model_master` (
  `ID` int(11) NOT NULL,
  `Model` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `model_master`
--

INSERT INTO `model_master` (`ID`, `Model`) VALUES
(1, 'RAV 4'),
(2, 'XTRAIL');

-- --------------------------------------------------------

--
-- Table structure for table `pridedrive_vehicles`
--

CREATE TABLE `pridedrive_vehicles` (
  `VehicleId` int(11) NOT NULL,
  `CompanyId` varchar(255) NOT NULL,
  `VehicleReg` varchar(255) NOT NULL,
  `VehicleManufacturer` varchar(50) DEFAULT NULL,
  `VehicleModel` varchar(50) DEFAULT NULL,
  `VehicleBodyType` varchar(30) DEFAULT NULL,
  `VehicleColor` varchar(30) DEFAULT NULL,
  `VehicleYear` varchar(4) DEFAULT NULL,
  `VehicleFuel` varchar(20) DEFAULT NULL,
  `VehicleTransmission` varchar(20) DEFAULT NULL,
  `VehicleFuelEconomy` varchar(255) DEFAULT NULL,
  `VehicleMaxPassengers` varchar(20) DEFAULT NULL,
  `VehicleEngineCapacity` varchar(20) DEFAULT NULL,
  `VehicleDoors` varchar(20) DEFAULT NULL,
  `VehicleMileageLimit` varchar(255) DEFAULT NULL,
  `VehicleLeasePreparation` varchar(255) DEFAULT NULL,
  `VehicleMoreFeatures` varchar(4000) DEFAULT NULL,
  `VehicleAddOns` varchar(4000) DEFAULT NULL,
  `VehicleInventoryLocation` varchar(20) DEFAULT NULL,
  `VehicleDailyPrice` varchar(20) DEFAULT NULL,
  `VehicleImg` varchar(2000) DEFAULT NULL,
  `RegDate` date DEFAULT NULL,
  `RegTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pridedrive_vehicles`
--

INSERT INTO `pridedrive_vehicles` (`VehicleId`, `CompanyId`, `VehicleReg`, `VehicleManufacturer`, `VehicleModel`, `VehicleBodyType`, `VehicleColor`, `VehicleYear`, `VehicleFuel`, `VehicleTransmission`, `VehicleFuelEconomy`, `VehicleMaxPassengers`, `VehicleEngineCapacity`, `VehicleDoors`, `VehicleMileageLimit`, `VehicleLeasePreparation`, `VehicleMoreFeatures`, `VehicleAddOns`, `VehicleInventoryLocation`, `VehicleDailyPrice`, `VehicleImg`, `RegDate`, `RegTime`) VALUES
(3, '28', 'KAA 234J', '1', '1', '2', '1', '1', '1', '1', 'Economic', '5', '2', '4', '2', '3 hours', '', 'Driver @2000 within Nairobi Per day Unlimited mileage', '1', '5000 plus vat', '../../uploads/company28/img/KAA_234Jimage.png', '2021-07-15', '16:53:19'),
(4, '28', 'KAA 234K', '1', '1', '2', '1', '1', '1', '1', 'Economic', '5', '2', '4', '2', '3 hours', '1,2,3', 'Driver @2000 within Nairobi Per day Unlimited mileage', '1', '5000 plus vat', '../uploads/company28/img/15-07-2021-1626357456image.jpg', '2021-07-15', '16:57:36'),
(5, '28', 'KBD 123H', '1', '1', '1', '1', '2', '1', '1', '', '', '1', '3', '1', '', '1,2,3', 'Driver @2000 within Nairobi Per day Unlimited mileage', '2', '', '', '2021-07-16', '10:26:17'),
(6, '28', 'KBP 101G', '2', '2', '3', '1', '2', '1', '1', '5', '5', '3', '5', '2', '3 hours', '1,2,3', 'Driver @2000 within Nairobi Per day Unlimited mileage', '1', '6000 plus VAT', '../../uploads/company28/img/KBP_101Gimage.png', '2021-07-23', '08:42:58');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_settings`
--

CREATE TABLE `smtp_settings` (
  `smtp_id` int(11) NOT NULL,
  `smtp_host` varchar(255) NOT NULL,
  `smtp_port` varchar(255) NOT NULL,
  `smtp_username` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_secure` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smtp_settings`
--

INSERT INTO `smtp_settings` (`smtp_id`, `smtp_host`, `smtp_port`, `smtp_username`, `smtp_password`, `smtp_secure`) VALUES
(3, 'mail.dereva.com', '587', 'noreply@dereva.com', 'Zye@rTd70k51', 'tls');

-- --------------------------------------------------------

--
-- Table structure for table `transmission_master`
--

CREATE TABLE `transmission_master` (
  `ID` int(11) NOT NULL,
  `Transmission` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transmission_master`
--

INSERT INTO `transmission_master` (`ID`, `Transmission`) VALUES
(1, 'Automatic'),
(2, 'Manual');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_inventory_location_master`
--

CREATE TABLE `vehicle_inventory_location_master` (
  `ID` int(11) NOT NULL,
  `InventoryLocation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_inventory_location_master`
--

INSERT INTO `vehicle_inventory_location_master` (`ID`, `InventoryLocation`) VALUES
(1, 'Nairobi City'),
(2, 'MOMBASA');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_more_features_master`
--

CREATE TABLE `vehicle_more_features_master` (
  `ID` int(11) NOT NULL,
  `MoreFeatures` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_more_features_master`
--

INSERT INTO `vehicle_more_features_master` (`ID`, `MoreFeatures`) VALUES
(1, 'Bluetooth Device'),
(2, 'Radio'),
(3, 'GPS'),
(4, 'Tv');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_year_master`
--

CREATE TABLE `vehicle_year_master` (
  `ID` int(11) NOT NULL,
  `VehicleYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_year_master`
--

INSERT INTO `vehicle_year_master` (`ID`, `VehicleYear`) VALUES
(1, 2011),
(2, 2007);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `body_type_master`
--
ALTER TABLE `body_type_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `booking_table`
--
ALTER TABLE `booking_table`
  ADD PRIMARY KEY (`BookingId`);

--
-- Indexes for table `color_master`
--
ALTER TABLE `color_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engine_capacity_master`
--
ALTER TABLE `engine_capacity_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `fuel_master`
--
ALTER TABLE `fuel_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ibooq_services`
--
ALTER TABLE `ibooq_services`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `job_card_more_features_master`
--
ALTER TABLE `job_card_more_features_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `job_card_more_features_record`
--
ALTER TABLE `job_card_more_features_record`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `job_card_more_features_record_out`
--
ALTER TABLE `job_card_more_features_record_out`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kenya_city`
--
ALTER TABLE `kenya_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `logis_company_master`
--
ALTER TABLE `logis_company_master`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `logis_company_sales_rep`
--
ALTER TABLE `logis_company_sales_rep`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `logis_company_sales_rep_clients`
--
ALTER TABLE `logis_company_sales_rep_clients`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `logis_company_subadmin`
--
ALTER TABLE `logis_company_subadmin`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `logis_company_superadmin_master`
--
ALTER TABLE `logis_company_superadmin_master`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `logis_drivers_kin`
--
ALTER TABLE `logis_drivers_kin`
  ADD PRIMARY KEY (`drivers_kin_id`);

--
-- Indexes for table `logis_drivers_master`
--
ALTER TABLE `logis_drivers_master`
  ADD PRIMARY KEY (`drv_id`);

--
-- Indexes for table `logis_employee_details`
--
ALTER TABLE `logis_employee_details`
  ADD PRIMARY KEY (`empdt_id`);

--
-- Indexes for table `logis_emp_docs`
--
ALTER TABLE `logis_emp_docs`
  ADD PRIMARY KEY (`edocs_id`);

--
-- Indexes for table `logis_flag_driver`
--
ALTER TABLE `logis_flag_driver`
  ADD PRIMARY KEY (`flag_drv_id`);

--
-- Indexes for table `logis_nhif_master`
--
ALTER TABLE `logis_nhif_master`
  ADD PRIMARY KEY (`nhif_id`);

--
-- Indexes for table `logis_nssf_master`
--
ALTER TABLE `logis_nssf_master`
  ADD PRIMARY KEY (`nssf_id`);

--
-- Indexes for table `logis_reports`
--
ALTER TABLE `logis_reports`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `logis_report_performance`
--
ALTER TABLE `logis_report_performance`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `logis_users`
--
ALTER TABLE `logis_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `logis_vehicle`
--
ALTER TABLE `logis_vehicle`
  ADD PRIMARY KEY (`logis_vehicle_id`);

--
-- Indexes for table `manufacturer_master`
--
ALTER TABLE `manufacturer_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mileage_master`
--
ALTER TABLE `mileage_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `model_master`
--
ALTER TABLE `model_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pridedrive_vehicles`
--
ALTER TABLE `pridedrive_vehicles`
  ADD PRIMARY KEY (`VehicleId`),
  ADD UNIQUE KEY `VehicleReg` (`VehicleReg`);

--
-- Indexes for table `smtp_settings`
--
ALTER TABLE `smtp_settings`
  ADD PRIMARY KEY (`smtp_id`);

--
-- Indexes for table `transmission_master`
--
ALTER TABLE `transmission_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_inventory_location_master`
--
ALTER TABLE `vehicle_inventory_location_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_more_features_master`
--
ALTER TABLE `vehicle_more_features_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_year_master`
--
ALTER TABLE `vehicle_year_master`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `body_type_master`
--
ALTER TABLE `body_type_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color_master`
--
ALTER TABLE `color_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `engine_capacity_master`
--
ALTER TABLE `engine_capacity_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fuel_master`
--
ALTER TABLE `fuel_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ibooq_services`
--
ALTER TABLE `ibooq_services`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `job_card_more_features_master`
--
ALTER TABLE `job_card_more_features_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `job_card_more_features_record`
--
ALTER TABLE `job_card_more_features_record`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `job_card_more_features_record_out`
--
ALTER TABLE `job_card_more_features_record_out`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kenya_city`
--
ALTER TABLE `kenya_city`
  MODIFY `city_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `logis_company_master`
--
ALTER TABLE `logis_company_master`
  MODIFY `comp_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `logis_company_sales_rep`
--
ALTER TABLE `logis_company_sales_rep`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `logis_company_sales_rep_clients`
--
ALTER TABLE `logis_company_sales_rep_clients`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `logis_company_subadmin`
--
ALTER TABLE `logis_company_subadmin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `logis_company_superadmin_master`
--
ALTER TABLE `logis_company_superadmin_master`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `logis_drivers_kin`
--
ALTER TABLE `logis_drivers_kin`
  MODIFY `drivers_kin_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `logis_drivers_master`
--
ALTER TABLE `logis_drivers_master`
  MODIFY `drv_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `logis_employee_details`
--
ALTER TABLE `logis_employee_details`
  MODIFY `empdt_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT for table `logis_emp_docs`
--
ALTER TABLE `logis_emp_docs`
  MODIFY `edocs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `logis_flag_driver`
--
ALTER TABLE `logis_flag_driver`
  MODIFY `flag_drv_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `logis_nhif_master`
--
ALTER TABLE `logis_nhif_master`
  MODIFY `nhif_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logis_nssf_master`
--
ALTER TABLE `logis_nssf_master`
  MODIFY `nssf_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logis_reports`
--
ALTER TABLE `logis_reports`
  MODIFY `report_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `logis_report_performance`
--
ALTER TABLE `logis_report_performance`
  MODIFY `report_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `logis_users`
--
ALTER TABLE `logis_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logis_vehicle`
--
ALTER TABLE `logis_vehicle`
  MODIFY `logis_vehicle_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `manufacturer_master`
--
ALTER TABLE `manufacturer_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mileage_master`
--
ALTER TABLE `mileage_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `model_master`
--
ALTER TABLE `model_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pridedrive_vehicles`
--
ALTER TABLE `pridedrive_vehicles`
  MODIFY `VehicleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `smtp_settings`
--
ALTER TABLE `smtp_settings`
  MODIFY `smtp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transmission_master`
--
ALTER TABLE `transmission_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_inventory_location_master`
--
ALTER TABLE `vehicle_inventory_location_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_more_features_master`
--
ALTER TABLE `vehicle_more_features_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vehicle_year_master`
--
ALTER TABLE `vehicle_year_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
