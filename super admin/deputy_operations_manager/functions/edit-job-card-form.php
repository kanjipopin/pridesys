<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$superadmin_email_add = $_SESSION['superadmin_email_add'];

if(isset($_POST['submit'])){

    // Job card more features details
    $Post_append = [];

    $PostCheck = $_POST['JobCardMoreFeaturesCheckAfter'];

    $Job_Card_No = $_POST['Job_Card_No'];
    $Job_Card_DriversName = $_POST['Job_Card_DriversName'];
    $Job_Card_Vehicle_Reg = $_POST['Job_Card_Vehicle_Reg'];
    $Job_Card_ClientsName = $_POST['Job_Card_ClientsName'];
    $Job_Card_Opened_By = $_POST['Job_Card_Opened_By'];
    $Job_Card_Department = $_POST['Job_Card_Department'];
    $Job_Card_DateTimeIn = $_POST['Job_Card_DateTimeIn'];
    $Job_Card_FuelIn = $_POST['Job_Card_FuelIn'];
    $Job_Card_MileageIn = $_POST['Job_Card_MileageIn'];
    $job_to_be_done = $_POST['job_to_be_done'];

    $Job_Card_DateTimeOut = $_POST['Job_Card_DateTimeOut'];
    $Job_Card_FuelOut = $_POST['Job_Card_FuelOut'];
    $Job_Card_MileageOut = $_POST['Job_Card_MileageOut'];

    $job_to_be_done_imploded = rtrim(implode(",",$job_to_be_done), ",");


    foreach($PostCheck as $val) {

        $Post_append[] = $val;

    }

    $Master_append = [];
    $MasterCols = [];

    $value = $conn->prepare("SELECT * from job_card_more_features_master");
    $value->execute();
    while($valueRow = $value->fetch()){
        $Master_append[] = str_replace(" ","_",$valueRow['JobCardMoreFeatures']);

    }

    $MasterCols["JobCardNo"] = $Job_Card_No;
    $MasterCols["JCFeaturesStatus"] = "Out";

    foreach($Master_append as $key => $row) {

        //echo $row." - master appends<br/>";

        if(in_array($row,$Post_append)) {
           // echo $row;
           $MasterCols[$row] = "1";
        } else {
            $MasterCols[$row] = "0";
        }


    }

    $JobCardMoreFeaturesTable = "job_card_more_features_record_out";

    $JobCardCols = array("Job_Card_No"=>$Job_Card_No,"Job_Card_Drivers_Name"=>$Job_Card_DriversName,"Job_Card_Vehicle_Reg"=>$Job_Card_Vehicle_Reg,"Job_Card_Clients_Name"=>$Job_Card_ClientsName,"Job_Card_Opened_By"=>$Job_Card_Opened_By,"Job_Card_Department"=>$Job_Card_Department,"Jobs_To_Be_Done"=>$job_to_be_done_imploded,"Job_Card_Date_Time_In"=>$Job_Card_DateTimeIn,"Job_Card_Date_Time_Out"=>$Job_Card_DateTimeOut,"Job_Card_Fuel_In"=>$Job_Card_FuelIn,"Job_Card_Fuel_Out"=>$Job_Card_FuelOut,"Job_Card_Mileage_In"=>$Job_Card_MileageIn,"Job_Card_Mileage_Out"=>$Job_Card_MileageOut, "Record_Date"=>$database->now_date_only, "Record_Time"=>$database->now_time_only);
    $JobCardTable = "job_card";
    $condition = "Job_Card_No='".$Job_Card_No."'";
    $updateToJobCardTable = $connection->UpdateQuery($JobCardTable,$JobCardCols,$condition);
    

    if($updateToJobCardTable == "success"){

        //$database->search_if_out_table_row_exists($Job_Card_No);

     if($database->search_if_out_table_row_exists($Job_Card_No) === "true") {

        $condition = "JobCardNo='".$Job_Card_No."'";

        $updateJobCardMoreFeaturesOut = $connection->UpdateQuery($JobCardMoreFeaturesTable,$MasterCols,$condition);

        if($updateJobCardMoreFeaturesOut == "success"){
            $connection->redirect("../job_card.php");
        }
        else {
            echo "failed to update to j card more features out";
        } 

    }  else {

        $insertJobCardMoreFeaturesOut = $connection->InsertQuery($JobCardMoreFeaturesTable,$MasterCols);

        if($insertJobCardMoreFeaturesOut == "success"){
            $connection->redirect("../job_card.php");
        }
        else {
            echo "failed to insert to j card more features";
        }    


    }  

         

     }
    else {

        //$connection->redirect("../job_card.php");
        echo "failed to insert to j card table";

    }  

    
}
?>