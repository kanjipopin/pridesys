<?php
	session_start();
	@$superadmin_email_add = $_SESSION['superadmin_email_add'];

	require_once('../config/db.php');
    require_once('../config/includes/initialise.php');

	if($superadmin_email_add == ""){
		$connection->redirect('../../index.php');
	}

    $searchTxt = isset($_GET['searchTxt']) ? $_GET['searchTxt'] : "";

    if($searchTxt != ""){

   		$stmt = $conn->prepare("SELECT * from spares_inventory WHERE SpareName LIKE '%".$searchTxt."%' or SpareCategory LIKE '%".$searchTxt."%' or SpareStock LIKE '%".$searchTxt."%' ");
   		$stmt->execute();
   		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rowCount = $stmt->rowCount();

	}
	else {
		$stmt = $conn->prepare("SELECT * from spares_inventory ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rowCount = $stmt->rowCount();
	}
?>