<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$superadmin_email_add = $_SESSION['superadmin_email_add'];

if(isset($_POST['submit'])){


    // Vehicle Master Details

    $MasterValue = isset($_POST['Job_Card_More_Feature']) ? $_POST['Job_Card_More_Feature'] : "";

    $MasterCols = array("JobCardMoreFeatures"=>$MasterValue);
    $MasterTable = "job_card_more_features_master";
    $insertMaster = $connection->InsertQuery($MasterTable,$MasterCols);


    if($insertMaster == "success"){
        $database->create_job_card_more_features_record_table();
        $connection->redirect("../job_card_more_features.php");
    }
    else {
        $connection->redirect("../job_card_more_features.php");
    }
}
?>