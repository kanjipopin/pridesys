<?php
@$superadmin_email_add = $_SESSION['superadmin_email_add'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$superadmin_email_add}'");
$comp->execute();
$comprow = $comp->fetch();

	$user_id = $comprow['Id'];

	$user_name = $comprow['Name'];

  $user_dept = $comprow['Department'];

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
} 


include'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Spares inventory</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="spares_inventory.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_spares_inventory.php" method="POST" id="form" name="form">
                  <span class="show_driver_details">
                    
                    <div class="form-group">
                      <label for="inputsuperadmin_email_add3" class="col-sm-3 control-label">Spare's category</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="spares_category" id="spares_category" value="" >

                      </div>

                    </div>

                      <div class="form-group">
                        <label for="inputsuperadmin_email_add3" class="col-sm-3 control-label">Spare's Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="spares_name" id="spares_name" required>

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputsuperadmin_email_add3" class="col-sm-3 control-label">Spare's Stock</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="spares_stock" id="spares_stock" required>

                        </div>

                      </div>

                    </div>

                  </span>


          <span class="show_salary">

            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Submit</button>
              </div>
            </div>
          </span>

        </form>


            </div>

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your superadmin_email_add for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }




    </script>



    <script src="../js/checkbox_select.js"></script>

<?php include'footer.php'; ?>