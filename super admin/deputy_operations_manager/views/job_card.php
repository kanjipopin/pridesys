<?php 

include'header.php'; 

if(isset($_POST['delete_job_card'])) {

  $job_card_id = $_POST['del_job_card_id'];

  $JobCardNoField = "Job_Card_No";
  $JobCardTable = "job_card";
  $deleteJobCard = $connection->DeleteRow($JobCardTable,$JobCardNoField,$job_card_id);

  if($deleteJobCard=="success"){
      $connection->redirect("job_card.php?ref=success");
  }
  else {
      $connection->redirect("job_card.php?ref=error");
  }

}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>My Job Cards (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_job_card.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="job_card.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Job card No</th>
                <th>Vehicle reg</th>
                <th>Client</th>
                <th>Datetime opened</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = base64_encode($fd['Job_Card_No']);

                  $job_card_no = $fd['Job_Card_No'];
                  $job_card_vehicle_reg = $fd['Job_Card_Vehicle_Reg'];
                  $job_card_client = $fd['Job_Card_Clients_Name'];
                  $Job_Card_Date_Time_In = $fd['Job_Card_Date_Time_In'];

                  $jcard_datetime = new DateTime($Job_Card_Date_Time_In);
                  $formatted_jcard_date = $jcard_datetime->format('Y-m-d');
                  $formatted_jcard_time = $jcard_datetime->format('h:i');

                  $vehicle_master = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$job_card_vehicle_reg' ");
                  $vehicle_master->execute();
                  $getVehicleData = $vehicle_master->fetch(PDO::FETCH_ASSOC);

                  $job_card_client_master = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE Id='$job_card_client' ");
                  $job_card_client_master->execute();
                  $getJobCardClientData = $job_card_client_master->fetch(PDO::FETCH_ASSOC);

            ?>
              <tr>
                <td><?php echo $job_card_no; ?></td>
                <td><?php echo $getVehicleData['VehicleReg']; ?></td>
                <td><?php echo $getJobCardClientData['ClientsName']; ?></td>
                <td><?php echo $formatted_jcard_date." at ".$formatted_jcard_time; ?></td>
                <td>
                  <a class="editBtn" href="<?php echo 'edit-job-card.php?id='.$id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>
                  <a href="#deleteJobCardDialog" class="openDeleteDialog" data-toggle="modal" data-jobcardid="<?php echo $job_card_no; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Delete
                    </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd['Job_Card_No']);

                  $job_card_no = $fd['Job_Card_No'];
                  $job_card_vehicle_reg = $fd['Job_Card_Vehicle_Reg'];
                  $job_card_client = $fd['Job_Card_Clients_Name'];
                  $job_card_opened_by = $fd['Job_Card_Opened_By'];
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $job_card_no; ?>
            </div>
            <div class="block-2">
              <h5><?php echo $job_card_vehicle_reg; ?></h5>
              <h5><?php echo $job_card_client; ?></h5>
              <h5><?php echo $job_card_opened_by; ?></h5>
              <a href="<?php echo 'job-card-detail.php?id='.$id; ?>" class="view-profile">View</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="vehicles.php"><img src="images/drivers-icon1.svg">My Vehicles(<?php echo $rowCount; ?>)</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteJobCardDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Delete job card</h2>
                
                <p class="delete-p">Are you sure you want to delete this job card?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myDeleteJobCardId" class="form-control" name="del_job_card_id" placeholder="JobCardId">

                  <input type="submit" style="float: right; " name="delete_job_card" value="Delete" class="btn btn-danger">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });

    $("#filterForm").on('click',function(e){
      var data="";
      e.preventDefault();
      var drvName = $("#drvName").val();
      var drvLicenseNum = $("#dln").val();
      var contactNo = $("#contact").val();
      //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

      $.ajax({
        type : "POST",
        url  : "functions/drivers.php",
        data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
        success : function(msg){
           // var msg = msg;
           //alert("this message:"+msg);
           $("#showDriverData").html("");
           $("#showDriverData").html(msg);
        }
      });


    });

    $(document).on("click", ".openDeleteDialog", function () {
            var myJobCardId = $(this).data('jobcardid');
            $("#myDeleteJobCardId").attr("value", myJobCardId);
  });


  </script>

<?php include'footer.php'; ?>