<?php
  ob_start();
  session_start();
  
  $superadmin_email_add = $_SESSION['superadmin_email_add'];

  $adminQry = $conn->prepare("SELECT * from logis_company_superadmin_master Where superadmin_email_add = '$superadmin_email_add' ");
  $adminQry->execute();
  $adminRes = $adminQry->fetch(PDO::FETCH_ASSOC);
  $adminName = $adminRes['user_name'];


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Deputy operations manager and QC</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="../images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">

      <!-- Flaticon CSS -->
      <link rel="stylesheet" href="../fonts/flaticon.css">
    <!-- Css -->
    <link rel="stylesheet" href="../css/portal.css">
    <link rel="stylesheet" href="../css/chat.css">
    <link rel="stylesheet" href="../css/portal-responsive.css">
    <link rel="stylesheet" href="../css/showup.css">
    <link rel="stylesheet" href="../css/datepicker.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="../css/tabs-style.css">
    <link href="../css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />


    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.php"><img src="../images/prime_logo_sizable.png"  alt="Pride drive" title="Pride drive" style="width: 160px; height: 60px;"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <!--  <div style="margin-top: 30px;display: inline-block;float: right;">
                <img src = "../images/notification1.png">
            </div> -->
            <ul class="nav navbar-nav navbar-right">
              <!-- <li><a href = "#"><img src = "../images/notification1.png"><span class="notify">0</span></a></li> -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <!-- <img style="padding-right: 7px;" src="<?php// echo $compLogo; ?>" class="imgLogo" width="35px" alt="User Name"/> -->
                <?php echo $adminName; ?>
                <span class="caret"></span></a>
                <ul class="dropdown-menu">

                  <li><a href="functions/logout.php">Log Out</a></li> 
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <div class="bg-color">
      <div class="container-fluid">
        <div class="sidebar stick hidden-xs" role="navigation">
          <div class="sidebar-nav navbar-collapse">


              <ul class="nav nav-sidebar-menu sidebar-toggle-view" id="side-menu">

                  <li class="nav-item sidebar-nav-item">

                    <a href="../sub_admin.php" class="<?php if(@$activeClass == "Home") { echo "active-class"; } ?>"><img src="../images/dashboard-icon1.png"/>Home</a>

                  </li>

                  <li class="nav-item sidebar-nav-item">

                      <a href="job_card.php" class="<?php if(@$activeClass == "JobCard") { echo "active-class"; } ?>"><img src="../images/dashboard-icon1.png"/>Job Card</a>

                  </li>

                  <li class="nav-item sidebar-nav-item">
                      <a href="#" class="nav-link

                            <?php 
                                  if(@$activeClass == "JobCardMoreFeatures") 
                                      { echo "active-class"; } 
                            ?>">

                          <img src="../images/drivers-icon1.png"/>Masters</a>
                      <ul class="nav sub-group-menu">

                        <li class="nav-item">
                              <a href="job_card_more_features.php" class="nav-link"><i class="glyphicon glyphicon-chevron-right"></i>Job card more features master</a>
                          </li>

                      </ul>
                  </li>

                  <li class="nav-item sidebar-nav-item">

                      <a href="spares_inventory.php" class="<?php if(@$activeClass == "Spares") { echo "active-class"; } ?>"><img src="../images/dashboard-icon1.png"/>Spares inventory</a>

                  </li>

              </ul>

          </div>
          <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->