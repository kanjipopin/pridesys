<?php

	session_start();
	require_once('../config/db.php');
	require_once('../config/includes/initialise.php');

	@$superadmin_email_add = $_SESSION['superadmin_email_add'];
	if($superadmin_email_add == ""){
		$connection->redirect('../index.php');
	}

	$activeClass = "Spares";
	include 'views/add_spares_inventory.php';

?>