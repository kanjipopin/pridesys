<?php
ob_start();
session_start();
include '../../config/db.php';
include '../../config/includes/initialise.php';
@$superadmin_email_add = $_SESSION['superadmin_email_add'];

if(isset($_POST['submit'])){


    // Sub admin Details

    $SubAdminName = isset($_POST['SubAdminName']) ? $_POST['SubAdminName'] : "";
    $SubAdminEmail = isset($_POST['SubAdminEmail']) ? $_POST['SubAdminEmail'] : "";
    $SubAdminPosition = isset($_POST['SubAdminPosition']) ? $_POST['SubAdminPosition'] : "";
    $SubAdminDepartment = isset($_POST['SubAdminDepartment']) ? $_POST['SubAdminDepartment'] : "";
    $SubAdminPassword = $_POST['SubAdminPassword'];
    $SubAdminConfirmPassword = $_POST['SubAdminConfirmPassword'];

    if($SubAdminPassword == $SubAdminConfirmPassword) {

        $database->set_password($SubAdminPassword);

        $password = $database->password;

        $RegDate = $database->now_date_only;

        $RegTime = $database->now_time_only;

        $comp_id = "28";

        $userCols = array("Name"=>$SubAdminName,"comp_id"=>$comp_id,"Email"=>$SubAdminEmail,"Position"=>$SubAdminPosition,"Department"=>$SubAdminDepartment,"Password"=>$password,"RegDate"=>$RegDate,"RegTime"=>$RegTime);
        $userTable = "logis_company_subadmin";
        $insertUser = $connection->InsertQuery($userTable,$userCols);


        if($insertUser == "success"){
            $connection->redirect("../sub_admin.php");
        }
        else {
            $connection->redirect("../add_sub_admin.php?error_msg=Failed to register user");
        }
    } else {

        $connection->redirect("../add_sub_admin.php?error_msg=Passwords do not match");

    }
}
?>