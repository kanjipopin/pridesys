<?php

@$superadmin_email_add = $_SESSION['superadmin_email_add'];


$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
$error_msg = isset($_GET['error_msg']) ? $_GET['error_msg'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}

$sales_rep_id = base64_decode($_GET['id']);

$stmt = $conn->prepare("SELECT * from logis_company_sales_rep WHERE Id='$sales_rep_id' ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rowCount = $stmt->rowCount();


foreach($results as $row) {
    $sales_rep_name = $row["Name"];
}


include 'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4><?php echo $sales_rep_name;?>'s clients</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="sales_rep_customers_details.php?id=<?php echo $_GET['id'];?>" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($error_msg)){ echo $error_msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_sales_rep_customers.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Add clients</h4>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Account name :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="ClientsName" id="ClientsName" placeholder="Account name" required>
                <input type="hidden" class="form-control" name="SalesRepId" id="SalesRepId" value="<?php echo $sales_rep_id;?>" >
              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Contact person :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="ClientsContactPerson" id="ClientsContactPerson" placeholder="Contact person" required>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Email address:</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="ClientsEmail" id="ClientsEmail" placeholder="Separate with comma if multiple" required>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Phone number :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="ClientsPhone" id="ClientsPhone" placeholder="Separate with comma if multiple" required>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Location :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="ClientsLocation" id="ClientsLocation" placeholder="Location" required>

              </div>

            </div>



          </span>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Create</button>
                        </div>
                    </div>

                </form>

                <!-- CLOSE ADD BRANCH - POPUP -->
            </div>

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="sales_rep.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Sales rep</a>
                    </li>

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#lstFruits').multiselect({
        //         includeSelectAllOption: true
        //     });
        // });
    </script>

<?php include'footer.php'; ?>