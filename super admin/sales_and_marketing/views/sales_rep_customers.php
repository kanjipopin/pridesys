<?php include'header.php'; ?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>My sales representatives clients (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>


        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="sales_rep_customers.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Name</th>
                <th>Clients</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = base64_encode($fd['Id']);

                $uncoded_id = $fd['Id'];

                  $Name = $fd['Name'];

                  $stmt_sales_rep_clients = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE SalesRepId='$uncoded_id' ");
		              $stmt_sales_rep_clients->execute();
		              $customers = $stmt_sales_rep_clients->rowCount();

            ?>
              <tr>
                <td><?php echo $Name; ?></td>
                <td><?php echo $customers; ?></td>
                <td>
                  <a class="viewBtn" href="<?php echo 'sales_rep_customers_details.php?id='.$id; ?>" style="display:inline-block;color: #0f4da1;font-weight: 400;">View clients
                  </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd1['Id']);

              $uncoded_id = $fd['Id'];

                  $Name = $fd1['Name'];
                  
                  $stmt_sales_rep_clients = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE SalesRepId='$uncoded_id' ");
		              $stmt_sales_rep_clients->execute();
		              $customers = $stmt_sales_rep_clients->rowCount();

        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $Name; ?>
            </div>
            <div class="block-2">
              <h5><?php echo $customers; ?></h5>
              <a href="<?php echo 'sales_rep_customers_details.php?id='.$id; ?>" class="view-profile">View clients</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="sales_rep.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Sales rep</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="js/metisMenu.min.js"></script>
  <script src="js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });

    $("#filterForm").on('click',function(e){
      var data="";
      e.preventDefault();
      var drvName = $("#drvName").val();
      var drvLicenseNum = $("#dln").val();
      var contactNo = $("#contact").val();
      //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

      $.ajax({
        type : "POST",
        url  : "functions/drivers.php",
        data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
        success : function(msg){
           // var msg = msg;
           //alert("this message:"+msg);
           $("#showDriverData").html("");
           $("#showDriverData").html(msg);
        }
      });


    });



  $(".dltDriver").click(function (event){

    var deletedvr = confirm("Are you sure want to delete these job card?");

    if(deletedvr == true){
      var dataid = $(event.currentTarget).attr('data-id');
      window.location = dataid;
    }
  });


  </script>

<?php include'footer.php'; ?>