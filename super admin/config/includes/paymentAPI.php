<?php

	require_once("initialise.php");

	class paymentAPI extends database {

		public $service_name,$business_no,$transaction_ref,$internal_transId,$trans_timestamp,
				$trans_type,$account_no,$sender_phone,$first_name,$middle_name,$last_name,$full_name,$amount,$table_capacity,$currency,$acc_deposit,$signature;
		public $pay_id,$pay_rs_id,$pay_drk_id,$pay_owner_id,$pay_user_email,$pay_amount;

		public $rs_name,$rs_id,$rs_total_amount;

		public  $fetched_service_name,
				$fetched_business_no,
				$fetched_transaction_ref,
				$fetched_internal_transId,
				$fetched_trans_timestamp,
				$fetched_trans_type,
				$fetched_account_no,
				$fetched_sender_phone,
				$fetched_first_name,
				$fetched_middle_name,
				$fetched_last_name,
				$fetched_amount;

		public $fetched_bal_reserve_id,
				$fetched_bal_payer_phone,
				$fetched_bal_payer_balance,
				$fetched_bal_update_date,
				$fetched_bal_update_time,$fetched_bal_table_capacity;
				
		public $start_rent_date,$end_rent_date;		

		public $account_balance,$pay_now_date_only,$pay_now_time_only;

        public $pay_date,$pay_time,$arrival_date,$arrival_time,$ending_time;
				
		
		private $payLaterTable = "poppinreservelater";
		private $receiptTable = "poppin_later_tickets_res";
		public $reserveTable = "poppinreserve";
		private $regTable = "poppinreg_res";
		
		public $oid,$phone,$email,$sid;


		    public function set_order_id($oid)
            {
                $this->oid = $oid;
            }
            
            
            public function set_phone($phone)
            {
                $this->phone = $phone;
            }
            
            public function set_email($email)
            {
                $this->email = $email;
            }
            
            public function set_sid($sid)
            {
                $this->sid = $sid;
            }

			public function set_service_name($attr) 
			{
					$this->service_name = $attr;
			}

			public function set_business_no($attr) 
			{
					$this->business_no = $attr;
			}

			public function set_transaction_ref($attr) 
			{
					$this->transaction_ref = $attr;
			}

			public function set_internal_transId($attr) 
			{
					$this->internal_transId = $attr;
			}

			public function set_trans_timestamp($attr) 
			{
					$this->trans_timestamp = $attr;
			}

			public function set_trans_type($attr) 
			{
					$this->trans_type = $attr;
			}

			public function set_account_no($attr) 
			{
					$this->account_no = $attr;
			}
			
			public function set_table_capacity($tkt_capacity)
			{
				$this->table_capacity = $tkt_capacity;
			}

			public function set_sender_phone($attr) 
			{
					$this->sender_phone = $attr;
			}

			public function set_first_name($attr) 
			{
					$this->first_name = $attr;
			}

			public function set_middle_name($attr) 
			{
					$this->middle_name = $attr;
			}

			public function set_last_name($attr) 
			{
					$this->last_name = $attr;
			}
			
			public function set_full_name($f,$l) 
			{
					$this->full_name = $f." ".$l;
			}

			public function set_amount($attr) 
			{
					$this->amount = $attr;
			}

			public function set_acc_deposit($acc)
			{
				$this->acc_deposit = $acc;
			}

			public function set_pay_id($pay_id)
			{
				$this->pay_id = $pay_id;
			}

			public function set_pay_rs_id($rs_id)
			{
				$this->pay_rs_id = $rs_id;
			}
			
			public function set_pay_owner_id($owner_id)
			{
				$this->pay_owner_id = $owner_id;
			}
			
			public function set_pay_user_email($user_email)
			{
				$this->pay_user_email = $user_email;
			}
			
    		public function set_payment_date($pay_date)
    		{
    		    $this->pay_date = $pay_date;
    		}

    		public function set_payment_time($pay_time)
    		{
    		    $this->pay_time = $pay_time;
    		}
    
    		public function set_pay_phone($mpesa_phone)
    		{
    		    $this->mpesa_phone = $mpesa_phone;
    		}

    		public function set_start_rent_date($start_rent_date)
    		{
    		    $this->start_rent_date = $start_rent_date;
    		}

    		public function set_end_rent_date($end_rent_date)
    		{
    		    $this->end_rent_date = $end_rent_date;
			}
			

		public function is_pay_later_done()
		{

			        $this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->payLaterTable(TransactionReference,PaymentId,ReserveId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,TableCapacity,AccDeposit,StartRentDate,EndRentDate,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_rs_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->table_capacity);
					$this->stmt->bindParam(10,$this->acc_deposit);
					$this->stmt->bindParam(11,$this->start_rent_date);
					$this->stmt->bindParam(12,$this->end_rent_date);
					$this->stmt->bindParam(13,$this->pay_date);
					$this->stmt->bindParam(14,$this->pay_time);

					if($this->stmt->execute()) {
						return true;
					} else {
						return false;
					}

    		
		}
		

		
		public function startsWith($string, $startString)
        {
            $len = strlen($startString);
            return (substr($string, 0, $len) === $startString);
        }



			public function fetch_by_payer_from_payment_table($payer_phone,$ReserveId)
			{


				$sql = $this->dbase->query("SELECT * FROM $this->payLaterTable WHERE PayerPhone='$payer_phone' AND ReserveId='$ReserveId' ");

				if($sql->rowCount() > 0 ) {
					
					foreach($sql as $row) {

						$this->fetched_bal_reserve_id = $row["ReserveId"];
						$this->fetched_bal_payer_phone = $row["PayerPhone"];
						$this->fetched_bal_table_capacity = $row["TableCapacity"];

						return true;

					}
					
				} else {
					return false;
				}

			}


			public function count_reserves_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->payLaterTable WHERE OwnerId='$OwnerId' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReserves"];
					}
				} else {
					return "0";
				}

			}
			
			public function count_total_reserved_tables_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(DISTINCT c.ReserveId) as TotalReservations FROM $this->payLaterTable b,$this->receiptTable c 
				                            WHERE b.ReserveId=c.ReserveId AND  b.OwnerId='$OwnerId' AND b.AccDeposit='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReservations"];
					}
				} else {
					return "0";
				}

			}
			
			public function count_table_reservations_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(DISTINCT c.TicketId) as TotalReservations FROM $this->payLaterTable b,$this->receiptTable c 
				                            WHERE b.PaymentId=c.PaymentId AND  b.OwnerId='$OwnerId' AND c.TicketStatus='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReservations"];
					}
				} else {
					return "0";
				}

			}

			public function sum_of_reserves_total_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->payLaterTable WHERE OwnerId='$OwnerId' AND AccDeposit='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalAmount"];
					}
				} else {
					return "0";
				}

			}

			public function count_all_reserves()
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable  ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function count_reserves_by_owner_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable WHERE OwnerId='$OwnerId' AND Category='$Category' ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function update_pay_withdrawal_status()
			{
				date_default_timezone_set('Africa/Nairobi');
			
				$this->pay_now_date_only = date("Y-m-d");
				
				$this->pay_now_time_only = date("H:i:s");

				$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET AccDeposit='1',PayDate=?,PayTime=? WHERE AccDeposit='0'");

				$this->stmt->bindParam(1,$this->pay_now_date_only);
				$this->stmt->bindParam(2,$this->pay_now_time_only);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}




	}

	$payment = new paymentAPI();


?>