<?php

class Automate {

	public $randomChar="A",$randomTicketChar="P",$randomNumber,$job_card_no,$random_pass,$timestamp,$random_user_id,$random_admin_code,$photo_id,$ticket_id,$receipt_id,$rs_id;
	public $commChar = "DRV";
	public $admin_code_char = "DRV";
	public $passChar = "PASS";
	public $payChar = "DRVPAY";
	public $ticketChar = "TKT";
	public $receiptChar = "DRK";

	public function generate_random_char()
	{
		$length = 3;

		$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		$characterLength = strlen($characters);

		for($i=0; $i<$length; $i++) {

			$this->randomChar .=$characters[rand(0,$characterLength)]; 

		}

		return $this->randomChar;

	}

	public function generate_job_card_random_char()
	{
		$length = 1;

		$characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		$characterLength = strlen($characters);

		for($i=0; $i<$length; $i++) {

			$this->randomChar .=$characters[rand(0,$characterLength)]; 

		}

		return $this->randomChar;

	}

	public function generate_random_ticket_char()
	{
		$length = 4;

		$characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		$characterLength = strlen($characters);

		for($i=0; $i<$length; $i++) {

			$this->randomTicketChar.=$characters[rand(0,$characterLength)]; 

		}

		return $this->randomTicketChar;

	}

	public function generate_random_number()
	{

		return $this->randomNumber = rand(1000,9000);

	}

	public function generate_current_timestamp()
	{
		return $this->timestamp = date("dms",time());
	}

	public function generate_user_id()
	{
		$this->random_user_id = $this->generate_random_char().$this->generate_random_number().$this->commChar;
	}
	
	public function generate_admin_code()
	{
		$this->random_admin_code = $this->generate_random_char().$this->generate_random_number().$this->admin_code_char;
	}
	
	public function generate_new_pass()
	{
		$this->random_pass = $this->generate_random_char().$this->generate_random_number().$this->passChar;
	}

	public function generate_photo_id()
	{
		$this->photo_id = $this->generate_random_number().$this->generate_random_char();
	}

	public function generate_ticked_id()
	{
		$this->ticket_id = $this->ticketChar.$this->generate_random_number().$this->generate_random_ticket_char();
	}
	
	public function generate_receipt_id()
	{
		$this->receipt_id = $this->receiptChar.$this->generate_random_number().$this->generate_random_ticket_char();
	}
	
	public function generate_reserve_id()
	{
		$this->rs_id = $this->generate_random_number().$this->generate_random_char();
	}

	public function generate_job_card_no()
	{
		$this->job_card_no = $this->generate_job_card_random_char().$this->generate_random_number();
	}

}

$automation = new Automate();


?>