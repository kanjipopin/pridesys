<?php

	require_once("initialise.php");

	class ticket extends paymentAPI {

		public $ticketsLaterTable = "poppin_later_tickets_res";
		public $reserveTable = "poppinreserve";
		public $paymentLaterTable = "poppinreservelater";

		public $ticket_id,$pay_id,$reserve_id,$payer_phone,$ticket_url,$ticket_status,$ticket_date,$ticket_time;
		public $receipt_id,$drink_id,$receipt_url,$receipt_status,$receipt_date,$receipt_time;
		public $reserve_name,$ticket_no,$ticket_capacity,$payer_name,$payer_email,$pay_date,$pay_time,$ending_time,$amount_paid,$category;

		public function set_ticket_id($ticketId)
		{
			$this->ticket_id = $ticketId;
		}
		
		public function set_pay_id($payId)
		{
			$this->pay_id = $payId;
		}

		public function set_reserve_id($reserveId)
		{
			$this->reserve_id = $reserveId;
		}

		public function set_payer_phone($payerPhone)
		{
			$this->payer_phone = $payerPhone;
		}

		public function set_ticket_url($ticketUrl)
		{
			$this->ticket_url = $ticketUrl;
		}

		public function set_ticket_status($status)
		{
			$this->ticket_status = $status;
		}
		
		public function set_receipt_id($receiptId)
		{
			$this->receipt_id = $receiptId;
		}

		public function set_drink_id($drinkId)
		{
			$this->drink_id = $drinkId;
		}
		
		public function set_receipt_url($receiptUrl)
		{
			$this->receipt_url = $receiptUrl;
		}

		public function set_receipt_status($status)
		{
			$this->receipt_status = $status;
		}

		public function insert_into_unpaid_tickets_table()
		{

			$sql = $this->dbase->query("SELECT PaymentId FROM $this->ticketsLaterTable WHERE PaymentId='$this->pay_id' ");

				//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

			if($sql->rowCount() > 0 ) {

				$this->stmt = $this->dbase->prepare("UPDATE $this->ticketsLaterTable SET TicketId=?,PaymentId=?,ReserveId=?,PayerPhone=?,TicketUrl=?,TicketStatus=?,TicketDate=now(),TicketTime=now() 
					WHERE PaymentId='$this->pay_id' ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			} else {

				
				$this->stmt = $this->dbase->prepare("INSERT INTO $this->ticketsLaterTable(TicketId,PaymentId,ReserveId,PayerPhone,TicketUrl,TicketStatus,TicketDate,TicketTime) 
								VALUES(?,?,?,?,?,?,now(),now() ) ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

		}
		
		public function get_unpaid_ticket_id_details($PaymentId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a,$this->ticketsLaterTable b,$this->paymentLaterTable c 
				WHERE c.PaymentId='$PaymentId' AND c.ReserveId=b.ReserveId AND a.ReserveId=b.ReserveId  ");

			foreach ($sql as $key => $row) {
				# code...
				$this->reserve_name = $row["ReserveName"];
	              $this->payer_phone = $row["PayerPhone"];
	              $this->ticket_no = $row["TicketId"];
	              $this->category = $row["Category"];
          			$this->amount_paid = $row["AmountPaid"];
	              $this->ticket_capacity = $row["TableCapacity"];
	              $this->payer_name = $row["PayerName"];
	              $this->payer_email = $row["PayerEmail"];
	              $this->pay_date = $row["StartRentDate"];
				  $this->pay_time = $row["EndRentDate"];
			}

		}

		public function get_unpaid_distinct_ticket_id($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.PaymentId,c.OwnerId,c.PaymentId,b.TicketStatus,b.TicketId FROM $this->ticketsLaterTable b,$this->paymentLaterTable c 
				WHERE b.PaymentId=c.PaymentId AND c.OwnerId='$OwnerId' AND b.TicketStatus='0' ");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		
		public function get_unpaid_distinct_car_ticket_id($ReserveId)
		{

			$sql = $this->dbase->query("SELECT b.PaymentId,c.OwnerId,c.PaymentId,b.TicketStatus,b.TicketId,c.ReserveId FROM $this->ticketsLaterTable b,$this->paymentLaterTable c 
				WHERE b.PaymentId=c.PaymentId AND c.ReserveId='$ReserveId' AND b.TicketStatus='0' ");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		

	}

	$ticket = new ticket();

?>