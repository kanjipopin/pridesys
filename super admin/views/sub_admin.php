<?php 

include'header.php'; 


if(isset($_POST['update_sub_admin_details'])) {


  $edit_sub_admin_id = $_POST['edit_sub_admin_id'];
  $edit_sub_admin_name = $_POST['edit_sub_admin_name'];
  $edit_sub_admin_email = $_POST['edit_sub_admin_email'];
  $edit_sub_admin_position = $_POST['edit_sub_admin_position'];
  $edit_sub_admin_department = $_POST['edit_sub_admin_department'];

 
        $editCols = array("Name"=>$edit_sub_admin_name,"Email"=>$edit_sub_admin_email,"Position"=>$edit_sub_admin_position,"Department"=>$edit_sub_admin_department);
        $editTable = "logis_company_subadmin";
        $condition = "Id='".$edit_sub_admin_id."'";
        $updateSubAdmin = $connection->UpdateQuery($editTable,$editCols,$condition);

        if($updateSubAdmin=="success"){
            $connection->redirect("sub_admin.php?ref=success");
        }
        else {
            $connection->redirect("sub_admin.php?ref=error");
        } 



}

if(isset($_POST['update_sub_admin_password'])) {


  $edit_sub_admin_pass_id = $_POST['edit_sub_admin_pass_id'];
  $edit_sub_admin_pass = $_POST['edit_sub_admin_pass'];
  $edit_sub_admin_confirm_pass = $_POST['edit_sub_admin_confirm_pass'];

    if($edit_sub_admin_pass == $edit_sub_admin_confirm_pass) {

        $database->set_password($edit_sub_admin_pass);

        $password = $database->password;
 
        $editCols = array("Password"=>$password);
        $editTable = "logis_company_subadmin";
        $condition = "Id='".$edit_sub_admin_pass_id."'";
        $updateSubAdminPass = $connection->UpdateQuery($editTable,$editCols,$condition);

        if($updateSubAdminPass=="success"){
            $connection->redirect("sub_admin.php?ref=success");
        }
        else {
            $connection->redirect("sub_admin.php?ref=error");
        } 

    }

}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">

    

      <div class="heading">
      <br/>
        <h4>My sub admins (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_sub_admin.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="sub_admin.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Position</th>
                <th>Department</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = base64_encode($fd['Id']);

                $user_id = $fd['Id'];

                  $Name = $fd['Name'];
                  $Email = $fd['Email'];
                  $position_encoded = $fd['Position'];
                  $department_encoded = $fd['Department'];

                  //head of sales
                  if($position_encoded == "head_of_sales_and_marketing") {
                    $Position = "Head of sales and marketing";
                  }

                  if($department_encoded == "sales_and_marketing") {
                    $Department = "Sales and marketing";
                  }

                  //sales_account_managers
                  if($position_encoded == "sales_account_managers") {
                    $Position = "Sales account manager";
                  }

                  //operations_manager
                  if($position_encoded == "operations_manager") {
                    $Position = "Operations manager";
                  }

                  if($department_encoded == "operations") {
                    $Department = "Operations";
                  }

                  //deputy_operations_manager
                  if($position_encoded == "deputy_operations_manager") {
                    $Position = "Deputy operations manager";
                  }

                  //Branch manager
                  if($position_encoded == "branch_manager") {
                    $Position = "Branch manager";
                  }

                  if($department_encoded == "branch_managers") {
                    $Department = "Branch manager";
                  }

                  //data entry
                  if($position_encoded == "data_entry") {
                    $Position = "Data entry";
                  }

                  if($department_encoded == "data_entry") {
                    $Department = "Data entry";
                  }

                  /* $vehicle_master = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$job_card_vehicle_reg' ");
                  $vehicle_master->execute();
                  $getVehicleData = $vehicle_master->fetch(PDO::FETCH_ASSOC); */

            ?>
              <tr>
                <td><?php echo $Name; ?></td>
                <td><?php echo $Email; ?></td>
                <td><?php echo $Position; ?></td>
                <td><?php echo $Department; ?></td>
                <td>
                <a href="#editSubAminDialog" class="openEditDetailsDialog" data-toggle="modal" data-editname="<?php echo $Name; ?>" data-editemail="<?php echo $Email; ?>" data-editposition="<?php echo $position_encoded; ?>" data-editdepartment="<?php echo $department_encoded; ?>" data-editid="<?php echo $user_id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>
                  <a href="#editSubAminPasswordDialog" class="openEditPasswordDialog" data-toggle="modal" data-editpassid="<?php echo $user_id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Update password
                  </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">

        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd1['Id']);

                $user_id = $fd1['Id'];

                  $Name = $fd1['Name'];
                  $Email = $fd1['Email'];
                  $position_encoded = $fd1['Position'];
                  $department_encoded = $fd1['Department'];

                  //head of sales
                  if($position_encoded == "head_of_sales_and_marketing") {
                    $Position = "Head of sales and marketing";
                  }

                  if($department_encoded == "sales_and_marketing") {
                    $Department = "Sales and marketing";
                  }

                  //sales_account_managers
                  if($position_encoded == "sales_account_managers") {
                    $Position = "Sales account manager";
                  }

                  //operations_manager
                  if($position_encoded == "operations_manager") {
                    $Position = "Operations manager";
                  }

                  if($department_encoded == "operations") {
                    $Department = "Operations";
                  }

                  //deputy_operations_manager
                  if($position_encoded == "deputy_operations_manager") {
                    $Position = "Deputy operations manager";
                  }

                  //Branch manager
                  if($position_encoded == "branch_manager") {
                    $Position = "Branch manager";
                  }

                  if($department_encoded == "branch_managers") {
                    $Department = "Branch manager";
                  }

                  //data entry
                  if($position_encoded == "data_entry") {
                    $Position = "Data entry";
                  }

                  if($department_encoded == "data_entry") {
                    $Department = "Data entry";
                  }
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $Name; ?>
              
            </div>
            <div class="block-2">
              <h5><?php echo $Email; ?></h5>
              <h5><?php echo $Position; ?></h5>
              <h5><?php echo $Department; ?></h5>
              
              <a href="#editSubAminDialog" class="openEditDetailsDialog" data-toggle="modal" data-editname="<?php echo $Name; ?>" data-editemail="<?php echo $Email; ?>" data-editposition="<?php echo $position_encoded; ?>" data-editdepartment="<?php echo $department_encoded; ?>" data-editid="<?php echo $user_id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>
                  <a href="#editSubAminPasswordDialog" class="openEditPasswordDialog" data-toggle="modal" data-editpassid="<?php echo $user_id; ?>" style="display:inline-block;margin-left: 10px;padding-left: 10px;border-left: 1px solid #000;">Update password
                  </a>

            </div>


          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="../images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="vehicles.php"><img src="../images/drivers-icon1.svg">Sub admins (<?php echo $rowCount; ?>)</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <div class="modal fade" id="editSubAminDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h4 class="delete-title">Edit sub admin</h4>
                

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >
                
                
                  <input type="hidden" id="editSubAdminId" class="form-control" name="edit_sub_admin_id" placeholder="User Id">
                  
                  <label for="editSubAdminName">Name</label>
                  <input type="text" id="editSubAdminName" class="form-control" name="edit_sub_admin_name" placeholder="User name">

                  <br/>
                  <label for="editSubAdminEmail">Email address</label>
                  <input type="text" id="editSubAdminEmail" class="form-control" name="edit_sub_admin_email" placeholder="User email">

                  <br/>
                  <label for="editSubAdminPosition">Position</label>
                  <select class="form-control" id="editSubAdminPosition" name="edit_sub_admin_position">
                    
                        <option value ="head_of_sales_and_marketing">Head of sales and marketing</option>
                        <option value ="sales_account_managers">Sales account manager</option>
                        <option value ="operations_manager">Operations manager</option>
                        <option value ="deputy_operations_manager">Deputy operations manager and quality control</option>
                        <option value ="branch_manager">Branch manager</option>
                        <option value ="data_entry">Data entry</option>

                  </select>

                  <br/>
                  <label for="editSubAdminDepartment">Department</label>
                  <select class="form-control" id="editSubAdminDepartment" name="edit_sub_admin_department">
                    
                        <option value ="sales_and_marketing">Sales and marketing</option>
                        <option value ="operations">Operations</option>
                        <option value ="branch_managers">Branch managers</option>
                        <option value ="data_entry">Data entry</option>

                  </select>

                  <input type="submit" style="float: right; " name="update_sub_admin_details" value="Update" class="btn btn-primary">
                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="editSubAminPasswordDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Update password</h2>
                

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="text" id="editSubAdminPasswordId" class="form-control" name="edit_sub_admin_pass_id" placeholder="Id">
                  
                  <br/>
                  <label>Password</label>
                  <input type="text" class="form-control" name="edit_sub_admin_pass" placeholder="Password">

                  <br/>
                  <label>Confirm password</label>
                  <input type="text" class="form-control" name="edit_sub_admin_confirm_pass" placeholder="Confirm password">

                  <input type="submit" style="float: right; " name="update_sub_admin_password" value="Update" class="btn btn-danger">
                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    
    $(document).on("click", ".openEditDetailsDialog", function () {
            var editSubAdminId = $(this).data('editid');
            var editSubAdminName = $(this).data('editname');
            var editSubAdminEmail = $(this).data('editemail');
            var editSubAdminPosition = $(this).data('editposition');
            var editSubAdminDepartment = $(this).data('editdepartment');

            $("#editSubAdminId").attr("value", editSubAdminId);
            $("#editSubAdminName").attr("value", editSubAdminName);
            $("#editSubAdminEmail").attr("value", editSubAdminEmail);
            $("#editSubAdminPosition").val(editSubAdminPosition);
            $("#editSubAdminDepartment").val(editSubAdminDepartment);

    });

    $(document).on("click", ".openEditPasswordDialog", function () {
            var editSubAdminPasswordId = $(this).data('editpassid');


            $("#editSubAdminPasswordId").attr("value", editSubAdminPasswordId);


    });


  </script>

<?php include'footer.php'; ?>