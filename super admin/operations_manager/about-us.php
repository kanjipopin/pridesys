<?php
  ob_start();
  session_start();
  date_default_timezone_set("Africa/Nairobi");
  include("config/db.php");

  @$Email = $_SESSION['superadmin_email_add'];

  if($_SERVER['HTTPS'] != "on"){ 
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    header('Location:'. $redirect); 
  } 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <head>
    <title>Dereva Enterprise</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Css -->
    <link rel="stylesheet" href="css/portal.css">
    <link rel="stylesheet" href="css/chat.css">
    <link rel="stylesheet" href="css/portal-responsive.css">
    <link rel="stylesheet" href="css/showup.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <!-- WOW Effect -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="css/tabs-style.css">
    <link href="css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121038053-2');
</script>
  </head>

  <body class="bodyDisbld">
  <header>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-sm-3 col-md-3">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php if($Email == ""){ ?>
            <a class="navbar-brand" href="index.php"><img src="images/logo.svg" alt="Dereva Enterprise" title="Dereva Enterprise"></a>
          <?php } else { ?>
            <a class="navbar-brand" href="dashboard.php"><img src="images/logo.svg" alt="Dereva Enterprise" title="Dereva Enterprise"></a>
          <?php } ?>
        </div>

        <div class="col-sm-8 col-md-9">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <?php if($Email == ""){ ?>
            <ul class="nav navbar-nav navbar-right">
              <li><a class="btn btn-default header-btn" href="login.php" role="button" style="margin-right: 15px;padding: 10px 10px;">Login</a></li>
              <li><a class="btn btn-default header-btn" href="sign-up.php" role="button" style="padding: 10px 10px;">Sign Up</a></li>
            </ul>

          <?php } else { ?>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="profile.php">Profile</a></li>
              <li><a href="about-us.php">About Us</a></li>
              <li><a href="pricing_table.php">Pricing</a></li>
              <li><a href="invoice.php">Billing</a></li>
              <li><a href="functions/logout.php">Log Out</a></li>
            </ul>
          <?php } ?>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </nav>
  </header>

	<!-- <section class="how_it_worksFirst">
		<div class="container">
      <h3>Why Use Dereva Enterprise?</h3>
			<div class="row">
				<div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-1-Licence-renewal-reminder.png">
          <p>Licence <br>Renewal <br>reminder</p>
				</div>

        <div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-2-Easy-to-use-platform.png">
          <p>Easy to use <br>platform</p>
        </div>

        <div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-3-Essential-documents-on-demand.png">
          <p>Essential <br>documents <br>on demand</p>
        </div>

        <div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-4-Edit-records-with-ease.png">
          <p>Edit records with <br>ease</p>
        </div>

        <div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-5-Safe-and-secure.png">
          <p>Safe and <br>secure</p>
        </div>

        <div class="col-md-4 firstSection">
          <img src="images/Dereva_enterprise_template/Image-6-User-access-control.png">
          <p>User access <br>control</p>
        </div>
			</div>
		</div>
	</section> -->


  <section class="how_it_worksSeconds">
    <div class="container">
      <p>Dereva Enterprise is a cloud based solution that enables fleet owners to effectively manage driver information and documents. Whether you are a small business or a multinational, the platform provides enterprise-grade software with intuitive design and powerful security at an affordable price. With quick and easy setup, and accessibility from any device or web browser, you can be productive from the word go. The platform provides ground-breaking features that are an industry first so that you can focus on running your fleet more effectively.</p>
    </div>
  </section>


  <section class="how_it_worksThird">
    <div class="container">
      <h3>Our Core Features</h3>
      <div class="row">
        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/Image-1-manage-edit-and-access-driver.png">
          <p>Manage, Edit and <br>Access <br>Driver Documents <br>on the Cloud</p> 
        </div>

        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/image-3-manage-and-renew-driver-contract.png">
          <p>Manage <br>Driver Contract</p> 
        </div>

        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/Image-2-flag-malicious-drivers.png">
          <p>Flag Bad <br>Drivers </p> 
        </div>

        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/Image-5-Driving-licence-renewal-reminders.png">
          <p>Driving License <br>Renewal <br>Reminders </p> 
        </div>

        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/Image-7-Upload-vehicle-inspection-certificates.png">
          <p>Vehicle Inspection<br> Renewal reminder</p> 
        </div>

        <div class="col-md-4 SecondSection">
          <img src="images/Dereva_enterprise_template/Image-6-Insurance-renewal-reminders.png">
          <p>Insurance Renewal <br>Reminders </p> 
        </div>

      </div>
    </div>
  </section>


  <!-- <section class="how_it_worksSecond">
    <div class="container">
      <p>The platforms innovative software design paired with potential functionality and an easy-to-use user interface makes Dereva Enterprise the ultimate solution to your Fleets organizational hierarchy. </p>

      <p>Dereva Enterprise is directly accessible from our Enterprise Cloud and is easily accessible from almost any web based browser. Our web based mobile client is light and responsive which makes it easy to run on mobile device such as tablets and ensures control of your documents from anywhere with ease.</p>
    </div>
  </section> -->


  <section class="how_it_worksFourth">
    <div class="container">
      <h3>So what are you waiting for?</h3>
      <?php if($Email == ""){ ?>
        <a href="https://enterprise.dereva.com/sign-up.php" class="SignUpBtn">Sign Up</a>
      <?php } else { ?>
        <a href="https://enterprise.dereva.com/dashboard.php" class="SignUpBtn">Sign Up</a>
      <?php } ?>
    </div>
  </section>

	<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <footer class="new-footer">
    <div class="container-fluid hidden-xs">
      <div class="col-sm-4">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-4">
        <p>&copy; Dereva.com | All rights reserved.</p>
        <a href="terms-of-service.php">Terms of Service</a>
      </div>

      <div class="col-sm-4">
        <ul style="float: right;">
          <li>Follow Us</li><br>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="../images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="../images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/company/derevaofficial" target="_blank"><img src="../images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/dereva.official/" target="_blank"><img src="../images/insta.svg"></a></li>
        </ul>
      </div>
    </div>

      <div class="container-fluid visible-xs" style="text-align: center;">
      <div class="col-sm-3">
        <p style="color: #fff;">Pent Office, 6th floor, Nivina Towers <br> Westlands Road, Nairobi.<br>
          <a href="tel: +254753000888" target="_blank" style="color: #fff;">+254753000888</a> <br>
          <a href="mailto: enterprise.support@dereva.com " target="_blank" style="color: #fff;">enterprise.support@dereva.com </a>
        </p>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><a href="https://www.facebook.com/derevaofficial" target="_blank"><img src="images/fb.svg"></a></li>
          <li><a href="https://twitter.com/derevaofficial" target="_blank"><img src="images/twitter.svg"></a></li>
          <li><a href="https://www.linkedin.com/derevaofficial" target="_blank"><img src="images/linkedin.svg"></a></li>
          <li><a href="https://www.instagram.com/derevaofficial/" target="_blank"><img src="images/insta.svg"></a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <p style="font-size: 12px;"><a href="terms-of-service.php">Terms of Service</a> <br> &copy; Dereva.com | All rights reserved.</p>
      </div>
    </div>
    </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/showup.js"></script>
<script src="js/wow.js"></script>