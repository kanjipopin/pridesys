<?php
	ob_start();
	session_start();
	require_once('../config/db.php');
	@$Email = $_SESSION['superadmin_email_add'];

	if(isset($_POST['submit'])){

		// Driver Details

		$logis_entries_id = isset($_POST['logis_entries_id']) ? $_POST['logis_entries_id'] : "";

		$ship_arrived_date1 = isset($_POST['ship_arrived_date']) ? $_POST['ship_arrived_date'] : "";

		if($ship_arrived_date1 != ""){
			$ship_arrived_date = date("Y-m-d H:i:s",strtotime($ship_arrived_date));
		} else {
			$ship_arrived_date = "";
		}

		$mainfest_rec_date1 = isset($_POST['mainfest_rec_date']) ? $_POST['mainfest_rec_date'] : "";

		if($mainfest_rec_date1 != ""){
			$mainfest_rec_date = date("Y-m-d H:i:s",strtotime($mainfest_rec_date));
		} else {
			$mainfest_rec_date = "";
		}

		$entry_registered_date1 = isset($_POST['entry_registered_date']) ? $_POST['entry_registered_date'] : "";

		if($entry_registered_date1 != ""){
			$entry_registered_date = date("Y-m-d H:i:s",strtotime($entry_registered_date));
		} else {
			$entry_registered_date = "";
		}

		$cd_lodged_date1 = isset($_POST['cd_lodged_date']) ? $_POST['cd_lodged_date'] : "";

		if($cd_lodged_date1 != ""){
			$cd_lodged_date = date("Y-m-d H:i:s",strtotime($cd_lodged_date));
		} else {
			$cd_lodged_date = "";
		}

		$local_coc_submit_date1 = isset($_POST['local_coc_submit_date']) ? $_POST['local_coc_submit_date'] : "";

		if($local_coc_submit_date1 != ""){
			$local_coc_submit_date = date("Y-m-d H:i:s",strtotime($local_coc_submit_date));
		} else {
			$local_coc_submit_date = "";
		}

		$duty_paid_date1 = isset($_POST['duty_paid_date']) ? $_POST['duty_paid_date'] : "";

		if($duty_paid_date1 != ""){
			$duty_paid_date = date("Y-m-d H:i:s",strtotime($duty_paid_date));
		} else {
			$duty_paid_date = "";
		}

		$entry_passed_date1 = isset($_POST['entry_passed_date']) ? $_POST['entry_passed_date'] : "";

		if($entry_passed_date1 != ""){
			$entry_passed_date = date("Y-m-d H:i:s",strtotime($entry_passed_date));
		} else {
			$entry_passed_date = "";
		}

		$original_bl_rec_date1 = isset($_POST['original_bl_rec_date']) ? $_POST['original_bl_rec_date'] : "";

		if($original_bl_rec_date1 != ""){
			$original_bl_rec_date = date("Y-m-d H:i:s",strtotime($original_bl_rec_date));
		} else {
			$original_bl_rec_date = "";
		}

		$do_lodged_date1 = isset($_POST['do_lodged_date']) ? $_POST['do_lodged_date'] : "";

		if($do_lodged_date1 != ""){
			$do_lodged_date = date("Y-m-d H:i:s",strtotime($do_lodged_date));
		} else {
			$do_lodged_date = "";
		}

		$do_released_date1 = isset($_POST['do_released_date']) ? $_POST['do_released_date'] : "";

		if($do_released_date1 != ""){
			$do_released_date = date("Y-m-d H:i:s",strtotime($do_released_date));
		} else {
			$do_released_date = "";
		}

		$cont_left_mombasa_date1 = isset($_POST['cont_left_mombasa_date']) ? $_POST['cont_left_mombasa_date'] : "";

		if($cont_left_mombasa_date1 != ""){
			$cont_left_mombasa_date = date("Y-m-d H:i:s",strtotime($cont_left_mombasa_date));
		} else {
			$cont_left_mombasa_date = "";
		}

		$cont_arrived_icd_date1 = isset($_POST['cont_arrived_icd_date']) ? $_POST['cont_arrived_icd_date'] : "";

		if($cont_arrived_icd_date1 != ""){
			$cont_arrived_icd_date = date("Y-m-d H:i:s",strtotime($cont_arrived_icd_date));
		} else {
			$cont_arrived_icd_date = "";
		}

		$doc_lodged_date1 = isset($_POST['doc_lodged_date']) ? $_POST['doc_lodged_date'] : "";

		if($doc_lodged_date1 != ""){
			$doc_lodged_date = date("Y-m-d H:i:s",strtotime($doc_lodged_date));
		} else {
			$doc_lodged_date = "";
		}

		$verification_date1 = isset($_POST['verification_date']) ? $_POST['verification_date'] : "";

		if($verification_date1 != ""){
			$verification_date = date("Y-m-d H:i:s",strtotime($verification_date));
		} else {
			$verification_date = "";
		}

		$ro_date1 = isset($_POST['ro_date']) ? $_POST['ro_date'] : "";

		if($ro_date1 != ""){
			$ro_date = date("Y-m-d H:i:s",strtotime($ro_date));
		} else {
			$ro_date = "";
		}

		$port_charges_paid_date1 = isset($_POST['port_charges_paid_date']) ? $_POST['port_charges_paid_date'] : "";

		if($port_charges_paid_date1 != ""){
			$port_charges_paid_date = date("Y-m-d H:i:s",strtotime($port_charges_paid_date));
		} else {
			$port_charges_paid_date = "";
		}


		$cont_loaded_date1 = isset($_POST['cont_loaded_date']) ? $_POST['cont_loaded_date'] : "";

		if($cont_loaded_date1 != ""){
			$cont_loaded_date = date("Y-m-d H:i:s",strtotime($cont_loaded_date));
		} else {
			$cont_loaded_date = "";
		}

		$doc_completed_date1 = isset($_POST['doc_completed_date']) ? $_POST['doc_completed_date'] : "";

		if($doc_completed_date1 != ""){
			$doc_completed_date = date("Y-m-d H:i:s",strtotime($doc_completed_date));
		} else {
			$doc_completed_date = "";
		}

		$free_day_exp_date = isset($_POST['free_day_exp_date']) ? $_POST['free_day_exp_date'] : "";

		if($free_day_exp_date != ""){
			$free_day_exp_date = date("Y-m-d H:i:s",strtotime($free_day_exp_date));
		} else {
			$free_day_exp_date = "";
		}

		$empty_cont_return_date1 = isset($_POST['empty_cont_return_date']) ? $_POST['empty_cont_return_date'] : "";

		if($empty_cont_return_date1 != ""){
			$empty_cont_return_date = date("Y-m-d H:i:s",strtotime($empty_cont_return_date1));
		} else {
			$empty_cont_return_date = "";
		}

		$remark = isset($_POST['remark']) ? $_POST['remark'] : "";

		$CreatedDate = date("Y-m-d H:i:s");

		// 

		// if($ship_arrived_date !="" && $mainfest_rec_date !="" && $entry_registered_date !="" && $cd_lodged_date !="" && $local_coc_submit_date !="" && $duty_paid_date !="" && $original_bl_rec_date !="" && $do_lodged_date !="" && $do_released_date !="" && $cont_left_mombasa_date !="" && $cont_arrived_icd_date !="" && $doc_lodged_date !="" && $verification_date !="" && $ro_date !="" && $port_charges_paid !="" && $cont_loaded_date !="" && $doc_completed_date !="" && $free_day_exp_date !="" && $empty_cont_return_date !=""){

	  $entries = $conn->prepare("SELECT * from logis_entries_master where logis_entries_id ='{$logis_entries_id}' ");
	  $entries->execute();
	  $entriesRow = $entries->fetch();
	  $entriesCount =$entries->rowCount();

	  if($empty_cont_return_date != ""){
	  	$status = "complete";
	  }
	  else {
	  	$status = "incomplete";
	  }

		if($entriesCount == 0){

			$entriesCols = array("logis_entries_id"=>$logis_entries_id,"ship_arrived_date"=>$ship_arrived_date,"mainfest_rec_date"=>$mainfest_rec_date,"entry_registered_date"=>$entry_registered_date,"cd_lodged_date"=>$cd_lodged_date,"local_coc_submit_date"=>$local_coc_submit_date,"duty_paid_date"=>$duty_paid_date,"entry_passed_date"=>$entry_passed_date,"original_bl_rec_date"=>$original_bl_rec_date,"do_lodged_date"=>$do_lodged_date,"do_released_date"=>$do_released_date,"cont_left_mombasa_date"=>$cont_left_mombasa_date,"cont_arrived_icd_date"=>$cont_arrived_icd_date,"doc_lodged_date"=>$doc_lodged_date,"verification_date"=>$verification_date,"ro_date"=>$ro_date,"port_charges_paid_date"=>$port_charges_paid_date,"cont_loaded_date"=>$cont_loaded_date,"doc_completed_date"=>$doc_completed_date,"free_day_exp_date"=>$free_day_exp_date,"empty_cont_return_date"=>$empty_cont_return_date,"remark"=>$remark,'CreatedDate'=>$CreatedDate,"status"=>$status);
			$entriesTable = "logis_entries_master";
			$insertDriver = $connection->InsertQuery($entriesTable,$entriesCols);
	}
	else{

	
		$ship_arrived_date1 = isset($_POST['ship_arrived_date']) ? $_POST['ship_arrived_date'] : "";

		if($ship_arrived_date1 != ""){
			$ship_arrived_date = date("Y-m-d H:i:s",strtotime($ship_arrived_date));
		} else {
			$ship_arrived_date = "";
		}

		$mainfest_rec_date1 = isset($_POST['mainfest_rec_date']) ? $_POST['mainfest_rec_date'] : "";

		if($mainfest_rec_date1 != ""){
			$mainfest_rec_date = date("Y-m-d H:i:s",strtotime($mainfest_rec_date));
		} else {
			$mainfest_rec_date = "";
		}

		$entry_registered_date1 = isset($_POST['entry_registered_date']) ? $_POST['entry_registered_date'] : "";

		if($entry_registered_date1 != ""){
			$entry_registered_date = date("Y-m-d H:i:s",strtotime($entry_registered_date));
		} else {
			$entry_registered_date = "";
		}

		$cd_lodged_date1 = isset($_POST['cd_lodged_date']) ? $_POST['cd_lodged_date'] : "";

		if($cd_lodged_date1 != ""){
			$cd_lodged_date = date("Y-m-d H:i:s",strtotime($cd_lodged_date));
		} else {
			$cd_lodged_date = "";
		}

		$local_coc_submit_date1 = isset($_POST['local_coc_submit_date']) ? $_POST['local_coc_submit_date'] : "";

		if($local_coc_submit_date1 != ""){
			$local_coc_submit_date = date("Y-m-d H:i:s",strtotime($local_coc_submit_date));
		} else {
			$local_coc_submit_date = "";
		}

		$duty_paid_date1 = isset($_POST['duty_paid_date']) ? $_POST['duty_paid_date'] : "";

		if($duty_paid_date1 != ""){
			$duty_paid_date = date("Y-m-d H:i:s",strtotime($duty_paid_date));
		} else {
			$duty_paid_date = "";
		}

		$entry_passed_date1 = isset($_POST['entry_passed_date']) ? $_POST['entry_passed_date'] : "";

		if($entry_passed_date1 != ""){
			$entry_passed_date = date("Y-m-d H:i:s",strtotime($entry_passed_date));
		} else {
			$entry_passed_date = "";
		}

		$original_bl_rec_date1 = isset($_POST['original_bl_rec_date']) ? $_POST['original_bl_rec_date'] : "";

		if($original_bl_rec_date1 != ""){
			$original_bl_rec_date = date("Y-m-d H:i:s",strtotime($original_bl_rec_date));
		} else {
			$original_bl_rec_date = "";
		}

		$do_lodged_date1 = isset($_POST['do_lodged_date']) ? $_POST['do_lodged_date'] : "";

		if($do_lodged_date1 != ""){
			$do_lodged_date = date("Y-m-d H:i:s",strtotime($do_lodged_date));
		} else {
			$do_lodged_date = "";
		}

		$do_released_date1 = isset($_POST['do_released_date']) ? $_POST['do_released_date'] : "";

		if($do_released_date1 != ""){
			$do_released_date = date("Y-m-d H:i:s",strtotime($do_released_date));
		} else {
			$do_released_date = "";
		}

		$cont_left_mombasa_date1 = isset($_POST['cont_left_mombasa_date']) ? $_POST['cont_left_mombasa_date'] : "";

		if($cont_left_mombasa_date1 != ""){
			$cont_left_mombasa_date = date("Y-m-d H:i:s",strtotime($cont_left_mombasa_date));
		} else {
			$cont_left_mombasa_date = "";
		}

		$cont_arrived_icd_date1 = isset($_POST['cont_arrived_icd_date']) ? $_POST['cont_arrived_icd_date'] : "";

		if($cont_arrived_icd_date1 != ""){
			$cont_arrived_icd_date = date("Y-m-d H:i:s",strtotime($cont_arrived_icd_date));
		} else {
			$cont_arrived_icd_date = "";
		}

		$doc_lodged_date1 = isset($_POST['doc_lodged_date']) ? $_POST['doc_lodged_date'] : "";

		if($doc_lodged_date1 != ""){
			$doc_lodged_date = date("Y-m-d H:i:s",strtotime($doc_lodged_date));
		} else {
			$doc_lodged_date = "";
		}

		$verification_date1 = isset($_POST['verification_date']) ? $_POST['verification_date'] : "";

		if($verification_date1 != ""){
			$verification_date = date("Y-m-d H:i:s",strtotime($verification_date));
		} else {
			$verification_date = "";
		}

		$ro_date1 = isset($_POST['ro_date']) ? $_POST['ro_date'] : "";

		if($ro_date1 != ""){
			$ro_date = date("Y-m-d H:i:s",strtotime($ro_date));
		} else {
			$ro_date = "";
		}

		$port_charges_paid_date1 = isset($_POST['port_charges_paid_date']) ? $_POST['port_charges_paid_date'] : "";

		if($port_charges_paid_date1 != ""){
			$port_charges_paid_date = date("Y-m-d H:i:s",strtotime($port_charges_paid_date));
		} else {
			$port_charges_paid_date = "";
		}


		$cont_loaded_date1 = isset($_POST['cont_loaded_date']) ? $_POST['cont_loaded_date'] : "";

		if($cont_loaded_date1 != ""){
			$cont_loaded_date = date("Y-m-d H:i:s",strtotime($cont_loaded_date));
		} else {
			$cont_loaded_date = "";
		}

		$doc_completed_date1 = isset($_POST['doc_completed_date']) ? $_POST['doc_completed_date'] : "";

		if($doc_completed_date1 != ""){
			$doc_completed_date = date("Y-m-d H:i:s",strtotime($doc_completed_date));
		} else {
			$doc_completed_date = "";
		}

		$free_day_exp_date = isset($_POST['free_day_exp_date']) ? $_POST['free_day_exp_date'] : "";

		if($free_day_exp_date != ""){
			$free_day_exp_date = date("Y-m-d H:i:s",strtotime($free_day_exp_date));
		} else {
			$free_day_exp_date = "";
		}

		$empty_cont_return_date1 = isset($_POST['empty_cont_return_date']) ? $_POST['empty_cont_return_date'] : "";

		if($empty_cont_return_date1 != ""){
			$empty_cont_return_date = date("Y-m-d H:i:s",strtotime($empty_cont_return_date1));
		} else {
			$empty_cont_return_date = "";
		}

		$remark = isset($_POST['remark']) ? $_POST['remark'] : "";


		$CreatedDate = date("Y-m-d H:i:s");

		

		// $entries = $conn->prepare("SELECT * from logis_entries_master where logis_entries_id ='{$logis_entries_id}' ");
		// $entries->execute();
		// $entriesRow = $entries->fetch();
		// print_r($entriesRow);

		// echo $chck_empty_cont_return_date = $entriesRow['empty_cont_return_date'];

		// if($chck_empty_cont_return_date != ""){

		// echo $status ="complete";

		if($empty_cont_return_date != ""){
		  $status = "complete";
		}
		else {
		  $status = "incomplete";
		}

		$entriesCols = array("logis_entries_id"=>$logis_entries_id,"ship_arrived_date"=>$ship_arrived_date,"mainfest_rec_date"=>$mainfest_rec_date,"entry_registered_date"=>$entry_registered_date,"cd_lodged_date"=>$cd_lodged_date,"local_coc_submit_date"=>$local_coc_submit_date,"duty_paid_date"=>$duty_paid_date,"entry_passed_date"=>$entry_passed_date,"original_bl_rec_date"=>$original_bl_rec_date,"do_lodged_date"=>$do_lodged_date,"do_released_date"=>$do_released_date,"cont_left_mombasa_date"=>$cont_left_mombasa_date,"cont_arrived_icd_date"=>$cont_arrived_icd_date,"doc_lodged_date"=>$doc_lodged_date,"verification_date"=>$verification_date,"ro_date"=>$ro_date,"port_charges_paid_date"=>$port_charges_paid_date,"cont_loaded_date"=>$cont_loaded_date,"doc_completed_date"=>$doc_completed_date,"free_day_exp_date"=>$free_day_exp_date,"empty_cont_return_date"=>$empty_cont_return_date,"remark"=>$remark,'CreatedDate'=>$CreatedDate,"status"=>$status);
		$entriesTable = "logis_entries_master";
		$condition = "logis_entries_id='".$logis_entries_id."'";
		$insertDriver = $connection->UpdateQuery($entriesTable,$entriesCols,$condition);

	// }
		
	}
	if($insertDriver == "success"){
			// $connection->redirect("../drivers.php");
			echo "success";
		}
		else {
			// $connection->redirect("../drivers.php");
			// $connection->redirect("../add_driver.php?ref=error");
			echo "fail";
		}

		
	}
?>