<?php
ob_start();
session_start();
require_once('../../config/db.php');
require_once('../../config/includes/initialise.php');
@$Email = $_SESSION['superadmin_email_add'];

if(isset($_POST['createBookingOrder'])){

    $automation->generate_job_card_no();

    // Booking Details
    $Booking_Id = $automation->job_card_no;

    $VehicleReg = isset($_POST['VehicleReg']) ? $_POST['VehicleReg'] : "";
    
    $Driver_Type = $_POST['Driver_Type'];

    if($Driver_Type == "Our_Driver") {

        $driver_id = $_POST['bookingOurDriver'];
        $driver_name = "";
        $driver_phone = "";

    } else if($Driver_Type == "Clients_Driver") {

        $driver_id = "";
        $driver_name = $_POST['bookingClientDriverName'];
        $driver_phone = $_POST['bookingClientDriverPhone'];

    }

    
    $Client_Type = $_POST['Client_Type'];

    if($Client_Type == "Existing") {

        $client_id = $_POST['bookingClientExisting'];
        $client_name = "";
        $client_phone = "";
        $client_email = "";

    } else if($Client_Type == "Walk_In") {

        $client_id = "";
        $client_name = $_POST['bookingClientWalkInName'];
        $client_phone = $_POST['bookingClientWalkInPhone'];
        $client_email = $_POST['bookingClientWalkInEmail'];

    }


    $rent_from_date = $_POST['rent_from_date'];
    $expected_return_date = $_POST['expected_return_date'];
    $returned_date = "0000-00-00";

    $availability = "0";
    $booking_additional_notes = $_POST['booking_additional_notes'];

    $RecordDateTime = $database->now_date_only." ".$database->now_time_only;

    $bookingCols = array("BookingId"=>$Booking_Id,"VehicleReg"=>$VehicleReg,"DriverType"=>$Driver_Type,"DriverId"=>$driver_id,"DriverName"=>$driver_name,"DriverPhone"=>$driver_phone,"ClientType"=>$Client_Type,"ClientId"=>$client_id,"ClientName"=>$client_name,"ClientPhone"=>$client_phone,"ClientEmail"=>$client_email,"AdditionalNotes"=>$booking_additional_notes,"RentFrom"=>$rent_from_date,"ExpectedReturnDate"=>$expected_return_date,"ReturnedDate"=>$returned_date,"Availability"=>$availability,"RecordDateTime"=>$RecordDateTime);
    $bookingTable = "booking_table";
    $insertBooking = $connection->InsertQuery($bookingTable,$bookingCols);


    if($insertBooking == "success"){
        $connection->redirect("../all_pending_bookings.php");
    }
    else {
        $connection->redirect("../add_booking.php");
    }
}
?>