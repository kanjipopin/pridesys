<?php
ob_start();
require_once('../config/db.php');

$imgFile="";


if(isset($_POST['submit'])){
    //Upload the image and documents to the uploads folder.
    $img_name = $_POST['vehicleImg1'];

    $uploaddir = '../../uploads/company'.$_POST["compId"];
    if(!file_exists($uploaddir)){
        mkdir($uploaddir,0755);
    }
    $imgfolder = $uploaddir."/img";
    if(!file_exists($imgfolder)){
        mkdir($imgfolder,0755);
    }

    if($_FILES['vehicleImg']['name'])
    {

        $type = substr($_FILES['vehicleImg']['name'],strrpos($_FILES['vehicleImg']['name'],'.')+1);

        $imgName = str_replace(" ","_",$_POST['VehicleReg'])."image.".$type;
        $uploadImg = $imgfolder."/".$imgName;

        if (move_uploaded_file($_FILES['vehicleImg']['tmp_name'], $uploadImg))
        {
            $imgFile = $imgfolder."/".$imgName;
            //echo "Img File is valid, and was successfully uploaded.\n";
        } else {
            //echo "Possible file upload attack!\n";
            $err='er-up-img';
            $connection->redirect("../edit-vehicle.php?err=".$err);
        }
    }
    else if($_FILES['vehicleImg']['name'] == "" && $img_name != ""){
        $imgFile = $img_name;
    }

    else if($_FILES['vehicleImg']['name'] == "" && $img_name == ""){
        $imgFile = "";
    }


    // Vehicle Details
    $VehicleReg = $_POST['VehicleReg'];
    $VehicleManufacturer = isset($_POST['VehicleManufacturer']) ? $_POST['VehicleManufacturer'] : "";
    $VehicleModel = isset($_POST['VehicleModel']) ? $_POST['VehicleModel'] : "";
    $VehicleBodyType = isset($_POST['VehicleBodyType']) ? $_POST['VehicleBodyType'] : "";
    $VehicleColor = isset($_POST['VehicleColor']) ? $_POST['VehicleColor'] : "";
    $VehicleYear = isset($_POST['VehicleYear']) ? $_POST['VehicleYear'] : "";
    $VehicleFuel = isset($_POST['VehicleFuel']) ? $_POST['VehicleFuel'] : "";
    $VehicleTransmission = isset($_POST['VehicleTransmission']) ? $_POST['VehicleTransmission'] : "";
    $VehicleFuelEconomy = isset($_POST['VehicleFuelEconomy']) ? $_POST['VehicleFuelEconomy'] : "";
    $VehicleMaxPassengers = isset($_POST['VehicleMaxPassengers']) ? $_POST['VehicleMaxPassengers'] : "";
    $VehicleEngineCapacity = isset($_POST['VehicleEngineCapacity']) ? $_POST['VehicleEngineCapacity'] : "";
    $VehicleDoors = isset($_POST['VehicleDoors']) ? $_POST['VehicleDoors'] : "";
    $VehicleMileageLimit = isset($_POST['VehicleMileageLimit']) ? $_POST['VehicleMileageLimit'] : "";
    $VehicleLeasePreparation = isset($_POST['VehicleLeasePreparation']) ? $_POST['VehicleLeasePreparation'] : "";

    @$implodeVehicleMoreFeatures = implode(",", $_POST['VehicleMoreFeatures']);
    @$VehicleMoreFeatures = isset($implodeVehicleMoreFeatures) ? $implodeVehicleMoreFeatures : "";

    $VehicleAddOns = isset($_POST['VehicleAddOns']) ? $_POST['VehicleAddOns'] : "";
    $VehicleInventoryLocation = isset($_POST['VehicleInventoryLocation']) ? $_POST['VehicleInventoryLocation'] : "";
    $VehicleDailyPrice = isset($_POST['VehicleDailyPrice']) ? $_POST['VehicleDailyPrice'] : "";

    //Insert data in Employee Details table
    $vehiclesCols = array("VehicleManufacturer"=>$VehicleManufacturer,"VehicleModel"=>$VehicleModel,"VehicleBodyType"=>$VehicleBodyType,"VehicleColor"=>$VehicleColor,"VehicleYear"=>$VehicleYear,"VehicleFuel"=>$VehicleFuel,"VehicleTransmission"=>$VehicleTransmission,"VehicleFuelEconomy"=>$VehicleFuelEconomy,"VehicleMaxPassengers"=>$VehicleMaxPassengers,"VehicleEngineCapacity"=>$VehicleEngineCapacity,"VehicleDoors"=>$VehicleDoors,"VehicleMileageLimit"=>$VehicleMileageLimit,"VehicleLeasePreparation"=>$VehicleLeasePreparation,"VehicleMoreFeatures"=>$VehicleMoreFeatures,"VehicleAddOns"=>$VehicleAddOns,"VehicleInventoryLocation"=>$VehicleInventoryLocation,"VehicleDailyPrice"=>$VehicleDailyPrice,"VehicleImg"=>$imgFile);
    $vehiclesTable = "pridedrive_vehicles";
    $condition = "VehicleReg='".$VehicleReg."'";
    $updateVehicle = $connection->UpdateQuery($vehiclesTable,$vehiclesCols,$condition);

    echo $updateVehicle;

    if($updateVehicle=="success"){
        $connection->redirect("../vehicles.php?ref=success");
    }
    else {
        $connection->redirect("../edit-vehicle.php?ref=error");
    }
}