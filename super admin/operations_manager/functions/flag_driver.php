<?php
  session_start();
  require_once('../config/db.php');
  include("../php-mailer/class.phpmailer.php");

  $FlagNSN = $_SESSION['FlagNSN'];
  $FlagcompId = $_SESSION['FlagcompId'];
  $FlagUsername = $_SESSION['FlagUsername'];
  $Flagdescription = $_SESSION['Flagdescription'];
  $createdDate = date("Y-m-d H:i:s");

  $FlagedCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where comp_id = '{$FlagcompId}'");
  $FlagedCompDetails->execute();
  $FlagedCompDetailsRow = $FlagedCompDetails->fetch();

  $checkDrvNSN = $conn->prepare("SELECT * from logis_drivers_master where drv_NSN = '{$FlagNSN}' and drv_curr_comp_id = '{$FlagcompId}'");
  $checkDrvNSN->execute();
  $checkDrvNSNCount = $checkDrvNSN->rowCount();
  $checkDrvNSNRow = $checkDrvNSN->fetch();

  if($checkDrvNSNCount > 0){

      $insertFlagDriver = $conn->prepare("INSERT into logis_flag_driver 
        (flag_drv_compid,flag_drv_name,flag_drv_nsn,flag_drv_comment,flag_drv_date,flag_drv_status) 
        values ('$FlagcompId','$FlagUsername','$FlagNSN','$Flagdescription','$createdDate','Active')");
      $insertFlagDriver->execute();

      $getCurrentCompDriver = $conn->prepare("SELECT * from logis_drivers_master where drv_NSN = '{$FlagNSN}' and drv_curr_comp_id != '{$FlagcompId}'");
      $getCurrentCompDriver->execute();
      while($getCurrentCompDriverRow = $getCurrentCompDriver->fetch()){

        $DrvcompanyDetails = $conn->prepare("SELECT * from logis_company_subadmin where comp_id = '{$getCurrentCompDriverRow['drv_curr_comp_id']}'");
        $DrvcompanyDetails->execute();
        $DrvcompanyDetailsRow = $DrvcompanyDetails->fetch();

        $flagNSN = $conn->prepare("SELECT * from logis_flag_driver where flag_drv_nsn = '{$getCurrentCompDriverRow['drv_NSN']}' order by flag_drv_id desc");
        $flagNSN->execute();
        $flagNSNCount = $flagNSN->rowCount();
        $flagNSNRow = $flagNSN->fetch();


        $smtpQuery = $connection->OneRow("*","smtp_settings");

        $userMsg = "<html><body>
        <img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
        $userMsg .= "<p>Dear ".$DrvcompanyDetailsRow['comp_name'].",</p>
        <p>Please note that a Driver in your fleet, ".$flagNSNRow['flag_drv_name']." was recently flagged by ".$FlagedCompDetailsRow['comp_name'].". We recommend that you read the comments below.</p>
        <p>Flag Details:</p>";
        $userMsg .= "<p>Driver Name : ".$flagNSNRow['flag_drv_name']."</p>";
        $userMsg .= "<p>Driver National ID : ".$flagNSNRow['flag_drv_nsn']."</p>";
        $userMsg .= "<p>Comments : ".$flagNSNRow['flag_drv_comment']."</p>";
        $userMsg .= "<p>For more information on the Driver please contact ".$FlagedCompDetailsRow['comp_name']." on ".$FlagedCompDetailsRow['contact_code']."-".$FlagedCompDetailsRow['contact_num']."</p>";
        $userMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Dereva Team</p></td></tr></table></td></tr></table></body></html>";


        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 0;
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = $smtpQuery['smtp_secure'];
        $mail->Host = $smtpQuery['smtp_host']; 
        $mail->Port = $smtpQuery['smtp_port']; 
        $mail->Username = $smtpQuery['smtp_username']; 
        $mail->Password = $smtpQuery['smtp_password']; 
        $mail->From = $smtpQuery['smtp_username'];
        $mail->FromName = "Dereva Enterprise";
        $mail->Subject = "Flagged Driver Alert";
        $mail->MsgHTML($userMsg);
        $mail->AddAddress($DrvcompanyDetailsRow['Email'], $DrvcompanyDetailsRow['comp_name']);
        $mail->send();
      }

      unset($_SESSION['FlagNSN']);
      unset($_SESSION['FlagcompId']);
      unset($_SESSION['FlagUsername']);
      unset($_SESSION['Flagdescription']);

      header("location: ../flag_driver.php?action=success");
    }
    else {
      header("location: ../flag_driver.php?action=no_driver");
    }
?>