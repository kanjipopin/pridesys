<?php
	session_start();
	require_once('../config/db.php');
	include("../../php-mailer/class.phpmailer.php");

	@$Email = $_SESSION['superadmin_email_add'];

	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";

	if($type == "checkpin"){

		$pin = $_REQUEST['pin'];

		$checkPin = $conn->prepare("SELECT * from logis_company_subadmin where comp_pin = '{$pin}'");
		$checkPin->execute();
		echo $checkPinCount = $checkPin->rowCount();
	}

	elseif($type == "checkEmail"){

		$email = $_REQUEST['email'];

		$ConfirmEmail = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$email}'");
		$ConfirmEmail->execute();
			echo $ConfirmEmailCount = $ConfirmEmail->rowCount();
	} elseif($type == "checkVehicleModel"){

		$vehicle_reg = $_REQUEST['vehicle_reg'];

		$vehicleReg = $conn->prepare("SELECT * from pridedrive_vehicles where VehicleId = '{$vehicle_reg}'");
		$vehicleReg->execute();
		$vehicleRegRow = $vehicleReg->fetch();

		$vehicleRegModelId = $vehicleRegRow['VehicleModel'];

		$vehicleRegModel = $conn->prepare("SELECT * from model_master where ID = '{$vehicleRegModelId}'");
		$vehicleRegModel->execute();
		$vehicleRegModelRow = $vehicleRegModel->fetch();

		echo $vehicleRegModelRow['Model'];
	}

	elseif($type == "checkDuplicateNSN"){

		$NSN = $_REQUEST['NSN'];
		$compId = $_REQUEST['compId'];

		$checkNSN = $conn->prepare("SELECT * from logis_drivers_master where drv_NSN = '{$NSN}'");
		$checkNSN->execute();
		$checkNSNCount = $checkNSN->rowCount();
		$checkNSNRow = $checkNSN->fetch();

		if($checkNSNCount > 0){
			if($checkNSNRow['drv_curr_comp_id'] == $compId){
				echo "same";
			}
			elseif($checkNSNRow['drv_curr_comp_id'] != $compId && $checkNSNRow['driver_work_status'] == 'working') {
				echo "different";
			}
		}
	}

    elseif($type == "checkDuplicateVehicleReg"){

        $VehicleReg= $_REQUEST['VehicleReg'];
        $compId = $_REQUEST['compId'];

        $checkVehicleReg = $conn->prepare("SELECT * from pridedrive_vehicles where VehicleReg = '{$VehicleReg}'");
        $checkVehicleReg->execute();
        $checkVehicleRegCount = $checkVehicleReg->rowCount();
        $checkVehicleRegRow = $checkVehicleReg->fetch();

        if($checkVehicleRegCount > 0){
            if($checkVehicleRegRow['CompanyId'] == $compId){
                echo "same";
            }
            elseif($checkVehicleRegRow['CompanyId'] != $compId) {
                echo "different";
            }
        }
    }

	elseif($type == "checkflagNSN"){

		$NSN = $_REQUEST['NSN'];
		$compId = $_REQUEST['compId'];

		$flagNSN = $conn->prepare("SELECT * from logis_flag_driver where flag_drv_nsn = '{$NSN}' order by flag_drv_id desc");
		$flagNSN->execute();
		echo $flagNSNCount = $flagNSN->rowCount();
		$flagNSNRow = $flagNSN->fetch();

		if($flagNSNCount > 0){

			$getCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where comp_id = '{$compId}'");
			$getCompDetails->execute();
			$getCompDetailsRow = $getCompDetails->fetch();

			$FlagedCompDetails = $conn->prepare("SELECT * from logis_company_subadmin where comp_id = '{$flagNSNRow['flag_drv_compid']}'");
			$FlagedCompDetails->execute();
			$FlagedCompDetailsRow = $FlagedCompDetails->fetch();

			$smtpQuery = $connection->OneRow("*","smtp_settings");

			$userMsg = "<html><body>
			<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
			$userMsg .= "<p>Dear ".$getCompDetailsRow['comp_name'].",</p>
			<p>Please note that ".$flagNSNRow['flag_drv_name']." was flagged by ".$FlagedCompDetailsRow['comp_name'].". We recommend that you read the comments below before you employee to this Driver.</p>
			<p>Flag Details:</p>";
			$userMsg .= "<p>Driver Name : ".$flagNSNRow['flag_drv_name']."</p>";
			$userMsg .= "<p>Driver National ID : ".$flagNSNRow['flag_drv_nsn']."</p>";
			$userMsg .= "<p>Comments : ".$flagNSNRow['flag_drv_comment']."</p>";
			$userMsg .= "<p>For more information please contact ".$FlagedCompDetailsRow['comp_name']." on ".$FlagedCompDetailsRow['contact_code'].$FlagedCompDetailsRow['contact_num']."</p>";
			$userMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Dereva Team</p></td></tr></table></td></tr></table></body></html>";


			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug  = 0;
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = $smtpQuery['smtp_secure'];
			$mail->Host = $smtpQuery['smtp_host']; 
			$mail->Port = $smtpQuery['smtp_port']; 
			$mail->Username = $smtpQuery['smtp_username']; 
			$mail->Password = $smtpQuery['smtp_password']; 
			$mail->From = $smtpQuery['smtp_username'];
			$mail->FromName = "Dereva Enterprise";
			$mail->Subject = "Flagged driver alert";
			$mail->MsgHTML($userMsg);
			$mail->AddAddress($getCompDetailsRow['Email'], $getCompDetailsRow['comp_name']);
			$mail->send();
		}
	}

	elseif($type == "checkFlagDriverNSN"){
		$NSN = isset($_REQUEST['NSN']) ? $_REQUEST['NSN'] : "";
		$compId = isset($_REQUEST['compId']) ? $_REQUEST['compId'] : "";

		$checkDrvNSN = $conn->prepare("SELECT * from logis_drivers_master where drv_NSN = '{$NSN}' and drv_curr_comp_id = '{$compId}'");
		$checkDrvNSN->execute();
		$checkDrvNSNCount = $checkDrvNSN->rowCount();
		$checkDrvNSNRow = $checkDrvNSN->fetch();

		if($checkDrvNSNCount > 0){

			// if($checkDrvNSNRow['driver_work_status'] == "notworking"){
			// 	echo "not working";
			// }
			// elseif($checkDrvNSNRow['driver_work_status'] == "working"){
				echo "success";
			// }
		}
		else {
			echo "no_driver";
		}
	}


	elseif($type == "delete"){
		$id = base64_decode($_REQUEST['id']);

		$deleteDriver = $conn->prepare("UPDATE logis_drivers_master set delete_flag='Yes' where drv_NSN = '{$id}'");
		$deleteDriver->execute();


			header("location: ../drivers.php");
	}

    elseif($type == "delete_vehicle"){
        $id = base64_decode($_REQUEST['id']);

        $deleteVehicle = $conn->prepare("DELETE FROM pridedrive_vehicles where VehicleReg = '{$id}'");
        $deleteVehicle->execute();

        header("location: ../vehicles.php");
    }


	elseif($type == "checkCode"){
		$email = $_POST['email'];

		$Codevalidation = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$email}'");
		$Codevalidation->execute();
		$CodevalidationCount = $Codevalidation->rowCount();
		$rnum = $Codevalidation->fetch();

		if($CodevalidationCount > 0){
			echo "1";

			$smtpQuery = $connection->OneRow("*","smtp_settings");
			$userMsg = "<html><body>
			<img src='https://enterprise.dereva.com/images/logo.png' class='img-responsive' style='width:145px;' alt='Dereva Portal' title='Dereva Portal'><br><br>";
			$userMsg .= "<p>Dear ".$rnum['fname'].",</p><p>Link to reset your password :<a href='https://enterprise.dereva.com/renew_pass.php?id=".$rnum['random_num']."'>Click here</a></p>";
			$userMsg .="</td></tr><tr> <td height='25' valign='middle'><p>The Enterprise Team</p></td></tr></table></td></tr></table></body></html>";

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug  = 0;
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = $smtpQuery['smtp_secure'];
			$mail->Host = $smtpQuery['smtp_host']; 
			$mail->Port = $smtpQuery['smtp_port']; 
			$mail->Username = $smtpQuery['smtp_username']; 
			$mail->Password = $smtpQuery['smtp_password']; 
			$mail->From = $smtpQuery['smtp_username'];
			$mail->FromName = "Dereva Enterprise";
			$mail->Subject = "Reset Password";
			$mail->MsgHTML($userMsg);
			$mail->AddAddress($rnum['Email'], $rnum['fname']);
			$mail->send();
		} else {
			echo "0";
		}
	}

	elseif($type == "deleteKin"){
		$KinId = isset($_REQUEST['KinId']) ? $_REQUEST['KinId'] : "";

		$deleteKinQuery = $conn->prepare("DELETE from logis_drivers_kin where drivers_kin_id = '{$KinId}'");
		$deleteKinQuery->execute();
	}

	elseif($type == "checkOldPassword"){
		$old_password = $_REQUEST['old_password'];

		$checkPass = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}' and comp_pwd= '{$old_password}'");
		$checkPass->execute();
		echo $checkPassCount = $checkPass->rowCount();
	}
?>