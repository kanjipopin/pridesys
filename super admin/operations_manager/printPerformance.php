<?php
  if(!isset($performace)) die();
  require_once ("config/db.php");

  $id = $performace['reportId'];
  $pid = $performace['performanceId'];

  $PerformanceDate = $conn->prepare("SELECT * from logis_report_performance where report_id = '$pid'");
  $PerformanceDate->execute();
  $PerformanceDateRow = $PerformanceDate->fetch();

  $ReportDate = $conn->prepare("SELECT * from logis_reports where report_id IN ($id)");
  $ReportDate->execute();

  $companyDetails = $conn->prepare("SELECT * from logis_company_subadmin where comp_id = '{$PerformanceDateRow['company_id']}'");
  $companyDetails->execute();
  $companyDetailsRow = $companyDetails->fetch();
?>

  <div class="form-group invoiceDetails" style="padding: 10px 30px;">
    <h3 style="text-align: center;margin-bottom: 30px;font-size: 20px;">Driver Performance Report</h3>

    <div class="billing" style="float: left;">
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;"><?php echo $companyDetailsRow['comp_name']; ?></p>
      <p style="font-size: 18px;margin-bottom: 5px;"><?php echo $companyDetailsRow['Email']; ?></p>
      <p style="font-size: 18px;margin-bottom: 5px;"><?php echo $companyDetailsRow['contact_num']; ?></p>
      <p style="font-size: 18px;margin-bottom: 5px;">Report Date : <?php echo date("M d, Y H:i:s"); ?></p>
    </div>

    <!-- <div class="issuer" style="float: right;margin-left: 430px">
      <h4 style="color: #575757;font-weight: 500;border-bottom: 1px solid #e0e0e0;padding: 13px 0;margin-bottom: 15px;text-transform: uppercase;font-size: 18px;">Issuer</h4>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">CLIFFORD TECHNOLOGIES LTD</p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">P051639270T</p>
      <p style="font-size: 18px;text-transform: lowercase;margin-bottom: 5px;">info@clifford.co.ke</p>
      <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">+254(0)753000888</p>
    </div> -->

    <div class="OrderDetails" style="margin-top: 250px;">
      <!-- <p style="font-size: 18px;text-transform: capitalize;margin-bottom: 5px;">Order Details :</p> -->
      <table border="1" cellspacing="0" cellpadding="0" style="border-color: #000;" width="99%">
      <thead>
        <tr>
            <th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;padding: 5px 0;color:#000;">Date</th>
            <th style="width:10%;font-size: 14px;text-align:center;font-weight: 700;border-color: #000;color:#000;">Vehicle</th>
            <th style="width:20%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Driver</th>
            <th style="width:10%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Report type</th>
            <th style="width:25%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Report details</th>
            <th style="width:25%; text-align:center;font-size: 14px;font-weight: 700;border-color: #000;color:#000;">Report status</th>
        </tr>
      </thead>

      <tbody>
      <?php 
        while($ReportDateRow = $ReportDate->fetch()){

          $getDriverName = $conn->prepare("SELECT * from logis_drivers_master where drv_id = '{$ReportDateRow['driver_name']}'");
          $getDriverName->execute();
          $getDriverNameRow = $getDriverName->fetch();
      ?>
        <tr>
          <td style="text-align:center;border-color: #000;color: #000;font-size: 13px;"><?php echo date("M d, Y", strtotime($ReportDateRow['report_date'])); ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding:15px 15px;font-size: 15px;"><?php echo $ReportDateRow['vehicle']; ?></td>
          <td style="text-align:center;border-color: #000;font-size: 13px;color: #000;"><?php echo $getDriverNameRow['drv_fname'].' '.$getDriverNameRow['drv_lname']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $ReportDateRow['report_type']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $ReportDateRow['report_details']; ?></td>
          <td style="text-align:center;border-color: #000;color: #000;padding: 5px 10px;"><?php echo $ReportDateRow['report_status']; ?></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
    <p style="font-weight: 500;font-size: 17px;">Rating is : <?php echo $PerformanceDateRow['rating']; ?></p>
    <p style="font-weight: 500;font-size: 17px;">Confirmed</p>
    <div>
  </div>