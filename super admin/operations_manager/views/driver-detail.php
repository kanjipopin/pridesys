<?php include'header.php'; ?>

  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <!-- Accordion styles -->
	<link rel="stylesheet" href="css/smk-accordion.css" />

	<div class="page-rightWidth">
      <div class="drivers">
          <div>
              <div class="driver-detail-heading">
					<h4>Driver Details</h4>

					<a href="drivers.php" type="button" class="btn btn-default pull-right"  style="margin-left:10px">< Back</a>

					<a href="edit-drivers.php?id=<?php echo base64_encode($getEmpData['empdt_NSN']); ?>" type="button" class="btn btn-default pull-right"  style="margin-left:10px">Edit Driver</a>

					<?php if($orderDetails != "false"){ ?>
						<a href="<?php echo 'functions/ajax.php?type=delete&id='.base64_encode($getEmpData['empdt_NSN']); ?>" type="button" class="btn btn-default pull-right">Delete Driver</a>
						<div class="clearfix"></div>

					<?php } else { ?>

						<a href="#" type="button" class="btn btn-default pull-right">Delete Driver</a>
						<div class="clearfix"></div>
					<?php } ?>
         	  </div>

			  <div class="verifying-driver-page driver-detail-page">
              	<div class="row">
              		<div class="col-sm-8 hidden-xs">
              			<div class="table-responsive">
              				<table class="table table-striped">
              					<tbody>
              						<tr>
              							<td>Name:</td>
              							<td align="right"><?php echo $getEmpData['drv_fname']." ".$getEmpData['drv_mname']." ".$getEmpData['drv_lname']; ?></td>
              						</tr>
              						<tr>
              							<td>National Indetification Number:</td>
              							<td align="right"><?php echo $getEmpData['drv_NSN']; ?></td>
              						</tr>
              						<tr>
              							<td>Contact:</td>
              							<td align="right"><?php echo $getEmpData['drv_contact_no']; ?></td>
              						</tr>
              						<tr>
              							<td>Vehicle Type:</td>
              							<td align="right"><?php echo $getEmpData['vehicleType']; ?></td>
              						</tr>

                                    <?php

                                        if($getEmpData['vehicleType'] === "PSV") {

                                    ?>
                                    <tr>
                                        <td>Vehicle Assignment:</td>
                                        <td align="right"><?php echo $getEmpData['vehiPlatNumber']; ?></td>
                                    </tr>

                                    <?php } ?>
              						<tr>
              							<td>Driving Licence Number:</td>
              							<td align="right"><?php echo $getEmpData['drv_driving_license_no']; ?></td>
              						</tr>
              						<tr>
              							<td>Branch:</td>
              							<td align="right"><?php echo $getEmpData['empdt_dept_id']; ?></td>
              						</tr>
              						<!--<tr>
              							<td>Driver Port Pass Number:</td>
              							<td align="right"><?php /*echo $getEmpData['driver_port_number']; */?>, Valid till : <?php /*echo $getEmpData['driver_port_expire_date']; */?></td>
              						</tr>
              						<tr>
              							<td>Vehicle Port Pass Number:</td>
              							<td align="right"><?php /*echo $getEmpData['vehicle_port_number']; */?>, Valid till : <?php /*echo $getEmpData['vehicle_port_expire_date']; */?></td>
              						</tr>-->
              						<!-- <tr>
              							<td>Date of Birth:</td>
              							<td align="right"><?php //echo date("Y-m-d", strtotime($getEmpData['drv_NSN'])); ?></td>
              						</tr> -->
              					<!-- 	<tr>
              							<td>Emergency Contact:</td>
              							<td align="right"> -->
              							<?php
              								// $getFirstKnContact = $conn->prepare("SELECT * from logis_drivers_kin where drv_nsnId = '{$getEmpData['drv_NSN']}' and drv_comp_id = '{$comp_id}' limit 1");
              								// $getFirstKnContact->execute();
              								// $getFirstKnContactRow = $getFirstKnContact->fetch();
              								// 	echo $getFirstKnContactRow['drivers_kin_emrg_contact'];
              							?>
           								<!-- </td> -->
              						<!-- </tr> -->
              						<tr>
              							<td>Email:</td>
              							<td align="right"><?php echo $getEmpData['drv_email']; ?></td>
              						</tr>
              					</tbody>
              				</table>
              			</div>
              		</div>
              		<div class="col-sm-4">
              			<img src="../<?php echo @$img = ltrim($getEmpData['drv_img'],"../"); ?>" class="img-responsive driver-img">
              			<p class="belowImgTxt"><span style="font-weight: 500;">Commenced Service:</span><span style="float: right;"><?php echo date("M Y", strtotime($getEmpData['empdt_contract_start'])); ?></span></p>

              			<p class="belowImgTxt"><span style="font-weight: 500;">Status:</span>
              			
              			<?php if($orderDetails != "false"){ ?>

	              			<?php if($getEmpData['driver_work_status'] == "working"){ ?>

	              				<a href="edit-drivers.php?id=<?php echo base64_encode($getEmpData['empdt_NSN']); ?>"><span style="float: right;color: #6dbe48;text-transform: capitalize;"><?php echo $getEmpData['driver_work_status']; ?></span></a>
	              			<?php } else { ?>
	              				<a href="edit-drivers.php?id=<?php echo base64_encode($getEmpData['empdt_NSN']); ?>"><span style="float: right;color: red;text-transform: capitalize;"><?php echo $getEmpData['driver_work_status']; ?></span></a>
	              			<?php } ?>
	              		
	              		<?php } else { ?>

	              			<?php if($getEmpData['driver_work_status'] == "working"){ ?>

	              				<a href="#"><span style="float: right;color: #6dbe48;text-transform: capitalize;"><?php echo $getEmpData['driver_work_status']; ?></span></a>

	              			<?php } else { ?>

	              				<a href="#"><span style="float: right;color: red;text-transform: capitalize;"><?php echo $getEmpData['driver_work_status']; ?></span></a>
	              			<?php } ?>

              			<?php } ?>

              			</p>
              		</div>
              		<div class="visible-xs">
	              		<div class="col-sm-8 driver-detail-resp">
	              			<h4><span class="span1">Name:</span><span class="span2"><?php echo $getEmpData['drv_fname']." ".$getEmpData['drv_mname']." ".$getEmpData['drv_lname']; ?></span></h4>
	              			<h4><span class="span1">ID:</span><span class="span2"><?php echo $getEmpData['drv_NSN']; ?></span></h4>
	              			<h4><span class="span1">Contact:</span><span class="span2"><?php echo $getEmpData['drv_contact_no']; ?></span></h4>
	              			<h4><span class="span1">Vehicle:</span><span class="span2"><?php echo $getEmpData['vehiPlatNumber']; ?></span></h4>
	              			<h4><span class="span1">Driving Licence Number:</span><span class="span2"><?php echo $getEmpData['drv_driving_license_no']; ?></span></h4>
	              			<h4><span class="span1">Driver Port Pass Number:</span><span class="span2"><?php echo $getEmpData['driver_port_number']; ?>, Valid till : <?php echo $getEmpData['driver_port_expire_date']; ?></span></h4>
	              			<h4><span class="span1">Vehicle Port Pass Number:</span><span class="span2"><?php echo $getEmpData['vehicle_port_number']; ?>, Valid till : <?php echo $getEmpData['vehicle_port_expire_date']; ?></span></h4>
	              			<h4><span class="span1">Branch:</span><span class="span2"><?php echo $getEmpData['empdt_dept_id']; ?></span></h4>
	              			<h4><span class="span1">Date of Birth:</span><span class="span2">31-12-1980</span></h4>
	              			<h4><span class="span1">Email:</span><span class="span2"><?php echo $getEmpData['drv_email']; ?></span></h4>
	              		</div>
              		</div>
              	</div>

              	<div class="add-docu-kin-block hidden-xs">
			         <div class="heading-tabs">
			           <ul class="nav nav-tabs ">
			              <li class="active"><a href="#tab_default_1" data-toggle="tab">Address</a></li>
			              <li><a href="#tab_default_2" data-toggle="tab">Drivers Documents</a></li>
			              <li><a href="#tab_default_3" data-toggle="tab">Next of Kin</a></li>
			           </ul>
			          </div>

			          <div class="driver-detail-tab">
			             <div class="tab-content">
			                <div class="tab-pane active" id="tab_default_1">
			                  <div class="table-responsive">
			                    <table class="table table-striped">
			                      <tbody>
			                        <tr>
			                          <td>Physical Address:</td>
			                          <td class="text-right"><?php echo $getEmpData['drv_address']; ?></td>
			                        </tr>
			                        <tr>
			                          <td></td>
			                          <td class="text-right"><?php echo $getEmpData['drv_address2']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>P.O.Box:</td>
			                          <td class="text-right"><?php echo $getEmpData['drv_pobox']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Postal Code:</td>
			                          <td class="text-right"><?php echo $getEmpData['drv_postal']; ?></td>
			                        </tr>
			                        <!-- <tr>
			                          <td>Province:</td>
			                          <td class="text-right"><?php //echo $getEmpData['driver_work_status']; ?></td>
			                        </tr> -->
			                        <tr>
			                          <td>City:</td>
			                          <td class="text-right"><?php echo $getEmpData['drv_city']; ?></td>
			                        </tr>
			                      </tbody>
			                    </table>
			                  </div>
			                </div>
			                <div class="tab-pane" id="tab_default_2">
			                  <div class="table-responsive">
			                    <table class="table table-striped">
			                      <thead>
			                        <tr>
			                          <th width="40%">Document</th>
			                          <th width="30%" class="text-center">Available</th>
			                          <th width="30%" class="text-center">Action</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                      <?php
			                      	$drvDocs = $conn->prepare("SELECT * from logis_emp_docs where edocs_nsn = '{$empdt_NSN}' and edocs_comp_id = '{$comp_id}'");
			                      	$drvDocs->execute();
			                      	$drvDocsRow = $drvDocs->fetch();
			                      		$id = base64_encode($drvDocsRow['edocs_nsn']);
			                      		if($drvDocsRow['edocs_id_card'] != ""){
			                      			@$NSN = "../".ltrim($drvDocsRow['edocs_id_card'],"../");
			                      ?>
			                        <tr>
			                          <td>National Indetification Number</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Available Icon"></td>
			                          <td class="text-center"><a href="<?php echo $NSN; ?>" target="_blank">View</a> | <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | <a href="<?php echo $NSN; ?>" download>Download</a> | <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=nsn&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                       	<?php } else { ?>
			                       	<tr>
			                          <td>National Indetification Number</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Available Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                       	<?php } ?>

			                       	<?php
			                       		if($drvDocsRow['edocs_dl_copy'] != ""){
			                      			@$dl = "../".ltrim($drvDocsRow['edocs_dl_copy'],"../");
			                       	?>
			                        <tr>
			                          <td>Driving License</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Available Icon"></td>
			                          <td class="text-center"><a href="<?php echo $dl; ?>" target="_blank">View</a> | <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | <a href="<?php echo $dl; ?>" download>Download</a> | <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=dl&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                       	<tr>
			                          <td>Driving License</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Available Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                       	<?php
			                       		if($drvDocsRow['edocs_app_letter'] != ""){
			                      			@$app = "../".ltrim($drvDocsRow['edocs_app_letter'],"../");
			                       	?>
			                        <tr>
			                          <td>Application Letter</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $app; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $app; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=app&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>Application Letter</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                        <?php
			                       		if($drvDocsRow['edocs_cv'] != ""){
			                      			@$cv = "../".ltrim($drvDocsRow['edocs_cv'],"../");
			                       	?>
			                        <tr>
			                          <td>CV</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $cv; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $cv; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=cv&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>CV</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                        <?php
			                       		if($drvDocsRow['edocs_qualification'] != ""){
			                      			@$qualification = "../".ltrim($drvDocsRow['edocs_qualification'],"../");
			                       	?>
			                        <tr>
			                          <td>Professional & Academic Qualification</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $qualification; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $qualification; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=qualification&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>Professional & Academic Qualification</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                        <?php
			                       		if($drvDocsRow['edocs_conduct_cert'] != ""){
			                      			@$conduct = "../".ltrim($drvDocsRow['edocs_conduct_cert'],"../");
			                       	?>
			                        <tr>
			                          <td>Certificate of good Conduct</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $conduct; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $conduct; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=conduct&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>Certificate of good Conduct</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                        <?php
			                       		if($drvDocsRow['edocs_pin_cert'] != ""){
			                      			@$pin = "../".ltrim($drvDocsRow['edocs_pin_cert'],"../");
			                       	?>
			                        <tr>
			                          <td>KRA PIN</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $pin; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $pin; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=pin&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>KRA PIN</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>

			                        <?php
			                       		if($drvDocsRow['edocs_nssf'] != ""){
			                      			@$nssf = "../".ltrim($drvDocsRow['edocs_nssf'],"../");
			                       	?>
			                        <tr>
			                          <td>NSSF</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $nssf; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $nssf; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=nssf&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>NSSF</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>


			                        <?php
			                       		if($drvDocsRow['edocs_nhif'] != ""){
			                      			@$nhif = "../".ltrim($drvDocsRow['edocs_nhif'],"../");
			                       	?>
									<tr>
			                          <td>NHIF</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $nhif; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $nhif; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=nhif&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>NHIF</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>


			                        <?php
			                       		if($getEmpData['contract_file'] != ""){
			                      			@$contract = "../".ltrim($getEmpData['contract_file'],"../");
			                       	?>
									<tr>
			                          <td>Contract File</td>
			                          <td class="text-center"><img src="images/doc_right.png" alt="Delete Icon"></td>
			                          <td class="text-center">
			                          <a href="<?php echo $contract; ?>" target="_blank"">View</a> | 
			                          <a href="<?php echo 'edit-drivers.php?id='.$id; ?>">Upload</a> | 
			                          <a href="<?php echo $contract; ?>">Download</a> | 
			                          <?php if($orderDetails != "false"){ ?><a href="<?php echo 'update-docs.php?docs=contract&id='.$id; ?>">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } else { ?>
			                        <tr>
			                          <td>Contract File</td>
			                          <td class="text-center"><img src="images/doc_delete.png" alt="Delete Icon"></td>
			                          <td class="text-center"><a href="#">View</a> | <a href="#">Upload</a> | <a href="#">Download</a> | <?php if($orderDetails != "false"){ ?><a href="#">Delete</a><?php } ?></td>
			                        </tr>
			                        <?php } ?>
			                      </tbody>
			                    </table>
			                  </div>
			                </div>
			                <div class="tab-pane" id="tab_default_3">
			                  <div class="table-responsive">
			                    <table class="table table-striped">
			                      <tbody>
			                      <?php
			                      	$getKin = $conn->prepare("SELECT * from logis_drivers_kin where drv_nsnId = '{$empdt_NSN}' and drv_comp_id = '{$comp_id}'");
			                      	$getKin->execute();
			                      	$i = 0;
			                      	while($getKinRow = $getKin->fetch()){
			                      		$i++;
			                      ?>
			                        <tr>
			                          <td colspan="2" style="font-size: 20px;">Next of Kin <?php echo $i; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Name:</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_name']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>National Indetification Number:</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_nsn']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Contact:</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_contact']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Email:</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_email']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Relation to Driver:</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_relation']; ?></td>
			                        </tr>
			                        <tr>
			                          <td>Address :</td>
			                          <td class="text-right"><?php echo $getKinRow['drivers_kin_address']; ?></td>
			                        </tr>
			                        <tr>
			                          <td colspan="2">&nbsp;</td>
			                        </tr>
			                      <?php } ?>
			                        
			                      </tbody>
			                    </table>
			                  </div>
			                </div>
			             </div>
			          </div>
			    </div>
            </div>

            <div class="visible-xs">
              	<div class="driver-detail-page-accordion">
			      	<!-- Accordion begin -->
						<div class="accordion_example">
						
							<!-- Section 1 -->
							<div class="accordion_in">
								<div class="acc_head">Driver Documents</div>
								<div class="acc_content">
								<?php
			                      	$drvDocs = $conn->prepare("SELECT * from logis_emp_docs where edocs_nsn = '{$empdt_NSN}' and edocs_comp_id = '{$comp_id}'");
			                      	$drvDocs->execute();
			                      	$drvDocsRow = $drvDocs->fetch();
			                      		$id = base64_encode($drvDocsRow['edocs_nsn']);

			                      		if($drvDocsRow['edocs_id_card'] != ""){
			                      			@$NSN = "../".ltrim($drvDocsRow['edocs_id_card'],"../");
			                      ?>
									<h4>
										<span class="span1">National Indetification Number</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $NSN; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $NSN; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=nsn&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">National Indetification Number</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_dl_copy'] != ""){
		                      			@$dl = "../".ltrim($drvDocsRow['edocs_dl_copy'],"../");
		                       	?>
									<h4>
										<span class="span1">Driving Lincence Number</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $dl; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $dl; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=dl&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">Driving Lincence Number</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_app_letter'] != ""){
		                      			@$appletter = "../".ltrim($drvDocsRow['edocs_app_letter'],"../");
		                       	?>
		                       		<h4>
										<span class="span1">Application Letter</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $appletter; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $appletter; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=app&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
		                       	<?php } else { ?>
			                       	<h4>
										<span class="span1">Application Letter</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_cv'] != ""){
		                      			@$cv = "../".ltrim($drvDocsRow['edocs_cv'],"../");
		                       	?>
									<h4>
										<span class="span1">CV</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $cv; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $cv; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=cv&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">CV</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_qualification'] != ""){
		                      			@$qualification = "../".ltrim($drvDocsRow['edocs_qualification'],"../");
		                       	?>
									<h4>
										<span class="span1">Professional & Academic Qualification</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $qualification; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $qualification; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=qualification&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">Professional & Academic Qualification</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_conduct_cert'] != ""){
		                      			@$certificate = "../".ltrim($drvDocsRow['edocs_conduct_cert'],"../");
		                       	?>
									<h4>
										<span class="span1">Certificate of good Conduct</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $certificate; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $certificate; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=conduct&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">Certificate of good Conduct</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>


			                    <?php
		                       		if($drvDocsRow['edocs_pin_cert'] != ""){
		                      			@$pin = "../".ltrim($drvDocsRow['edocs_pin_cert'],"../");
		                       	?>
									<h4>
										<span class="span1">KRA PIN</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $pin; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $pin; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=pin&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">KRA PIN</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_nssf'] != ""){
		                      			@$nssf = "../".ltrim($drvDocsRow['edocs_nssf'],"../");
		                       	?>
									<h4>
										<span class="span1">NSSF</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $nssf; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $nssf; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=nssf&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">NSSF</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

			                    <?php
		                       		if($drvDocsRow['edocs_nhif'] != ""){
		                      			@$nhif ="../".ltrim($drvDocsRow['edocs_nhif'],"../");
		                       	?>
									<h4>
										<span class="span1">NHIF</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $nhif; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $nhif; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=nhif&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">NHIF</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>


			                    <?php
		                       		if($getEmpData['contract_file'] != ""){
		                      			@$contract = "../".ltrim($getEmpData['contract_file'],"../");
		                       	?>
									<h4>
										<span class="span1">Contract</span>
										<span class="span2">
											<ul>
												<li><a href="<?php echo $contract; ?>" target="_blank" style="color: #000000;font-size: 17px;"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo $contract; ?>" download style="color: #6ebe47;font-size: 17px;"><i class="fa fa-download" aria-hidden="true"></i></a></li>
												<li><a href="<?php echo 'update-docs.php?docs=contract&id='.$id; ?>" style="color: #ed1e25;font-size: 17px;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
								<?php } else { ?>
			                       	<h4>
										<span class="span1">Contract</span>
										<span class="span2">
											<ul>
												<li>Upload File <a href="<?php echo 'edit-drivers.php?id='.$id; ?>" style="color: #2e3192;font-size: 17px;"> <i class="fa fa-upload" aria-hidden="true"></i></a></li>
											</ul>
										</span>
									</h4>
			                    <?php } ?>

								</div>
							</div>

							<!-- Section 2 -->
							<div class="accordion_in">
								<div class="acc_head">Address</div>
								<div class="acc_content">
									<h4>
										<span class="span1">Physical Address:</span>
										<span class="span2">
											<?php echo $getEmpData['drv_address']; ?>
										</span>
									</h4>
									<h4>
										<span class="span1"></span>
										<span class="span2">
											<?php echo $getEmpData['drv_address2']; ?>
										</span>
									</h4>
									<h4>
										<span class="span1">P.O.Box:</span>
										<span class="span2"><?php echo $getEmpData['drv_pobox']; ?></span>
									</h4>
									<h4>
										<span class="span1">Postal Code:</span>
										<span class="span2"><?php echo $getEmpData['drv_postal']; ?></span>
									</h4>
									<h4>
										<span class="span1">City:</span>
										<span class="span2"><?php echo $getEmpData['drv_city']; ?></span>
									</h4>
								</div>
							</div>

							<!-- Section 3 -->
							<div class="accordion_in">
								<div class="acc_head">Next Of Kin</div>
								<?php
			                      	$getKin = $conn->prepare("SELECT * from logis_drivers_kin where drv_nsnId = '{$empdt_NSN}' and drv_comp_id = '{$comp_id}'");
			                      	$getKin->execute();
			                      	$i = 0;
			                      	while($getKinRow = $getKin->fetch()){
			                      		$i++;
			                    ?>
								<div class="acc_content">
									<h4>
										<span class="span1">Name:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_name']; ?></span>
									</h4>
									<h4>
										<span class="span1">National Indetification Number:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_nsn']; ?></span>
									</h4>
									<h4>
										<span class="span1">Contact:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_contact']; ?></span>
									</h4>
									<h4>
										<span class="span1">Email:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_email']; ?></span>
									</h4>
									<h4>
										<span class="span1">Relation to Driver:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_relation']; ?></span>
									</h4>
									<h4>
										<span class="span1">Address:</span>
										<span class="span2"><?php echo $getKinRow['drivers_kin_address']; ?></span>
									</h4>
								</div>
								<?php } ?>
							</div>

						</div>
						<!-- Accordion end -->
			      </div>
			      </div>
            </div>
          </div>

         </div>

         <div class="row visible-xs" style="margin: 0;">
	        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
	          <div class="sidebar-nav navbar-collapse">
	            <ul class="nav" id="side-menu">
	              <li>
	                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
	              </li>
	              <li>
	                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
	              </li>
	              <li>
	                <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
	              </li>
	            </ul>
	          </div>
	        <!-- /.sidebar-collapse -->
	        </div>
	      </div>
      </div>
  </div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="js/rate/jquery.rateyo.min.css"/>
<script type="text/javascript" src="js/rate/jquery.min.js"></script>
<script type="text/javascript" src="js/rate/jquery.rateyo.js"></script>

<script src="js/jquery.popupoverlay.js"></script>
<script src="../js/datepicker.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script>
	function getleaves(){
		$.ajax({
			type: "POST",
      		url:"functions/ajaxaddrate.php",
      		data : $("#form_leav").serialize(),
      		success: function(msg){
		     	// $("#addLeaves").modal();
		     	// $("#fetch_leave").html(msg);
		     	window.location.reload();
		    },
		});
	}

// function getadvanced(){
// 		var l_name = $("#drv_name").val();
// 		var l_nsn = $("#drv_nsn").val();

// 		$.ajax({
// 			type: "POST",
//       		url:"functions/ajaxaddrate.php",
//       		data : $("#form_leav").serialize()+"&name="+l_name+"&nsn="+l_nsn,
//       		success: function(msg){
// 		     	// alert(msg);
// 		     	alert("Added Successfully");
// 		     	$("#fetch_leave").html(msg);
// 		     	$('#addLeaves').modal('hide');
// 		    },
// 		});
// 	}


	$(document).ready(function() {

		$("#redflagDiv").hide();
		$('.notworking').click(function() {
		 	$('#redflagDiv').show();
		});
		$('.working').click(function() {
		 	$('#redflagDiv').hide();
		});


	    $('#fade').popup({
	      transition: 'all 0.3s',
	      scrolllock: true
	    });


		$("#renewC").hide();
		$("#Cyes").click(function(){
		 	$("#renewC").show("slow");
		});
		$("#Cno").click(function(){
		 	$("#renewC").hide("slow");
		});


		$('#startDate').datepicker({
	          format: "yyyy/mm/dd"
	    }).on('changeDate', function(ev){
	       	// alert("working");
	         var date1 = $('#startDate').datepicker('getDate');
	         var date2 = date1.setDate(date1.getDate()+364);
	        $('#endDate').datepicker('setDate',date1);
	    });

	    $('#endDate').datepicker({
	        format: "yyyy/mm/dd",
	    });

	  	var f_leaves = $('#from_leaves').datepicker({
					        format: "yyyy/mm/dd",
					    });
		var t_leaves = $('#to_leaves').datepicker({
					        format: "yyyy/mm/dd",
					    });

		$("#to_leaves").change(function (){
			var fl = $("#from_leaves").datepicker('getDate');
			var tl = $("#to_leaves").datepicker('getDate');
			
			var t = Math.floor((tl - fl) / (1000 * 60 * 60 * 24));
			var tt = parseInt(t) + 1;
			
			$("#total_leaves").val(tt);
		});
	});
		
  	function getRenew(){
    	$.ajax({
      		type: "POST",
      		url:"functions/ajaxaddrate.php",
      		data: $('#addrenew').serialize()+"type=sc",
      
      		success: function(msg){
	    		// alert("Contract Renew Successfully.");
	    		window.location.reload();
      		},
    	});
  	}

    // $(function(){
    //     var rating = $("#rt").val();
    //     if(rating == ""){
    //   	  rating = 0;        	
    //     }

    //     console.log("On rating Rate"+rating);

    //     $(".counter").text(rating);

    //     $("#rateYo1").on("rateyo.init", function () { console.log("rateyo.init"); });

    //     $("#rateYo1").rateYo({
    //       rating: rating,
    //       numStars: 5,
    //       precision: 2,
    //       starWidth: "64px",
    //       spacing: "5px",
	   //    rtl: true,
    //       multiColor: {
    //         startColor: "#000000",
    //         endColor  : "#ffffff"
    //       },
    //       onInit: function () {
    //         console.log("On Init");
    //       },
    //       onSet: function () {
    //         console.log("On Set");
    //       }
    //     }).on("rateyo.set", function () { console.log("rateyo.set"); })
    //       .on("rateyo.change", function () { console.log("rateyo.change"); });

    //     $(".rateyo").rateYo();

    //     $(".rateyo-readonly-widg").rateYo({
    //       rating: rating,
    //       numStars: 5,
    //       precision: 2,
    //       minValue: 1,
    //       maxValue: 5
    //     }).on("rateyo.change", function (e, data) {
    //       $("#rt").val(data.rating);
    //       console.log(data.rating);
    //     });
    //   });


	function UpdateWorkStatus(){

	    $.ajax({
	      type: "POST",
	      url: "functions/ajaxaddrate.php",
	      data: $('#workingForm').serialize(),

	      success: function(msg){
	      	$(".workUpdateMsg").text("Work Status updated Successfully.");
	    	window.location.reload();
	     	},
		});
	}

	// function removeRedfalg(){
	// 	var removeRed = $("#redflagNo").val();
	// 	var drv_nsn = $("#drv_nsn").val();
	// 	$.ajax({
	// 		type: "POST",
	//      	url: "functions/ajaxaddrate.php",
	//      	data: {"type":"removeRedFlag","removeRed":removeRed,"drv_nsn":drv_nsn},

	//      	success: function(msg){
	//     		window.location.reload();
	//      	},
	// 	})
	// }
</script>
<script type="text/javascript" src="js/smk-accordion.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$(".accordion_example").smk_Accordion({
				closeAble: true, //boolean
			});			
	});
</script>

<?php include'footer.php'; ?>