<?php
@$Email = $_SESSION['superadmin_email_add'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$comp->execute();
$comprow = $comp->fetch();

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}


include 'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Vehicle Inventory</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="vehicles.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_vehicle.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Vehicle Details</h4>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Registration Number: </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="VehicleReg" id="VehicleReg" placeholder="Enter the vehicle registration number *"  onchange="checkVehicleReg();">
                <p class="CheckVehicleRegMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Manufacturer :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleManufacturer" id="VehicleManufacturer" placeholder="Vehicle manufacturer" >-->

                  <select class="form-control" id="VehicleManufacturer" name="VehicleManufacturer">
                    <option value="" selected disabled>Select vehicle manufacturer</option>
                    <?php
                    $value = $conn->prepare("SELECT * from manufacturer_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Manufacturer']; ?></option>
                    <?php } ?>
                  </select>

                <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comprow['comp_id']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Model :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleModel" id="VehicleModel" placeholder="Vehicle model" >-->

                  <select class="form-control" id="VehicleModel" name="VehicleModel">
                    <option value="" selected disabled>Select vehicle model</option>
                    <?php
                    $value = $conn->prepare("SELECT * from model_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Model']; ?></option>
                    <?php } ?>
                  </select>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Body type :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleBodyType" id="VehicleBodyType" placeholder="Vehicle body type" >-->

                  <select class="form-control" id="VehicleBodyType" name="VehicleBodyType">
                    <option value="" selected disabled>Select vehicle body type</option>
                    <?php
                    $value = $conn->prepare("SELECT * from body_type_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['BodyType']; ?></option>
                    <?php } ?>
                  </select>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Color :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleColor" id="VehicleColor" placeholder="Vehicle color" >-->

                  <select class="form-control" id="VehicleColor" name="VehicleColor">
                    <option value="" selected disabled>Select vehicle color</option>
                    <?php
                    $value = $conn->prepare("SELECT * from color_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Color']; ?></option>
                    <?php } ?>
                  </select>
              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Year :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleYear" id="VehicleYear" placeholder="Vehicle year" >-->

                  <select class="form-control" id="VehicleYear" name="VehicleYear">
                    <option value="" selected disabled>Select vehicle year</option>
                    <?php
                    $value = $conn->prepare("SELECT * from vehicle_year_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['VehicleYear']; ?></option>
                    <?php } ?>
                  </select>
              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Fuel :</label>
              <div class="col-sm-6">

                  <select class="form-control" id="VehicleFuel" name="VehicleFuel">
                    <option value="" selected disabled>Select vehicle fuel</option>
                    <?php
                    $value = $conn->prepare("SELECT * from fuel_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Fuel']; ?></option>
                    <?php } ?>
                  </select>

                <!--<input type="radio" name="VehicleFuel" id="VehicleFuelPetrol" value="Petrol" style="height:auto"> Petrol
                <input type="radio" name="VehicleFuel" id="VehicleFuelDiesel" value = "Diesel" style="height:auto"> Diesel-->
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Transmission :</label>
              <div class="col-sm-6">

                  <select class="form-control" id="VehicleTransmission" name="VehicleTransmission">
                    <option value="" selected disabled>Select vehicle transmission</option>
                    <?php
                    $value = $conn->prepare("SELECT * from transmission_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Transmission']; ?></option>
                    <?php } ?>
                  </select>
                <!--<input type="radio" name="VehicleTransmission" id="VehicleTransmissionAuto" value="Automatic" style="height:auto"> Automatic
                <input type="radio" name="VehicleTransmission" id="VehicleTransmissionManual" value = "Manual" style="height:auto"> Manual-->
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Fuel economy :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleFuelEconomy" id="VehicleFuelEconomy" placeholder="Vehicle fuel economy" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Max passengers :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleMaxPassengers" id="VehicleMaxPassengers" placeholder="Vehicle maximum passengers" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Engine capacity :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleEngineCapacity" id="VehicleEngineCapacity" placeholder="Vehicle engine capacity" >-->

                  <select class="form-control" id="VehicleEngineCapacity" name="VehicleEngineCapacity">
                    <option value="" selected disabled>Select vehicle engine capacity</option>
                    <?php
                    $value = $conn->prepare("SELECT * from engine_capacity_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['EngineCapacity']; ?></option>
                    <?php } ?>
                  </select>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Doors :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleDoors" id="VehicleDoors" placeholder="Vehicle doors" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Mileage Limit :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleMileageLimit" id="VehicleMileageLimit" placeholder="Vehicle mileage limit" >-->

                  <select class="form-control" id="VehicleMileageLimit" name="VehicleMileageLimit">
                    <option value="" selected disabled>Select vehicle mileage limit</option>
                    <?php
                    $value = $conn->prepare("SELECT * from mileage_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['Mileage']; ?></option>
                    <?php } ?>
                  </select>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Preparation time before two leases :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleLeasePreparation" id="VehicleLeasePreparation" placeholder="Vehicle lease prep time" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">More features(Separate features using commas) :</label>
              <div class="col-sm-6">

                  <select id="VehicleMoreFeatures" name="VehicleMoreFeatures[]" multiple="multiple" class="form-control chosen-select">
                      <?php
                      $vehicle = $conn->prepare("SELECT * from vehicle_more_features_master ");
                      $vehicle->execute();
                      while($vehicleRow = $vehicle->fetch()){
                          ?>
                          <option value="<?php echo $vehicleRow['ID']; ?>"><?php echo $vehicleRow['MoreFeatures']; ?></option>
                      <?php } ?>
                  </select>

                <!--<textarea class="form-control" cols="45" rows="5" name="VehicleMoreFeatures" placeholder="Separate features using commas"></textarea>-->
              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Available add ons at checkout :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleAddOns" id="VehicleAddOns" placeholder="Vehicle at checkout add ons" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Inventory Location :</label>
              <div class="col-sm-6">
                <!--<input type="text" class="form-control" name="VehicleInventoryLocation" id="VehicleInventoryLocation" placeholder="Vehicle inventory location" >-->

                  <select class="form-control" id="VehicleInventoryLocation" name="VehicleInventoryLocation">
                    <option value="" selected disabled>Select vehicle inventory location</option>
                    <?php
                    $value = $conn->prepare("SELECT * from vehicle_inventory_location_master");
                    $value->execute();
                    while($valueRow = $value->fetch()){
                        ?>
                        <option value = "<?php echo $valueRow['ID']; ?>"><?php echo $valueRow['InventoryLocation']; ?></option>
                    <?php } ?>
                  </select>

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Price per day :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="VehicleDailyPrice" id="VehicleDailyPrice" placeholder="Vehicle price per day" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
              <div class="col-sm-5">
                <input type="file" id="vehicleImg" name="vehicleImg">
                <small>upload only png and jpg image.</small>
              </div>
            </div>
          </span>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Submit</button>
                        </div>
                    </div>

                </form>

                <!-- CLOSE ADD BRANCH - POPUP -->
            </div>
            <!-- <div class="msg">
                <p>sorry these data already exists.kindky update from drivers page.</p>
            </div> -->
        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
            //   $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#inspectionExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#commenced_startDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#driver_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
            //   $('#vehicle_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function checkVehicleReg(){
            var VehicleReg = $("#VehicleReg").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkDuplicateVehicleReg","VehicleReg":VehicleReg,"compId":compId },

                success : function(msg){

                    if(msg == "same"){
                        $(".CheckVehicleRegMsg").text("Vehicle already exists in your account.");
                        $("#submit").attr("disabled",true);
                    } else if(msg == "different") {
                        $(".CheckVehicleRegMsg").text("Vehicle already exists on another account.");
                        $("#submit").attr("disabled",true);
                    } else {
                        $(".CheckVehicleRegMsg").text("");
                        $("#submit").attr("disabled",false);
                    }
                }
            });
        }

        function getDept(){
            $(".load_branch").hide();
            $.ajax({
                type: "POST",
                url:"functions/ajaxaddmaster.php",
                data: $('#addDeptt').serialize(),
                success: function(msg){
                    // alert(msg);
                    $(".old_branch").hide();
                    $(".load_branch").show();
                    $(".load_branch").html(msg);
                    $('#addbranch').modal('hide');
                },
            });
        }



        $(document).ready(function() {
            $('#nssf').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nssf1').val(txtVal);
            });


            $('input[type=radio][name=Vehicle_Type]').change(function() {

                if(this.value === "Other") {

                    document.getElementById("toggle_Plat_Number").style.display = "none";

                } else {

                    document.getElementById("toggle_Plat_Number").style.display = "block";

                }

            });

            $('#nhifNo').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nhifNo1').val(txtVal);
            });
        });


        function getSecondKin(){
            $(".kinTwo").show();
            $("#secondKinBtn").hide();
        }

        function getThirdKin(){
            $(".kinThree").show();
            $("#ThirdKinBtn").hide();
            $("#secondKinBtnDel").hide();
        }

        function getSecondKinHide(){
            $(".kinTwo").hide();
            $("#secondKinBtn").show();
            $("#secondKinBtnDel").show();
        }

        function getThirdKinHide(){
            $(".kinThree").hide();
            $("#ThirdKinBtn").show();
            $("#secondKinBtnDel").show();
        }
    </script>

    <!-- <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script>
    $( "#form" ).validate({
      rules: {
        email: {
          email: true
        },
       }
    });
    </script> -->


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#lstFruits').multiselect({
        //         includeSelectAllOption: true
        //     });
        // });
    </script>

<?php include'footer.php'; ?>