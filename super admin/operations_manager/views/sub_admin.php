<?php include'header.php'; ?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>My sub admins (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_sub_admin.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="sub_admin.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Position</th>
                <th>Department</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($results as $fd) {
                //@$img = ltrim($fd['VehicleImg'], "../");
                $id = base64_encode($fd['Id']);

                  $Name = $fd['Name'];
                  $Email = $fd['Email'];
                  $position_encoded = $fd['Position'];
                  $department_encoded = $fd['Department'];

                  if($position_encoded == "head_of_sales_and_marketing") {
                    $Position = "Head of sales and marketing";
                  }

                  if($department_encoded == "sales_and_marketing") {
                    $Department = "Sales and marketing";
                  }

                  /* $vehicle_master = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$job_card_vehicle_reg' ");
                  $vehicle_master->execute();
                  $getVehicleData = $vehicle_master->fetch(PDO::FETCH_ASSOC); */

            ?>
              <tr>
                <td><?php echo $Name; ?></td>
                <td><?php echo $Email; ?></td>
                <td><?php echo $Position; ?></td>
                <td><?php echo $Department; ?></td>
                <td>
                  <a class="viewBtn" href="<?php echo 'sub-admin-detail.php?id='.$id; ?>" style="display:inline-block;color: #0f4da1;font-weight: 400;">View
                  </a>
                  <a class="editBtn" href="<?php echo 'edit-sub-admin.php?id='.$id; ?>" style="display:inline-block;margin-left: 10px;color: #6dbe48;font-weight: 400;border-left: 1px solid #000;padding-left: 10px;">Edit
                  </a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
              //@$img = ltrim($fd1['VehicleImg'], "../");
              $id = base64_encode($fd['Id']);

                  $Name = $fd1['Name'];
                  $Email = $fd1['Email'];
                  $position_encoded = $fd1['Position'];
                  $department_encoded = $fd1['Department'];

                  if($position_encoded == "head_of_sales_and_marketing") {
                    $Position = "Head of sales and marketing";
                  }

                  if($department_encoded == "sales_and_marketing") {
                    $Department = "Sales and marketing";
                  }
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <?php echo $Name; ?>
            </div>
            <div class="block-2">
              <h5><?php echo $Email; ?></h5>
              <h5><?php echo $Position; ?></h5>
              <h5><?php echo $Department; ?></h5>
              <a href="<?php echo 'edit-sub-admin.php?id='.$id; ?>" class="view-profile">View</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="vehicles.php"><img src="images/drivers-icon1.svg">My Vehicles(<?php echo $rowCount; ?>)</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>
    $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
    });

    $("#filterForm").on('click',function(e){
      var data="";
      e.preventDefault();
      var drvName = $("#drvName").val();
      var drvLicenseNum = $("#dln").val();
      var contactNo = $("#contact").val();
      //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

      $.ajax({
        type : "POST",
        url  : "functions/drivers.php",
        data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
        success : function(msg){
           // var msg = msg;
           //alert("this message:"+msg);
           $("#showDriverData").html("");
           $("#showDriverData").html(msg);
        }
      });


    });

  //   function myFilter(value) {
  //   // Declare variables 
  //   var input, filter, table, tr, td, i;
  //   input = value;
  //   filter = input.toUpperCase();
  //   table = document.getElementById("driverList");
  //   tr = table.getElementsByTagName("tr");

  //   // Loop through all table rows, and hide those who don't match the search query
  //   for (i = 0; i < tr.length; i++) {
  //     td = tr[i].getElementsByTagName("td")[0];
  //     if (td) {
  //       if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
  //         tr[i].style.display = "";
  //       } else {
  //         tr[i].style.display = "none";
  //       }
  //     } 
  //   }
  // }

  $(".dltDriver").click(function (event){

    var deletedvr = confirm("Are you sure want to delete these job card?");

    if(deletedvr == true){
      var dataid = $(event.currentTarget).attr('data-id');
      window.location = dataid;
    }
  });

  //   function dltDriver(event){
  //     var deletedvr = confirm("Are you sure want to delete these Driver?");
     
  //     if(deletedvr == true){
  //     var id = $(event.currentTarget).attr("id");
  //     // alert(id);

  //     $.ajax({
  //       type : "POST",
  //       url  : "edit-drivers.php",
  //       data : {"type":"deletedriver","id":id},
  //       success : function(msg){
  //         // alert(msg);
  //         window.location.reload();
  //       }
  //     })
  //   }
  // }
  </script>

<?php include'footer.php'; ?>