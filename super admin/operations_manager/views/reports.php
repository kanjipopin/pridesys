<?php include'header.php'; ?>

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="css/datepicker.css">
  <style type="text/css">
    table.dataTable.no-footer {
      border-bottom: 1px solid #ffe2b0;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current { background: #ffe2b0;border: 1px solid #ffe2b0; }
  </style>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <div class="filters">
          <div class="hidden-xs">
            <form id="filterBox" name="filterBox" class="form-inline" method="GET">
              <div class="form-inline">
                <select name="selectDriver" id="selectDriver" class="form-control">
                  <option value="" disabled="" selected>Select Driver</option>
                  <option value="">All Driver</option>

                  <?php
                    $driverData = $conn->prepare("SELECT * from logis_drivers_master where driver_work_status = 'working' and drv_curr_comp_id = '{$comprow['comp_id']}'");
                    $driverData->execute();
                    while($driverDataRow = $driverData->fetch()){
                  ?>
                    <option value="<?php echo $driverDataRow['drv_fname']." ".$driverDataRow['drv_lname']; ?>"><?php echo $driverDataRow['drv_fname']." ".$driverDataRow['drv_lname']; ?></option>                  
                  <?php } ?>
                </select> 

                <input type="hidden" id="type" name="type" value="filter">
                <input type="text" class="form-control" id="from_date" name="from_date" value="<?php echo @$_GET['from_date']; ?>" placeholder="From Date" required>
                <input type="text" class="form-control" id="to_date" name="to_date" value="<?php echo @$_GET['to_date']; ?>" placeholder="To Date" required>

                <input type="submit" class="form-control" value="Search">

                <a href="add_reports.php">
                  <input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add Report" style="border-radius: 0px;">
                </a>

                <br>
                <?php if(@$_GET['type'] == "filter"){ ?>
                  <a href="reports.php?type=clear_search"><small style="color: red;">clear filter</small></a>
                <?php } ?>
              </div>
            </form>
          </div>
        </div>
      </div>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped table-hover" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Date</th>
                <!-- <th>Update Date</th> -->
                <th>Driver</th>
                <th>Vehicle</th>
                <th>Report type</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($getReportsRow as $getReportsRow1) {

                $getDriverName = $conn->prepare("SELECT * from logis_drivers_master where drv_id = '{$getReportsRow1['driver_name']}'");
                $getDriverName->execute();
                $getDriverNameRow = $getDriverName->fetch();

                $id = base64_encode($getReportsRow1['report_id']);
            ?>
              <tr>
                <td><?php echo date("M d, Y", strtotime($getReportsRow1['report_date'])); ?></td>
                <!-- <td><?php //echo date("M d, Y H:i:s", strtotime($getReportsRow1['report_update'])); ?></td> -->
                <td class="drv"><?php echo $getDriverNameRow['drv_fname'].' '.$getDriverNameRow['drv_lname']; ?></td>
                <td><?php echo $getReportsRow1['vehicle']; ?></td>
                <td><?php echo $getReportsRow1['report_type']; ?></td>
                <td>
                  <a class="viewBtn" href="<?php echo 'view_reports.php?id='.$id; ?>" style="display:inline-block;color: #0f4da1;font-weight: 400;">View Report
                  </a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($getReportsRow as $getReportsRow2){

            $getDriverName1 = $conn->prepare("SELECT * from logis_drivers_master where drv_id = '{$getReportsRow2['driver_name']}'");
            $getDriverName1->execute();
            $getDriverNameRow1 = $getDriverName1->fetch();

            $id = base64_encode($getReportsRow2['report_id']);
        ?>
          <div class="resp-my-driver-block">
            <!-- <div class="block-1"> -->
              <!-- <img src="<?php //echo $img; ?>" class="img-responsive"> -->
            <!-- </div> -->
            <div class="block-2">
              <h5>Name : <?php echo $getDriverNameRow1['drv_fname'].' '.$getDriverNameRow1['drv_lname']; ?></h5>
              <h5>Date : <?php echo date("M d, Y H:i:s", strtotime($getReportsRow2['report_date'])); ?></h5>
              <h5>Update Date : <?php echo date("M d, Y H:i:s", strtotime($getReportsRow2['report_update'])); ?></h5>
              <h5>Vehicel : <?php echo $getReportsRow2['vehicle']; ?></h5>
              <h5>Report Type : <?php echo $getReportsRow2['report_type']; ?></h5>

              <a href="<?php echo 'view_reports.php?id='.$id; ?>" class="view-profile">View Report</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
              </li>
              <li>
                <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script src="../js/datepicker.js"></script>
  <script>
    // $('tr[data-href]').on("click", function() {
    //   document.location = $(this).data('href');
    // });

    // $("#filterForm").on('click',function(e){
    //   var data="";
    //   e.preventDefault();
    //   var drvName = $("#drvName").val();
    //   var drvLicenseNum = $("#dln").val();
    //   var contactNo = $("#contact").val();
    //   //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

    //   $.ajax({
    //     type : "POST",
    //     url  : "functions/drivers.php",
    //     data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
    //     success : function(msg){
    //        // var msg = msg;
    //        //alert("this message:"+msg);
    //        $("#showDriverData").html("");
    //        $("#showDriverData").html(msg);
    //     }
    //   });
    // });
  </script>

  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $('#driverList').DataTable({
      "iDisplayLength": 30,
      "bLengthChange": false,
      "info":     false,
      "bFilter": false,
      "order": [[ 0, "desc" ]],
      "columnDefs": [
        { targets: [1, 2, 3, 4], orderable: false}
      ]
    });

    $(document).ready(function() {
      // SEARCH BY COMPANY
      $('#selectDriver').off('change');
      $('#selectDriver').on('change', function() {
          // Your search term, the value of the input
          var searchTerm = $('#selectDriver').val();
          // table rows, array
          var tr = [];

          // Loop through all TD elements
          $('#driverList').find('.drv').each(function() {
              var value = $(this).html();
              // if value contains searchterm, add these rows to the array
              if (value.includes(searchTerm)) {
                  tr.push($(this).closest('tr'));
              }
          });

          // If search is empty, show all rows
          if ( searchTerm == '') {
              $('tr').show();
          } else {
              // Else, hide all rows except those added to the array
              $('tr').not('thead tr').hide();
              tr.forEach(function(el) {
                  el.show();
              });
          }
      });


      $("#from_date").datepicker();
      $("#to_date").datepicker();
    });
  </script>

<?php include'footer.php'; ?>