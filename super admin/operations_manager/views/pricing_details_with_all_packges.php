<?php
  session_start();
  require_once('../config/db.php');

  @$action = $_GET['action'];

  include 'pricing_header.php';

  if($_SERVER['HTTPS'] != "on"){ 
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    header('Location:'. $redirect);
  }

  @$Email = $_SESSION['superadmin_email_add'];
  if($Email == ""){
    $connection->redirect('../index.php');
  }

  $checkFirstPack = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id desc limit 1");
  $checkFirstPack->execute();
  $checkFirstPackCount = $checkFirstPack->rowCount();
  $checkFirstPackRow = $checkFirstPack->fetch();

  if($checkFirstPackCount == 0){
    $display = "";
    $active = "";
  } else {
    $display = "display:none";
    $active = "active";
  }
?>

<!-- Accordion styles -->
<link rel="stylesheet" href="css/smk-accordion.css" />
<style type="text/css">
/* Tabs panel */
.tabbable-panel {
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #f00;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}

/* Below tabs mode */
.tabbable-line.tabs-below > .nav-tabs > li {
  border-top: 4px solid transparent;
}
.tabbable-line.tabs-below > .nav-tabs > li > a {
  margin-top: 0;
}
.tabbable-line.tabs-below > .nav-tabs > li:hover {
  border-bottom: 0;
  border-top: 4px solid #fbcdcf;
}
.tabbable-line.tabs-below > .nav-tabs > li.active {
  margin-bottom: -2px;
  border-bottom: 0;
  border-top: 4px solid #f3565d;
}
.tabbable-line.tabs-below > .tab-content {
  margin-top: -10px;
  border-top: 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 15px;
}
</style>

<div class="page-rightWidth">
  <div class="col-sm-12">
    <div>
      <div class="heading" style="margin-bottom: 0px;">
        <h4>Select your plan</h4>
      </div>

      <?php
        if($action == "expire"){
      ?>
        <p style="font-size: 15px;color: red;font-weight: 500;">Your subscription has expired. Please select a plan to renew your subscription.</p>
      <?php } ?>

      <div class="tabbable-panel hidden-xs">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
              1 - 99 Drivers </a>
            </li>

            <li>
              <a href="#tab_default_2" data-toggle="tab">
              100 - 999 Drivers </a>
            </li>

            <li style="<?php echo $display; ?>">
              <a href="#tab_default_3" data-toggle="tab">
              Free trial for 30 days</a>
            </li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1" style="padding-left: 15px;">
              <section id="plans">
                <div>
                  <div class="row">
                    <!-- item -->
                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing" style="transform: scale(1.0,0.94);">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">Per Month<br/><strong>KES 5,000</strong></p>
                          <small style="margin-bottom: 10px;">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 99 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                        <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                          <p style="margin-top: 20px;font-size: 13px;">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com">enterprise.support@dereva.com</a> or call 0753000888</p>
                        <?php } else { ?>
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p50l30adn">BUY NOW!</a>
                        <?php } ?>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing main-panel">
                        <div class="panel-heading">
                          <h3>Best Value</h3>
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">For 12 Months<br/><strong>KES 50,000</strong></p>
                          <small style="margin-bottom: 10px;color: #fff">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 99 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                        <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                          <p style="margin-top: 20px;font-size: 13px;color: #fff">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com" style="color: #fff"><u>enterprise.support@dereva.com</u></a> or call 0753000888</p>
                        <?php } else { ?>
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p500l360adn">BUY NOW!</a>
                        <?php } ?>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">For 6 Months<br/><strong>KES 25,000</strong></p>
                          <small style="margin-bottom: 10px;">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 99 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                        <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                          <p style="margin-top: 20px;font-size: 13px;">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com">enterprise.support@dereva.com</a> or call 0753000888</p>
                        <?php } else { ?>
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p250l180adn">BUY NOW!</a>
                        <?php } ?>
                        </div>
                      </div>
                    </div>
                    <!-- /item -->
                  </div>
                </div>
              </section>
            </div>

            <div class="tab-pane" id="tab_default_2" style="padding-left: 15px;">
              <section id="plans">
                <div>
                  <div class="row">
                    <!-- item -->
                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing" style="transform: scale(1.0,0.94);">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">Per Month<br/><strong>KES 10,000</strong></p>
                          <small style="margin-bottom: 10px;">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 999 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p100l30adn">BUY NOW!</a>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing main-panel">
                        <div class="panel-heading">
                          <h3>Best Value</h3>
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">For 12 Months<br/><strong>KES 100,000</strong></p>
                          <small style="margin-bottom: 10px;color: #fff;">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 999 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p1000l360adn">BUY NOW!</a>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 text-center">
                      <div class="panel panel-danger panel-pricing">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body text-center">
                          <p style="margin-bottom: 0px;">For 6 Months<br/><strong>KES 50,000</strong></p>
                          <small style="margin-bottom: 10px;">+ vat</small>
                        </div>
                        <ul class="list-group text-center">
                          <li class="list-group-item">Manage and Update Essential Driver Documents on the Cloud</li>
                          <li class="list-group-item">Manage Driver Contracts</li>
                          <li class="list-group-item">Flag Drivers</li>
                          <li class="list-group-item">Manage up to 999 active Drivers in your fleet</li>
                          <li class="list-group-item">Auto driving license renewal reminders</li>
                        </ul>
                        <div class="text-center">
                          <a class="btn btn-lg btn-block btn-danger" href="payment.php?plan=dernetvpas_p500l180adn">BUY NOW!</a>
                        </div>
                      </div>
                    </div>
                    <!-- /item -->
                  </div>
                </div>
              </section>
            </div>

            <div class="tab-pane" id="tab_default_3" style="<?php echo $display; ?>;padding-left: 15px;">
              <p class="freeTrailMsg">30 days free trial.</p>
              
              <div class="enterpriseFeatures">
                <p>Features :</p>
                <ul>
                  <li>Manage and Update Essential Driver Documents on the Cloud</li>
                  <li>Manage Driver Contracts</li>
                  <li>Flag Drivers</li>
                  <li>Manage up to 99 active Drivers in your fleet</li>
                  <li>Auto license renewal reminders</li>
                </ul>

                <div class="col-md-3" style="border-top: none;padding: 10px 15px;">
                  <a class="btn btn-lg btn-block btn-danger" href="trail_package.php" style="margin-left: 10px;">START NOW!</a>
                </div>

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
        </div>
      </div>


      <div class="visible-xs">
        <div class="pricing-table-content-resp">
          <h3 style="color: #ed282c;">For 1-99 Drivers</h3>

          
          <!-- Accordion begin -->
            <div class="accordion_example">
            
              <!-- Section 1 -->
              <div class="accordion_in">
                <div class="acc_head">KES 5,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> per month</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 99 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>

                  <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                    <p style="margin-top: 15px;font-size: 12px;">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com">enterprise.support@dereva.com</a> or call 0753000888</p>
                  <?php } else { ?>
                    <a href="payment.php?plan=dernetvpas_p50l30adn" class="btn btn-default">Buy Now</a>
                  <?php } ?>
                </div>
              </div>

              <!-- Section 2 -->
              <div class="accordion_in">
                <div class="acc_head">KES 25,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> for 6 months</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 99 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  
                  <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                    <p style="margin-top: 15px;font-size: 12px;">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com">enterprise.support@dereva.com</a> or call 0753000888</p>
                  <?php } else { ?>
                    <a href="payment.php?plan=dernetvpas_p250l180adn" class="btn btn-default">Buy Now</a>
                  <?php } ?>
                </div>
              </div>

              <!-- Section 3 -->
              <div class="accordion_in">
                <div class="acc_head">KES 50,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> for 12 months</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 99 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  <?php if($checkFirstPackRow['driver_limit'] == '999'){ ?>
                    <p style="margin-top: 15px;font-size: 12px;">To Downgrade your account please contact <a href="mailto:enterprise.support@dereva.com">enterprise.support@dereva.com</a> or call 0753000888</p>
                  <?php } else { ?>
                    <a href="payment.php?plan=dernetvpas_p500l360adn" class="btn btn-default">Buy Now</a>
                  <?php } ?>
                </div>
              </div>

            </div>
            <!-- Accordion end -->
            <h3 style="color: #ed282c;">For 100-999 Drivers</h3>
            <!-- Accordion begin -->
            <div class="accordion_example">
              <!-- Section 1 -->
              <div class="accordion_in">
                <div class="acc_head">KES 10,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> per month</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 999 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  <a href="payment.php?plan=dernetvpas_p100l30adn" class="btn btn-default">Buy Now</a>
                </div>
              </div>

              <!-- Section 2 -->
              <div class="accordion_in">
                <div class="acc_head">KES 50,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> for 6 months</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 999 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  <a href="payment.php?plan=dernetvpas_p500l180adn" class="btn btn-default">Buy Now</a>
                </div>
              </div>

              <!-- Section 3 -->
              <div class="accordion_in">
                <div class="acc_head">KES 100,000 <small style="display: inline-block;padding-left: 0px;padding-right: 8px;border: none;">+ vat</small> for 12 months</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 999 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  <a href="payment.php?plan=dernetvpas_p1000l360adn" class="btn btn-default">Buy NOW!</a>
                </div>
              </div>
            </div>
            <!-- Accordion end -->

            <h3 style="color: #ed282c; <?php echo $display; ?>">Free Trial</h3>
            <!-- Accordion begin -->
            <div class="accordion_example" style="<?php echo $display; ?>">
            
              <!-- Section 1 -->
              <div class="accordion_in">
                <div class="acc_head">30 days Free Trial</div>
                <div class="acc_content">
                  <ul>
                    <li>Manage and Update Essential Driver Documents on the Cloud</li>
                    <li>Manage Driver Contracts</li>
                    <li>Flag Drivers</li>
                    <li>Manage up to 99 active Drivers in your fleet</li>
                    <li>Auto driving license renewal reminders</li>
                  </ul>
                  <a href="trail_package.php" class="btn btn-default">START Now</a>
                </div>
              </div>
            </div>
            <!-- Accordion end -->

            <small>Click here to indicate that you have read and agree to the <a href="#" style="color: #ed2024;">terms of service</a> of the Dereva Terms of service listed on the website</small>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script type="text/javascript" src="js/smk-accordion.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($){
    $(".accordion_example").smk_Accordion({
        closeAble: true, //boolean
      });
  });
</script>

<?php include'footer.php'; ?>