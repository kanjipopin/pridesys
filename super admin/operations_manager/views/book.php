<?php

include'header.php'; 

@$Email = $_SESSION['superadmin_email_add'];


$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
$error_msg = isset($_GET['error_msg']) ? $_GET['error_msg'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}

$getCompID = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$getCompID->execute();
$getCompIDRow = $getCompID->fetch();

$sale_rep_id = $getCompIDRow['Id'];

$sales_rep_name = $getCompIDRow['Name'];

$is_clicked = $_POST['is_clicked'];

  if($is_clicked === "Yes") {
      
      $final_start_date = $_POST['booking_day_start'];
      $final_end_date = $_POST['booking_day_end'];
      
      $days_rented = "1";
      
  } else {
      
      $final_start_date = $_POST['booking_day_start'];
      $booking_day_end = $_POST['booking_day_end'];
      
      $end_date = new DateTime($booking_day_end);
      $end_date->modify("-1 day");
      $final_end_date = $end_date->format("Y-m-d");
      
      $earlier = new DateTime($final_start_date);
      $later = new DateTime($booking_day_end);
      
      $days_rented = $later->diff($earlier)->format("%a"); 
      
  }

  $service_id = $_POST['service_id'];
  $user_id = $_POST['user_id'];


?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($error_msg)){ echo $error_msg; } ?></p>

            <div class="addDriver-form">
                
            <form action="functions/book.php" method="post" >

                <div id="display"></div>
                    
                    <div class="form-group">

                        <select class="form-control" id="VehicleReg" name="VehicleReg" >
                                <option value="" selected disabled>Select vehicle reg</option>
                                <?php

                                $value = $conn->prepare("SELECT * from pridedrive_vehicles");
                                $value->execute();
                                $chkMaster = "";
                                while($masterRow = $value->fetch()){

                                    ?>
                                    <option value = "<?php echo $masterRow['VehicleId']; ?>" > <?php echo $masterRow['VehicleReg']; ?></option>
                                <?php } ?>
                        </select>
                    
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Driver type :</label>
                    <div class="col-sm-9">
                        <input type="radio" name="Driver_Type" id="Our_Driver" value="Our_Driver" style="height:auto" checked> Our driver
                        <input type="radio" name="Driver_Type" id="Clients_Driver" value="Clients_Driver" style="height:auto"> Clients driver
                    </div>
                </div>

                <br/>

                <div class="form-group" id="toggle_client_driver" style="display:none;">
              
                    <label for="inputEmail3" class="col-sm-3 control-label">Driver's name: </label>
                    <div class="col-sm-9">
                        <input type="text" name="bookingClientDriverName" placeholder="Driver's name" class="form-control">
                    </div>

                    <label for="inputEmail3" class="col-sm-3 control-label">Driver's phone number: </label>
                    <div class="col-sm-9">
                        <input type="text" name="bookingClientDriverPhone" placeholder="Driver's phone number" class="form-control">
                    </div>

                    <hr/>

                </div>

                <div class="form-group" id="toggle_our_driver">


                    <label for="inputEmail3" class="col-sm-3 control-label">Driver's name: </label>
                    <div class="col-sm-9">

                        <select class="form-control" id="bookingOurDriver" name="bookingOurDriver" >
                                <option value="" selected disabled>Select driver</option>
                                <?php

                                $value = $conn->prepare("SELECT * from logis_drivers_master ");
                                $value->execute();
                                $chkMaster = "";
                                while($masterRow = $value->fetch()){

                                    ?>
                                    <option value = "<?php echo $masterRow['drv_NSN']; ?>" > <?php echo $masterRow['drv_fname']." ".$masterRow['drv_mname']." ".$masterRow['drv_lname']; ?></option>
                                <?php } ?>
                        </select>
                        
                    </div>

                    
                </div>

                <br/>

                

                <!-- client type -->

                <div class="form-group">
                <hr>

                    <label for="inputEmail3" class="col-sm-3 control-label">Client type :</label>
                    <div class="col-sm-9">
                        <input type="radio" name="Client_Type" id="Walk_In_Client" value="Walk_In" style="height:auto"> Walk in
                        <input type="radio" name="Client_Type" id="Existing_Client" value="Existing" style="height:auto" checked> Existing
                    </div>
                </div>

                <br/>

                <div class="form-group" id="toggle_client_walk_in" style="display:none;">

                    
                    <label for="inputEmail3" class="col-sm-3 control-label">Client's name: </label>
                    <div class="col-sm-9">
                        <input type="text" name="bookingClientWalkInName" placeholder="Client's name" class="form-control">
                    </div>

                    <label for="inputEmail3" class="col-sm-3 control-label">Client's phone number: </label>
                    <div class="col-sm-9">
                        <input type="text" name="bookingClientWalkInPhone" placeholder="Client's phone number" class="form-control">
                    </div>

                    <label for="inputEmail3" class="col-sm-3 control-label">Client's email: </label>
                    <div class="col-sm-9">
                        <input type="text" name="bookingClientWalkInEmail" placeholder="Client's email" class="form-control">
                    </div>

                    <hr/>

                </div>

                <div class="form-group" id="toggle_client_existing">


                    <label for="inputEmail3" class="col-sm-3 control-label">Client name: </label>
                    <div class="col-sm-9">

                        <select class="form-control" id="bookingClientExisting" name="bookingClientExisting" >
                                <option value="" selected disabled>Select client</option>
                                <?php

                                $value = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE Approval='1' ");
                                $value->execute();
                                $chkMaster = "";
                                while($masterRow = $value->fetch()){

                                    ?>
                                    <option value = "<?php echo $masterRow['Id']; ?>" > <?php echo $masterRow['ClientsName']; ?></option>
                                <?php } ?>
                        </select>
                        
                    </div>

                    
                </div>

                <br/>

                <hr>

                  <div class="form-group">

                        <label for="Rent_Date" class="col-sm-3 control-label">Rent from</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="Rent_Date" placeholder="Starting date" name="rent_from_date" value="<?php echo $final_start_date; ?>" required>
                        </div>
                    </div>

                  <div class="form-group">
                        <label for="expected_return_date" class="col-sm-3 control-label">Expected return date</label>
                        <div class="col-sm-9">

                            <input type="date" class="form-control" id="expected_return_date" placeholder="Ending date" name="expected_return_date" value="<?php echo $final_end_date; ?>" required>
                  
                        </div>
                    </div>

                

                <hr/>

                <div class="form-group">
                    <label for="booking_additional_notes" class="col-sm-3 control-label">Additional notes</label>
                    <div class="col-sm-9">
                        <textarea  class="form-control" id="booking_additional_notes" name="booking_additional_notes" placeholder="Additional notes" ></textarea>
                    </div>
                </div>

                <div class="form-group">

                    <button type="submit" class="btn btn-primary" id="createBookingOrder" name="createBookingOrder">Book vehicle</button>

                </div>

            </form>
                
            </div> 

            

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="sales_rep.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Sales rep</a>
                    </li>

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('input[type=radio][name=Client_Type]').change(function() {

                if(this.value === "Walk_In") {

                    document.getElementById("toggle_client_walk_in").style.display = "block";
                    document.getElementById("toggle_client_existing").style.display = "none";

                } else {

                    document.getElementById("toggle_client_walk_in").style.display = "none";
                    document.getElementById("toggle_client_existing").style.display = "block";

                }

            });

            $('input[type=radio][name=Driver_Type]').change(function() {

                if(this.value === "Clients_Driver") {

                    document.getElementById("toggle_client_driver").style.display = "block";
                    document.getElementById("toggle_our_driver").style.display = "none";

                } else {

                    document.getElementById("toggle_client_driver").style.display = "none";
                    document.getElementById("toggle_our_driver").style.display = "block";

                }

            });


        });
    </script>

<?php include'footer.php'; ?>