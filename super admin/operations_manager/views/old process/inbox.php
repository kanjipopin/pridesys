<?php 

include'header.php';
$comp_id = $_SESSION['comp_id'];

?>

      <div class="col-sm-9 mrgnTOP15">
          <div class="wht-bg inbox-page">
              <div class="heading">
                <h4>Inbox</h4>
              </div>
              
              <div class="row mrgnRIGHT0">
                  <div class="col-sm-4 padRIGHT0">
                    <div class="left-side">
                      <div class="all-message">
                          <h4>All Messages</h4>
                      </div>
                      <div class="icon-addon addon-lg">
                          <input type="text" placeholder="Search" class="form-control" id="searchc" name="searchc">
                          <label for="text" class="glyphicon glyphicon-search" rel="tooltip" title="Search"></label>
                      </div>
                      <ul class="email-list">
                        <?php
                          foreach($compRow as $com){
                        ?>
                          <a class="old_comp" onclick="getComp(event),getMSG(<?php echo $com['comp_id']; ?>);" id="selectcomp" data-id="<?php echo $com['comp_id']; ?>">
                            <li><?php echo $com['comp_org_name']; ?></li>
                          </a>

                          
                        <?php } ?>
                          <div class="filter_comp"></div>
                      </ul>
                  </div>
                  </div>
                  <div class="col-sm-8 padLEFT0">
                      <div class="right-side">
                          <div class="mail-heading">
                              <h4 class="chat_comp"></h4>
                              <input type="hidden" name="diff_comp" id="diff_comp">
                              <!-- <p class="date">Date : 15 January 2017</p> -->
                          </div>

                          <div class="content msg_block">

                            <div class="show_new_received_msg">
                            </div>
                            <div class="msg-box">
                              <div class = "received_msg msg">
                                <p class="show_msg_received"></p>
                                <small class="show_date_received date-n-time"></small>
                              </div>
                            </div>

                            <div class="show_new_send_msg">
                            </div>
                            <div class="msg-box">
                              <div class = "send_msg msg">
                                <p class="show_msg_send"></p>
                                <small class="show_date_send date-n-time"></small>
                              </div>
                            </div>

                          </div>

                          <div class="reply-outer-box">
                              <div class="reply-box">
                                  <div class="typing"></div>
                                  <input type = "hidden" name="session_comp" id="session_comp" value="<?php echo $comp_id; ?>">
                                  <textarea class="form-control" rows="1" placeholder="Click here to type" name="chatmsg" id="chatmsg"></textarea>
                                  <a class="btn btn-default" href="#" role="button" name="send_msg" id="send_msg" onclick="getchatMsg();">Send</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script>
///////////////////////
//  FILTER COMPANY  //
//////////////////////

// $(document).ready(function (){
//   var diffcomps = $("#diff_comps").val();
//   alert(diffcomps);
// });

$("#searchc").keyup(function (){
  var searchtxt = $("#searchc").val();

  $.ajax({
    type : "POST",
    url : "functions/chat/searchcomp.php",
    data : {"type":"filtercomp","searchtxt":searchtxt},

    success : function(msg){
      $(".old_comp").hide();
      $(".filter_comp").html(msg);
    }
  });
});


//////////////////////
//  GET COMPANY ID //
/////////////////////
var getcomp = "";
function getComp(event){
  getcomp = $(event.currentTarget).attr("data-id");
  // $("#selectcomp").addClass("active");

  

  $.ajax({
    type : "POST",
    url : "functions/chat/searchcomp.php",
    data : {"getcomp":getcomp,"type":"getcompID"},
  
    success : function(msg){
      var getname = msg.substring(0,msg.indexOf(","));
      var getID = msg.substring(msg.indexOf(",")+1,msg.length);
        $(".chat_comp").text(getname);
        $("#diff_comp").val(getID);
      }
  });
}

/////////////////////////
//  AUTO REFRESH DIV  //
////////////////////////
setInterval("getMSG(getcomp);",2000);



/////////////////////
//  SEND MESSAGE  //
////////////////////
$(".show_new_received_msg").hide();
$(".show_new_send_msg").hide();
$(".msg-box").hide();

function getchatMsg(){
  var chat = $("#chatmsg").val();
  var diffcomp = $("#diff_comp").val();
  var SessionComp = $("#session_comp").val();

  $.ajax({
      type : "POST",
      url : "functions/chat/searchcomp.php",
      data : {"type":"chatMSG","chat":chat,"diffcomp":diffcomp},

      success : function(msg){
        $("#chatmsg").val('');
        $(".msg-box").hide();
  
        getMSG(diffcomp);
      }
  }); 
}

/////////////////////////////////
//  GET MSG ON COMPANY CLICK  //
////////////////////////////////
function getMSG(id){
  // var getmsgID = $(event.currentTarget).attr("data-id");
var getmsgID = id;

  $.ajax({
      type : "POST",
      url : "functions/chat/searchcomp.php",
      data : {"type":"showMSG","getmsgID":getmsgID},

      success : function(msg){
        $(".msg-box").hide();
        $(".show_new_received_msg").show();
        $(".show_new_received_msg").html(msg);
      }
  });
}
</script>

<?php include'footer.php';?>

<!-- link 
  http://stackoverflow.com/questions/220767/auto-refreshing-div-with-jquery-settimeout-or-another-method
-->