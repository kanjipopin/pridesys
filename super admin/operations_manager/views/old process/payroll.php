<?php include'header.php';?>

      <div class="col-sm-9 mrgnTOP15">
          <div class="drivers-detail payroll-page">
              <div class="heading">
                <div class="block-width">
                  <h4>Drivers Payroll</h4>
                  <div class="filters">
                        <div class="form-inline">
                          <!-- <button type="button" class="btn btn-default">Add</button> -->
                          <a href="#fade" type="button" class="fade_open btn btn-default">Add</a>
                        </div>
                  </div>
                </div>

              </div>

              <div class="table-responsive detail-table hover-css">
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>Month</th>
                          <th>Total Drivers</th>
                          <th>Total Amounts (KSh)</th>
                      </tr>
                  </thead>
                  <tbody>
                      <!-- <tr data-href="payroll-drivers.php">
                          <td>January 2017</td>
                          <td>400</td>
                          <td>500000</td>
                      </tr>
                      <tr data-href="payroll-drivers.php">
                          <td>December 2016</td>
                          <td>320</td>
                          <td>450000</td>
                      </tr>
                      <tr data-href="payroll-drivers.php">
                          <td>November 2016</td>
                          <td>350</td>
                          <td>470000</td>
                      </tr>
                      <tr data-href="payroll-drivers.php">
                          <td>October 2016</td>
                          <td>400</td>
                          <td>500000</td>
                      </tr> -->
                  </tbody>
              </table>
              </div>
          </div>
      </div>
      </div>
  </div>
  <!-- Popup Content -->

  <div id="fade" class="well popup-content addPAYROLL-popup">
    <div class="popup-head">
      <h4>Add Payroll for Month</h4>
    </div>
    <div class="popup-body">
      <form class="generate-mnth text-center">
        <div class="form-group">
          <input  type="text" class="datepicker" placeholder="Select Month"  id="example1">
        </div>
        <button type="submit" class="btn btn-default">Generate</button>
      </form>
    </div>
    <button class="fade_close btn btn-default popup-btn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
  </div>
  <!-- End Popup Content -->
  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="js/jquery.popupoverlay.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});
</script>
<script>
$(document).ready(function () {

    $('#fade').popup({
      transition: 'all 0.3s',
      scrolllock: true,
    });

});
</script>
<script src="../js/datepicker.js"></script>
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function () {
      
      $('#example1').datepicker({
          format: "MM-yyyy",
          viewMode:"months",
          minViewMode:"months",
      });
  });
</script>
<?php include'footer.php';?>