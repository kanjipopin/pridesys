<?php
@$Email = $_SESSION['operations_manager'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$comp->execute();
$comprow = $comp->fetch();

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}


include'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Vehicle year master</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="vehicle_year_master.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_vehicle_year_master.php" method="POST" id="form" name="form">

                    <span class="show_driver_details">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Vehicle year: </label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="VehicleYear" id="VehicleYear" placeholder="Enter the vehicle year *"  onchange="checkVehicleMsg();">
                            <p class="CheckVehicleMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
                          </div>
                        </div>


                      </span>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Add</button>
                        </div>
                    </div>

                </form>

                <!-- CLOSE ADD BRANCH - POPUP -->
            </div>

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>


    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

        });

        function checkVehicleMsg(){
            var VehicleReg = $("#VehicleReg").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkDuplicateVehicleReg","VehicleReg":VehicleReg,"compId":compId },

                success : function(msg){

                    if(msg == "same"){
                        $(".CheckVehicleRegMsg").text("Vehicle already exists in your account.");
                        $("#submit").attr("disabled",true);
                    } else if(msg == "different") {
                        $(".CheckVehicleRegMsg").text("Vehicle already exists on another account.");
                        $("#submit").attr("disabled",true);
                    } else {
                        $(".CheckVehicleRegMsg").text("");
                        $("#submit").attr("disabled",false);
                    }
                }
            });
        }


    </script>




    <script src="../js/checkbox_select.js"></script>

<?php include'footer.php'; ?>