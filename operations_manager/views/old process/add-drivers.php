<?php 
  include'header.php';
?>

  <div class="col-sm-12">
    <div class="wht-bg">
      <div class="heading">
        <h4>Driver</h4>

        <div class="filters">
          <div class="form-inline">
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
          </div>
        </div>
      </div>

    <div class="addDriver-form">
    	<div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="functions/add-driver.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Personal Details</h4>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter a First Name *" required>

                <!--<input type="hidden" class="form-control" name="compId" id="compId" value="<?php //echo $_SESSION['comp_id'];?>">-->
                <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id; ?>">
              </div>
               <div class="col-sm-3">
                <input type="text" class="form-control" name="mname" id="mname" placeholder="Enter a Middle Name *" required>
              </div>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter a Last Name *" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Gender :</label>
              <div class="col-sm-3">
                <input type="radio" name="gender" id="gender" value="male" style="height:auto"> Male
                <input type="radio" name="gender" id="gender" value = "female" style="height:auto"> Female
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">National ID Number: </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="NSN" id="NSN" placeholder="Enter a National ID Number *" required>
                <input type="hidden" class="form-control" name="updateQuery" id="updateQuery" value="no">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Driver Vehicle: </label>
              <div class="col-sm-5">
                  <select id="lstFruits" name="vehiclelist[]" multiple="multiple">
                  <?php
                    $vehicle = $conn->prepare("select * from logis_vehicle where logis_vehicle_status = 'Active'");
                    $vehicle->execute();
                    while($vehicleRow = $vehicle->fetch()){
                  ?>
                      <option value="<?php echo $vehicleRow['logis_vehicle_id']; ?>"><?php echo $vehicleRow['logis_vehicle_name']; ?></option>
                  <?php } ?>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Driving Licence :</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" placeholder="Enter licence no." id="dlNum" name="dlNum" >
              </div>
              <div class="col-sm-2">
                 <input  type="text" class="datepicker" placeholder="Renewed on"  id="issueDate" name="issueDate" >
              </div>
              <div class="col-sm-2">
                 <input  type="text" class="datepicker" placeholder="Expiry Date"  id="expiryDate" name="expiryDate" >
              </div>
              <div class="col-sm-3">
                <input type="file" class="fl" id="driverLicence" name="driverLicence">
                <p id="drvng"></p>
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Mobile Number :</label>
              <div class="col-sm-5">
                <div class="col-sm-6 padLEFT0">
                  <input type="text" class="form-control" id="contact1" name="contact1" placeholder="Enter a Mobile No.1" onkeypress="return isNumber(event)" maxlength="12" >
                </div>
                <div class="col-sm-6 padRIGHT0">
                  <input type="text" class="form-control" id="contact2" name="contact2" placeholder="Enter a Mobile No.2" onkeypress="return isNumber(event)" maxlength="12" >
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Address :</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter a Address 1" >
                <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter a Address 2" >
                <div class="col-sm-6 padLEFT0">
                  <select class="form-control" id="city" name="city">
                    <option value="" selected disabled>Select a City</option>
                    <?php
                        $city = $conn->prepare("SELECT * from kenya_city");
                        $city->execute();
                        while($cityRow = $city->fetch()){
                    ?>
                        <option value = "<?php echo $cityRow['city_id']; ?>"><?php echo $cityRow['city_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-6 padRIGHT0">
                  <input type="text" class="form-control" id="state" name="state" placeholder="Enter a PO Box" >
                </div>
                <div class="col-sm-6 padLEFT0">
                  <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Enter a Postal Code" >
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Email :</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter a Email ID" >
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
              <div class="col-sm-5">
                <input type="file" id="driverImg" name="driverImg">

                <img src="" class="img-responsive" id="img" name="img" style="width: 100px;"/>
                <small>upload only png and jpg image.</small>
              </div>
            </div>
             <div class="form-group hide_nssf">
              <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="nssf" name="nssf" placeholder="Enter a NSSF" maxlength="12" >
              </div>
            </div>
             <div class="form-group hide_nhif">
              <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="nhifNo" name="nhifNo" placeholder="Enter a NHIF" maxlength="12" >
              </div>
            </div>
          </span>
          <span class="show_next_kin">
            <h4>Next of kin</h4>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="kinname" id="kinname" placeholder="Enter a Full Name" >
              </div>
            </div> 
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">National ID Number :</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="kinnsn" id="kinnsn" placeholder="Enter a National ID" >
              </div>
            </div>
             <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>
              <div class="col-sm-5">
                  <textarea class="form-control" name="kinadd" id="kinadd" placeholder="Enter a Address"></textarea>
              </div>
            </div>
             <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="kincontact" id="kincontact" placeholder="Enter a Contact Number" onkeypress="return isNumber(event)" maxlength="12">
              </div>
            </div>
             <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="kinrelation" id="kinrelation" placeholder="Enter a Relationship" >
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Emergency Contact :</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="emrg_contact" id="emrg_contact" placeholder="Enter a Emergency Contact" onkeypress="return isNumber(event)" maxlength="12">
              </div>
            </div>
          </span>
          <span class="show_employee_details">
            <h4>Employment Details</h4>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Payroll No./Employee Id :</label>
              <div class="col-sm-5">
                  <input type="text" class="form-control" name="empId" id="empId" placeholder="Payroll No./Employee Id" >
                  <!-- <input type="hidden" class="form-control" name="updateEmpQuery" id="updateEmpQuery" value="no">   --> 
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Contract :</label>
              <div class="col-sm-3">
              <label for="inputPassword3" class="control-label">From</label>
                 <input  type="text" class="datepicker" placeholder="Select Date"  id="startDate" name="startDate" >
              </div>
              <div class="col-sm-3">
              <label for="inputPassword3" class=" control-label">To</label>
                 <input  type="text" class="datepicker" placeholder="Select Date"  id="endDate" name="endDate" >
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Branch :</label>
              <div class="load_branch">
              </div>
                <div class="col-sm-3 old_branch">
                <select name="deptId" id="deptId" class="form-control chosen-select" >
                  <option default selected disabled="disabled" value="0">Select Branch</option>
                  <?php
                    while($dept = $department->fetch()){
                      echo "<option value='".$dept['dept_id']."'>".$dept['dept_name']."</option>";
                    }
                  ?>
                </select>
               <!--  <a class="btn btn-default add-branch" href="#" role="button">Add</a> -->
              </div>
              
              <div class="col-sm-3 add_branch">
                 <a href="#" type="button" class="btn btn-default" data-toggle="modal" data-target="#addbranch" data-backdrop="static">Add Branch</a>
               </div>
            </div>                     

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Annual Leave Days:</label>
              <div class="col-sm-1">
                <input type="text" class="form-control" id="leaveAllowance" name="leaveAllowance" onkeypress="return isNumber(event);" maxlength="3" >
              </div>
            </div>
          </span>
          <span class="show_employee_document">
            <h4>Employment Documents</h4>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Curriculum Vitae :</label>
              <div class="col-sm-6">
                <input type="file" id="cv" name="cv">
                <p id="cvv"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Application Letter :</label>
              <div class="col-sm-6">
                <input type="file" id="appLetter" name="appLetter">
                <p id="app"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Professional & Academic qualifications :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="qualification" name="qualification">
              </div>
              <div class="col-sm-3">
                <input type="file" id="qualificationD" name="qualificationD">
                <p id="qulfy"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">ID Card copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="idCard" name="idCard">
              </div>
              <div class="col-sm-3">
                <input type="file" id="idCardD" name="idCardD">
                <p id="icard"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">KRA PIN copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="pin" name="pin">
              </div>
              <div class="col-sm-3">
                <input type="file" id="pinD" name="pinD">
                <p id="pinn"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="nssf1" name="nssf1" readonly="readonly">
              </div>
              <div class="col-sm-3">
                <input type="file" id="nssfD" name="nssfD">
                <p id="nssfid"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="nhifNo1" name="nhifNo1" readonly="readonly">
              </div>
              <div class="col-sm-3">
                <input type="file" id="nhifD" name="nhifD">
                <p id="nhifid"></p>
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Certificate of Good Conduct :</label>
              <div class="col-sm-6">
                <input type="file" class="fl" id="bonafideCert" name="bonafideCert">
                <p id="bonaid"></p>
              </div>
            </div>
          </span>
          <span class="show_salary">
            <h4>Salary</h4>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Gross Salary :</label>
              <div class="col-sm-2">
                <input type="text" class="form-control comma_value" id="grossSal" name="grossSal" onkeypress="return isNumber(event)">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
          </span>
          </form>

          <!--  ADD BRANCH - POPUP -->
          <div class="modal" id="addbranch" tabindex="-1" role="dialog" aria-labelledby="addDeptModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="addDeptModal">Add Department</h4>
                </div>
                
                <form id="addDeptt" name="addDeptt" action="functions/addmaster.php" method="POST">
                <div class="modal-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-6 control-label">Department Name</label>
                   <div class="col-sm-6">
                      <input type="hidden" class="form-control" name="addDept" id="addDept" value="addDept">

                      <input type="text" class="form-control" id="Dept" name="Dept" placeholder="Department">

                      <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comp_id = $_SESSION['comp_id']; ?>">
                  </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div>
                    <button type="button" name="adddepart" id="adddepart" class="btn btn-default" onclick="getDept();">Submit</button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
          <!-- CLOSE ADD BRANCH - POPUP -->
        </div>
          <div class="msg">
              <p>sorry these data already exists.kindky update from drivers page.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script src="js/typeahead.min.js"></script>
<script src="../js/datepicker.js"></script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function () {
    $(".msg").hide();
      $('#NSN').change(function () {
        var value = $("#NSN").val();
        var comp = $("#compId").val();
        //console.log("Hi this is new box: "+value); 

        // alert(value);
        // alert(comp);

        if(value!=null)
        {
          $.ajax({
            method:"GET",
            url: "functions/searchDriver.php?addDriverNSN="+value+"&compI="+comp,
          })
          .done(function(data){
            // alert(data);
              //$( this ).addClass( "done" );
              console.log("the data = "+data);
              data = JSON.parse(data);

              if(data.logis_drivers==false && data.logis_employee==false)
              {
                //Condition when driver is new. Data will be added.
                console.log("The driver is new "+data);
                    // $("input").val('');
                    $("#NSN").val(value);
                    $("#compId").val(comp);
              }
              
              /////////////////////////////
              //  SAME COMPANY WORKING  //
              ////////////////////////////
              else if(data.logis_drivers != false && data.logis_employee != false){
                //$(".addDriver-form").hide();
                alert("The Driver details already exist in Database");
                $('input').attr('readonly',true);
                $('input').attr('readonly',true);
                $("#fname").val(data.logis_drivers['drv_fname']);
                $("#mname").val(data.logis_drivers['drv_mname']);
                $("#lname").val(data.logis_drivers['drv_lname']);
                $("#driverImg").attr('disabled',true);     
                var i = data.logis_drivers['drv_img'];
                var ii = i.replace("../","");
                $("#img").attr('src', ii);
                $("#contact1").val(data.logis_drivers['drv_contact_no']);
                $("#contact2").val(data.logis_drivers['drv_alt_contact_no']);
                $("#address1").val(data.logis_drivers['drv_address']);
                $("#address2").val(data.logis_drivers['drv_address2']);
                $("#city").val(data.logis_drivers['drv_city']);
                $("#state").val(data.logis_drivers['drv_state']);
                $("#zipcode").val(data.logis_drivers['drv_zipcode']);
                $("#email").val(data.logis_drivers['drv_email']);
                $("#lstFruits").val(data.logis_drivers['logis_drivers_vehicle_class']).attr('disabled',true);
                $("#dlNum").val(data.logis_drivers['drv_driving_license_no']);
                $("#issueDate").val(data.logis_drivers['drv_dl_issue_date']).attr('disabled',true);
                $("#expiryDate").val(data.logis_drivers['drv_dl_expiry_date']).attr('disabled',true);
                $("#nssf").val(data.logis_employee['empdt_nssfNo']);
                $("#nhifNo").val(data.logis_employee['empdt_nhif']);

                $(".show_next_kin").hide();
                $(".show_employee_document").hide();
                $(".show_employee_details").hide();
                $(".show_salary").hide();
             }

              ////////////////////////////////
              //  ANOTHER COMPANY WORKING  //
              ///////////////////////////////
              else if(data.logis_drivers != false && data.logis_employee_comp != false){
                alert("The Driver already working with another company");

                $('input').attr('readonly',true);
                $("#fname").val(data.logis_drivers['drv_fname']);
                $("#mname").val(data.logis_drivers['drv_mname']);
                $("#lname").val(data.logis_drivers['drv_lname']);
                $("#driverImg").attr('disabled',true);     
                var i = data.logis_drivers['drv_img'];
                var ii = i.replace("../","");
                $("#img").attr('src', ii);
                $("#contact1").val(data.logis_drivers['drv_contact_no']);
                $("#contact2").val(data.logis_drivers['drv_alt_contact_no']);
                $("#address1").val(data.logis_drivers['drv_address']);
                $("#address2").val(data.logis_drivers['drv_address2']);
                $("#city").val(data.logis_drivers['drv_city']);
                $("#state").val(data.logis_drivers['drv_state']);
                $("#zipcode").val(data.logis_drivers['drv_zipcode']);
                $("#email").val(data.logis_drivers['drv_email']);
                $("#lstFruits").val(data.logis_drivers['logis_drivers_vehicle_class']).attr('disabled',true);
                $("#dlNum").val(data.logis_drivers['drv_driving_license_no']);
                $("#issueDate").val(data.logis_drivers['drv_dl_issue_date']).attr('disabled',true);
                $("#expiryDate").val(data.logis_drivers['drv_dl_expiry_date']).attr('disabled',true);
                $(".hide_nssf").hide();
                $(".hide_nhif").hide();

                $(".show_next_kin").hide();
                $(".show_employee_document").hide();
                $(".show_employee_details").hide();
                $(".show_salary").hide();
              }


              ///////////////////////////
              //  NOT WORKING DRIVER  //
              //////////////////////////
              else if(data.logis_employee_notwork != false){
                alert("This Driver currently not working with any company");

                // $('input').attr('readonly',true);
                $("#fname").val(data.logis_employee_notwork['drv_fname']);
                $("#mname").val(data.logis_employee_notwork['drv_mname']);
                $("#lname").val(data.logis_employee_notwork['drv_lname']);
                $("#driverImg");     
                var i = data.logis_employee_notwork['drv_img'];
                var ii = i.replace("../","");
                $("#img").attr('src', ii);
                $("#contact1").val(data.logis_employee_notwork['drv_contact_no']);
                $("#contact2").val(data.logis_employee_notwork['drv_alt_contact_no']);
                $("#address1").val(data.logis_employee_notwork['drv_address']);
                $("#address2").val(data.logis_employee_notwork['drv_address2']);
                $("#city").val(data.logis_employee_notwork['drv_city']);
                $("#state").val(data.logis_employee_notwork['drv_state']);
                $("#zipcode").val(data.logis_employee_notwork['drv_zipcode']);
                $("#email").val(data.logis_employee_notwork['drv_email']);
                $("#lstFruits").val(data.logis_employee_notwork['logis_drivers_vehicle_class']);
                $("#dlNum").val(data.logis_employee_notwork['drv_driving_license_no']);
                $("#issueDate").val(data.logis_employee_notwork['drv_dl_issue_date']);
                $("#expiryDate").val(data.logis_employee_notwork['drv_dl_expiry_date']);
                // $(".hide_nssf").hide();
                // $(".hide_nhif").hide();

                // $(".show_next_kin").hide();
                // $(".show_employee_document").hide();
                // $(".show_employee_details").hide();
                // $(".show_salary").hide();
              }


              /////////////////////////////////
              //  NOT WORKING WITH RED FLAG //
              ////////////////////////////////
              else if(data.logis_employee_redflag != false){
                var firstalert = "WARNING : DRIVER REDFLAGGED";
                var secondalert = "BY (" + data.redflag_comp['comp_org_name'] + ")";
                var thirdalert = "EMPLOYEE AT YOUR OWN RISK";

                alert(firstalert + "\n" + secondalert + "\n" + thirdalert);

                // $('input').attr('readonly',true);
                $("#fname").val(data.logis_drivers['drv_fname']);
                $("#mname").val(data.logis_drivers['drv_mname']);
                $("#lname").val(data.logis_drivers['drv_lname']);
                $("#driverImg");     
                var i = data.logis_drivers['drv_img'];
                var ii = i.replace("../","");
                $("#img").attr('src', ii);
                $("#contact1").val(data.logis_drivers['drv_contact_no']);
                $("#contact2").val(data.logis_drivers['drv_alt_contact_no']);
                $("#address1").val(data.logis_drivers['drv_address']);
                $("#address2").val(data.logis_drivers['drv_address2']);
                $("#city").val(data.logis_drivers['drv_city']);
                $("#state").val(data.logis_drivers['drv_state']);
                $("#zipcode").val(data.logis_drivers['drv_zipcode']);
                $("#email").val(data.logis_drivers['drv_email']);
                $("#lstFruits").val(data.logis_drivers['logis_drivers_vehicle_class']);
                $("#dlNum").val(data.logis_drivers['drv_driving_license_no']);
                $("#issueDate").val(data.logis_drivers['drv_dl_issue_date']);
                $("#expiryDate").val(data.logis_drivers['drv_dl_expiry_date']);
                // $(".hide_nssf").hide();
                // $(".hide_nhif").hide();

                // $(".show_next_kin").hide();
                // $(".show_employee_document").hide();
                // $(".show_employee_details").hide();
                // $(".show_salary").hide();
              }

              console.log("value of data "+data);
          });
        }
        else{
          alert("Please enter the National Security number to proceed");
        }
      });

      var format = function(num){
        var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
        if(str.indexOf(".") > 0) {
          parts = str.split(".");
          str = parts[0];
        }
        str = str.split("").reverse();
        for(var j = 0, len = str.length; j < len; j++) {
          if(str[j] != ",") {
            output.push(str[j]);
            if(i%3 == 0 && j < (len - 1)) {
              output.push(",");
            }
            i++;
          }
        }
        formatted = output.reverse().join("");
        return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
      };


      $(function(){
          $(".comma_value").keyup(function(e){
              $(this).val(format($(this).val()));
          });
      });


      $('#empId').change(function () {
        var value = $("#empId").val();      

        if(value!=null)
        {
          $.ajax({
            method:"GET",
            url: "functions/searchDriver.php?chkempId="+value
          })
          .done(function(empdata) {
              empdata = JSON.parse(empdata);
              var e = empdata.empdt_empID;

              if(empdata!=false)
              {
                console.log("The Employee ID is Check"+empdata);
                 alert("Employee ID is aleardy exist Please enter a new Id.");
                 $("#empId").val('');
              }
              console.log("value of data="+empdata);
          });
        }
        else{
          alert("Please enter the Employee Id to proceed");
        }
      });

    var url = window.location.href;
    var msg = url.substring(url.indexOf('?')+5);
    console.log(msg);
      if(msg=="success"){
        $("#alertBox").removeClass("hidden");
        $("#alertBox").addClass("alert-success");
        $("#alertBox").html("Driver Details submitted succesfully");
        window.location.href ='drivers.php';
      }
      else if(msg=="error"){    
        $("#alertBox").removeClass("hidden");
        $("#alertBox").addClass("alert-danger");
        $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
      }
      else{
      	$("#alertBox").addClass("hidden");
        $("#alertBox").removeClass("alert-success");
        $("#alertBox").removeClass("alert-danger");
        $("#alertBox").html("Sorry there was an error in submitting your data. Kindly try after some time.");
      }

      $('#startDate').datepicker({
          format: "yyyy/mm/dd"
      }).on('changeDate', function(ev){
       // alert("working");
         var date1 = $('#startDate').datepicker('getDate');
         var date2 = date1.setDate(date1.getDate()+364)
        $('#endDate').datepicker('setDate',date1);
      });

      $('#endDate').datepicker({
          format: "yyyy/mm/dd",
      });

      $('#issueDate').datepicker({
          format: "yyyy/mm/dd" 
      }).on('changeDate', function(ev){
        $("#expiryDate").val("");
      }); 

      $('#expiryDate').datepicker({
          format: "yyyy/mm/dd"
      }).on('changeDate', function(ev){
       // alert("working");
         var date1 = $('#issueDate').datepicker('getDate');
         var date2 = $('#expiryDate').datepicker('getDate');
         if(date1 > date2)
         {
          alert("Date cannot be less than issue Date");
          $('#expiryDate').datepicker('setDate',date1);
         }
        
      });
  
  });


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function() {
  $('#nssf').keyup(function(e) {
    var txtVal = $(this).val();
    txtVal2 = txtVal;
    $('#nssf1').val(txtVal);
  });

   $('#nhifNo').keyup(function(e) {
    var txtVal = $(this).val();
    txtVal2 = txtVal;
    $('#nhifNo1').val(txtVal);
  });
});

function getDept(){
  $(".load_branch").hide();
  $.ajax({
    type: "POST",
    url:"functions/ajaxaddmaster.php",
    data: $('#addDeptt').serialize(),
    success: function(msg){
      // alert(msg); 
      $(".old_branch").hide();
      $(".load_branch").show();
      $(".load_branch").html(msg);
      $('#addbranch').modal('hide');
    },
  });
}
</script>

<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script> 
<script>
$( "#form" ).validate({
  rules: {
    email: {
      email: true
    },
   }
});
</script>


<script src="../js/checkbox_select.js"></script> 
<script type="text/javascript">
    $(function () {
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<?php include'footer.php'; ?>