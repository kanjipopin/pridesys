<?php include'header.php';?>

      <div class="col-sm-9 mrgnTOP15">
          <div class="wht-bg">
              <div class="heading">
                <h4>My Profile</h4>
              </div>
              <div class="verifying-driver-page">
                <div class="profile-page row">
                  <form name="profileForm" method="post" action="functions/profile.php" enctype="multipart/form-data">
                      <div id="alertBox" name="alertBox" class="alert "></div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputEmail1">Organization Name</label>
                          <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Organization Name" value="<?php echo $companyRes['comp_org_name'];?>">
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputEmail1">Full Name</label>
                          <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" value="<?php echo $companyRes['comp_admin_fname'];?>">
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">Address </label>
                          <textarea type="text" class="form-control" id="address" name="address" placeholder="Company Address" rows="6"><?php echo $companyRes['comp_address'];?></textarea>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">Company Logo </label>
                          <input type="file" id="logo" name="logo" style="padding: 2px 0;" class="form-control">
                          <img src=<?php echo $compLogo; ?> class="img-responsive" id="imglogo" name="imglogo" style="width: 100px;float:left;">
                          <span class="logoimg">Image to be uploaded should be in:<br/> Format: jpg or png.<br/> Size: 100px X 100px</span>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">City </label>
                          <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $companyRes['comp_city']; ?>">
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">Contact No.</label>
                          <input type="telephone" class="form-control" id="contact" name="contact" placeholder="Contact No" value="<?php echo $companyRes['company_contact_no'];?>">
                        </div>                        
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">Website</label>
                          <input type="text" class="form-control" id="website" name="website" placeholder="Company Website" value="<?php echo $companyRes['comp_website'];?>">
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="exampleInputPassword1">Email</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Official Email" value="<?php echo $companyRes['comp_official_email'];?>" readonly>
                        </div>
                        <div class="col-sm-12">
                          <input type="submit" name="submit" id="submit" class="btn btn-default" value="Submit"> 
                        </div>
                  </form>
                </div>
              </div>
              
          </div>
      </div>
      </div>
  </div>


  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script> 
$(document).load(function(){
     location.replace('profile.php');
 });

$(document).ready(function(){
 
 $("#alertBox").hide();
    
  var timeoutID = null;    
    //$("#prodSelect").hide();
  var url = window.location.href;
  var msg = url.substring(url.indexOf('?')+1);
  console.log(msg+ ' ');
  if(msg=="suc"){    
    $("#alertBox").html("Profile updated successfully");
    $("#alertBox").addClass("alert-success");
    $("#alertBox").show();
  }
  else if(msg=="er-up-img"){    
       
    $("#alertBox").html("Sorry there was an error while uploading your logo. Try again after some time");
    $("#alertBox").addClass("alert-danger");
    $("#alertBox").show(); 
  }
  else if(msg=="er-up-data" || msg=="er-up-pdt"){    
       
    $("#alertBox").html("Sorry there was an error while updating your profile details. Try again after some time");
    $("#alertBox").addClass("alert-danger");
    $("#alertBox").show(); 
  }
});
</script>

<?php include'footer.php';?>