<?php
@$Email = $_SESSION['operations_manager'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$comp->execute();
$comprow = $comp->fetch();

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}


include'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Driver</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="drivers.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_driver.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Personal Details</h4>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">National Indetification Number: </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="NSN" id="NSN" placeholder="Enter a National Indetification Number *"  onchange="checkNSN(); flagNSN();">
                <p class="CheckNSNMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
                <p class="CheckFlagNSNMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter a First Name *" >

                <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comprow['comp_id']; ?>">
              </div>
               <div class="col-sm-3">
                <input type="text" class="form-control" name="mname" id="mname" placeholder="Enter a Middle Name *" >
              </div>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter a Last Name *" >
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Gender :</label>
              <div class="col-sm-3">
                <input type="radio" name="gender" id="gender" value="male" style="height:auto"> Male
                <input type="radio" name="gender" id="gender" value = "female" style="height:auto"> Female
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
              <div class="col-sm-5">
                <input type="file" id="driverImg" name="driverImg">
                <small>upload only png and jpg image.</small>
              </div>
            </div>
          </span>

                    <span class="show_driver_details">
            <h4>Contact Information</h4>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Mobile Number :</label>
              <div class="col-sm-5">
                <div class="col-sm-6 padLEFT0">
                  <input type="text" class="form-control" id="contact1" name="contact1" placeholder="Enter a Mobile No.1" onkeypress="return isNumber(event)">
                </div>

                <div class="col-sm-6 padRIGHT0">
                  <input type="text" class="form-control" id="contact2" name="contact2" placeholder="Enter a Mobile No.2" onkeypress="return isNumber(event)" >
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Email :</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter a Email ID" >
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Physical Address :</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter a Address 1" >
                <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter a Address 2" >
                <div class="col-sm-6 padLEFT0">
                  <select class="form-control" id="city" name="city">
                    <option value="" selected disabled>Select a City</option>
                    <?php
                    $city = $conn->prepare("SELECT * from kenya_city");
                    $city->execute();
                    while($cityRow = $city->fetch()){
                        ?>
                        <option value = "<?php echo $cityRow['city_id']; ?>"><?php echo $cityRow['city_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-sm-6 padRIGHT0">
                  <input type="text" class="form-control" id="pobox" name="pobox" placeholder="Enter a PO Box" onkeypress="return isNumber(event)">
                </div>

                <div class="col-sm-6 padLEFT0">
                  <input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="Enter a Postal Code" onkeypress="return isNumber(event)">
                </div>
              </div>
            </div>
          </span>

                    <span class="show_driver_details">
            <h4>Driving Licence Details</h4>
            <div class="form-group">


              <label for="inputPassword3" class="col-sm-3 control-label">Driving Licence Number:</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" placeholder="Driving License Number" id="dlNum" name="dlNum">
              </div>


              <div class="col-sm-2">
                 <input type="date" class="datepicker" placeholder="Renewed on"  id="issueDate" name="issueDate" style="width: 100%;">
              </div>

              <div class="col-sm-2">
                 <input type="date" class="datepicker" placeholder="Expiry Date"  id="expiryDate" name="expiryDate" style="width: 100%;">
              </div>

              <div class="col-sm-3">
                <input type="file" class="fl" id="driverLicence" name="driverLicence">
                <p id="drvng"></p>
              </div>

              <label for="inputEmail3" class="col-sm-3 control-label">Is License PSV? :</label>
              <div class="col-sm-2">
                <input type="radio" name="isLicensePSV" id="PSV_License_Yes" value="Yes" style="height:auto"> Yes
                <input type="radio" name="isLicensePSV" id="PSV_License_No" value="No" style="height:auto"> No
              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Driver License Class: </label>
              <div class="col-sm-5">
                  <select id="vehiclelist" name="vehiclelist[]" multiple="multiple" class="form-control chosen-select">
                  <?php
                  $vehicle = $conn->prepare("SELECT * from logis_vehicle where logis_vehicle_status = 'Active'");
                  $vehicle->execute();
                  while($vehicleRow = $vehicle->fetch()){
                      ?>
                      <option value="<?php echo $vehicleRow['logis_vehicle_id']; ?>"><?php echo $vehicleRow['logis_vehicle_name']; ?></option>
                  <?php } ?>
                  </select>
              </div>
            </div>
          </span>

                    <!--<span class="show_driver_details">
                      <h4>Driver Port Pass</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Driver Port Pass Number :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" placeholder="Driver Port Pass Number" id="driver_port_number" name="driver_port_number">
                        </div>

                        <div class="col-sm-3">
                          <input type="date" class="datepicker" placeholder="Driver Port Pass Expiry Date" id="driver_port_expire_date" name="driver_port_expire_date" style="width: 100%;">
                        </div>
                      </div>
                    </span>

                    <span class="show_driver_details">
                      <h4>Vehicle Port Pass</h4>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Vehicle Port Pass Number :</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" placeholder="Vehicle Port Pass Number" id="vehicle_port_number" name="vehicle_port_number">
                        </div>

                        <div class="col-sm-3">
                          <input type="date" class="datepicker" placeholder="Vehicle Port Pass Expiry Date" id="vehicle_port_expire_date" name="vehicle_port_expire_date" style="width: 100%;">
                        </div>
                      </div>
                    </span>-->

                    <span class="show_driver_details">
            <h4>Vehicle Assignment</h4>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Vehicle type :</label>
              <div class="col-sm-3">
                <input type="radio" name="Vehicle_Type" id="PSV_Vehicles" value="PSV" style="height:auto"> PSV
                <input type="radio" name="Vehicle_Type" id="Other_Vehicles" value="Other" style="height:auto"> Other
              </div>
            </div>

            <div class="form-group" id="toggle_Plat_Number">
              <label for="inputEmail3" class="col-sm-3 control-label">Vehicle Plate Number: </label>
              <div class="col-sm-5">
              <input type="text" name="vehiPlatNumber" id="vehiPlatNumber" placeholder="Vehicle Plate Number" class="form-control">
              </div>
            </div>
          </span>

                    <!--<span class="show_driver_details">
                      <h4>Vehicle Insurance Details</h4>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Vehicle Insurance Type: </label>
                        <div class="col-sm-5">
                          <input type="radio" name="insuranceType" id="insuranceType" value="private" style="height:auto"> Private
                          <input type="radio" name="insuranceType" id="insuranceType" value="commercial" style="height:auto"> Commercial
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Insurance Reference ID: </label>
                        <div class="col-sm-5">
                          <input type="text" name="insuranceRefNum" id="insuranceRefNum" placeholder="Reference Number" class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Valid till: </label>
                        <div class="col-sm-5">
                          <input type="date" name="insuranceExpDate" id="insuranceExpDate" placeholder="Expiry Date" class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                          <input type="file" id="insuranceImg" name="insuranceImg">
                        </div>
                      </div>
                    </span>

                    <span class="show_driver_details">
                      <h4>Vehicle Inspection Details</h4>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Inspection Ref No: </label>
                        <div class="col-sm-5">
                          <textarea name="vehicleInspection" id="vehicleInspection" placeholder="Vehicle Inspection Details" class="form-control"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
                        <div class="col-sm-5">
                          <input type="file" id="vehicleInspectionFile" name="vehicleInspectionFile">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Valid till: </label>
                        <div class="col-sm-5">
                          <input type="date" name="inspectionExpDate" id="inspectionExpDate" placeholder="Expiry Date" class="form-control">
                        </div>
                      </div>
                    </span>-->

                    <!--  <div class="form-group hide_nssf">
                      <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="nssf" name="nssf" placeholder="Enter a NSSF" maxlength="12" >
                      </div>
                    </div> -->
                    <!-- <div class="form-group hide_nhif">
                     <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                     <div class="col-sm-4">
                       <input type="text" class="form-control" id="nhifNo" name="nhifNo" placeholder="Enter a NHIF" maxlength="12" >
                     </div>
                   </div> -->

                    <span class="show_next_kin">
            <div class="kinOne">
              <h4>Next of kin First</h4>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinname[]" id="kinname" placeholder="Enter a Full Name" >
                </div>
              </div> 

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">National Indetification Number :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinnsn[]" id="kinnsn" placeholder="Enter a National Indetification Number" onkeypress="return isNumber(event)">
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>
                <div class="col-sm-5">
                    <textarea class="form-control" name="kinadd[]" id="kinadd" placeholder="Enter a Address"></textarea>
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kincontact[]" id="kincontact" placeholder="Enter a Contact Number" onkeypress="return isNumber(event)">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kin_email[]" id="kin_email" placeholder="Enter a Email">
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinrelation[]" id="kinrelation" placeholder="Enter a Relationship">
                </div>
              </div>

              <div class="col-offset-sm-3 form-group">
                <button type="button" name="secondKinBtn" id="secondKinBtn" class="btn btn-default" onclick="getSecondKin();" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> + Add</button>
              </div>
            </div>

            <div class="kinTwo" style="display: none;">
              <h4>Next of kin Second <button type="button" name="secondKinBtnDel" id="secondKinBtnDel" class="btn-default btn" onclick="getSecondKinHide();" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> - Remove</button> </h4>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinname[]" id="kinname" placeholder="Enter a Full Name" >
                </div>
              </div> 

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">National Indetification Number :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinnsn[]" id="kinnsn" placeholder="Enter a National ID" onkeypress="return isNumber(event)">
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>
                <div class="col-sm-5">
                    <textarea class="form-control" name="kinadd[]" id="kinadd" placeholder="Enter a Address"></textarea>
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kincontact[]" id="kincontact" placeholder="Enter a Contact Number" onkeypress="return isNumber(event)">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kin_email[]" id="kin_email" placeholder="Enter a Email">
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinrelation[]" id="kinrelation" placeholder="Enter a Relationship">
                </div>
              </div>

              <div class="form-group col-offset-sm-3">
                <button type="button" name="ThirdKinBtn" id="ThirdKinBtn" class="btn btn-default" onclick="getThirdKin();" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> + Add</button>
              </div>
            </div>

            <div class="kinThree" style="display: none;">
              <h4>Next of kin Third <button type="button" name="thirdKinBtnDel" id="thirdKinBtnDel" class="btn-default btn" onclick="getThirdKinHide();" style="background: #e3e3e3;border-color: #e3e3e3;padding: 8px 10px;color: #000;text-transform: capitalize;"> - Remove</button></h4>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Full Name :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinname[]" id="kinname" placeholder="Enter a Full Name">
                </div>
              </div> 

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">National Indetification Number :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinnsn[]" id="kinnsn" placeholder="Enter a National ID" onkeypress="return isNumber(event)">
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>
                <div class="col-sm-5">
                    <textarea class="form-control" name="kinadd[]" id="kinadd" placeholder="Enter a Address"></textarea>
                </div>
              </div>

               <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Contact Number:</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kincontact[]" id="kincontact" placeholder="Enter a Contact Number" onkeypress="return isNumber(event)">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kin_email[]" id="kin_email" placeholder="Enter a Email">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Relation with driver :</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="kinrelation[]" id="kinrelation" placeholder="Enter a Relationship">
                </div>
              </div>
            </div>
          </span>

                    <span class="show_employee_details">
            <h4>Employment Contract</h4>
                        <!-- <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Payroll No./Employee Id :</label>
                          <div class="col-sm-5">
                              <input type="text" class="form-control" name="empId" id="empId" placeholder="Payroll No./Employee Id" > -->
                        <!-- <input type="hidden" class="form-control" name="updateEmpQuery" id="updateEmpQuery" value="no">   -->
                        <!-- </div>
                      </div> -->

            <input type="hidden" class="datepicker" placeholder="Commenced Start Date" id="commenced_startDate" name="commenced_startDate" style="width: 100%;">
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Contract Date:</label>
              <div class="col-sm-3">
              <label for="inputPassword3" class="control-label">From</label>
                 <input type="date" class="datepicker" placeholder="Start Date" id="startDate" name="startDate" style="width: 100%;">
              </div>

                <!--<div class="col-sm-3">
                <label for="inputPassword3" class=" control-label">To</label>
                   <input type="date" class="datepicker" placeholder="Expiry Date" id="endDate" name="endDate" style="width: 100%;">
                </div>-->
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Branch Location:</label>
              <div class="load_branch">
              </div>
                <div class="col-sm-5 old_branch">
                <input type="text" name="deptId" id="deptId" class="form-control" placeholder="Branch">

                    <!-- <select name="deptId" id="deptId" class="form-control chosen-select" > -->
                    <!-- <option default selected disabled="disabled" value="0">Select Branch</option> -->
                  <?php
                  // $department = $conn->prepare("SELECT * from logis_dept_master");
                  // $department->execute();
                  // while($dept = $department->fetch()){
                  //   echo "<option value='".$dept['dept_id']."'>".$dept['dept_name']."</option>";
                  // }
                  ?>
                <!-- </select> -->
                    <!--  <a class="btn btn-default add-branch" href="#" role="button">Add</a> -->
              </div>

                <!--  <div class="col-sm-3 add_branch">
                    <a href="#" type="button" class="btn btn-default" data-toggle="modal" data-target="#addbranch" data-backdrop="static">Add Branch</a>
                  </div> -->
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Department:</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="department" name="department" placeholder="Department">
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Image :</label>
              <div class="col-sm-5">
                <input type="file" id="ContractFile" name="ContractFile">
              </div>
            </div>

                        <!-- <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Annual Leave Days:</label>
                          <div class="col-sm-2">
                            <input type="text" class="form-control" id="leave" name="leave" placeholder="Leaves" onkeypress="return isNumber(event);" maxlength="3" >
                          </div>
                        </div> -->
          </span>

                    <span class="show_employee_document">
            <h4>Employment Documents</h4>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Curriculum Vitae :</label>
              <div class="col-sm-6">
                <input type="file" id="cv" name="cv">
                <p id="cvv"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Application Letter :</label>
              <div class="col-sm-6">
                <input type="file" id="appLetter" name="appLetter" >
                <p id="app"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Professional & Academic qualifications :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Enter Professional & Academic qualifications">
              </div>
              <div class="col-sm-3">
                <input type="file" id="qualificationD" name="qualificationD">
                <p id="qulfy"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">ID Card copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="idCard" name="idCard" placeholder="Enter ID Card">
              </div>
              <div class="col-sm-3">
                <input type="file" id="idCardD" name="idCardD">
                <p id="icard"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">KRA PIN copy :</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="pin" name="pin" placeholder="Enter KRA Pin">
              </div>
              <div class="col-sm-3">
                <input type="file" id="pinD" name="pinD">
                <p id="pinn"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">N.S.S.F No/Card copy :</label>
                <!--  <div class="col-sm-3">
                   <input type="text" class="form-control" id="nssf1" name="nssf1">
                 </div> -->
              <div class="col-sm-3">
                <input type="file" id="nssfD" name="nssfD">
                <p id="nssfid"></p>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">N.H.I.F No/Card copy :</label>
                <!--    <div class="col-sm-3">
                     <input type="text" class="form-control" id="nhifNo1" name="nhifNo1">
                   </div> -->
              <div class="col-sm-3">
                <input type="file" id="nhifD" name="nhifD">
                <p id="nhifid"></p>
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Certificate of Good Conduct :</label>
              <div class="col-sm-6">
                <input type="file" class="fl" id="bonafideCert" name="bonafideCert">
                <p id="bonaid"></p>
              </div>
            </div>
          </span>

                    <span class="show_salary">
            <h4>Salary</h4>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-3 control-label">Gross Salary :</label>
              <div class="col-sm-5">
                <input type="text" class="form-control comma_value" id="grossSal" name="grossSal" placeholder="Salary" onkeypress="return isNumber(event)">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Submit</button>
              </div>
            </div>
          </span>
                </form>

                <!--  ADD BRANCH - POPUP -->
                <!-- <div class="modal" id="addbranch" tabindex="-1" role="dialog" aria-labelledby="addDeptModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="addDeptModal">Add Department</h4>
                </div>
                
                <form id="addDeptt" name="addDeptt" action="functions/addmaster.php" method="POST">
                <div class="modal-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-6 control-label">Department Name</label>
                   <div class="col-sm-6">
                      <input type="hidden" class="form-control" name="addDept" id="addDept" value="addDept">

                      <input type="text" class="form-control" id="Dept" name="Dept" placeholder="Department">

                      <input type="hidden" class="form-control" name="compId" id="compId" value="<?php// echo $comp_id = $_SESSION['comp_id']; ?>">
                  </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div>
                    <button type="button" name="adddepart" id="adddepart" class="btn btn-default" onclick="getDept();" >Submit</button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div> -->
                <!-- CLOSE ADD BRANCH - POPUP -->
            </div>
            <!-- <div class="msg">
                <p>sorry these data already exists.kindky update from drivers page.</p>
            </div> -->
        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
            //   $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#inspectionExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#commenced_startDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#driver_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
            //   $('#vehicle_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function checkNSN(){
            var NSN = $("#NSN").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkDuplicateNSN","NSN":NSN,"compId":compId },

                success : function(msg){

                    if(msg == "same"){
                        $(".CheckNSNMsg").text("Driver already exists in your account.");
                        $("#submit").attr("disabled",true);
                    } else if(msg == "different") {
                        $(".CheckNSNMsg").text("Driver already exists on another account.");
                        $("#submit").attr("disabled",true);
                    } else {
                        $(".CheckNSNMsg").text("");
                        $("#submit").attr("disabled",false);
                    }
                }
            });
        }

        function flagNSN(){
            var NSN = $("#NSN").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkflagNSN","NSN":NSN,"compId":compId },

                success : function(msg){
                    if(msg > 0){
                        // $(".CheckFlagNSNMsg").text("This driver was Flaged. Please check your email address for more details about driver.");

                        $("#delivery_popup").modal("show");
                    } else {
                        $("#delivery_popup").modal("hide");
                    }
                }
            });
        }

        function getDept(){
            $(".load_branch").hide();
            $.ajax({
                type: "POST",
                url:"functions/ajaxaddmaster.php",
                data: $('#addDeptt').serialize(),
                success: function(msg){
                    // alert(msg);
                    $(".old_branch").hide();
                    $(".load_branch").show();
                    $(".load_branch").html(msg);
                    $('#addbranch').modal('hide');
                },
            });
        }



        $(document).ready(function() {
            $('#nssf').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nssf1').val(txtVal);
            });


            $('input[type=radio][name=Vehicle_Type]').change(function() {

                if(this.value === "Other") {

                    document.getElementById("toggle_Plat_Number").style.display = "none";

                } else {

                    document.getElementById("toggle_Plat_Number").style.display = "block";

                }

            });

            $('#nhifNo').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nhifNo1').val(txtVal);
            });
        });


        function getSecondKin(){
            $(".kinTwo").show();
            $("#secondKinBtn").hide();
        }

        function getThirdKin(){
            $(".kinThree").show();
            $("#ThirdKinBtn").hide();
            $("#secondKinBtnDel").hide();
        }

        function getSecondKinHide(){
            $(".kinTwo").hide();
            $("#secondKinBtn").show();
            $("#secondKinBtnDel").show();
        }

        function getThirdKinHide(){
            $(".kinThree").hide();
            $("#ThirdKinBtn").show();
            $("#secondKinBtnDel").show();
        }
    </script>

    <!-- <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script>
    $( "#form" ).validate({
      rules: {
        email: {
          email: true
        },
       }
    });
    </script> -->


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#lstFruits').multiselect({
        //         includeSelectAllOption: true
        //     });
        // });
    </script>

<?php include'footer.php'; ?>