<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['operations_manager'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $action = isset($_GET['action']) ? $_GET['action'] : "";

  // if($action == "not_wroking"){
  //   $msg = "These driver is currently not working with your company.";
  // }

  if($action == "no_driver"){
    $msg = "Driver does not exist";
  }
  elseif($action == "success"){
    $msg = "You have successfully flagged this Driver.";
  }
?>

<div class="page-rightWidth">
  <div class="flag-driver-page">
      <div class="heading">
        <h4>Flag Driver</h4>
        <div class="filters">
          <div class="form-inline">
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 15px;color: #897e74;font-size: 13px;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="flag_driver_confirmation.php" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Driver National Indetification Number : </label>
            <div class="col-sm-5">
              <input type="text" class="form-control" name="NSN" id="NSN" placeholder="Enter a National Indetification Number *" required onchange="checkNSN(); getNSN();" onkeypress="return isNumber(event)">
              <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Confirm National Indetification Number : </label>
            <div class="col-sm-5">
              <input type="text" class="form-control" name="confmNSN" id="confmNSN" placeholder="Confirm National Indetification Number *" disabled required onchange="getNSN();" onkeypress="return isNumber(event)">
              <p class="confmNsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Driver Name :</label>
            <div class="col-sm-5">
              <input type="hidden" class="form-control" name="compId" id="compId" value="<?php echo $comprow['comp_id']; ?>">

              <input type="text" class="form-control" name="Username" id="Username" placeholder="Enter a Name *" required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Comments :</label>
            <div class="col-sm-5">
              <textarea name="description" id="description" class="form-control" placeholder="Enter a Comments *" rows="5" required></textarea>
            </div>
          </div>

          <?php if($orderDetails != "false"){ ?>
            <div class="modal-footer">
              <button type="submit" name="submitFlag" id="submitFlag" class="btn btn-default" disabled>Submit</button>
            </div>
          <?php } ?>
        </form>
      </div>
  </div>
</div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a class="active-class" href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  function isNumber(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }
    return true;
  }

  function checkNSN(){
    var NSN = $("#NSN").val();
    var compId = $("#compId").val();

    $.ajax({
      type : "post",
      url : "functions/ajax.php",
      data : { "type":"checkFlagDriverNSN","NSN":NSN,"compId":compId },

      success : function(msg){
        // if(msg == "not working"){
        //   $(".nsnMsg").text("These driver is currently not working with your company.");
        //   $("#submitFlag").attr("disabled",true);
        // }
        if(msg == "no_driver"){
          $(".nsnMsg").text("Driver does not exist.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "success"){
          $(".nsnMsg").text('');
          $("#submitFlag").attr("disabled",false);
          $("#confmNSN").attr("disabled",false);
        }
      }
    });
  }

  function getNSN(){
    var pwd = $("#NSN").val();
    var Cpwd = $("#confmNSN").val();

    if(Cpwd != ""){
      if(pwd != Cpwd){
        $("#submitFlag").attr("disabled",true);
        $("#NSN").css("border-color","red");
        $("#confmNSN").css("border-color","red");
        $(".confmNsnMsg").show();
        $(".confmNsnMsg").text("The ID does not match.");
      }
      else {
        $("#submitFlag").attr("disabled",false);
        $("#NSN").css("border-color","#e8e8e8");
        $("#confmNSN").css("border-color","#e8e8e8");
        $(".confmNsnMsg").hide();
      }
    }
  }
</script>

<?php include'footer.php'; ?>