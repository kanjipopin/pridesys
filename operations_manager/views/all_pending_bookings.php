<?php include'header.php'; 

if(isset($_POST['update_returned_booking'])) {

  $returned_booking_id = $_POST['returned_booking_id'];
  $returned_date = $_POST['returned_date'];
  $Availability = "1";

  $bookingCols = array("ReturnedDate"=>$returned_date,"Availability"=>$Availability);
  $condition = "BookingId='".$returned_booking_id."'";
  $bookingTable = "booking_table";
  $updateBooking = $connection->UpdateQuery($bookingTable,$bookingCols,$condition);

  if($updateBooking=="success"){
      $connection->redirect("all_pending_bookings.php?ref=success");
  }
  else {
      $connection->redirect("all_pending_bookings.php?ref=error");
  }

}

if(isset($_POST['delete_booking'])) {

  $del_booking_id = $_POST['del_booking_id'];
 
  $bookingField = "BookingId";
  $bookingTable = "booking_table";
  $deleteBooking = $connection->DeleteRow($bookingTable,$bookingField,$del_booking_id);

  if($deleteBooking=="success"){
    $connection->redirect("all_pending_bookings.php?ref=success");
  }
  else {
      $connection->redirect("all_pending_bookings.php?ref=error");
  }

}

?>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>Active bookings (<span style="color: #6dbe48;"><?php echo $rowCount; ?></span>)</h4>

        <div class="filters">
        <div class="hidden-xs">
          <form id="filterBox" name="filterBox" class="form-inline" method="GET">
            <div class="form-inline">
              Search :
            </div>
            <div class="form-inline">
              <input type="text" class="form-control" id="searchTxt" name="searchTxt" required>
              <input type="submit" class="form-control" value="Search">
            </div>
          </form>
        </div>
        <div class="visible-xs">
          <div id="custom-search-input">
              <div class="input-group col-md-12">
              <form id="filterBox" name="filterBox" class="form-inline" method="GET">
                <input type="text" class="search-query form-control" id="searchTxt" name="searchTxt" required placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </form>
              </div>
          </div>
        </div>
        </div>
      </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="add_booking.php"><input type="button" class="form-control" id="searchBtn" name="searchBtn" value="Add" style="border-radius: 0px;width: 80px;"></a>
        </div>

        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
            <a href="all_pending_booking_calendar.php"><input type="button" class="form-control" id="viewCalendarBtn" name="viewCalendarBtn" value="View on calendar" style="border-radius: 0px;width: 150px;"></a>
        </div>

        <?php if($searchTxt != ""){ ?>
        <div class="form-inline" style="margin-bottom: 20px;margin-left: 10px;">
          <p><a href="all_pending_bookings.php">Clear Search</a></p>
        </div>
      <?php } ?>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <table class="table table-striped" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Booking ID</th>
                <th>Vehicle reg</th>
                <th>Client's name</th>
                <th>Driver's name</th>
                <th>Rent from</th>
                <th>Expected return</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              
              foreach($results as $fd) {

                $id = $fd['BookingId'];

                if($searchTxt != ""){

                  $VehicleRegID = $fd['VehicleId'];

                } else {

                  $VehicleRegID = $fd['VehicleReg'];

                }

                $stmt_vehicles = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$VehicleRegID' ");
                $stmt_vehicles->execute();
                $results_vehicle = $stmt_vehicles->fetch(PDO::FETCH_ASSOC);

                //vehicle details
                $vehicle_reg = $results_vehicle['VehicleReg'];

                
                $Driver_Type = $fd['DriverType'];

                if($Driver_Type == "Our_Driver") {

                    $driver_id = $fd['DriverId'];

                    $stmt_driver = $conn->prepare("SELECT * from logis_drivers_master WHERE drv_NSN = '{$driver_id}' ");
                    $stmt_driver->execute();
                    $results_driver = $stmt_driver->fetch();

                    $driver_type = "Our driver";
                    $driver_name = $results_driver['drv_fname']." ".$results_driver['drv_mname']." ".$results_driver['drv_lname'];
                    $driver_phone = $results_driver['drv_contact_no'];

                } else if($Driver_Type == "Clients_Driver") {

                    $driver_id = "";
                    $driver_type = "Client's driver";
                    $driver_name = $fd['DriverName'];
                    $driver_phone = $fd['DriverPhone'];

                }

                $Client_Type = $fd['ClientType'];

                if($Client_Type == "Existing") {

                    $client_id = $fd['ClientId'];

                    $stmt_client = $conn->prepare("SELECT * from logis_company_sales_rep_clients WHERE Id = '{$client_id}' ");
                    $stmt_client->execute();
                    $results_client = $stmt_client->fetch();

                    $client_type = "Our client";
                    $client_name = $results_client["ClientsName"];
                    $client_phone = $results_client["ClientsPhone"];
                    $client_email = $results_client["ClientsEmail"];

                } else if($Client_Type == "Walk_In") {

                    $client_id = "";
                    $client_type = "Walk in client";
                    $client_name = $fd['ClientName'];
                    $client_phone = $fd['ClientPhone'];
                    $client_email = $fd['ClientEmail'];

                }

                $rent_from_date = $fd['RentFrom'];
                $expected_return_date = $fd['ExpectedReturnDate'];
                $return_date = $database->now_date_only;
                $booking_additional_notes = $fd['AdditionalNotes'];
                

            ?>
              <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $vehicle_reg; ?></td>
                <td><?php echo $client_name; ?></td>
                <td><?php echo $driver_name; ?></td>
                <td><?php echo $fd['RentFrom']; ?></td>
                <td><?php echo $fd['ExpectedReturnDate']; ?></td>

                <td>

                  <a href="#returnedBookingDialog" data-bookingid="<?php echo $id; ?>" data-vehiclereg="<?php echo $vehicle_reg; ?>" 
                  data-drivertype="<?php echo $driver_type; ?>" 
                  data-drivername="<?php echo $driver_name; ?>" data-driverphone="<?php echo $driver_phone; ?>"
                   data-clienttype="<?php echo $client_type; ?>" data-clientname="<?php echo $client_name; ?>" 
                   data-clientphone="<?php echo $client_phone; ?>" data-clientemail="<?php echo $client_email; ?>" 
                   data-rentdate="<?php echo $rent_from_date; ?>" data-expecteddate="<?php echo $expected_return_date; ?>" 
                   data-returndate="<?php echo $return_date; ?>" data-additionalnotes="<?php echo $booking_additional_notes; ?>" class="openReturnedDialog btn btn-success" data-toggle="modal" style="display:block;color: white;font-weight: 400;">Mark as returned</a>

                   <br/>
                   <a href="<?php echo 'book_thankyou.php?id='.$id; ?>" class="btn btn-dark" style="display:block;color: white;font-weight: 400;">Download report
                  </a>

                  <br/>
                  <a href="#deleteBookingDialog" data-bookingid="<?php echo $id; ?>" data-vehiclereg="<?php echo $vehicle_reg; ?>" class="openDeleteDialog btn btn-danger" data-toggle="modal" style="display:block;color: white;font-weight: 400;">Delete booking</a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="resp-my-driver visible-xs">
        <?php
          foreach($results as $fd1) {
            @$img = ltrim($fd1['drv_img'], "../");
            $id = base64_encode($fd1['empdt_NSN']);
        ?>
          <div class="resp-my-driver-block">
            <div class="block-1">
              <img src="<?php echo $img; ?>" class="img-responsive">
            </div>
            <div class="block-2">
              <h5><?php echo $fd1['drv_fname'].' '.$fd1['drv_lname']; ?></h5>
              <h5><?php echo $fd1['drv_driving_license_no']; ?></h5>
              <h5><?php echo $fd1['drv_contact_no']; ?></h5>
              <a href="<?php echo 'driver-detail.php?id='.$id; ?>" class="view-profile">View Profile</a>
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers(<?php echo $rowCount; ?>)</a>
              </li>
              <li>
                <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <div class="modal fade" id="returnedBookingDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h4 class="delete-title">Mark as returned</h4>
                

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >


                  <input type="hidden" id="returned_booking_id" class="form-control" name="returned_booking_id">
                  
                  <div class="form-group">
                    <label for="returned_vehicle_reg" class="col-sm-3 control-label">Vehicle reg</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_vehicle_reg" class="form-control" name="returned_vehicle_reg" disabled>
                      </div>
                  </div>    

                  <br/>
                  <div class="form-group">
                    <label for="returned_driver_type" class="col-sm-3 control-label">Driver type</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_driver_type" class="form-control" name="returned_driver_type" disabled>
                      </div>
                  </div>    

                  <br/>
                  <div class="form-group">
                    <label for="returned_driver_name" class="col-sm-3 control-label">Driver name</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_driver_name" class="form-control" name="returned_driver_name" disabled>
                      </div>
                  </div>    
                  
                  <br/>
                  <div class="form-group">
                    <label for="returned_driver_phone" class="col-sm-3 control-label">Driver phone</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_driver_phone" class="form-control" name="returned_driver_phone" disabled>
                      </div>
                  </div>

                  <br/>
                  <div class="form-group">
                    <label for="returned_client_type" class="col-sm-3 control-label">Client type</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_client_type" class="form-control" name="returned_client_type" disabled>
                      </div>
                  </div>
                  
                  <br/>
                  <div class="form-group">
                    <label for="returned_client_name" class="col-sm-3 control-label">Client name</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_client_name" class="form-control" name="returned_client_name" disabled>
                      </div>
                  </div>

                  <br/>
                  <div class="form-group">
                    <label for="returned_client_phone" class="col-sm-3 control-label">Client phone</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_client_phone" class="form-control" name="returned_client_phone" disabled>
                    </div>
                  </div>

                  <br/>
                  <div class="form-group">
                    <label for="returned_client_email" class="col-sm-3 control-label">Client email</label>
                      <div class="col-sm-9">
                          <input type="text" id="returned_client_email" class="form-control" name="returned_client_email" disabled>
                      </div>
                  </div>    

                  <br/>
                  <div class="form-group">
                    <label for="returned_rent_from_date" class="col-sm-3 control-label">Rent from</label>
                      <div class="col-sm-9">
                        <input type="text" id="returned_rent_from_date" class="form-control" name="returned_rent_from_date" disabled>
                      </div>
                  </div>
                  <br/>

                  <div class="form-group">
                    <label for="returned_expected_returned_date" class="col-sm-3 control-label">Expected return date</label>
                    <div class="col-sm-9">
                      <input type="text" id="returned_expected_returned_date" class="form-control" name="returned_expected_returned_date" disabled>
                    </div>  
                  </div>

                  <br/>
                  <div class="form-group">
                    <label for="returned_additional_notes" class="col-sm-3 control-label">Additional notes</label>
                    <div class="col-sm-9">
                      <textarea  class="form-control col-sm-9" id="returned_additional_notes" name="returned_additional_notes" disabled></textarea>
                    </div>
                  </div>  
                    
                  <br/>
                  <div class="form-group">
                      <label for="returned_date" class="col-sm-3 control-label">Returned on</label>
                      <div class="col-sm-9">
                        <input type="date" id="returned_date" class="form-control" name="returned_date" >
                      </div>
                  </div>

                  <input type="submit" style="float: right; " name="update_returned_booking" value="Mark as returned" class="btn btn-success">
                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>   
  
  <div class="modal fade" id="deleteBookingDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Delete booking order</h2>
                
                <p class="delete-p">Are you sure you want to delete this booking order?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="text" id="del_booking_id" class="form-control" name="del_booking_id" >
                    
                  <input type="submit" style="float: right; " name="delete_booking" value="Delete" class="btn btn-danger">

                </form>
              
                <button type="button" class="btn btn-dark center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>
  <script>

  $(document).on("click", ".openDeleteDialog", function () {

            var del_booking_id = $(this).data('bookingid');
            var del_vehicle_reg = $(this).data('vehiclereg');
            $("#del_booking_id").attr("value", del_booking_id);

  });
    
    $(document).on("click", ".openReturnedDialog", function () {

      
            var returned_booking_id = $(this).data('bookingid');
            var returned_vehicle_reg = $(this).data('vehiclereg');
            var returned_driver_type = $(this).data('drivertype');
            var returned_driver_name = $(this).data('drivername');
            var returned_driver_phone = $(this).data('driverphone');
            var returned_client_type = $(this).data('clienttype');
            var returned_client_name = $(this).data('clientname');
            var returned_client_phone = $(this).data('clientphone');
            var returned_client_email = $(this).data('clientemail');
            var returned_rent_from_date = $(this).data('rentdate');
            var returned_expected_returned_date = $(this).data('expecteddate');
            var returned_date = $(this).data('returndate');
            var returned_additional_notes = $(this).data('additionalnotes');


            $("#returned_booking_id").attr("value", returned_booking_id);
            $("#returned_vehicle_reg").attr("value", returned_vehicle_reg);
            $("#returned_driver_type").attr("value", returned_driver_type);
            $("#returned_driver_name").attr("value", returned_driver_name);
            $("#returned_driver_phone").attr("value", returned_driver_phone);
            $("#returned_client_type").attr("value", returned_client_type);
            $("#returned_client_name").attr("value", returned_client_name);
            $("#returned_client_phone").attr("value", returned_client_phone);
            $("#returned_client_email").attr("value", returned_client_email);
            $("#returned_rent_from_date").attr("value", returned_rent_from_date);
            $("#returned_expected_returned_date").attr("value", returned_expected_returned_date);
            $("#returned_date").attr("value", returned_date);
            $("#returned_additional_notes").text(returned_additional_notes);

  }); 

  </script>

<?php include'footer.php'; ?>