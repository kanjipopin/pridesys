<?php

@$Email = $_SESSION['operations_manager'];

$comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$comp->execute();
$comprow = $comp->fetch();

$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
$error_msg = isset($_GET['error_msg']) ? $_GET['error_msg'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}


include 'header.php';
?>

    <div class="page-rightWidth">
        <div class="add-driver-page">
            <div class="heading">
                <h4>Sub admin</h4>
                <div class="filters">
                    <div class="form-inline">
                        <a href="sub_admin.php" type="button" class="btn btn-default" style="background: #f8f0e8;border-radius: 0; border-color: #ede3d9;text-shadow: none;box-shadow: none;margin-right: 12px;padding: 6px 20px;color: #9a9188;">Back</a>
                    </div>
                </div>
            </div>

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($error_msg)){ echo $error_msg; } ?></p>

            <div class="addDriver-form">
                <div id="alertBox" class="alert hidden alert-message"></div>
                <form class="form-horizontal" enctype="multipart/form-data" action="functions/add_sub_admin.php" method="POST" id="form" name="form">
          <span class="show_driver_details">
            <h4>Add sub admin</h4>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="SubAdminName" id="SubAdminName" placeholder="Name" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="SubAdminEmail" id="SubAdminEmail" placeholder="Email" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Position :</label>
              <div class="col-sm-6">

                  <select class="form-control" id="SubAdminPosition" name="SubAdminPosition">
                    <option value="" selected disabled>Select sub admin position</option>

                        <option value ="head_of_sales_and_marketing">Head of sales and marketing</option>

                  </select>

              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Department :</label>
              <div class="col-sm-6">

                  <select class="form-control" id="SubAdminDepartment" name="SubAdminDepartment">
                    <option value="" selected disabled>Select sub admin department</option>

                        <option value ="sales_and_marketing">Sales and marketing</option>

                  </select>

              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Password :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="SubAdminPassword" id="SubAdminPassword" placeholder="Password" >

              </div>

            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Confirm Password :</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="SubAdminConfirmPassword" id="SubAdminConfirmPassword" placeholder="Confirm password" >

              </div>

            </div>


          </span>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="submit" name="submit" class="btn btn-default" style="border-color: #F00;background: #F00;">Create account</button>
                        </div>
                    </div>

                </form>

                <!-- CLOSE ADD BRANCH - POPUP -->
            </div>

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
                    </li>
                    <li>
                        <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
                    </li>
                    <li>
                        <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
            //   $('#insuranceExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#inspectionExpDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#commenced_startDate').datepicker({ format: "yyyy/mm/dd" });
            //   $('#driver_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
            //   $('#vehicle_port_expire_date').datepicker({ format: "yyyy/mm/dd" });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function checkVehicleReg(){
            var VehicleReg = $("#VehicleReg").val();
            var compId = $("#compId").val();

            $.ajax({
                type : "post",
                url : "functions/ajax.php",
                data : { "type":"checkDuplicateVehicleReg","VehicleReg":VehicleReg,"compId":compId },

                success : function(msg){

                    if(msg == "same"){
                        $(".CheckVehicleRegMsg").text("Vehicle already exists in your account.");
                        $("#submit").attr("disabled",true);
                    } else if(msg == "different") {
                        $(".CheckVehicleRegMsg").text("Vehicle already exists on another account.");
                        $("#submit").attr("disabled",true);
                    } else {
                        $(".CheckVehicleRegMsg").text("");
                        $("#submit").attr("disabled",false);
                    }
                }
            });
        }

        function getDept(){
            $(".load_branch").hide();
            $.ajax({
                type: "POST",
                url:"functions/ajaxaddmaster.php",
                data: $('#addDeptt').serialize(),
                success: function(msg){
                    // alert(msg);
                    $(".old_branch").hide();
                    $(".load_branch").show();
                    $(".load_branch").html(msg);
                    $('#addbranch').modal('hide');
                },
            });
        }



        $(document).ready(function() {
            $('#nssf').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nssf1').val(txtVal);
            });


            $('input[type=radio][name=Vehicle_Type]').change(function() {

                if(this.value === "Other") {

                    document.getElementById("toggle_Plat_Number").style.display = "none";

                } else {

                    document.getElementById("toggle_Plat_Number").style.display = "block";

                }

            });

            $('#nhifNo').keyup(function(e) {
                var txtVal = $(this).val();
                txtVal2 = txtVal;
                $('#nhifNo1').val(txtVal);
            });
        });


        function getSecondKin(){
            $(".kinTwo").show();
            $("#secondKinBtn").hide();
        }

        function getThirdKin(){
            $(".kinThree").show();
            $("#ThirdKinBtn").hide();
            $("#secondKinBtnDel").hide();
        }

        function getSecondKinHide(){
            $(".kinTwo").hide();
            $("#secondKinBtn").show();
            $("#secondKinBtnDel").show();
        }

        function getThirdKinHide(){
            $(".kinThree").hide();
            $("#ThirdKinBtn").show();
            $("#secondKinBtnDel").show();
        }
    </script>

    <!-- <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script>
    $( "#form" ).validate({
      rules: {
        email: {
          email: true
        },
       }
    });
    </script> -->


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#lstFruits').multiselect({
        //         includeSelectAllOption: true
        //     });
        // });
    </script>

<?php include'footer.php'; ?>