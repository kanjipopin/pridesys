<?php 
  include'header.php';

  if(isset($_REQUEST["RatingBtn"])){

    $rating = isset($_REQUEST['rating']) ? $_REQUEST['rating'] : "";
    $reportID = isset($_REQUEST['reportID']) ? $_REQUEST['reportID'] : "";
    $confirmed = isset($_REQUEST['confirmed']) ? $_REQUEST['confirmed'] : "";

    // $driverSignTxt = isset($_REQUEST['driverSignTxt']) ? $_REQUEST['driverSignTxt'] : "";
    // $employerSignTxt = isset($_REQUEST['employerSignTxt']) ? $_REQUEST['employerSignTxt'] : "";

    $comp = $conn->prepare("SELECT * from logis_reports where report_id IN ($reportID)");
    $comp->execute();
    $comprow = $comp->fetch();

    @$driver = $_SESSION['driver'];
    @$from_date = date("Y-m-d H:i:s", strtotime($_SESSION['from_date']));
    @$to_date = date("Y-m-d H:i:s", strtotime($_SESSION['to_date']));
    $comp_id = $comprow['company_id'];

    $insertData = $conn->prepare("INSERT into logis_report_performance 
      (driver_id,company_id,rating,confirmed,report_start_date,report_end_date) 
      values ('$driver','$comp_id','$rating','$confirmed','$from_date','$to_date')");
    $insertData->execute();

    $latsID = $conn->lastInsertId(); 

    $report_id = "";
    $checkDriver = $conn->prepare("SELECT * from logis_reports where driver_name = '{$driver}' and company_id = '{$comprow['comp_id']}' and report_date >= '{$from_date}' and report_date <= '{$to_date}'");
    $checkDriver->execute();
    while($checkDriverRow = $checkDriver->fetch()){
      $report_id .= $checkDriverRow['report_id'].",";
    }
      $trimreport_id = rtrim($report_id,",");


    header("location: reports_performance.php?id=".$reportID."&type=print&performancecount=".$rating."&pid=".$latsID);
  }

  @$pid = $_GET['pid'];

  $getPerformanceData = $conn->prepare("SELECT * from logis_report_performance where report_id = '{$pid}'");
  $getPerformanceData->execute();
  $getPerformanceDataRow = $getPerformanceData->fetch();
?>

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="css/datepicker.css">
  <style type="text/css">
    table.dataTable.no-footer {
      border-bottom: 1px solid #ffe2b0;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current { background: #ffe2b0;border: 1px solid #ffe2b0; }

    .wrapper { position: relative;width: 300px;height: 170px;-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;user-select: none; }
    .signature-pad { position: absolute;left: 0;top: 0;width:300px;height:170px;background-color: white; }
  </style>

  <div class="page-rightWidth">
    <div class="drivers-detail driver-page">
      <div class="heading">
        <h4>Performance Report</h4>
      </div>

        <div class="table-responsive detail-table hover-css hidden-xs">
          <?php
            if($getReportDetailsCount > 0){
          ?>
          <table class="table table-striped table-hover" name="driverList" id="driverList">
            <thead>
              <tr>
                <th>Date</th>
                <th>Vehicle</th>
                <th>Driver</th>
                <th>Report type</th>
                <th>Report details</th>
                <th>Report status</th>
              </tr>
            </thead>

            <tbody id="showDriverData">
            <?php
              foreach($getReportDetailsRow as $getReportDetailsRow1) {

                $getDriverName = $conn->prepare("SELECT * from logis_drivers_master where drv_id = '{$getReportDetailsRow1['driver_name']}'");
                $getDriverName->execute();
                $getDriverNameRow = $getDriverName->fetch();
            ?>
              <tr>
                <td><?php echo date("M d, Y", strtotime($getReportDetailsRow1['report_date'])); ?></td>
                <td><?php echo $getReportDetailsRow1['vehicle']; ?></td>
                <td class="drv"><?php echo $getDriverNameRow['drv_fname'].' '.$getDriverNameRow['drv_lname']; ?></td>
                <td><?php echo $getReportDetailsRow1['report_type']; ?></td>
                <td><?php echo $getReportDetailsRow1['report_details']; ?></td>
                <td><?php echo $getReportDetailsRow1['report_status']; ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>

          <?php if($report_type == "print"){ ?>
            <p style="padding-left: 15px;">Total Rating : <?php echo @$_GET['performancecount']; ?></p>
            <p style="padding-left: 15px;">Confirmed</p>
            <hr>



            <!-- <div class="form-group">
              <div class="col-md-6">
                <p>Driver Sign :</p>
                <img src="<?php// echo $getPerformanceDataRow['driver_sign']; ?>">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6">
                <p>Employer Sign :</p>
                <img src="<?php// echo $getPerformanceDataRow['employer_sign']; ?>">
              </div>
            </div> -->


            <a href="pdf_order_performance.php?type=pdf&id=<?php echo base64_encode($reprot_id); ?>&pid=<?php echo base64_encode($perfomance_id); ?>"><button type="button" class="btn btn-default" style="margin-left: 15px;background: #ffe2b0;border-color: #ffe2b0;color: #515151;">Print Performance</button></a>
          <?php } ?>

          <?php if($report_type == "Give_report"){ ?>
            <form name="setRatingForm" id="setRatingForm" action="#" method="POST" style="padding-left: 15px;">

              <div class="form-group">
                <input type="hidden" name="reportID" id="reportID" value="<?php echo $reprot_id; ?>">
                <input type="text" name="rating" id="rating" class="form-control" placeholder="Rating" style="width: 300px;" required>
              </div>

              <input type="checkbox" name="confirmed" id="confirmed" value="confirmed" checked required> Confirm

              <!-- <div class="form-group">
                <div class="col-md-6">
                  <p>Driver Signature</p>
                  <div class="wrapper">
                    <canvas id="driver_signature_pad" class="driver_signature_pad" style="border: 3px solid rgb(255, 226, 176);"></canvas>
                  </div>
                  <button id="drv_save_jpeg" type="button" style="border: none;padding: 6px;background: #ec2326;color: #fff;">Generate Driver Sign</button>
                  <button id="drv_clear" type="button" style="border: none;padding: 6px;background: #ec2326;color: #fff;">Clear</button>
                </div>

                <div class="col-md-6">
                  <p>Employer Signature</p>
                  <div class="wrapper">
                    <canvas id="employer_signature_pad" class="employer_signature_pad" style="border: 3px solid rgb(255, 226, 176);"></canvas>
                  </div>
                  <button id="emp_save_jpeg" type="button" style="border: none;padding: 6px;background: #ec2326;color: #fff;">Generate Employer Sign</button>
                  <button id="emp_clear" type="button" style="border: none;padding: 6px;background: #ec2326;color: #fff;">Clear</button>
                </div>
              </div> -->

              <!-- <div class="form-group">
                <div class="col-md-6">
                  <div class="img">
                    <img src="" class="driverSign" class="img-responsive">
                    <input type="hidden" name="driverSignTxt" id="driverSignTxt">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="img">
                    <img src="" class="employerSign" class="img-responsive">
                    <input type="hidden" name="employerSignTxt" id="employerSignTxt">
                  </div>
                </div>
              </div> -->

              <div class="clearfix"></div>

              <div class="form-group" style="margin-top: 20px;">
                <button type="submit" name="RatingBtn" id="RatingBtn" class="btn btn-default">Submit</button>
              </div>
            </form>
          <?php } ?>

          <?php } else { ?>
            <p style="font-size: 20px;padding: 20px;color: red;">No Record Found</p>
          <?php } ?>
        </div>
      </div>
    </div>

      <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
              </li>
              <li>
                <a class="active-class" href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
              </li>
              <li>
                <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
              </li>
            </ul>
          </div>
        <!-- /.sidebar-collapse -->
        </div>
      </div>
    </div>
  </div>

  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/metisMenu.min.js"></script>
  <script src="../js/sb-admin-2.js"></script>
  <!-- Back To Top -->
  <script src="../js/showup.js"></script>

  <script src="js/signature_pad.js"></script>
  <script type="text/javascript">
    var canvas = document.getElementById('driver_signature_pad');

    function resizeCanvas() {
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }

    window.onresize = resizeCanvas;
    resizeCanvas();

    var signaturePad = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255)'
    });

    document.getElementById('drv_save_jpeg').addEventListener('click', function () {
      if (signaturePad.isEmpty()) {
        return alert("Please provide a signature first.");
      }

      var data = signaturePad.toDataURL('image/jpeg');
      $(".driverSign").show();
      $(".driverSign").attr('src',data);

      $("#driverSignTxt").val(data);
    });

    document.getElementById('drv_clear').addEventListener('click', function () {
      signaturePad.clear();
      $(".driverSign").hide();
      $(".driverSign").attr('src','');

      $("#driverSignTxt").val('');
    });
    </script>

    <script src="js/signature_pad1.js"></script>
    <script type="text/javascript">
    var canvas = document.getElementById('employer_signature_pad');

    function resizeCanvas() {
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }

    window.onresize = resizeCanvas;
    resizeCanvas();

    var signaturePad1 = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255)'
    });

    document.getElementById('emp_save_jpeg').addEventListener('click', function () {
      if (signaturePad1.isEmpty()) {
        return alert("Please provide a signature first.");
      }

      var data = signaturePad1.toDataURL('image/jpeg');
      $(".employerSign").show();
      $(".employerSign").attr('src',data);

      $("#employerSignTxt").val(data);
    });

    document.getElementById('emp_clear').addEventListener('click', function () {
      signaturePad1.clear();
      $(".employerSign").hide();
      $(".employerSign").attr('src','');

      $("#employerSignTxt").val('');
    });
  </script>


  <script>
    // $('tr[data-href]').on("click", function() {
    //   document.location = $(this).data('href');
    // });

    // $("#filterForm").on('click',function(e){
    //   var data="";
    //   e.preventDefault();
    //   var drvName = $("#drvName").val();
    //   var drvLicenseNum = $("#dln").val();
    //   var contactNo = $("#contact").val();
    //   //alert("sjk: "+drvName +" name:"+drvLicenseNum+" contact:"+contactNo);

    //   $.ajax({
    //     type : "POST",
    //     url  : "functions/drivers.php",
    //     data : {"filter":"yes","drvName":drvName,"drvLicense":drvLicenseNum,"contactNo":contactNo},
    //     success : function(msg){
    //        // var msg = msg;
    //        //alert("this message:"+msg);
    //        $("#showDriverData").html("");
    //        $("#showDriverData").html(msg);
    //     }
    //   });
    // });
  </script>

  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    // $('#driverList').DataTable({
    //   "iDisplayLength": 30,
    //   "bLengthChange": false,
    //   "info":     false,
    //   "bFilter": false,
    //   "order": [[ 0, "desc" ]],
    //   "columnDefs": [
    //     { targets: [1, 2, 3, 4], orderable: false}
    //   ]
    // });

    // $(document).ready(function() {
    //   // SEARCH BY COMPANY
    //   $('#selectDriver').off('change');
    //   $('#selectDriver').on('change', function() {
    //       // Your search term, the value of the input
    //       var searchTerm = $('#selectDriver').val();
    //       // table rows, array
    //       var tr = [];

    //       // Loop through all TD elements
    //       $('#driverList').find('.drv').each(function() {
    //           var value = $(this).html();
    //           // if value contains searchterm, add these rows to the array
    //           if (value.includes(searchTerm)) {
    //               tr.push($(this).closest('tr'));
    //           }
    //       });

    //       // If search is empty, show all rows
    //       if ( searchTerm == '') {
    //           $('tr').show();
    //       } else {
    //           // Else, hide all rows except those added to the array
    //           $('tr').not('thead tr').hide();
    //           tr.forEach(function(el) {
    //               el.show();
    //           });
    //       }
    //   });


    //   $("#from_date").datepicker();
    //   $("#to_date").datepicker();
    // });
  </script>

<?php include'footer.php'; ?>