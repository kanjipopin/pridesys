<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['operations_manager'];
  $id = base64_decode($_GET['id']);

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $getReportsData = $conn->prepare("SELECT * from logis_reports where report_id = '{$id}'");
  $getReportsData->execute();
  $getReportsDataRow = $getReportsData->fetch();

  if(isset($_REQUEST['submitReports'])){

    $report_status = $_REQUEST['report_status'];
    $date = date("Y-m-d H:i:s");

    $updateReport = $conn->prepare("UPDATE logis_reports set report_status='$report_status',report_update='$date' where report_id = '$id'");
    $updateReport->execute();

    header("location: reports.php");
  }
?>

<div class="page-rightWidth">
  <div class="flag-driver-page">
      <div class="heading">
        <h4>Add Reports</h4>
        <div class="filters">
          <div class="form-inline">
            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 15px;color: #897e74;font-size: 13px;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="#" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Driver : </label>
            <div class="col-sm-5">
             <?php
                $driverData = $conn->prepare("SELECT * from logis_drivers_master where driver_work_status = 'working' and drv_curr_comp_id = '{$comprow['comp_id']}' and drv_id = '{$getReportsDataRow['driver_name']}'");
                $driverData->execute();
                $driverDataRow = $driverData->fetch();

                echo $driverDataRow['drv_fname']." ".$driverDataRow['drv_lname'];
            ?>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Vehicle : </label>
            <div class="col-sm-5">
              <?php echo $getReportsDataRow['vehicle']; ?>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Report Type :</label>
            <div class="col-sm-5">
              <?php echo $getReportsDataRow['report_type']; ?>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Report File :</label>
            <div class="col-sm-5">
            <?php
              $reportFile = explode("|", $getReportsDataRow['report_file']);
              $reportFileCount = count($reportFile);
              
              for($i=0; $i<$reportFileCount; $i++){
                if($reportFile[$i] != ""){   
            ?>                
              <a href="<?php echo @$report_file = ltrim($reportFile[$i], "../"); ?>" download>Download</a> | 
              <a href="<?php echo @$report_file = ltrim($reportFile[$i], "../"); ?>" target="_blank">View</a>
            <?php } else { echo "No file found"; } } ?> 
            <!-- <small style="border-bottom: 1px dotted #777;color: #777;cursor: pointer;" class="addReportFile">Add more</small> -->

            <input type="file" class="form-control report_fileType" name="report_file[]" id="report_file" multiple style="padding-bottom: 45px;display: none;margin-top: 10px;">
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Abstract File :</label>
            <div class="col-sm-5">
            <?php
              $reportAbsFile = explode("|", $getReportsDataRow['report_abstract_file']);
              $reportAbsFileCount = count($reportAbsFile);

              for($i=0; $i<$reportAbsFileCount; $i++){
                if($reportAbsFile[$i] != ""){
            ?>                
              <a href="<?php echo @$report_abstract_file = ltrim($reportAbsFile[$i], "../"); ?>" download>Download</a> |
              <a href="<?php echo @$report_abstract_file = ltrim($reportAbsFile[$i], "../"); ?>" target="_blank">View</a> 
            <?php } else { echo "No file found"; } } ?>
            <!-- <small style="border-bottom: 1px dotted #777;color: #777;cursor: pointer;" class="addReportAbstractFile">Add more</small> -->

            <input type="file" class="form-control report_abstract_fileType" name="report_abstract_file[]" id="report_abstract_file" multiple style="padding-bottom: 45px;display: none;margin-top: 10px;">
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Details :</label>
            <div class="col-sm-5">
              <?php echo $getReportsDataRow['report_details']; ?>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Status :</label>
            <div class="col-sm-5">
              <textarea name="report_status" id="report_status" class="form-control" placeholder="Enter Status *" rows="5" required><?php echo $getReportsDataRow['report_status']; ?></textarea>
            </div>
          </div>

          <div class="modal-footer">
            <button type="submit" name="submitReports" id="submitReports" class="btn btn-default">Submit</button>

            <a href="reports.php" class="btn btn-default">Back</a>
          </div>
        </form>
      </div>
  </div>
</div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a class="active-class" href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  $(".addReportFile").click(function () {
    $(".report_fileType").slideToggle();
  });

  $(".addReportAbstractFile").click(function () {
    $(".report_abstract_fileType").slideToggle();
  });
</script>

<?php include'footer.php'; ?>