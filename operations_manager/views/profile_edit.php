<?php 
  include'header.php';

  $getDetails = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $getDetails->execute();
  $getDetailsRow = $getDetails->fetch();

  $msg = "";
  $action = isset($_GET['action']) ? $_GET['action'] : "";
  if($action == "updated"){
    $msg = "Profile updated successfully.";
  }
?>

<!-- datepicker -->
<link rel="stylesheet" href="css/wbn-datepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="page-rightWidth">
  <div class="enterprise-updateSignup">
    <div class="heading">
      <h4>Company Profile <span style="color: #ed1f24;">Edit Mode</span></h4>
      <div class="filters">
        <!-- <div class="form-inline"> -->
          <!-- <button type="button" class="btn btn-default">Add</button> -->
          <!-- <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 20px;width:140px;color: #897e74;">< Back</a> -->
        <!-- </div> -->
      </div>
    </div>
    <div style="background: #FFF;padding: 15px 0;">
      <div class="row">
        <div class="col-sm-12">
        
          <div class="col-sm-6 col-sm-offset-3">
            <small style="color: red;font-size: 15px;font-weight: 600;display: block;margin-bottom: 20px;"><?php if(!empty($msg)){ echo $msg; } ?></small>

            <form enctype="multipart/form-data" method="POST" action="functions/profile.php" id="login_form" name="login_form">

            <input type="hidden" name="comp_id" id="comp_id" value="<?php echo $getDetailsRow['comp_id']; ?>">

              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-building"></i></div>
                <input type="text" name="comp_name" id="comp_name" placeholder="Company Name *" class="form-control" required value="<?php echo $getDetailsRow['comp_name']; ?>">
              </div>

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="text" name="comp_position" id="comp_position" placeholder="Position *" class="form-control" required value="<?php //echo $getDetailsRow['position']; ?>">
              </div> -->

              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-id-badge"></i></div>
                <input type="text" name="comp_pin" id="comp_pin" placeholder="Tax Indetification Number *" class="form-control" style="margin-bottom: 0px;text-transform: uppercase;" required value="<?php echo $getDetailsRow['comp_pin']; ?>" readonly> 
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></div>
                <select name="location" id="location" class="form-control" required>
                  <option value="" selected disabled>Select a City</option>
                  <?php
                    $city = $conn->prepare("SELECT * from kenya_city");
                    $city->execute();
                    while($cityRow = $city->fetch()){
                      $cityChek = "";
                      if($cityRow['city_name'] == $getDetailsRow['location']){
                        $cityChek = "selected";
                      } else {
                        $cityChek = "";
                      }
                  ?>
                    <option value = "<?php echo $cityRow['city_name']; ?>" <?php echo $cityChek; ?>><?php echo $cityRow['city_name']; ?></option>
                  <?php } ?>
                </select>
              </div>

            <!--   <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                <textarea name="comp_address" id="comp_address" placeholder="1st Floor, ABC Building, Road/Street *" class="form-control" required><?php// echo $getDetailsRow['address']; ?></textarea>
              </div> -->

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                <!-- <textarea name="comp_address" id="comp_address" placeholder="1st Floor, ABC Building, Road/Street *" class="form-control" required></textarea> -->
                <input type="text" name="address1" id="address1" placeholder="Address line 1" class="form-control" value="<?php echo $getDetailsRow['address1']; ?>" required>
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                <input type="text" name="address2" id="address2" placeholder="Address line 2" class="form-control" value="<?php echo $getDetailsRow['address2']; ?>" required>
              </div>

              <?php
                $explode = explode("-", $getDetailsRow['po_box']);
                $po_box = $explode[0];
                $po_box1 = $explode[1];
              ?>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                <input type="text" name="po_box" id="po_box" placeholder="P.O.Box *" class="form-control" onkeypress="return isNumber(event)" maxlength="5" required value="<?php echo $po_box; ?>">
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div>
                <input type="text" name="po_box1" id="po_box1" placeholder="Post Code *" class="form-control" onkeypress="return isNumber(event)" maxlength="5" required value="<?php echo $po_box1; ?>">
              </div>

              <!-- <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-id-card"></i></div>
                  <input type="text" name="comp_NSN" id="comp_NSN" placeholder="National ID *" class="form-control" onkeypress="return isNumber(event)" style="margin-bottom: 0px;" required value="<?php// echo $getDetailsRow['emp_nsn']; ?>">
              </div> -->

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                  <input type="text" name="comp_fname" id="comp_fname" placeholder="First Name *" class="form-control required" value="<?php// echo $getDetailsRow['fname']; ?>">
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="text" name="comp_mname" id="comp_mname" placeholder="Middle Name (optional)" class="form-control" required value="<?php //echo $getDetailsRow['mname']; ?>">
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <input type="text" name="comp_lname" id="comp_lname" placeholder="Last Name *" class="form-control" required value="<?php// echo $getDetailsRow['lname']; ?>">
              </div> -->

              <!-- <div class="input-group">
                <div class="btn-group" role="group" aria-label="...">
                <label>Select Gender: </label>
                <input type="radio" name="genderVal" id="genderVal" value="male" <?php //if($getDetailsRow['gender'] == "male"){ echo "checked"; } ?> class="radio-inline">Male
                <input type="radio" name="genderVal" id="genderVal" value="female" <?php// if($getDetailsRow['gender'] == "female"){ echo "checked"; } ?> class="radio-inline">Female
                </div>
              </div> -->

              <!-- <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                <input type="text" id="comp_dob" name="comp_dob" class="form-control wbn-datepicker" data-min="1960-01-01" data-max="2000-01-01" required value="<?php// echo $getDetailsRow['emp_dob']; ?>"/>
              </div> -->

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
                <select style="z-index: 0;" name="contactExtn" id="contactExtn" class="form-control">
                  <option value="" disabled>Select Country Code</option>
                  <?php
                    $slctCode = $getDetailsRow['contact_code'];
                    $getContryCode = $conn->prepare("SELECT * from country");
                    $getContryCode->execute();
                    while($getContryCodeRow = $getContryCode->fetch()){
                      $code = $getContryCodeRow['phonecode'];
                  ?>
                  <option value="<?php echo $code; ?>"<?php if($slctCode == $code){ echo "selected"; } ?>><?php echo $getContryCodeRow['nicename']." (+".$code.")"; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
                <input style="z-index: 0;" type="text" class="form-control" id="contacts" name="contacts" placeholder="Contact Number" onkeypress="return isNumber(event);" required value="<?php echo $getDetailsRow['contact_num']; ?>">
              </div>

              <div class="input-group">
                <!-- <div class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div> -->
                <!-- <input style="z-index: 0;" type="text" name="email" id="email" placeholder="Email ID *" class="form-control" onchange="isEmail(event);" readonly required value="<?php// echo $getDetailsRow['Email']; ?>"> -->
                <p style="z-index: 0;"><strong>Email :</strong> <?php echo $getDetailsRow['Email']; ?></p>
                <a href="change_email.php"><p><u>Change your email address</u></p></a>
              </div>

              <div class="input-group">
                <p style="z-index: 0;"><strong>Password : </strong><a href="update_password.php"><u>Change your Password</u></a></p>
              </div>

              <p id="terms_tag">* indicates required fields</p>
              <!-- <p id="terms_tag">By creating a Dereva account you agree to our <a href="../terms-of-service.php" style="color: #ff0200">Terms of Service</a></p> -->

              <div class="clearfix"></div>
              <input type="submit" id="submit" name="submit" class="btn btn-default form-btn" value="Update Profile">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

</div>
</div>


  <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script> 
$(document).load(function(){
     location.replace('profile.php');
 });

$(document).ready(function(){
 
 $("#alertBox").hide();
    
  var timeoutID = null;    
    //$("#prodSelect").hide();
  var url = window.location.href;
  var msg = url.substring(url.indexOf('?')+1);
  console.log(msg+ ' ');
  if(msg=="suc"){    
    $("#alertBox").html("Profile updated successfully");
    $("#alertBox").addClass("alert-success");
    $("#alertBox").show();
  }
  else if(msg=="er-up-img"){    
       
    $("#alertBox").html("Sorry there was an error while uploading your logo. Try again after some time");
    $("#alertBox").addClass("alert-danger");
    $("#alertBox").show(); 
  }
  else if(msg=="er-up-data" || msg=="er-up-pdt"){    
       
    $("#alertBox").html("Sorry there was an error while updating your profile details. Try again after some time");
    $("#alertBox").addClass("alert-danger");
    $("#alertBox").show(); 
  }
});
</script>

<script src="js/wbn-datepicker.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('.wbn-datepicker').datepicker()
  })
</script>

<?php include'footer.php';?>