<?php 
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['operations_manager'];
  if($Email == ""){
    $connection->redirect('../index.php');
  }

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $prvsDay = date("Y-m-d", strtotime("- 30 days"));
  $nextDay = date("Y-m-d", strtotime("+ 30 days"));

  $getDriver = $conn->prepare("SELECT * from logis_drivers_master where drv_dl_expiry_date > '$prvsDay' and drv_dl_expiry_date < '$nextDay' and driver_work_status = 'working' and delete_flag = 'No' and drv_curr_comp_id = '{$comprow['comp_id']}'");
  $getDriver->execute();
  $getDriverCount = $getDriver->rowCount();
  
?>

  <div class="page-rightWidth"> 
    <div class="dashboard-page dashboard-other-page">
      <h3>Driving Licences Expiring (<span style="color: #ed1e25;font-weight: 700;"><?php echo $getDriverCount; ?></span>)</h3>
      <a href="dashboard.php" type="button" class="btn btn-default backBtn">< Back</a>
      <div class="clearfix"></div>
      <div class="resp-driver-details">
      <?php
        if($getDriverCount > 0){
          while($getDriverRow = $getDriver->fetch()){
      ?>
        <div class="resp-driver-details-block">
          <h4><span class="span1">Driver:</span><span class="span2"><?php echo $getDriverRow['drv_fname']; ?></span></h4>
          <h4><span class="span1">Contact:</span><span class="span2"><?php echo $getDriverRow['drv_contact_no']; ?></span></h4>
          <h4><span class="span1">DL Ref:</span><span class="span2"><?php echo $getDriverRow['drv_driving_license_no']; ?></span></h4>
          <h4><span class="span1">License Expires On:</span><span class="span2"><?php echo date("d M y", strtotime($getDriverRow['drv_dl_expiry_date'])); ?></span></h4>
        </div>
      <?php } } else { ?>
        <div class="resp-driver-details-block">
          <p style="font-size: 18px;">No result found.</p>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>

  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a class="<?php if(@$activeClass == "Dashboard") { echo "active-class"; } ?>" href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>

</div>
</div>
   
<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/metisMenu.min.js"></script>
<script src="../js/sb-admin-2.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});
</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script>
  $(document).ready(function() {
    $('.counter').counterUp({
        delay: 8,
        time: 1000
    });

  if($('#contoday1').val()!==null){

    var bfortotal = daydiff(parseDate($('#contoday1').val()), parseDate($('#conexpiry1').val()));
  }

  var tostr = bfortotal.toString();
  var remove = tostr.replace("-","");

  //$("#remain_contractday1").text("expired "+remove+" days ago");

  var bfortotals = daydiff(parseDate($('#today1').val()), parseDate($('#expiry1').val()));
  // var bfortotals = "-4";

  var tostr = bfortotals.toString();
  var remove = tostr.replace("-","");

 // $("#remain_day1").text("expiried "+remove+" days ago");

});

/////////////////
//  DL EXPIRY //
////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(today, expiry) {
    return Math.round((expiry - today)/(1000*60*60*24));
}
var total = daydiff(parseDate($('#today').val()), parseDate($('#expiry').val()));
$("#remain_day").text(total+" days left");


///////////////////
//  DL EXPIRIED //
//////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(today1, expiry1) {
    return Math.round((expiry1 - today1)/(1000*60*60*24));
}

$(document).ready(function() {
});



///////////////////////
//  CONTARCT EXPIRY //
//////////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(contoday, conexpiry) {
    return Math.round((conexpiry - contoday)/(1000*60*60*24));
}
var total = daydiff(parseDate($('#contoday').val()), parseDate($('#conexpiry').val()));
$("#remain_contractday").text(total+" days left");


///////////////////
//  DL EXPIRIED //
//////////////////
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
function daydiff(contoday1, conexpiry1) {
    return Math.round((conexpiry1 - contoday1)/(1000*60*60*24));
}
</script>
<script type="text/javascript">
  

function sticky_relocate() {
    var footer_top = $("#footer").offset().top;
    
    if (footer_top)
        $('.sidebar').removeClass('stick');    
    else {
        $('.sidebar').addClass('stick');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
</script>

<?php include'footer.php'; ?>