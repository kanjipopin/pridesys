<?php
  // session_start();
  // require_once('../config/db.php');
  include 'header.php';

  @$Email = $_SESSION['operations_manager'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $package = $conn->prepare("SELECT * from enterprise_order_details where comp_email = '{$Email}' order by order_id desc");
  $package->execute();
  $packageRow = $package->fetch();

  $planDetails = $conn->prepare("SELECT * from enterprise_plan where package_id = '{$packageRow['package_id']}'");
  $planDetails->execute();
  $planDetailsCount = $planDetails->rowCount();
  $planDetailsRow = $planDetails->fetch();
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="page-rightWidth">
  <div class="col-sm-12">
    <div class="invoice-page new-profile-page">
      <div class="heading">
        <h4>Company Profile</h4>
        <div class="filters">
          <div class="form-inline">
            <a href="profile_edit.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;
            border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 20px;width:140px;color: #897e74;">Edit</a>

            <a href="drivers.php" type="button" class="btn btn-default" style="background: #FFF;border-radius: 0;
            border-color: #dbdbdb;text-shadow: none;box-shadow: none;padding: 6px 20px;width:140px;color: #897e74;">< Back</a>
          </div>
        </div>
      </div>

      <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($msg)){ echo $msg; } ?></p>

      <div class="addDriver-form">
        <div id="alertBox" class="alert hidden alert-message"></div>
        <form class="form-horizontal" enctype="multipart/form-data" action="functions/flag_driver.php" method="POST" id="form" name="form">
          <!-- <p class="nsnMsg" style="color: red;font-size: 14px; font-weight: 500;"></p> -->
          <div class="form-group">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th colspan="2" style="text-align: center;font-size: 18px;">Company Details</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Company Name</td>
                  <td><?php echo $comprow['comp_name']; ?></td>
                </tr>
                <tr>
                  <td>Tax Indetification Number</td>
                  <td><?php echo $comprow['comp_pin']; ?></td>
                </tr>
                <tr>
                  <td>Email Address</td>
                  <td><?php echo $comprow['Email']; ?></td>
                </tr>
                <tr>
                  <td>Contact</td>
                  <td><?php echo $comprow['contact_code']."-".$comprow['contact_num']; ?></td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td><?php echo $comprow['address1'].",<br>".$comprow['address2']; ?></td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td><?php echo $comprow['location']; ?></td>
                </tr>
                <tr>
                  <td>P.O Box</td>
                  <td><?php echo $comprow['po_box']; ?></td>
                </tr>
              </tbody>
            </table>

            <table class="table table-striped">
              <thead>
                <tr>
                  <th colspan="2" style="text-align: center;font-size: 18px;">Package Information</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Current Plan</td>
                  <td><?php echo str_replace("active driver","",$planDetailsRow['plan_name']); ?></td>
                </tr>
                <tr>
                  <td>Billing Days</td>
                  <td><?php echo $planDetailsRow['plan_days']." days"; ?></td>
                </tr>
                <tr>
                  <td>Total Drivers</td>
                  <td><?php echo "(".$comprow['working_driver'].") of ".$planDetailsRow['driver_limit']." Used"; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="row visible-xs" style="margin: 0;">
    <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="dashboard.php"><img src="images/dashboard-icon1.svg" class="img-responsive">Dashboard</a>
          </li>
          <li>
            <a href="drivers.php"><img src="images/drivers-icon1.svg">My Drivers</a>
          </li>
          <li>
            <a href="flag_driver.php"><img src="images/flag-driver.svg">Flag Driver</a>
          </li>
        </ul>
      </div>
    <!-- /.sidebar-collapse -->
    </div>
  </div>
</div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  function checkNSN(){
    var NSN = $("#NSN").val();
    var compId = $("#compId").val();

    $.ajax({
      type : "post",
      url : "functions/ajax.php",
      data : { "type":"checkFlagDriverNSN","NSN":NSN,"compId":compId },

      success : function(msg){
        if(msg == "not working"){
          $(".nsnMsg").text("These driver is currently not working with your company.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "no driver"){
          $(".nsnMsg").text("Please check driver National ID.");
          $("#submitFlag").attr("disabled",true);
        }
        else if(msg == "success"){
          $(".nsnMsg").text('');
          $("#submitFlag").attr("disabled",false);
        }
        
      }
    });
  }
</script>

<?php include'footer.php'; ?>