<?php

@$Email = $_SESSION['operations_manager'];


$ref = isset($_GET['ref']) ? $_GET['ref'] : "";
$error_msg = isset($_GET['error_msg']) ? $_GET['error_msg'] : "";
if($ref == "error"){
    $msg = "Server error please try again afetr sometime.";
}

$getCompID = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
$getCompID->execute();
$getCompIDRow = $getCompID->fetch();

$sale_rep_id = $getCompIDRow['Id'];

$sales_rep_name = $getCompIDRow['Name'];

$businessDays = "Everyday";
$openTime = "08:00:00";
$closeTime = "22:00:00";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Operations manager</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="../images/favicon.png" type="image/x-icon"> 
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">

      <!-- Flaticon CSS -->
      <link rel="stylesheet" href="../fonts/flaticon.css">
    <!-- Css -->
    <link rel="stylesheet" href="../css/portal.css">
    <link rel="stylesheet" href="../css/chat.css">
    <link rel="stylesheet" href="../css/portal-responsive.css">
    <link rel="stylesheet" href="../css/showup.css">
    <link rel="stylesheet" href="../css/datepicker.css">
    <!-- Tabs -->
    <link rel="stylesheet" href="css/tabs-style.css">
    <link href="../css/checkbox_select.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link href='../Front End/packages/core/main.css' rel='stylesheet' />
    <link href='../Front End/packages/daygrid/main.css' rel='stylesheet' />
    <link href='../Front End/packages/bootstrap/main.css' rel='stylesheet' />
    <script src='../Front End/packages/core/main.js'></script>
    <script src='../Front End/packages/interaction/main.js'></script>
    <script src='../Front End/packages/daygrid/main.js'></script>
    <script src='../Front End/packages/timegrid/main.js'></script>
    <script src='../Front End/packages/bootstrap/main.js'></script>
    <script src='../Front End/vendor/rrule.js'></script>
    <script src='../Front End/packages/moment/main.js'></script>
    <script src='../Front End/packages/moment-timezone/main.js'></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121038053-2"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121038053-2');
    </script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'momentTimezone', 'bootstrap', 'interaction', 'dayGrid' ],
      defaultView: "dayGridMonth",
      timeZone: 'local',
      themeSystem: 'bootstrap',
      weekLabel: "Week",
      weekNumbers: true,
      weekNumbersWithinDays: false,
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      allDaySlot: true,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth, dayGridWeek'
      },

      defaultDate: "<?php $t=time(); echo(date("Y-m-d",$t)); ?>",
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      unselectAuto: true,  
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      
      selectMinDistance: 0.1,
      
       dateClick: function(info) {
        //calendar.changeView('timeGridDay', info.date);
        //console.log(info);

      }, 

      /* selectOverlap: function(event) {
        return event.rendering === 'background';
      }, */

      editable: false,
      eventLimit: true, // allow "more" link when too many events

      eventLimiClick: "popover", function(info) {
        console.log("true");
      },


      eventSources: [

        // your event source
        {
          events: [ // put the array in the `events` property


            <?php
      
                $stmt = $conn->prepare("SELECT * from booking_table WHERE Availability='0' ");
                $stmt->execute();
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
              
                foreach($results as $row) {

                  $rent_from_date = $row['RentFrom'];
                  $expected_return_date = $row['ExpectedReturnDate'];

                  $end_date = new DateTime($expected_return_date);
                  $end_date->modify("+1 day");
                  $final_end_date = $end_date->format("Y-m-d");
                  
                  $VehicleRegID = $row['VehicleReg'];

                  $stmt_vehicles = $conn->prepare("SELECT * from pridedrive_vehicles WHERE VehicleId='$VehicleRegID' ");
                  $stmt_vehicles->execute();
                  $results_vehicle = $stmt_vehicles->fetch(PDO::FETCH_ASSOC);

                  //vehicle details
                  $vehicle_reg = $results_vehicle['VehicleReg'];

            ?>
            
            {
              title: '<?php echo $vehicle_reg; ?> - Booked',
              start: '<?php echo $rent_from_date; ?>',
              end: '<?php echo $final_end_date; ?>'
            },

            <?php
              }
            ?>



            
          ],
          color: 'black',     // an option!
          textColor: 'yellow' // an option!
        }

        // any other event sources...

      ]
      
    });

    calendar.render();
  });

  

</script>

  <style>

    .btn {
      max-width: auto;
      padding: 0;
      max-height: 15px;
      margin-left: -1px;
    }
    .br {
      background-color: #ffffff;
      height: 10px;
    }
    #calendar {
      max-width: 900px;
      margin: 0 auto;
    }
    
    div.fc-view {
      width: 100%;
      max-height: 70vh;
      overflow: auto;
    }
    .fc-head-container {
      background-color: #ffa500;
    }

    .fc-prev-button, .fc-next-button {
      width : 20px;
      height: 20px;
    }

  </style>

  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.php"><img src="../images/prime_logo_sizable.png"  alt="Pride drive" title="Pride drive" style="width: 160px; height: 60px;"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <!--  <div style="margin-top: 30px;display: inline-block;float: right;">
                <img src = "images/notification1.png">
            </div> -->
            <ul class="nav navbar-nav navbar-right">
              <!-- <li><a href = "#"><img src = "images/notification1.png"><span class="notify">0</span></a></li> -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                <?php echo $sales_rep_name; ?>
                <span class="caret"></span></a>
                <ul class="dropdown-menu">

                  <li><a href="functions/logout.php">Log Out</a></li> 
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <div class="bg-color">
      <div class="container-fluid">
        <div class="sidebar stick hidden-xs" role="navigation">
          <div class="sidebar-nav navbar-collapse">


              <ul class="nav nav-sidebar-menu sidebar-toggle-view" id="side-menu">
                
                  <li class="nav-item sidebar-nav-item">

                      <a href="all_pending_bookings.php" class="<?php if(@$activeClass == "BookingManager") { echo "active-class"; } ?>"><img src="../images/dashboard-icon1.png"/>Booking manager</a>

                  </li>


              </ul>

          </div>
          <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->

    <div class="page-rightWidth">
        <div class="add-driver-page">
            

            <p style="color: red;font-size: 15px;font-weight: 500;margin-left: 15px;"><?php if(!empty($error_msg)){ echo $error_msg; } ?></p>

            <div class="addDriver-form">
                
                <div class="bg-light" id='calendar'></div>
  
                
            </div> 

            

        </div>
    </div>

    <div class="row visible-xs" style="margin: 0;">
        <div class="sidebar" role="navigation" style="position: fixed;bottom: 0px;width: 100%;z-index: 99999;">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="sales_rep.php"><img src="../images/dashboard-icon1.svg" class="img-responsive">Sales rep</a>
                    </li>

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="delivery_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;top: 200px;">
            <div class="modal-content">
                <div class="modal-body" style="font-size: 18px;font-weight: 300;text-align: center;">
                    <h3>Warning</h3>
                    <p style="color: red;font-weight: 500;">Please note that this Driver has been Flagged. Please check your email for more details before you proceed to add this Driver to your Fleet.</p>
                </div>
                <div class="modal-footer" style="padding-top: 0px;text-align:center;border-top:none;">
                    <button type="button" class="btn btn-defalut" data-dismiss="modal" style="padding: 8px 16px;font-size: 16px;">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Back To Top -->
    <script src="../js/showup.js"></script>
    <!-- <script src="js/typeahead.min.js"></script> -->
    <script src="../js/datepicker.js"></script>

    <link rel="stylesheet" href="../css/chosen.css"/>
    <script src="../js/chosen.jquery.min.js"></script>
    <script src="../js/chosenImage.jquery.js"></script>


    <script src="../js/checkbox_select.js"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#lstFruits').multiselect({
        //         includeSelectAllOption: true
        //     });
        // });
    </script>

<?php //include'footer.php'; ?>