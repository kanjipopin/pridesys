<?php
  session_start();
  require_once('../config/db.php');
  include 'header.php';

  $activeClass = "Flag";
  @$Email = $_SESSION['operations_manager'];

  $comp = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
  $comp->execute();
  $comprow = $comp->fetch();

  $FlagNSN = isset($_REQUEST['NSN']) ? $_REQUEST['NSN'] : "";
  $FlagcompId = isset($_REQUEST['compId']) ? $_REQUEST['compId'] : "";
  $FlagUsername = isset($_REQUEST['Username']) ? $_REQUEST['Username'] : "";
  $Flagdescription = isset($_REQUEST['description']) ? $_REQUEST['description'] : "";

  $_SESSION['FlagNSN'] = $FlagNSN;
  $_SESSION['FlagcompId'] = $FlagcompId;
  $_SESSION['FlagUsername'] = $FlagUsername;
  $_SESSION['Flagdescription'] = $Flagdescription;
?>

<div class="page-rightWidth">
  <div class="col-sm-12">
    <div class="wht-bg">
      <div class="heading">
        <h4>Flag Driver Confirmation</h4>
      </div>

      <div class="addDriver-form">
  	    <div id="alertBox" class="alert hidden alert-message"></div>

          <p class="nsnMsg" style="font-size: 20px; font-weight: 500;">
            Are sure you want flag this driver ?
          </p>
          <a href="functions/flag_driver.php"><button class="btn btn-default" style="border-color: #EC2226;background: #EC2226;font-size: 14px;">Continue</button></a>

          <a href="../flag_driver.php"><button class="btn btn-default" style="border-color: #EC2226;background: #EC2226;font-size: 14px;">Cancel</button></a>
      </div>
    </div>
  </div>
</div>

<!-- IN HEADER -->
</div>
</div>
</div>
</div>

<a href="#" style="right: 20px; bottom: 20px;z-index: 100;" class="btn back-to-top btn-dark btn-fixed-bottom"> <img src="images/top-arrow.png" alt="Back to Top" title="Back to Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Back To Top -->
<script src="../js/showup.js"></script>

<?php include'footer.php'; ?>