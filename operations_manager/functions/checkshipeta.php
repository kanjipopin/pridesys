<?php
	require_once('../config/db.php');

	$created_date = date("Y-m-d H:i:s");
 	$ship_eta_date = date("Y-m-d H:i:s",strtotime($_REQUEST['Shipval']));

 	function dateDiff($ship_eta_date, $created_date){
	  	$date1_ts = strtotime($ship_eta_date);
	  	$date2_ts = strtotime($created_date);
	  	$diff = $date1_ts - $date2_ts;
	  		return round($diff / 86400);
	}

	$dateDiff = dateDiff($ship_eta_date, $created_date);

	if($dateDiff > 10){
		$ship_eta_val = "Ship ETA After 10 Days";
		
	}elseif($dateDiff < 10){
		$ship_eta_val = "Ship ETA in 10 Days";
	}

	echo $ship_eta_val;
?> 