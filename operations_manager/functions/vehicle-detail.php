<?php

	session_start();
	require_once('../config/db.php');
    require_once('../config/includes/initialise.php');

	@$Email = $_SESSION['operations_manager'];
	if($Email == ""){
		$connection->redirect('../index.php');
	}

	$getCompID = $conn->prepare("SELECT * from logis_company_subadmin where Email = '{$Email}'");
	$getCompID->execute();
	$getCompIDRow = $getCompID->fetch();

    $vehicle_reg = base64_decode($_GET['id']);
	$comp_id = $getCompIDRow['comp_id'];


	//	FETCH A DRIVER DATA  //
	$stmt = $conn->prepare("SELECT * from pridedrive_vehicles WHERE CompanyId='$comp_id' AND VehicleReg='$vehicle_reg' ");
	$stmt->execute();
	$getVehicleData = $stmt->fetch(PDO::FETCH_ASSOC);

	$vehicle_manufacturer_master = $getVehicleData['VehicleManufacturer'];
    $vehicle_model_master = $getVehicleData['VehicleModel'];
    $vehicle_body_type_master = $getVehicleData['VehicleBodyType'];
    $vehicle_color_master = $getVehicleData['VehicleColor'];
    $vehicle_year_master = $getVehicleData['VehicleYear'];
    $vehicle_fuel_master = $getVehicleData['VehicleFuel'];
    $vehicle_transmission_master = $getVehicleData['VehicleTransmission'];
    $vehicle_engine_capacity_master = $getVehicleData['VehicleEngineCapacity'];
    $vehicle_mileage_limit_master = $getVehicleData['VehicleMileageLimit'];
    $vehicle_inventory_location_master = $getVehicleData['VehicleInventoryLocation'];
    //$vehicle__master = $getVehicleData['VehicleManufacturer'];

    //	FETCH A MASTER DATA  //

    /** manufacturer master */
    $manufacturer_master = $conn->prepare("SELECT * from manufacturer_master WHERE ID='$vehicle_manufacturer_master' ");
    $manufacturer_master->execute();
    $getManufacturerData = $manufacturer_master->fetch(PDO::FETCH_ASSOC);

    /** body type master */
    $body_type_master = $conn->prepare("SELECT * from body_type_master WHERE ID='$vehicle_body_type_master' ");
    $body_type_master->execute();
    $getBodyTypeData = $body_type_master->fetch(PDO::FETCH_ASSOC);

    /** color master */
    $color_master = $conn->prepare("SELECT * from color_master WHERE ID='$vehicle_color_master' ");
    $color_master->execute();
    $getColorData = $color_master->fetch(PDO::FETCH_ASSOC);

    /** engine capacity master */
    $engine_capacity_master = $conn->prepare("SELECT * from engine_capacity_master WHERE ID='$vehicle_engine_capacity_master' ");
    $engine_capacity_master->execute();
    $getEngineCapacityData = $engine_capacity_master->fetch(PDO::FETCH_ASSOC);

    /** fuel master */
    $fuel_master = $conn->prepare("SELECT * from fuel_master WHERE ID='$vehicle_fuel_master' ");
    $fuel_master->execute();
    $getFuelData = $fuel_master->fetch(PDO::FETCH_ASSOC);

    /** vehicle inventory location master */
    $vehicle_inventory_location_master = $conn->prepare("SELECT * from vehicle_inventory_location_master WHERE ID='$vehicle_inventory_location_master' ");
    $vehicle_inventory_location_master->execute();
    $getInventoryLocationData = $vehicle_inventory_location_master->fetch(PDO::FETCH_ASSOC);

    /** mileage master */
    $mileage_master = $conn->prepare("SELECT * from mileage_master WHERE ID='$vehicle_mileage_limit_master' ");
    $mileage_master->execute();
    $getMileageData = $mileage_master->fetch(PDO::FETCH_ASSOC);

    /** model master */
    $model_master = $conn->prepare("SELECT * from model_master WHERE ID='$vehicle_model_master' ");
    $model_master->execute();
    $getModelData = $model_master->fetch(PDO::FETCH_ASSOC);

    /** transmission master */
    $transmission_master = $conn->prepare("SELECT * from transmission_master WHERE ID='$vehicle_transmission_master' ");
    $transmission_master->execute();
    $getTransmissionData = $transmission_master->fetch(PDO::FETCH_ASSOC);

    /** vehicle year master */
    $vehicle_year_master = $conn->prepare("SELECT * from vehicle_year_master WHERE ID='$vehicle_year_master' ");
    $vehicle_year_master->execute();
    $getYearData = $vehicle_year_master->fetch(PDO::FETCH_ASSOC);


?>