<?php

	session_start();

	unset($_SESSION['operations_manager']);

	session_destroy();
	header("Location:../../index.php");

?>