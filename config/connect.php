<?php
	
	class Connect{

		private $db="";

		function __construct($conn){
			$this->db= $conn;
		}

		

		public function login($username, $password, $tableName, $passfield, $idfield, $condition)
		{
			try
			{
				$stmt = $this->db->prepare("SELECT $passfield, $idfield,comp_id FROM $tableName WHERE $condition=:username LIMIT 1");
				$stmt->execute(array(':username'=>$username));
				$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
				if($stmt->rowCount() > 0){
					if($password == $userRow[$passfield]){	
						session_start();
						 $_SESSION['cv_user_session'] = $userRow[$idfield];
						 $_SESSION['comp_id'] = $userRow['comp_id'];
						return true;
					}else
					{
						return false;
					}
				}else{
						return false;
				}
			}
			catch(PDOException $e)
			 {
				echo $e->getMessage();
			}
		}

		public function redirect($url)
		{
			header("location:".$url);
		}

		public function is_logged_in()
		{
			if(isset($_SESSION['cv_user_session']))
			{
				return true;
			}
		}

		public function logout()
		{
			session_destroy();
			unset($_SESSION['cv_user_session']);
			return true;
		}

		public function tableData($field, $table)
		{
			try
			{
				$stmt = $this->db->prepare("select $field from $table");
				$stmt->execute();
				//$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		        
				return $stmt;

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}

		public function tableDataCondition($field, $table, $condition,$conditionValue)
		{
			try
			{
				$stmt = $this->db->prepare("select $field from $table where $condition=:conditionValue");
				$stmt->execute(array(':conditionValue'=>$conditionValue));
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		        
				return $results;

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}


		public function OneRow($field, $table)
		{
			try
			{
				$stmt = $this->db->prepare("select $field from $table");
				$stmt->execute();
				$results = $stmt->fetch(PDO::FETCH_ASSOC);
		        
				return $results;

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}
		
		
		public function sanitize_input($data) {
		   $data = trim($data);
		   $data = stripslashes($data);
		   $data = htmlspecialchars($data);
		   return $data;
		}



		public function OneRowCondition($field, $table, $condition)
		{
			try
			{
				$stmt = $this->db->prepare("select $field from $table where $condition");
				$stmt->execute();
				$results =  $stmt->fetch(PDO::FETCH_ASSOC);
		        
				return $results;

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}


		public function rowCount($field, $table, $condition)
		{
			try
			{
				if($condition!=""){
					$stmt = $this->db->prepare("select $field from $table where $condition");
				}else{
					$stmt = $this->db->prepare("select $field from $table");					
				}

				$stmt->execute();
				$results =  $stmt->rowCount();
		        
				return $results;

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}

		public function selectRow($table,$condition,$conditionValue)
		{
			try
			{
				
				$stmt = $this->db->prepare("SELECT * FROM $table WHERE $condition=:conditionValue");
				$stmt->execute(array(':conditionValue'=>$conditionValue));
				
				$results=$stmt->fetch(PDO::FETCH_ASSOC);

				//$stmt->execute();
		        
				return $results;

			}
			catch(PDOException $e)
			{
				//echo 'Query failed'.$e->getMessage();
				return 'err-'.$e->getMessage();
			}
		}


		public function DeleteRow($table, $field, $condition)
		{
			try
			{
				$stmt = $this->db->prepare("DELETE FROM $table WHERE $field='$condition'");
				$stmt->execute();
				return $succ='success';

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}


		public function InsertQuery($table, $cols)
		{
			try
			{	
				$colCount = count($cols);
				$columns = implode(", ",array_keys($cols));
				$colBind = implode(", :",array_keys($cols));
				//$values = implode("', '", array_values($cols));

				$stmt = $this->db->prepare("INSERT into $table ($columns) VALUES (:$colBind)");
				for($i=0; $i<$colCount;$i++){
					$key = array_keys($cols);
					$keys = $key[$i];
					$value = array_values($cols);
					$values = $value[$i];
					$stmt->bindValue(":".$keys, $values);
				}
				$stmt->execute();
				return $succ='success';
			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}

		

		public function UpdateQuery($table, $cols, $condition)
		{
			try
			{	
				$colCount = count($cols);
				$columns = array_keys($cols);
				$values="";

				$sql = "UPDATE $table SET ";
					for($j=0; $j<$colCount;$j++){
							$values .= $columns[$j]."=:". $columns[$j].", ";
					}
					if (substr($values, -2) == ", "){
					  $values2 = substr($values, 0, -2);
					}
				$sql .= $values2;
				$sql .= " where $condition";

				$stmt = $this->db->prepare($sql);
				for($i=0; $i<$colCount;$i++){

					$key = array_keys($cols);
					$keys = $key[$i];
					$value = array_values($cols);
					$values = $value[$i];
					$stmt->bindValue(":".$keys, $values);
				}

				$stmt->execute();
				return $succ='success';

			}
			catch(PDOException $e)
			{
				echo 'Query failed'.$e->getMessage();
			}
		}


		public function sendSMS($mobile, $message){

			$request ="";

			$param['method']= "sendMessage";
			$param['send_to'] = $mobile;
			$param['msg'] = $message;
					
			$param['userid'] = "2000119874";
			$param['password'] = "Modi@786";
					
			$param['v'] = "1.1";
			$param['msg_type'] = "TEXT"; 
			$param['auth_scheme'] = "PLAIN";
					
			foreach($param as $key=>$val) {
				$request.= $key."=".urlencode($val);
				$request.= "&";						
			}
			
			$request = substr($request, 0, strlen($request)-1);
					
			//$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
			
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$curl_scraped_page = curl_exec($ch);
			curl_close($ch);

			$sms_status = $curl_scraped_page;			

		}

		
	}
?>