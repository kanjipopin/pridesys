<?php

require_once("initialise.php");

	class generateNewPDF extends FPDF
	{
	    public $receipt_id;
	    public $append_path;
	    public $file = "Tickets/QR/TKT321138PSDKS.png";

		
		public function set_res_header_image($receipt_id)
		{
		    return $this->append_path.$receipt_id;
		}
		
		public function set_qr_code($receipt_id)
		{
		    $this->receipt_id = $receipt_id;
		}
		
		public function set_append_path($path)
		{
		    $this->append_path = $path;
		}

		public function Header()
		{
//
			$this->AddFont('Calligrapher','','calligra.php');
			$this->Image($this->set_res_header_image($this->receipt_id).'.png',10,10,50,50);
			$this->SetFont('Arial','',20);
			$this->SetFillColor(255);
			$this->SetTextColor(0);
			$this->SetXY(140,13);
			//$this->SetY(20);
			$this->Cell(60,10,$this->receipt_id,0,0,"L",true);
			$this->Ln(20);

		}

		
		public function create_booking_ticket($booking_id,$vehicle_reg,$vehicle_model,$vehicle_color,$driver_type,$driver_name,$driver_phone,$client_type,$client_name,$client_phone,$client_email,$rent_from_date,$expected_return_date,$booking_status,$booking_additional_notes)
		{
		    
			$this->SetMargins(10,10,10);
			$this->AddPage();

			$this->AddFont('Calligrapher','','calligra.php');

			// addresses
			$this->Ln(20);
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Status ",0,"","L",true);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$booking_status,0,"","L",true);

			$this->Ln(20);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,"RENTED TO",0,0,"L",true);
			
			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Name of client",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$client_name." (".$client_type.")",0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Phone number ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$client_phone,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Email address ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$client_email,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Rent date ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$rent_from_date,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Expected return date ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$expected_return_date,0,"","L",true);

		
			$this->Ln(20);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,"DRIVER DETAILS",0,0,"L",true);

			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Name",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$driver_name." (".$driver_type.")",0,"","L",true);

			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Phone number",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$driver_phone,0,"","L",true);


			$this->Ln(20);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(64,8,"Reg",0,0,"C",true);
			$this->Cell(63,8,"Model",0,0,"C",true);
			$this->Cell(63,8,"Color",0,0,"C",true);


				# code...
				$this->Ln();
				$this->setFont("Arial","",12);
				$this->SetTextColor(0);
				$this->SetFillColor(255);
				$this->Cell(64,15,$vehicle_reg,0,0,"C",true);
				$this->Cell(63,15,$vehicle_model,0,0,"C",true);
				$this->Cell(63,15,$vehicle_color,0,0,"C",true);
			

			$this->Ln(25);
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Additional note ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$booking_additional_notes,0,"","L",true);

			$this->SetAutoPageBreak(true,2);
		    
		}

		public function Footer()
		{

			$this->SetFont('Arial','',20);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->SetXY(10,-15);
			$this->Cell(0,15,"www.pridedrive.com",'T',0,'C',true);

		}


	}

	$pdf = new generateNewPDF('P','mm','A4');

?>