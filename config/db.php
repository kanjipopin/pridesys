<?php
	//set timezone
	date_default_timezone_set("Africa/Nairobi");

	error_reporting(E_ALL); // Error engine - always ON!

ini_set('display_errors', false); // Error display - OFF in production env or real server

ini_set('log_errors', TRUE); // Error logging

ini_set('error_log', 'error.log'); // Logging file

ini_set('log_errors_max_len', 1024); // Logging file size

	//database connect info

    //for local machine
	define('DBHOST','localhost');
	define('DBUSER','kanji_pride');
	define('DBPASS','kanji');
	define('DBNAME','pridesys');

	//for online sandbox
    /* define('DBHOST','localhost');
    define('DBUSER','cliffow0_prx');
    define('DBPASS','prxpa55word');
    define('DBNAME','cliffow0_prsb');  */

    //for online production
    /* define('DBHOST','localhost');
    define('DBUSER','cliffow0_prx');
    define('DBPASS','prxpa55word');
    define('DBNAME','cliffow0_prx'); */

	// @define('DIR','http://enterprise.dereva.com/');
	//$prefix = "";

	//Connect to the database
	try {
		$conn = new PDO("mysql:host=".DBHOST.";dbname=".DBNAME, DBUSER, DBPASS);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//echo "Connected successfully";
    }
	catch(PDOException $e) {
		echo "Connection failed: " . $e->getMessage();
    }

	//Select database for a mysql connection.
	include_once 'connect.php';
	$connection = new Connect($conn);

?>